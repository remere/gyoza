


CreateObviousIndecMatrices := function(dimv, arrows)
    local  dim, N, matrices, i;
    for dim in dimv do
        if not (dim = 0 or dim = 1) then
            return fail;
        fi;
    od;
    N := Length(arrows);
    matrices := [];
    N := Length(arrows);
    for i in [1..N] do
        if (dimv[arrows[i][1]] = 1 and dimv[arrows[i][2]] = 1) then
            Add(matrices, [String(arrows[i][3]), Z(2)*[[1]]]);
        fi;
        # Add(matrices, [arrows[i][3], NullMat(dimv[arrows[i][1]], dimv[arrows[i][2]], Z(2))+1 ]);
    od;
    return matrices;
end;




CreateLinearQuiverIndecs := function(A)
    local  Q, verts, N, shift_list, M, arrows, arr, src, trgt,
           indecs_list, b, d, dimv, mats;
    # checks that A is actually algebra GF(2) A_n
    Q := QuiverOfPathAlgebra(A);
    verts := VerticesOfQuiver(Q);
    N := Length(verts);

    if not NumberOfArrows(Q) = N-1 then
        return fail;
    fi;

    shift_list := [2..N];
    Add(shift_list, 1);
    M := PermutationMat(PermList(shift_list), N);
    M[N][1] := 0;
    if not M = AdjacencyMatrixOfQuiver(Q) then
        return fail;
    fi;

    if not OriginalPathAlgebra(A) = A then
        return fail;
    fi;

    arrows := [];
    for arr in ArrowsOfQuiver(Q) do
        src := Position(verts, SourceVertex(arr));
        trgt := Position(verts, TargetVertex(arr));
        Add(arrows, [src,trgt, arr]);
    od;

    indecs_list := [];
    for b in [1..N] do
        #simple module at b.. skipped
        for d in [b+1..N] do
            dimv := ListWithIdenticalEntries(N,0);
            dimv{[b..d]} := ListWithIdenticalEntries(d-b+1,1);
            mats := CreateObviousIndecMatrices(dimv ,arrows);
            Add(indecs_list, RightModuleOverPathAlgebra(A, dimv, mats));
        od;
    od;
    Append(indecs_list, SimpleModules(A));
    return indecs_list;
end;


CreateCommutativeLadderIndecs := function(A, orientation)
    local  n, Q2, QN, QP, Q, verts, arrows, arr, src, trgt,
           indecs_list, nonobvious_indecs, obvious_dimvecs, dimv,
           mats;
    n := Length(orientation);
    if n > 2 then
        Print("Ladder too long, currently not supported!\n");
        return fail;
    fi;

    Q2 := DynkinQuiver("A", 2, ["r"]);
    QN := DynkinQuiver("A", n+1, orientation);
    QP := QuiverProduct(Q2, QN);

    # This check assums that the QuiverProduct operation does not change
    # how it constructs (ordering of) the vertices
    if not AdjacencyMatrixOfQuiver(QP) = AdjacencyMatrixOfQuiver(QuiverOfPathAlgebra(A)) then
        return fail;
    fi;

    Q := QuiverOfPathAlgebra(A);
    verts := VerticesOfQuiver(Q);
    arrows := [];
    for arr in ArrowsOfQuiver(Q) do
        src := Position(verts, SourceVertex(arr));
        trgt := Position(verts, TargetVertex(arr));
        Add(arrows, [src,trgt, arr]);
    od;

    # TODO: somehow check that relations are correct..
    # RelationsOfAlgebra(A);

    indecs_list := [];
    nonobvious_indecs := [];
    if orientation = ["r"] then
        obvious_dimvecs := [ [ 1, 0, 0, 0 ],
                             [ 1, 1, 0, 0 ],
                             [ 1, 0, 1, 0 ],
                             [ 1, 1, 1, 0 ],
                             [ 0, 0, 1, 0 ],
                             [ 0, 1, 0, 0 ],
                             [ 1, 1, 1, 1 ],
                             [ 0, 1, 1, 1 ],
                             [ 0, 1, 0, 1 ],
                             [ 0, 0, 1, 1 ],
                             [ 0, 0, 0, 1 ] ];
    elif orientation = ["l"] then
        obvious_dimvecs := [ [ 1, 1, 0, 0 ],
                             [ 1, 1, 0, 1 ],
                             [ 0, 0, 0, 1 ],
                             [ 1, 1, 1, 1 ],
                             [ 1, 0, 0, 0 ],
                             [ 1, 0, 1, 1 ],
                             [ 1, 0, 1, 0 ],
                             [ 0, 0, 1, 1 ],
                             [ 0, 0, 1, 0 ],
                             [ 0, 1, 0, 0 ],
                             [ 0, 1, 0, 1 ] ];
    elif orientation = ["r", "l"] then
        # 30 indec isoclasses,
        # 2 non-obvious
        obvious_dimvecs := [ [ 1, 0, 0, 0, 0, 0 ],
                             [ 1, 1, 1, 0, 0, 0 ],
                             [ 1, 0, 0, 1, 0, 0 ],
                             [ 1, 1, 1, 1, 0, 0 ],
                             [ 1, 1, 1, 0, 0, 1 ],
                             [ 1, 1, 1, 1, 0, 1 ],
                             [ 0, 1, 1, 0, 0, 0 ],
                             [ 1, 1, 0, 0, 0, 0 ],
                             [ 0, 1, 1, 0, 0, 1 ],
                             [ 1, 1, 0, 1, 0, 0 ],
                             [ 1, 1, 1, 1, 1, 1 ],
                             [ 0, 0, 0, 0, 0, 1 ],
                             [ 0, 0, 0, 1, 0, 0 ],
                             [ 1, 1, 0, 1, 1, 1 ],
                             [ 0, 1, 1, 1, 1, 1 ],
                             [ 0, 1, 0, 0, 0, 0 ],
                             [ 0, 1, 0, 1, 1, 1 ],
                             [ 1, 1, 0, 1, 1, 0 ],
                             [ 0, 1, 1, 0, 1, 1 ],
                             [ 0, 1, 0, 1, 1, 0 ],
                             [ 0, 1, 0, 0, 1, 1 ],
                             [ 0, 0, 0, 1, 1, 1 ],
                             [ 0, 0, 0, 0, 1, 1 ],
                             [ 0, 0, 0, 1, 1, 0 ],
                             [ 0, 1, 0, 0, 1, 0 ],
                             [ 0, 0, 0, 0, 1, 0 ],
                             [ 0, 0, 1, 0, 0, 0 ],
                             [ 0, 0, 1, 0, 0, 1 ] ];
        nonobvious_indecs := [TrD(SimpleModules(A)[5])];
        Add(nonobvious_indecs, TrD(TrD(nonobvious_indecs[1])));
        Assert(0, DimensionVector(nonobvious_indecs[1]) = [ 0, 1, 0, 1, 2, 1 ]);
        Assert(0, DimensionVector(nonobvious_indecs[2]) = [ 1, 2, 1, 1, 1, 1 ]);
    elif orientation = ["l", "r"] then
        # 30 indec isoclasses,
        # 2 non-obvious
        obvious_dimvecs := [ [ 1, 1, 0, 0, 0, 0 ],
                             [ 0, 1, 1, 0, 1, 0 ],
                             [ 1, 1, 1, 0, 0, 0 ],
                             [ 1, 1, 0, 0, 1, 0 ],
                             [ 1, 1, 1, 0, 1, 0 ],
                             [ 0, 1, 1, 0, 1, 1 ],
                             [ 1, 1, 0, 1, 1, 0 ],
                             [ 1, 1, 1, 0, 1, 1 ],
                             [ 0, 0, 0, 0, 1, 0 ],
                             [ 1, 1, 1, 1, 1, 0 ],
                             [ 1, 0, 0, 0, 0, 0 ],
                             [ 0, 0, 1, 0, 0, 0 ],
                             [ 1, 0, 0, 1, 1, 0 ],
                             [ 1, 1, 1, 1, 1, 1 ],
                             [ 0, 0, 1, 0, 1, 1 ],
                             [ 1, 0, 1, 1, 1, 1 ],
                             [ 0, 0, 0, 1, 1, 0 ],
                             [ 0, 0, 0, 0, 1, 1 ],
                             [ 0, 0, 1, 1, 1, 1 ],
                             [ 1, 0, 0, 1, 1, 1 ],
                             [ 0, 0, 0, 1, 1, 1 ],
                             [ 0, 0, 1, 0, 0, 1 ],
                             [ 1, 0, 0, 1, 0, 0 ],
                             [ 0, 0, 0, 0, 0, 1 ],
                             [ 0, 0, 0, 1, 0, 0 ],
                             [ 0, 1, 0, 0, 0, 0 ],
                             [ 0, 1, 1, 0, 0, 0 ],
                             [ 0, 1, 0, 0, 1, 0 ] ];
        nonobvious_indecs := [DTr(SimpleModules(A)[2])];
        Add(nonobvious_indecs, DTr(DTr(nonobvious_indecs[1])));
        Assert(0, DimensionVector(nonobvious_indecs[1]) = [ 1, 2, 1, 0, 1, 0 ]);
        Assert(0, DimensionVector(nonobvious_indecs[2]) = [ 1, 1, 1, 1, 2, 1 ]);
    elif orientation = ["r", "r"] then
        # 29 indec isoclasses,
        # 2 non-obvious
        obvious_dimvecs := [ [ 1, 0, 0, 0, 0, 0 ],
                             [ 1, 1, 0, 0, 0, 0 ],
                             [ 1, 0, 0, 1, 0, 0 ],
                             [ 1, 1, 0, 1, 0, 0 ],
                             [ 1, 1, 1, 0, 0, 0 ],
                             [ 1, 1, 1, 1, 0, 0 ],
                             [ 0, 1, 0, 0, 0, 0 ],
                             [ 1, 1, 0, 1, 1, 0 ],
                             [ 0, 0, 0, 1, 0, 0 ],
                             [ 0, 1, 0, 1, 1, 0 ],
                             [ 1, 1, 1, 1, 1, 0 ],
                             [ 0, 1, 1, 0, 0, 0 ],
                             [ 0, 1, 1, 1, 1, 0 ],
                             [ 0, 1, 0, 0, 1, 0 ],
                             [ 1, 1, 1, 1, 1, 1 ],
                             [ 0, 1, 1, 0, 1, 0 ],
                             [ 0, 1, 1, 1, 1, 1 ],
                             [ 0, 0, 0, 1, 1, 0 ],
                             [ 0, 0, 1, 0, 0, 0 ],
                             [ 0, 0, 1, 1, 1, 1 ],
                             [ 0, 0, 0, 0, 1, 0 ],
                             [ 0, 1, 1, 0, 1, 1 ],
                             [ 0, 0, 1, 0, 1, 1 ],
                             [ 0, 0, 0, 1, 1, 1 ],
                             [ 0, 0, 0, 0, 1, 1 ],
                             [ 0, 0, 1, 0, 0, 1 ],
                             [ 0, 0, 0, 0, 0, 1 ] ];
        nonobvious_indecs := [];
        Add(nonobvious_indecs, DTr(DTr(SimpleModules(A)[1])));
        Add(nonobvious_indecs, TrD(TrD(SimpleModules(A)[6])));
        Assert(0, DimensionVector(nonobvious_indecs[1]) = [ 1, 2, 1, 1, 1, 0 ]);
        Assert(0, DimensionVector(nonobvious_indecs[2]) = [ 0, 1, 1, 1, 2, 1 ]);
    elif orientation = ["l", "l"] then
        # 29 indec isoclasses,
        # 2 non-obvious
        obvious_dimvecs := [ [ 1, 1, 1, 0, 0, 0 ],
                             [ 1, 1, 1, 0, 0, 1 ],
                             [ 0, 0, 0, 0, 0, 1 ],
                             [ 0, 1, 0, 0, 1, 1 ],
                             [ 1, 1, 1, 0, 1, 1 ],
                             [ 1, 1, 0, 0, 0, 0 ],
                             [ 0, 1, 0, 0, 1, 0 ],
                             [ 1, 1, 0, 0, 1, 1 ],
                             [ 1, 1, 1, 1, 1, 1 ],
                             [ 1, 1, 0, 0, 1, 0 ],
                             [ 1, 1, 0, 1, 1, 1 ],
                             [ 0, 0, 0, 0, 1, 1 ],
                             [ 1, 0, 0, 0, 0, 0 ],
                             [ 1, 0, 0, 1, 1, 1 ],
                             [ 0, 0, 0, 0, 1, 0 ],
                             [ 1, 1, 0, 1, 1, 0 ],
                             [ 0, 0, 0, 1, 1, 1 ],
                             [ 1, 0, 0, 1, 1, 0 ],
                             [ 0, 0, 0, 1, 1, 0 ],
                             [ 1, 0, 0, 1, 0, 0 ],
                             [ 0, 0, 0, 1, 0, 0 ],
                             [ 0, 1, 1, 0, 0, 0 ],
                             [ 0, 1, 1, 0, 0, 1 ],
                             [ 0, 1, 1, 0, 1, 1 ],
                             [ 0, 1, 0, 0, 0, 0 ],
                             [ 0, 0, 1, 0, 0, 0 ],
                             [ 0, 0, 1, 0, 0, 1 ] ];
        nonobvious_indecs := [];
        Add(nonobvious_indecs, DTr(DTr(SimpleModules(A)[3])));
        Add(nonobvious_indecs, TrD(TrD(SimpleModules(A)[4])));
        Assert(0, DimensionVector(nonobvious_indecs[1]) = [ 1, 2, 1, 0, 1, 1 ]);
        Assert(0, DimensionVector(nonobvious_indecs[2]) = [ 1, 1, 0, 1, 2, 1 ]);
    fi;

    for dimv in obvious_dimvecs do
        mats := CreateObviousIndecMatrices(dimv, arrows);
        Add(indecs_list, RightModuleOverPathAlgebra(A, dimv, mats));
    od;
    Append(indecs_list, nonobvious_indecs);


    return indecs_list;
end;
