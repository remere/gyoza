#include "mapchain.cpp"
#include "gyoza/z2.h"

namespace gyoza{
namespace Algebra{
template class MapChain<Core::Z2>;
template MapChain<Core::Z2> operator+(MapChain<Core::Z2>, const MapChain<Core::Z2>&);
template MapChain<Core::Z2> operator-(MapChain<Core::Z2>, const MapChain<Core::Z2>&);
template MapChain<Core::Z2> operator*(MapChain<Core::Z2>, const MapChain<Core::Z2>::value_type&);
}
}
