#include "gyoza/randomz2.h"

namespace Core{
std::random_device Z2RNG::m_rd;
std::mt19937 Z2RNG::m_rng(Z2RNG::m_rd());
std::uniform_int_distribution<int> Z2RNG::uid(0,1);
}
