#pragma once

#include "gyoza/common_definitions.h"

namespace gyoza{
namespace algorithms{

Z2Matrix random_matrix_rank_uniform(Index rows, Index cols); 
Z2Matrix random_matrix_with_rank(Index rows, Index cols, Index rank); 
Z2Matrix random_square_matrix_full_rank(Index size);
                                       

}
}
