#ifndef COMMON_DEFINITIONS_H
#define COMMON_DEFINITIONS_H


#include "gyoza/z2.h"
#include "gyoza/z2traits.h"
#include "gyoza/z2matrix.h"
#include "gyoza/randomz2.h"

namespace gyoza {
// moved to z2matrix.h

}

#endif
