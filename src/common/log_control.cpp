#include "gyoza/log_control.h"

namespace gyoza {
LogLevel GLOBAL_LOG_LEVEL = LOG_ERROR;

bool do_log(const LogLevel & level) {
  return (level <= GLOBAL_LOG_LEVEL); 
}
}
