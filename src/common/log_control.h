#ifndef LOG_CONTROL_H
#define LOG_CONTROL_H

#include <iostream>
#include <string>

namespace gyoza {

enum LogLevel {
  LOG_NOTHING,
  LOG_CRITICAL,
  LOG_ERROR, // default
  LOG_WARNING,
  LOG_INFO,
  LOG_DEBUG 
};

extern LogLevel GLOBAL_LOG_LEVEL;

bool do_log(const LogLevel & level);

template<LogLevel level>
void log(const char* msg, std::ostream& os = std::cout) {
  if (do_log(level)) {
    os << msg;
  }
}

template<LogLevel level>
void log(const std::string &  msg, std::ostream& os = std::cout) {
  if (do_log(level)) {
    os << msg;
  }
}



}

#endif
