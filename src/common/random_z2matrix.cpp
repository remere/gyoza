#include "gyoza/random_z2matrix.h"
#include <random>
#include <algorithm>
#include "gyoza/snf_algorithms.hpp"

namespace gyoza{
namespace algorithms{

Z2Matrix random_matrix_rank_uniform(Index rows, Index cols) {
  static std::random_device rd;
  static std::mt19937 gen(rd()); 
  std::uniform_int_distribution<> dis(1, std::min(rows,cols));

  int rank = dis(gen); 
  return random_matrix_with_rank(rows, cols, rank); 
}


Z2Matrix random_matrix_with_rank(Index rows,
                                 Index cols,
                                 Index rank) {
  Z2Matrix ans = Z2Matrix::Zero(rows, cols);
  if (rank <= 0) {
    return ans;
  }
  if (rank > std::min(rows,cols)) {
    rank = std::min(rows,cols);
  }

  for (Index i = 0; i < rank; ++i) {
    ans(i,i) = Core::Z2(1);
  }
  ans = ans * random_square_matrix_full_rank(cols);
  ans = random_square_matrix_full_rank(rows) * ans;

  return ans;
}


Z2Matrix random_square_matrix_full_rank(Index size){
  gyoza::algorithms::FullPivotSNF<gyoza::Z2Matrix> snf;
  bool done = false;
  Z2Matrix ans;
  while (not done) {      
    ans = Z2Matrix::Random(size, size);
    snf.compute(ans);
    if (snf.rank() == size) {
      done = true;
    }
  }
  return ans; 
}

}
}
