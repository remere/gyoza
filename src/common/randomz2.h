#pragma once
#include "gyoza/z2.h"
#include "gyoza/z2traits.h"
#include <random>

namespace Core {

struct Z2RNG {
  static std::random_device m_rd;
  static std::mt19937 m_rng;
  static std::uniform_int_distribution<int> uid;
};
}

namespace Eigen {
namespace internal {
template <>
struct random_impl<Core::Z2> {
  static inline Core::Z2 run(const Core::Z2& x, const Core::Z2& y) {
    return (x != y) ? Core::Z2(Core::Z2RNG::uid(Core::Z2RNG::m_rng)): x ;
  }

  static inline Core::Z2 run() {
    return run(Core::Z2(false), Core::Z2(true));
  }
};

}
}
