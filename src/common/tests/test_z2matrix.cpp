#include "catch.hpp"

#include "gyoza/z2matrix.h"
#include "gyoza/random_z2matrix.h"
#include <boost/optional/optional_io.hpp>
TEST_CASE("Z2 matrix write-read", "[z2matrix]"){
  gyoza::Z2Matrix expected(2,3);
  expected << 0,1,1,0,0,1;

  std::vector<Core::Z2> as_vec = {0,1,1,0,0,1};
  std::string as_string = "2 3\n011\n001";

  SECTION("from vector"){
    gyoza::Z2Matrix red = gyoza::z2matrix_from_vector(as_vec, 2, 3);
    REQUIRE(red == expected);

    red(0,0) = 1;
    REQUIRE_FALSE(red == expected);
    gyoza::Z2Matrix red_2 = gyoza::z2matrix_from_vector(as_vec, 2, 3);
    REQUIRE_FALSE(red == red_2);
  }

  SECTION("from string"){
    std::stringstream ss(as_string);
    auto mat_read = gyoza::z2matrix_ascii_read(ss);
    REQUIRE(mat_read != boost::none);
    REQUIRE(expected == *mat_read);
  }

  SECTION("insufficient data: rows"){
    // declare three rows instead?! data now insufficient:
    as_string[0] = '3';
    std::stringstream ss(as_string);
    auto mat_read = gyoza::z2matrix_ascii_read(ss);
    REQUIRE(mat_read == boost::none);
  }

  SECTION("mis-sized data: cols"){
    as_string.append("1");
    std::stringstream ss(as_string);
    auto mat_read = gyoza::z2matrix_ascii_read(ss);
    REQUIRE(mat_read == boost::none);
  }

}

TEST_CASE("Z2 matrix write-read random", "[z2matrix]"){
  SECTION("write-read"){
    gyoza::Z2Matrix mat = gyoza::Z2Matrix::Random(8,10);

    std::stringstream ss;
    gyoza::z2matrix_ascii_write(ss, mat);
    auto mat_read = gyoza::z2matrix_ascii_read(ss);

    REQUIRE(mat_read != boost::none);
    REQUIRE(mat == *mat_read);
  }
}
