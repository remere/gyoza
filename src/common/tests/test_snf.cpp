#include "catch.hpp"

#include "gyoza/common_definitions.h"
#include "gyoza/snf_algorithms.hpp"
#include <boost/optional/optional_io.hpp>

using Z2Matrix = gyoza::Z2Matrix;

TEST_CASE( "empty snf problems", "[snf]" ) {
  gyoza::algorithms::FullPivotSNF<Z2Matrix> snf;
  CHECK_THROWS( snf.get_snf() );
  CHECK_THROWS( snf.get_P() );
  CHECK_THROWS( snf.get_Q() );
  CHECK_THROWS( snf.get_P_inv() );
  CHECK_THROWS( snf.get_Q_inv() );  
  CHECK_THROWS( snf.rank() );  
  CHECK_THROWS( snf.get_inverse() );
}


TEST_CASE( "random matrices", "[snf]" ) {
  // first check that isZero() works...
  REQUIRE_FALSE( Z2Matrix::Identity(3,3).isZero() );  
  REQUIRE( Z2Matrix::Zero(3,3).isZero() );

  Z2Matrix mat;
  for (int i = 10; i < 20; ++i){
    for (int j = 10; j < 20; ++j){
      mat = Z2Matrix::Random(i,j);
      gyoza::algorithms::FullPivotSNF<Z2Matrix> snf(mat);
      CHECK( (snf.get_snf() - (snf.get_P() * mat * snf.get_Q())).isZero() );
    }
  }
  mat = Z2Matrix::Random(100,105);
  gyoza::algorithms::FullPivotSNF<Z2Matrix> snf(mat);
  CHECK( (snf.get_snf() - (snf.get_P() * mat * snf.get_Q())).isZero() );
}

TEST_CASE( "random matrices -- snf with inverses", "[snf]" ) {
  REQUIRE( Z2Matrix::Identity(3,3).isIdentity() );
  Z2Matrix mat;
  for (int i = 10; i < 20; ++i){
    for (int j = 10; j < 20; ++j){
      mat = Z2Matrix::Random(i,j);
      gyoza::algorithms::FullPivotSNF<Z2Matrix> snf(mat, true);
      CHECK( (snf.get_snf() - (snf.get_P() * mat * snf.get_Q())).isZero() );
      CHECK( ((snf.get_P_inv() * snf.get_snf() * snf.get_Q_inv()) -  mat).isZero() );      
    }
  }
  mat = Z2Matrix::Random(100,105);
  gyoza::algorithms::FullPivotSNF<Z2Matrix> snf(mat,true);
  CHECK( (snf.get_snf() - (snf.get_P() * mat * snf.get_Q())).isZero() );
  CHECK( ((snf.get_P_inv() * snf.get_snf() * snf.get_Q_inv()) -  mat).isZero() );

  CHECK( (snf.get_P() * snf.get_P_inv()) == Z2Matrix::Identity(100,100) );
  
  CHECK( (snf.get_P() * snf.get_P_inv()).eval().isIdentity() );
  CHECK( (snf.get_Q() * snf.get_Q_inv()).eval().isIdentity() );
}


TEST_CASE( "inverting matrices via snf", "[snf]" ) {
  Z2Matrix mat;

  SECTION("trivial checks"){
    mat = Z2Matrix::Identity(3,3);
    gyoza::algorithms::FullPivotSNF<Z2Matrix> snf_id(mat, true);
    CHECK(snf_id.get_inverse().get() == mat);

    mat = Z2Matrix::Zero(3,3);
    gyoza::algorithms::FullPivotSNF<Z2Matrix> snf_zero(mat, true);
    CHECK(snf_zero.get_inverse() == boost::none);
  }

  SECTION("on invertible matrix") {
    mat = Z2Matrix::Random(5,7);
    gyoza::algorithms::FullPivotSNF<Z2Matrix> snf(mat, true);
    Z2Matrix P = snf.get_P(); // is invertible;
    
    gyoza::algorithms::FullPivotSNF<Z2Matrix> snf_p(P);  
    CHECK(snf_p.get_inverse().get() == snf.get_P_inv());
  }  

}
