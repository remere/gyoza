#ifndef QUIVERCOMPLEX_H
#define QUIVERCOMPLEX_H

#include <vector>
#include <set>
#include <iostream>

#include "gyoza/quiver.h"
#include "gyoza/birthhandler.h"
#include "gyoza/quivercell.h"
#include "gyoza/quiverrepn.h"

namespace gyoza {
namespace quivers {

template<typename Chain_T>
class QuiverComplex;

// template<typename Chain_T>
// std::istream& operator>>(std::istream& input, QuiverComplex<Chain_T>& qc);

template<typename Chain_T>
class QuiverComplex {
 public:
  typedef Cell<Chain_T> Cell_T;
  typedef typename std::vector<Cell_T>::size_type CellIndex;

  int get_num_vertices()const{return quiv.get_num_vertices();}
  BirthHandler get_births()const{return births;}
  Quiver get_quiver()const{return quiv;}
  CellIndex size()const{return cells.size();}

  // check-ers
  bool check_integrity() const;


  // friend std::istream& operator>> <>(std::istream& input, QuiverComplex<Chain_T>& qc);
  void read_json(std::istream& input);
  void read_txt(std::istream& input);
  void print_cells(std::ostream& os)const;
  
  QuiverRepn naive_compute_persistence(int target_dimension);

  // template<typename RepnType>
  // RepnType naive_compute_persistence_as_repn(int target_dimension);

  void clear();
  void set_quiver(const Quiver& quiver);
  void set_births(const BirthHandler& bh);
  CellIndex create_cell(int dim, const Chain_T& bdd, const BirthHandler::BirthCode& bc);
  CellIndex create_cell(int dim, const Chain_T& bdd, const BirthType& bt);


 private:
  Quiver quiv;
  std::vector<Cell_T> cells;
  BirthHandler births;

};

}
}
#endif
