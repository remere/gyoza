#include "gyoza/birthhandler.h"

namespace gyoza {
namespace quivers {

void BirthHandler::clear() {
  data.clear();
  to_code.clear();
}

int BirthHandler::size()const{
  return data.size();
}

BirthHandler::BirthCode BirthHandler::add_type(const BirthType & bt) {
  boost::optional<BirthCode> ans = get_code(bt);
  if (ans == boost::none) {
    ans = data.size();
    data.push_back(bt);
    to_code[bt] = ans.get();
  }
  return ans.get();
}

BirthType BirthHandler::get_type(const BirthCode & bc) const {
  return data.at(bc);
}

boost::optional<BirthHandler::BirthCode>
BirthHandler::get_code(const BirthType & bt) const {
  auto ans = to_code.find(bt);
  if (ans != to_code.end()) {
    return ans->second;
  } else {
    return boost::none;
  }
}

bool BirthHandler::is_valid_code(const BirthCode & code)const {
  return (code >= 0) && (code < data.size());
}

std::vector<BirthType> BirthHandler::get_birth_types() const {
  return data;
}

bool  BirthHandler::less_than_eq(const BirthHandler::BirthCode& lhs,
                                 const BirthHandler::BirthCode& rhs)const {
  return get_type(lhs).is_subset_of(get_type(rhs));
}




}
}
