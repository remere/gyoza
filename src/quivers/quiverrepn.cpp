#include "gyoza/quiverrepn.h"
#include <sstream>


namespace gyoza {
namespace quivers {

Quiver QuiverRepn::get_quiver()const{
  return quiv;
}

int QuiverRepn::dimension_at(int vertex)const {
  return dimv.at(vertex);
}

void QuiverRepn::set_dimension_vector(const std::vector<int>& v) {
  if (v.size() != quiv.get_num_vertices()) {
    throw "Attempted to define dimension vector of inappropriate size!";
  }
  dimv = v;
}

void QuiverRepn::update_dimension_vector() {
  dimv = std::vector<int>(quiv.get_num_vertices(), 0);
  std::vector<bool> visited(quiv.get_num_vertices(), false);

  for (auto it = matrices.begin(); it != matrices.end(); ++it) {
    int source = it->first.first;
    int target = it->first.second;

    // matrices act by LEFT multiplication.
    if (not visited.at(source)) {
      dimv[source] = it->second.cols();
      visited[source] = true;
    }

    if (not visited.at(target)) {
      dimv[target] = it->second.rows();
      visited[target] = true;
    }
  }
}

std::vector<int> QuiverRepn::get_dimension_vector()const {
  return dimv;
}

Z2Matrix& QuiverRepn::matrix_at(int tail, int head) {
  return matrices[{tail,head}];
}

Z2Matrix QuiverRepn::matrix_at(int tail, int head)const{
  return matrices.at({tail,head});
}







void QuiverRepn::export_to_gap_qpa(std::ostream& output) const {
  if (quiv.get_num_vertices() <= 0) {
    throw "underlying quiver of repn has no vertices!";
  }

  quiv.export_to_gap_qpa(output);

  output << "imported_dimV := [";
  for (int vertex = 0; vertex < quiv.get_num_vertices() - 1; ++vertex) {
    output << dimv[vertex] << ",";
  }
  output << dimv[quiv.get_num_vertices() - 1] << "];;\n";

  std::stringstream matrices_array_entry;
  std::vector<std::string> matrix_definitions;

  Eigen::IOFormat gap_fmt(Eigen::StreamPrecision, Eigen::DontAlignCols,
                          ",", ",", "[", "]", "[", "]");

  for (int source = 0; source < quiv.get_num_vertices(); ++source){
    for (auto target : quiv.get_successors(source)) {
      const Z2Matrix& repn_matrix = matrices.at({source, target});
      if (repn_matrix.rows() != 0 && repn_matrix.cols() != 0) {
        // WARNING : GAP vertices start counting from 1.
        std::string arrow_name =  std::to_string(source+1) + "_" + std::to_string(target+1);
        output << "m" << arrow_name << " := ";
        output << "Z(2) * TransposedMat(" << repn_matrix.format(gap_fmt) << ");;\n";

        matrices_array_entry << "[\"" << arrow_name << "\"" << ", " << "m" << arrow_name << "]";
        matrix_definitions.push_back(matrices_array_entry.str());
        matrices_array_entry.str(std::string());
      }
    }
  }
  output << "imported_matrices := [";
  for (unsigned int i = 0; i < matrix_definitions.size() - 1; ++i) {
    output << matrix_definitions[i] << ", ";
  }
  output << *matrix_definitions.rbegin() << "];;\n";
  output << "imported_M := RightModuleOverPathAlgebra(imported_A, imported_dimV, imported_matrices);;\n";
  return;
}

}
}
