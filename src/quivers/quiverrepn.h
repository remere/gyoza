#ifndef QUIVERREPN_H
#define QUIVERREPN_H

#include <map>
#include "gyoza/common_definitions.h"
#include "gyoza/quiver.h"
#include <vector>
#include "gyoza/ladder_matrix_problem.h"

namespace gyoza {
namespace quivers {

class QuiverRepn {
 public:
  QuiverRepn() = delete;
  QuiverRepn(Quiver q): quiv(q) , dimv(q.get_num_vertices(),0) {};
  
  Quiver get_quiver()const;
  int dimension_at(int vertex)const;

  void update_dimension_vector();
  void set_dimension_vector(const std::vector<int>& v);
  std::vector<int> get_dimension_vector()const;
  
  Z2Matrix& matrix_at(int tail, int head);
  Z2Matrix matrix_at(int tail, int head)const;  

  void export_to_gap_qpa(std::ostream& output)const; 
  
  virtual boost::optional<matrixproblems::LadderMatrixProblem> get_matrix_problem() const {return boost::none;}

    
 protected:
  Quiver quiv;
  std::vector<int> dimv;
  // matrices act by LEFT multiplication.
  std::map< std::pair<int,int>, Z2Matrix > matrices; 
};

}
}

#endif
