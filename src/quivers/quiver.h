#ifndef QUIVER_H
#define QUIVER_H

// #include <boost/optional.hpp>
// #include <boost/optional/optional_io.hpp>

#include <vector>
#include <numeric>

#include <string>
#include <set>
#include <unordered_map>
#include <ostream>

#include "gyoza/birthhandler.h"
#include "gyoza/birthtype.h"

#include <exception>

namespace gyoza {
namespace quivers {


enum class QuiverType{
  Linear, Zigzag,
#define X(a,b,c) a,
  #include "gyoza/ladder_table.h"
#undef X
  General
  // Ladder_FB, Ladder_BF, Ladder_FF,
  // Ladder_FFF, Ladder_FFB, Ladder_BFB, Ladder_BBF,
  // Ladder_BBB, Ladder_FBB, Ladder_FBF, Ladder_BFF
};

struct Types{
  const static std::vector<gyoza::quivers::QuiverType> ladders_defined;
  const static std::map<std::string, QuiverType> code_to_ladder_type;

  static QuiverType code_to_ladder(std::string code);
  static std::string ladder_to_code(QuiverType ladder);

  static std::string ladder_to_short_code(QuiverType ladder);
};

class QuiverTypeMissingImplementationError : public std::runtime_error {  
public:
  QuiverTypeMissingImplementationError(std::string more_details)
      : runtime_error( "Missing implementation for ladder type."),
        details(more_details) {}
  
  virtual const char* what() const throw() {   
    return details.c_str();
  }
  std::string details;  
};


// Assume: no double arrows, no loops, no cycles
// => paths are uniquely identified by a sequence of vertices.
class Quiver {
 public:
  Quiver(int _num_vertices=0): type(QuiverType::General),
                               num_vertices(_num_vertices),
                               successors(_num_vertices){
  }

  int get_num_vertices() const { return num_vertices; }
  int add_vertex();
  bool add_edge(int source, int target);

  std::set<int> get_successors(int source)const {
    return successors.at(source);
  }

  // concerning specially recognized quivers
  bool is_special_quiver() const {return (type != QuiverType::General);}
  QuiverType get_type() const {return type;}
  
  bool allows_birth_type(const BirthType& bt)const;
  bool allows_births(const BirthHandler& bh)const;
  void export_to_gap_qpa(std::ostream& output)const;

  // check-ers
  // bool check_vertices()const;
  bool check_acyclicity()const;

  // operations?
  bool operator==(const Quiver&)const;

  // factories
  friend Quiver create_an_quiver(int);
  friend Quiver create_zigzag_fb_quiver(int);
  friend Quiver create_zigzag_bf_quiver(int);

  friend Quiver create_ladder_quiver(QuiverType type);

  
 private:
  QuiverType type;
  int num_vertices; // vertices are actually 0,1,...,num_vertices-1
  std::vector<std::set<int> > successors;


  // acyclicity checking tools
  enum class Color {WHITE, GRAY, BLACK};
  bool has_cycle_at(int vertex, std::vector<Color>& colors)const;
  bool nonrecurs_has_cycle_at(int vertex, std::vector<Color>& colors) const;

  // enumerating simple paths

};

//! Create A_n(f...f) quiver with n vertices.
/*! Comment: Gyoza is not meant to handle efficiently linear A_n type
  quivers. Instead, it is recommend to use things that compute the
  persistence diagram directly from the input filtration (or boundary
  matrix), for example PHAT. Rather, this functionality is added just
  to explore how well(?) the gyoza works in this case.
 */
Quiver create_an_quiver(int);

Quiver create_zigzag_fb_quiver(int);
Quiver create_zigzag_bf_quiver(int);

Quiver create_ladder_quiver(QuiverType type);





}
}
#endif
