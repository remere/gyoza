#include "catch.hpp"

#include "gyoza/setchain.h"
#include "gyoza/quivercomplex.h"

#include <sstream>

TEST_CASE("Quiver Complex json Reader - linear quiver", "[quivercomplex]") {
  gyoza::quivers::QuiverComplex<gyoza::Algebra::SetChain> qc;

  std::stringstream ss;
  ss << "{\"vertices\":3, \"quiver\":\"linear\", \"types\": [[1,1,1], [0,1,1]],\"cells\": [{\"boundary\":[], \"type\":0}, {\"boundary\":[], \"type\":0}, {\"boundary\":[0,1], \"type\":1}]}";

  qc.read_json(ss);
  CHECK(qc.get_num_vertices() == 3);
  gyoza::quivers::Quiver A3 = gyoza::quivers::create_an_quiver(3);
  CHECK(qc.get_quiver() == A3);
  CHECK(qc.get_births().size() == 2);
  CHECK(qc.check_integrity());
}

TEST_CASE("Quiver Complex json Reader - ladders", "[quivercomplex]") {
  gyoza::quivers::QuiverComplex<gyoza::Algebra::SetChain> qc;

  const std::vector<std::string> inputs = { "{\"quiver\":\"ladderf\"}",
                                            "{\"quiver\":\"ladderfb\"}",
                                            "{\"quiver\":\"ladderbf\"}",
                                            "{\"quiver\":\"ladderff\"}",
                                            "{\"quiver\":\"ladderfff\"}" };
  const std::vector<gyoza::quivers::QuiverType> expected =
      {gyoza::quivers::QuiverType::Ladder_F,
       gyoza::quivers::QuiverType::Ladder_FB,
       gyoza::quivers::QuiverType::Ladder_BF,
       gyoza::quivers::QuiverType::Ladder_FF,
       gyoza::quivers::QuiverType::Ladder_FFF};

  if (expected != gyoza::quivers::Types::ladders_defined) {
    std::cerr << "Warning: some ladder types not being checked for the quivercomplex reader.\n";
  }
  std::stringstream ldr;
  for (unsigned int i = 0; i < inputs.size(); ++i) {
    ldr << inputs.at(i);
    qc.read_json(ldr);    
    gyoza::quivers::Quiver Q = gyoza::quivers::create_ladder_quiver(expected.at(i));
    CHECK(qc.get_quiver() == Q);

    ldr.str("");
    ldr.clear();
  }
}


TEST_CASE("Quiver Complex Txt Reader - linear quiver", "[quivercomplex]") {
  gyoza::quivers::QuiverComplex<gyoza::Algebra::SetChain> qc;

  std::stringstream ss;
  ss << "linear 3\n"
     << "2\n"
     << "1,1,1\n"
     << "0,1,1\n"
     << "7\n"
     << "# cells below:\n"
     << "# DIM  BDDCELLS  TYPE \n"
     << "  0,            , 0\n"
     << "  0,            , 0\n"
     << "  0,            , 0\n"
     << "  1,   0,1      , 0\n"
     << "  1,   1,2      , 1\n"
     << "  1,   2,0      , 1\n"
     << "  2,   3,4,5    , 1\n";


  qc.read_txt(ss);
  CHECK(qc.get_num_vertices() == 3);
  gyoza::quivers::Quiver A3 = gyoza::quivers::create_an_quiver(3);
  CHECK(qc.get_quiver() == A3);
  CHECK(qc.get_births().size() == 2);
  CHECK(qc.check_integrity());
}

TEST_CASE("Quiver Complex - bad birthtype", "[quivercomplex]") {
  gyoza::quivers::QuiverComplex<gyoza::Algebra::SetChain> qc;

  std::stringstream ss;
  ss << "{\"vertices\":3, \"quiver\":\"linear\", \"types\": [[1,1,1], [0,1,1]],\"cells\": [{\"boundary\":[], \"type\":1}, {\"boundary\":[], \"type\":0}, {\"boundary\":[0,1], \"type\":0}]}";
  
  qc.read_json(ss);
  CHECK(qc.get_num_vertices() == 3);
  CHECK(qc.get_births().size() == 2);

  CHECK_FALSE(qc.check_integrity());
}

TEST_CASE("Quiver Complex - check bdd", "[quivercomplex]") {
  gyoza::quivers::QuiverComplex<gyoza::Algebra::SetChain> qc;

  std::stringstream ss;
  ss << "{\"vertices\":3,"
      "\"quiver\":\"linear\","
      "\"types\": [[1,1,1], [0,1,1]],"
      "\"cells\": ["
     << "{\"boundary\":[], \"type\":0},"     //0
     << "{\"boundary\":[], \"type\":0},"     //1
     << "{\"boundary\":[], \"type\":0},"     //2
     << "{\"boundary\":[], \"type\":0},"     //3
     << "{\"boundary\":[0,1], \"type\":0},"  //4
     << "{\"boundary\":[1,2], \"type\":0},"  //5
     << "{\"boundary\":[2,3], \"type\":1},"  //6
     << "{\"boundary\":[3,0], \"type\":1},"  //7
     << "{\"boundary\":[2,0], \"type\":0},"  //8
     << "{\"boundary\":[7,8,6], \"type\":1},"
     << "{\"boundary\":[4,5,8], \"type\":0}"
      "]}";
  qc.read_json(ss);  

  CHECK(qc.check_integrity());

}

TEST_CASE("Quiver Complex - bad bdd", "[quivercomplex]") {
  gyoza::quivers::QuiverComplex<gyoza::Algebra::SetChain> qc;

  std::stringstream ss;
  ss << "{\"vertices\":3,"
      "\"quiver\":\"linear\","
      "\"types\": [[1,1,1], [0,1,1]],"
      "\"cells\": ["
     << "{\"boundary\":[], \"type\":0},"     //0
     << "{\"boundary\":[], \"type\":0},"     //1
     << "{\"boundary\":[], \"type\":0},"     //2
     << "{\"boundary\":[], \"type\":0},"     //3
     << "{\"boundary\":[0,1], \"type\":0},"  //4
     << "{\"boundary\":[1,2], \"type\":0},"  //5
     << "{\"boundary\":[2,3], \"type\":1},"  //6
     << "{\"boundary\":[3,0], \"type\":1},"  //7
     << "{\"boundary\":[2,0], \"type\":0},"  //8
     << "{\"boundary\":[7,8,4], \"type\":1}," // malformed boundary
     << "{\"boundary\":[4,5,8], \"type\":0}"
      "]}";
  qc.read_json(ss);  
  CHECK_FALSE(qc.check_integrity());
}



TEST_CASE("Quiver Comple - various setting operations", "[quivercomplex]") {
  gyoza::quivers::QuiverComplex<gyoza::Algebra::SetChain> qc;
  gyoza::quivers::Quiver A3 = gyoza::quivers::create_an_quiver(3);
  gyoza::quivers::Quiver emptyquiver;
  gyoza::Algebra::SetChain emptybdd;

  gyoza::quivers::BirthType bt(3);
  bt[1] = true; bt[2] = true;

  SECTION("underlying quiver"){ 
    CHECK(qc.get_quiver() == emptyquiver);
    
    qc.set_quiver(A3);
    CHECK(qc.get_quiver() == A3);
    qc.clear();
    CHECK(qc.get_quiver() == emptyquiver);
  }
  
  SECTION("adding cells") {
    qc.set_quiver(A3);
    // no births at all:
    CHECK_THROWS(qc.create_cell(0, emptybdd, 0));
    CHECK(0 == qc.create_cell(0, emptybdd, bt));
    CHECK(qc.size() == 1);
    
    CHECK(qc.check_integrity());

    // we can now refert to birthcode 0:
    CHECK(1 == qc.create_cell(0, emptybdd, 0));
  }
  




}
