#include "gyoza/setchain.h"
#include "gyoza/quivercomplex.h"
#include <string>
#include <fstream>
#include <iostream>


int main(int argc, char** argv) {
  if (argc != 2) {
    std::cerr << "Incorrect number of arguments!" << std::endl;
    return 1;
  }
  std::ofstream ofile(argv[1]);
  if (!ofile.is_open()){
    std::cerr << "Output file cannot be opened!" << std::endl;
    return 1;
  }

  gyoza::quivers::QuiverComplex<gyoza::Algebra::SetChain> qc;
  std::stringstream ss;
  ss << "{\"vertices\":3, \"quiver\":\"linear\", "
      "\"types\": [[1,1,1], [0,1,1]], "
      "\"cells\": "
      "[{\"boundary\":[], \"type\":0}, "
      "{\"boundary\":[], \"type\":0}, "
      "{\"boundary\":[], \"type\":0}, "
      "{\"boundary\":[0,1], \"type\":1}, "
      "{\"boundary\":[0,2], \"type\":1}, "
      "{\"boundary\":[1,2], \"type\":1}]}";
  qc.read_json(ss);
  gyoza::quivers::QuiverRepn h0 = qc.naive_compute_persistence(0);

  h0.export_to_gap_qpa(ofile);

  ofile << "quit;;";

  ofile.close();


  return 0;
}
