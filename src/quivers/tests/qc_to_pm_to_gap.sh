#!/usr/bin/env sh

echo "Verifying that GAP format output can actually be read by GAP..."

tmpfile=$(mktemp /tmp/quivergap.XXXXXX) || exit 1
echo "file written to: " "$tmpfile"
# trap 'rm "$tmpfile"' EXIT

./qcomplex_to_gap "$tmpfile" || exit 1
cat "$tmpfile" | gap -q loader.g  || exit 1

exit 0
