#include "catch.hpp"

#include "gyoza/quiverrepn.h"
#include <sstream>


TEST_CASE("QuiverRepn Initialization", "[quiverrepn]") {  
  gyoza::quivers::Quiver A5 = gyoza::quivers::create_an_quiver(5);
  // empty quiver representation over A5.
  gyoza::quivers::QuiverRepn M(A5);
  
  CHECK(M.get_quiver() == A5);
  CHECK(M.get_dimension_vector() == std::vector<int>(5,0));

  CHECK_THROWS(M.set_dimension_vector(std::vector<int>(4,0)));

  M.update_dimension_vector();
  CHECK(M.get_dimension_vector() == std::vector<int>(5,0)); 
}


TEST_CASE("QuiverRepn", "[quiverrepn]") {  
  gyoza::quivers::Quiver A3 = gyoza::quivers::create_an_quiver(3);
  // empty quiver representation over A5.
  gyoza::quivers::QuiverRepn M(A3);

  M.matrix_at(0, 1) = gyoza::Z2Matrix::Random(2, 3);
  M.matrix_at(1, 2) = gyoza::Z2Matrix::Random(5, 2);

  // matrices act by LEFT multiplication.
  std::vector<int> expected = {3,2,5};
  // no auto-updating of dimension vector
  CHECK_FALSE(M.get_dimension_vector() == expected);

  M.update_dimension_vector();
  CHECK(M.get_dimension_vector() == expected); 
  

}
