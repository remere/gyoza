#!/usr/bin/env sh

echo "Verifying that GAP format output can actually be read by GAP..."

antmpfile=$(mktemp /tmp/quivergap.XXXXXX) || exit 1
ladderstmpfile=$(mktemp /tmp/quivergap.XXXXXX) || exit 1
echo "files written to: " "$antmpfile" " " "$ladderstmpfile"
# trap 'rm "$tmpfile"' EXIT

./an_to_gap "$antmpfile" 3 || exit 1
./ladders_to_gap "$ladderstmpfile" || exit 1

cat "$antmpfile" | gap -q loader.g  || exit 1
cat "$ladderstmpfile" | gap -q loader.g  || exit 1

echo "" 

exit 0


