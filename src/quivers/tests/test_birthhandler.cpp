#include "catch.hpp"
#include "gyoza/birthtype.h"
#include "gyoza/birthhandler.h"


TEST_CASE( "BirthType - constructors", "[birthtype]" ) {
  gyoza::quivers::BirthType bt;
  CHECK(bt.size() == 0);

  bt = gyoza::quivers::BirthType(5);
  CHECK(bt.size() == 5);
  CHECK(bt.count() == 0);

  bt.flip();
  CHECK(bt.size() == 5);
  CHECK(bt.count() == 5);  

  gyoza::quivers::BirthType bt2 = {1,0,0,0};
  CHECK(bt2.size() == 4);
  CHECK(bt2.count() == 1);

  bt2.flip();  
  CHECK(bt2.count() == 3);
  bt2.flip();  
  CHECK(bt2.count() == 1);
}


TEST_CASE( "BirthType - access", "[birthtype]" ) {
  gyoza::quivers::BirthType bt = {1,0,0,0};
  gyoza::quivers::BirthType bt2= bt;
  CHECK(bt == bt2);

  bt2 = {true, false, true, false};
  bt[2] = true;
  CHECK(bt == bt2);

  CHECK( bt[0] == true );
  CHECK( bt[1] == false );
  CHECK( bt[2] == true );
  CHECK( bt[3] == false );

  // out-of-range ACCESS does not seem to change size?!
  // (and seems to compile and run?!)
  CHECK( bt[10] == false );
  CHECK( bt.size() == 4 );
}

TEST_CASE( "BirthType - subset", "[birthtype]" ) {
  gyoza::quivers::BirthType bt_1 = {1,1,0,0};
  gyoza::quivers::BirthType bt_2 = {1,1,0,1};
  gyoza::quivers::BirthType bt_3 = {0,1,0,1};

  CHECK( bt_1.is_subset_of(bt_1) );

  CHECK( bt_1.is_subset_of(bt_2) );
  CHECK( bt_3.is_subset_of(bt_2) );

  CHECK_FALSE( bt_3.is_subset_of(bt_1) );
  CHECK_FALSE( bt_1.is_subset_of(bt_3) );
}

TEST_CASE( "BirthType - union", "[birthtype]" ) {
  gyoza::quivers::BirthType bt_1 = {1,1,0,0};
  gyoza::quivers::BirthType bt_2 = {1,1,0,1};
  
  gyoza::quivers::BirthType expected = {1,1,0,1};
  gyoza::quivers::BirthType bt_1_copy = bt_1;
  
  gyoza::quivers::BirthType answer = bt_1.get_union_with(bt_2);
  CHECK( answer == expected ); 

  CHECK( bt_1 == bt_1_copy );  
}





TEST_CASE( "BirthHandler", "[birthhandler]" ){
  gyoza::quivers::BirthHandler bh;

  // adding birth types.
  CHECK( 0 == bh.add_type({0,0,1,1,0}) );
  CHECK( 1 == bh.add_type({0,1,1,1,0}) );

  CHECK(bh.is_valid_code(0));
  CHECK(bh.is_valid_code(1));
  
  // adding an already existing birth type
  CHECK( 0 == bh.add_type({0,0,1,1,0}) );
  CHECK_FALSE(bh.is_valid_code(2));
    
  // queries
  CHECK( boost::none == bh.get_code({0,0,0,1,0}) );
  CHECK( bh.get_code({0,1,1,1,0}).get() == 1 );

  CHECK( bh.get_type(1) == gyoza::quivers::BirthType({0,1,1,1,0}) );
}

TEST_CASE( "BirthHandler iterators", "[birthhandler]" ){
  gyoza::quivers::BirthHandler bh;
  // adding birth types.
  CHECK( 0 == bh.add_type({0,0,1,1,0}) );
  CHECK( 1 == bh.add_type({0,1,1,1,0}) );

  auto it = bh.cbegin();
  CHECK( *it == gyoza::quivers::BirthType({0,0,1,1,0}) );
  ++it;
  CHECK( *it == gyoza::quivers::BirthType({0,1,1,1,0}) );
  ++it;
  CHECK( it == bh.cend() );
  CHECK(std::distance(bh.cbegin(), bh.cend()) == 2);
}
