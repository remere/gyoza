#include "gyoza/quiver.h"
#include <string>
#include <fstream>
#include <iostream>


int main(int argc, char** argv) {
  if (argc != 3) {
    std::cerr << "Incorrect number of arguments!" << std::endl;
    return 1;
  }
  std::ofstream ofile(argv[1]);
  if (!ofile.is_open()){
    std::cerr << "Output file cannot be opened!" << std::endl;
    return 1;
  }

  int n = std::stoi(std::string(argv[2]));
    
  gyoza::quivers::Quiver Q = gyoza::quivers::create_an_quiver(n);
  Q.export_to_gap_qpa(ofile);

  ofile << "quit;;";
  
  ofile.close();

  
  return 0;
}
