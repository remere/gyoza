#include "gyoza/quiver.h"
#include <string>
#include <fstream>
#include <iostream>
#include <vector>


int main(int argc, char** argv) {
  if (argc != 2) {
    std::cerr << "Incorrect number of arguments!" << std::endl;
    return 1;
  }
  std::ofstream ofile(argv[1]);
  if (!ofile.is_open()){
    std::cerr << "Output file cannot be opened!" << std::endl;
    return 1;
  }

  const auto to_test = gyoza::quivers::Types::ladders_defined;

  for (const auto& t : to_test) {
    gyoza::quivers::Quiver Q = gyoza::quivers::create_ladder_quiver(t);

    try {
      Q.export_to_gap_qpa(ofile);
    }catch (const gyoza::quivers::QuiverTypeMissingImplementationError & e) {
      std::cerr << e.what() << std::endl;
      return 1;
    }
    
    ofile << "\n";
  }

  ofile << "quit;";

  ofile.close();


  return 0;
}
