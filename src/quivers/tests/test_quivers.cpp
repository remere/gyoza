#include "catch.hpp"
#include "gyoza/quiver.h" 

TEST_CASE( "quiver - modification", "[quivers]") {
  gyoza::quivers::Quiver Q(4);
  CHECK(Q.get_num_vertices() == 4);
  CHECK(4 == Q.add_vertex());
  CHECK(Q.get_num_vertices() == 5);
  
  // no loops allowed!
  CHECK_THROWS(Q.add_edge(0,0));  
}



TEST_CASE( "Trivial checks. A_n linear quiver", "[quivers][linear_an]") {
  gyoza::quivers::Quiver Q = gyoza::quivers::create_an_quiver(3);
  CHECK( Q.is_special_quiver() );
  CHECK( Q.get_num_vertices() == 3 );

  CHECK_THROWS( gyoza::quivers::create_an_quiver(0) );
  CHECK_THROWS( gyoza::quivers::create_an_quiver(-2) );
}

TEST_CASE( "Ladder quiver factory", "[quivers][ladders]") {
  const auto to_test = gyoza::quivers::Types::ladders_defined;
  
  const std::vector<int> expected_vertices = {4,6,6,6,6,8,8,8,8,8,8,8,8};
  if (expected_vertices.size() != gyoza::quivers::Types::ladders_defined.size()){
    INFO("Warning: number of ladders defined different from expected here!\nPerhaps ladders_defined was updated and this test wasn't\n"); 
    CHECK(expected_vertices.size() == gyoza::quivers::Types::ladders_defined.size() ); 
  } else {
    for (int i = 0; i < to_test.size(); ++i) {
      gyoza::quivers::Quiver Q = gyoza::quivers::create_ladder_quiver(to_test.at(i));
      CHECK(Q.get_num_vertices() == expected_vertices.at(i));
    }
  }
}


TEST_CASE( "Quiver equality", "[quivers]") {
  gyoza::quivers::Quiver F(2);
  F.add_edge(0, 1);
  CHECK(F==F);

  gyoza::quivers::Quiver A2 = gyoza::quivers::create_an_quiver(2);
  // F is created manually, does not that special tag!
  CHECK_FALSE(F==A2);

  // While isomorphic as graphs, not actually equal.
  gyoza::quivers::Quiver B(2);
  B.add_edge(1, 0);
  CHECK_FALSE(F==B);
}


TEST_CASE( "Modifying quiver removes special tags,  A_n linear quiver", "[quivers][linear_an]") {
  gyoza::quivers::Quiver Q = gyoza::quivers::create_an_quiver(3);

  SECTION("Add vertex") {
    // new vertex is given index 3.
    // existing vertices are implicitly 0,1,2
    CHECK( 3 == Q.add_vertex() );
    CHECK_FALSE( Q.is_special_quiver() );
  }

  SECTION("Add edge") {
    Q.add_edge(0,2);
    CHECK_FALSE( Q.is_special_quiver() );
    CHECK_THROWS( Q.add_edge(0,3));
  }
}


TEST_CASE( "Modifying quiver removes special tags,  ladder quiver", "[quivers][ladders]") {
  const auto to_test = gyoza::quivers::Types::ladders_defined;
  
  for (const auto& t : to_test) {
    gyoza::quivers::Quiver Q = gyoza::quivers::create_ladder_quiver(t);
    CHECK( Q.is_special_quiver() );
    Q.add_vertex();
    CHECK_FALSE( Q.is_special_quiver() );
  }

  for (const auto& t : to_test) {
    gyoza::quivers::Quiver Q = gyoza::quivers::create_ladder_quiver(t);
    CHECK( Q.is_special_quiver() );
    Q.add_edge(0,1);
    CHECK_FALSE( Q.is_special_quiver() );
    // Note: special-ness is fragile. Even if Q already has edge 0-->1,
    // just calling add_edge destroys special status.
  }
}


TEST_CASE( "Birth type checking,  A_n linear quiver", "[quivers][linear_an]" ){
  gyoza::quivers::Quiver Q = gyoza::quivers::create_an_quiver(5);

  // birth type too long
  CHECK_FALSE(Q.allows_birth_type({1,1,1,1,1,1}));

  //some valid types
  CHECK(Q.allows_birth_type({1,1,1,1,1}));
  CHECK(Q.allows_birth_type({0,0,1,1,1}));
  CHECK(Q.allows_birth_type({0,0,0,1,1}));

  // birth type invalid
  CHECK_FALSE(Q.allows_birth_type({1,1,1,0,0}));
  CHECK_FALSE(Q.allows_birth_type({0,1,1,0,0}));
}


TEST_CASE( "Birth type checking,  Zigzag fb quiver", "[quivers][zigzag_fb]" ){
  gyoza::quivers::Quiver Q = gyoza::quivers::create_zigzag_fb_quiver(5);

  // birth type too long
  CHECK_FALSE(Q.allows_birth_type({1,1,1,1,1,1}));

  // invalid types
  CHECK_FALSE(Q.allows_birth_type({0,0,1,1,0}));
  CHECK_FALSE(Q.allows_birth_type({0,0,1,1,1}));

  //some valid types
  CHECK(Q.allows_birth_type({1,1,1,1,1}));
  CHECK(Q.allows_birth_type({0,1,1,1,1}));
  CHECK(Q.allows_birth_type({0,0,0,1,1}));
}


TEST_CASE( "Checking acyclicity", "[quivers][acyclic]") {
  CHECK(gyoza::quivers::create_an_quiver(3).check_acyclicity());
  CHECK(gyoza::quivers::create_an_quiver(1000).check_acyclicity());

  const auto ladders_to_test = gyoza::quivers::Types::ladders_defined;
  
  for (const auto& t: ladders_to_test){
    CHECK(gyoza::quivers::create_ladder_quiver(t).check_acyclicity());
  }

  gyoza::quivers::Quiver triangle(3);
  triangle.add_edge(0,1);
  triangle.add_edge(1,2);
  triangle.add_edge(2,0);
  CHECK_FALSE(triangle.check_acyclicity());

  int n = 1000;
  gyoza::quivers::Quiver longloop = gyoza::quivers::create_an_quiver(n);
  longloop.add_edge(n-1,0);
  CHECK_FALSE(longloop.check_acyclicity());

  int m = 8194; int h = 50;
  gyoza::quivers::Quiver complicated(m);
  for (int i = 0; i < m - h; ++i) {
    for (int j = i+1; j <= i+h; ++j) {
      complicated.add_edge(i,j);
    }
  }
  CHECK(complicated.check_acyclicity());
  complicated.add_edge(m-1,0);
  CHECK_FALSE(complicated.check_acyclicity());
}


// TEST_CASE( "simple paths", "[quivers]" ) {
//   // TODO: NEEDS WORK
//   gyoza::quivers::Quiver triangle(3);
//   triangle.add_edge(0,1);
//   triangle.add_edge(0,2);
//   triangle.add_edge(1,2);
//   triangle.add_edge(2,0);
//   triangle.simple_paths(0,2);

//   int m = 100; int h = 2;
//   gyoza::quivers::Quiver complicated(m);
//   for (int i = 0; i < m - h; ++i) {
//     for (int j = i+1; j <= i+h; ++j) {
//       complicated.add_edge(i,j);
//     }
//   }
//   complicated.simple_paths(0, 6);
// }

TEST_CASE("check hardcoded ladder types", "[quivers][ladders]"){
  for (auto const & lt : gyoza::quivers::Types::ladders_defined) {
    INFO("String code for ladder not found."); 
    CHECK(gyoza::quivers::Types::ladder_to_code(lt).size() != 0);
  }

  const auto ladders_to_test = gyoza::quivers::Types::ladders_defined;
  for (const auto& t: ladders_to_test){
    std::string short_code = gyoza::quivers::Types::ladder_to_short_code(t);
    gyoza::quivers::Quiver ladder = gyoza::quivers::create_ladder_quiver(t);
    int n = ladder.get_num_vertices();
    CHECK( n % 2 == 0);
    int half_n = n/2;

    CHECK(short_code.size() == half_n - 1);

    for (int i = 0; i < half_n; ++i) {
      // top vertex:
      const int source = half_n + i;
      std::set<int> expected = {};
      if (i > 0 && short_code.at(i-1) == 'b') {
        expected.insert(source - 1);
      }
      if (i < half_n - 1 && short_code.at(i) == 'f') {
        expected.insert(source + 1);
      }
      CHECK(ladder.get_successors(source) == expected);

      // bottom vertex:
      expected = {source};
      if (i > 0 && short_code.at(i-1) == 'b') {
        expected.insert(i - 1);
      }
      if (i < half_n - 1 && short_code.at(i) == 'f') {
        expected.insert(i + 1);
      }
      CHECK(ladder.get_successors(i) == expected);
    }

  }

  

}
