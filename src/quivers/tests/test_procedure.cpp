#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "gyoza/setchain.h"
#include "gyoza/quivercomplex.h"

#include <sstream>
#include <vector>




TEST_CASE("Quiver Complex to Persistence Module", "[persistence]") {
  gyoza::quivers::QuiverComplex<gyoza::Algebra::SetChain> qc;

  std::stringstream ss;
  ss << "{\"vertices\":3, \"quiver\":\"linear\", "
      "\"types\": [[1,1,1], [0,1,1]], "
      "\"cells\": "
      "[{\"boundary\":[], \"type\":0}, "
      "{\"boundary\":[], \"type\":0}, "
      "{\"boundary\":[], \"type\":0}, "
      "{\"boundary\":[0,1], \"type\":1}, "
      "{\"boundary\":[0,2], \"type\":1}, "
      "{\"boundary\":[1,2], \"type\":1}]}";
  qc.read_json(ss);

  gyoza::quivers::QuiverRepn h1 = qc.naive_compute_persistence(1);
  gyoza::quivers::QuiverRepn h0 = qc.naive_compute_persistence(0);

  std::vector<int> h1expected = {0,1,1};
  std::vector<int> h0expected = {3,1,1};

  CHECK(h1.get_dimension_vector() == h1expected);
  CHECK(h0.get_dimension_vector() == h0expected);


}
