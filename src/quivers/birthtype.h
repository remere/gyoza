#ifndef BIRTHTYPE_H
#define BIRTHTYPE_H

#include <iostream>
#include <vector>
#include <initializer_list>

// OPTIONS TO CONSIDER:
// #include <boost/serialization/strong_typedef.hpp>
 #include <boost/dynamic_bitset.hpp>

namespace gyoza {
namespace quivers {

// typedef std::vector<bool> BirthType;
// typedef boost::dynamic_bitset<> BirthType;

class BirthType {
 public:
  typedef boost::dynamic_bitset<>::reference reference;
  typedef boost::dynamic_bitset<>::size_type size_type;
  
  BirthType() = default;
  BirthType(size_type n) : data(n) {};
  
  BirthType(std::initializer_list<int> l) {
    for (auto it = l.begin(); it != l.end(); ++it) {
      data.push_back(bool(*it)); 
    }
  }

  void push_back(bool bit){data.push_back(bit);}
  reference operator[](size_type pos) {return data[pos];}
  bool operator[](size_type pos) const {return data[pos];}
  BirthType& flip(){data.flip(); return *this;}
  
  void clear() {data.clear();}
  size_type count() const { return data.count(); }
  size_type size() const { return data.size(); }
  
  bool is_subset_of( const BirthType & other ) const {
    return (this->data).is_subset_of(other.data);
  }

  BirthType get_union_with(BirthType other) const {
    other.data |= this->data;
    return other;
  }

  
  boost::dynamic_bitset<> data; 

};


bool operator<(const BirthType& lhs, const BirthType& rhs);
bool operator>(const BirthType& lhs, const BirthType& rhs); 
bool operator==(const BirthType& lhs, const BirthType& rhs);
bool operator!=(const BirthType& lhs, const BirthType& rhs);

}
}

std::ostream& operator<<(std::ostream& os, const gyoza::quivers::BirthType& bc);

#endif
