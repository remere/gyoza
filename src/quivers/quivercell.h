#ifndef QUIVERCELL_H
#define QUIVERCELL_H


#include <vector>
#include <set>
#include <istream>

#include "birthhandler.h"


namespace gyoza {
namespace quivers {


template<typename Chain_T>
struct Cell {
  typedef int Index;
  typedef BirthHandler::BirthCode BirthCode;
  typedef Chain_T Chain;
  
  Index index;
  int dimension;
  BirthCode code;
  Chain_T bdd; 
};


}
}

#endif
