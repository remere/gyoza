#define LADDER_TABLE              \
  X(Ladder_F,"ladderf","f")       \
  X(Ladder_FB,"ladderfb","fb")    \
  X(Ladder_BF,"ladderbf","bf")    \
  X(Ladder_FF,"ladderff","ff")    \
  X(Ladder_BB,"ladderbb","bb")    \
  X(Ladder_FFF,"ladderfff","fff") \
  X(Ladder_FFB,"ladderffb","ffb") \
  X(Ladder_BFB,"ladderbfb","bfb") \
  X(Ladder_BBF,"ladderbbf","bbf") \
  X(Ladder_BBB,"ladderbbb","bbb") \
  X(Ladder_FBB,"ladderfbb","fbb") \
  X(Ladder_FBF,"ladderfbf","fbf") \
  X(Ladder_BFF,"ladderbff","bff")

LADDER_TABLE

#undef LADDER_TABLE
