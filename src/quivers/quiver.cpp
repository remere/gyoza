#include "gyoza/quiver.h"
#include <stack>
#include <sstream>
#include <array>

namespace gyoza {
namespace quivers {

const std::vector<gyoza::quivers::QuiverType> Types::ladders_defined = 
{
#define X(a,b,c) QuiverType::a,
  #include "gyoza/ladder_table.h"
#undef X
};

const std::map<std::string, QuiverType> Types::code_to_ladder_type =
{
#define X(a,b,c) { b, QuiverType::a },
  #include "gyoza/ladder_table.h"
#undef X
};


QuiverType Types::code_to_ladder(std::string code) {
  auto cit = Types::code_to_ladder_type.find(code);
  if (cit == Types::code_to_ladder_type.cend()) {
    throw "requested ladder quiver currently not supported";
  } else {
    return cit->second;
  }
}

std::string Types::ladder_to_code(QuiverType ladder) {
  for (auto const & c_l_pair : Types::code_to_ladder_type) {
    if (c_l_pair.second == ladder) {
      return c_l_pair.first;
    }
  }
  return "";  
}


std::string Types::ladder_to_short_code(QuiverType ladder) {
  const static std::map<QuiverType, std::string> my_data =
      {
#define X(a,b,c) { QuiverType::a, c },
#include "gyoza/ladder_table.h"
#undef X
      };

  auto cit = my_data.find(ladder);
  if (cit == my_data.cend()) {
    return "";
  } else {
    return cit->second;
  }
}


int Quiver::add_vertex(){
  type = QuiverType::General;
  successors.emplace_back();
  return num_vertices++;
}

bool Quiver::add_edge(int source, int target) {
  if (source < 0 || target < 0 ||
      source >= num_vertices || target >= num_vertices) {
    throw ("edge cannot be created, vertex invalid");
  }
  if (source == target) {
    throw ("cannot add loop (source == target) to quiver");
  }
  type = QuiverType::General;
  successors.at(source).insert(target);
  return true;
}


bool Quiver::allows_birth_type(const BirthType& bt)const {
  if (bt.size() != num_vertices) {
    return false;
  }
  for (int cur_vertex = 0; cur_vertex < num_vertices; ++cur_vertex) {
    if (!bt[cur_vertex]) {
      continue;
    }
    // here, bt.at(cur_vertex) == true;
    for (auto const & successor_vertex : successors.at(cur_vertex)) {
      if (!bt[successor_vertex]){
        return false;
      }
    }
  }
  return true;
}

bool Quiver::allows_births(const BirthHandler& bh)const {
  bool status = true;
  for (auto it = bh.cbegin(); it != bh.cend(); ++it){
    if (not allows_birth_type(*it)){
      status = false;
      break;
    }
  }
  return status;
}


bool Quiver::operator==(const Quiver& other)const {
  return (type == other.type) && (num_vertices == other.num_vertices) && (successors == other.successors);
}

// void Quiver::simple_paths(int source, int target) const {

//INCORRECT:
// std::vector<Color> colors(num_vertices, Color::WHITE);
// std::stack<int> mystack;
// mystack.push(source);

// std::list<int> current_path;

// int current_vertex;
// while (not mystack.empty()){
//   current_vertex = mystack.top();
//   current_path.push_back(current_vertex);

//   if (current_vertex == target) {
//     std::cerr << "path found : ";
//     for (auto v : current_path) {
//       std::cerr << v << " ";
//     }
//     std::cerr << "\n";
//   }

//   // if (colors.at(current_vertex) == Color::GRAY ){
//   //   colors.at(current_vertex) = Color::BLACK;
//   //   mystack.pop();
//   //   current_path.pop_back();
//   //   continue;
//   // }

//   colors.at(current_vertex) = Color::GRAY;

//   bool is_terminus = true;
//   for (const auto & successor : successors.at(current_vertex)) {
//     if (colors.at(successor) == Color::GRAY){
//       // cycle ignored
//       continue;
//     }
//     if (colors.at(successor) == Color::WHITE){
//       is_terminus = false;
//       mystack.push(successor);
//     }
//   }

//   if (is_terminus) {
//     colors.at(current_vertex) = Color::BLACK;
//     mystack.pop();
//     current_path.pop_back();
//   }
// }
// return;
// }





bool Quiver::has_cycle_at(int vertex, std::vector<Color>& colors)const {
  colors.at(vertex) = Color::GRAY;

  for (const auto & successor : successors.at(vertex)) {
    if (colors.at(successor) == Color::GRAY){
      return true;
    }
    if (colors.at(successor) == Color::WHITE && has_cycle_at(successor, colors)){
      return true;
    }
  }
  colors.at(vertex) = Color::BLACK;
  return false;
}




bool Quiver::nonrecurs_has_cycle_at(int vertex, std::vector<Color>& colors) const {
  std::stack<int> mystack;
  mystack.push(vertex);

  int current_vertex;
  while (not mystack.empty()){
    current_vertex = mystack.top();
    // TO-CHECK:
    // added this optimization
    if (colors.at(current_vertex) == Color::GRAY ){
      // this only happens when current_vertex has already been visited before
      // and all of its successors have been processed.
      colors.at(current_vertex) = Color::BLACK;
      mystack.pop();
      continue;
    }

    colors.at(current_vertex) = Color::GRAY;

    bool is_terminus = true;
    for (const auto & successor : successors.at(current_vertex)) {
      if (colors.at(successor) == Color::GRAY){
        return true;
      }
      if (colors.at(successor) == Color::WHITE){
        is_terminus = false;
        mystack.push(successor);
      }
    }

    if (is_terminus) {
      colors.at(current_vertex) = Color::BLACK;
      mystack.pop();
    }
  }
  return false;
}

bool Quiver::check_acyclicity()const{
  std::vector<Color> colors(num_vertices, Color::WHITE);

  for (int vertex = 0; vertex < num_vertices; ++vertex) {
    if (colors.at(vertex) == Color::WHITE && nonrecurs_has_cycle_at(vertex, colors)){
      return false;
    }
  }
  return true;
}



std::string create_ladder_4_relations(std::array<char, 3> type) {
  std::stringstream ans;
  ans << "imported_A := imported_KQ/[";
  if (type[0] == 'f') {
    ans << "imported_KQ.1_2 * imported_KQ.2_6 - imported_KQ.1_5 * imported_KQ.5_6,";
  } else if (type[0] == 'b') {
    ans << "imported_KQ.2_1 * imported_KQ.1_5 - imported_KQ.2_6 * imported_KQ.6_5,";
  } else {
    throw;
  }

  if (type[1] == 'f') {
    ans << "imported_KQ.2_3 * imported_KQ.3_7 - imported_KQ.2_6 * imported_KQ.6_7,";
  } else if (type[1] == 'b') {
    ans << "imported_KQ.3_2 * imported_KQ.2_6 - imported_KQ.3_7 * imported_KQ.7_6,";
  } else {
    throw;
  }

  if (type[2] == 'f') {
    ans << "imported_KQ.3_4 * imported_KQ.4_8 - imported_KQ.3_7 * imported_KQ.7_8";
  } else if (type[2] == 'b') {
    ans << "imported_KQ.4_3 * imported_KQ.3_7 - imported_KQ.4_8 * imported_KQ.8_7";
  } else {
    throw;
  }

  ans << "];;\n";
  return ans.str();
}


std::string create_ladder_3_relations(std::array<char, 2> type) {
  std::stringstream ans;
  ans << "imported_A := imported_KQ/[";
  if (type[0] == 'f') {
    ans << "imported_KQ.1_2 * imported_KQ.2_5 - imported_KQ.1_4 * imported_KQ.4_5,";
  } else if (type[0] == 'b') {
    ans << "imported_KQ.2_1 * imported_KQ.1_4 - imported_KQ.2_5 * imported_KQ.5_4,";
  } else {
    throw;
  }

  if (type[1] == 'f') {
    ans << "imported_KQ.2_3 * imported_KQ.3_6 - imported_KQ.2_5 * imported_KQ.5_6";
  } else if (type[1] == 'b') {
    ans << "imported_KQ.3_2 * imported_KQ.2_5 - imported_KQ.3_6 * imported_KQ.6_5";
  } else {
    throw;
  }
  ans << "];;\n";
  return ans.str();
}


void Quiver::export_to_gap_qpa(std::ostream& output)const {
  output << "imported_Q := ";
  output << "Quiver(" << num_vertices << ", [";

  std::vector<std::string> arrow_definitions;
  std::stringstream arrow_entry;
  for (int source = 0; source < num_vertices; ++source) {
    for (auto const & target : successors.at(source)) {
      // WARNING : GAP vertices start counting from 1.
      arrow_entry << "[" << source+1 << "," << target+1 << ",";             // specify vertices
      arrow_entry << "\"" << source+1 << "_" << target+1 << "\"" << "]";    // specify name
      arrow_definitions.push_back(arrow_entry.str());
      arrow_entry.str(std::string());
    }
  }

  for (int i = 0; i < arrow_definitions.size() - 1; ++i) {
    output << arrow_definitions[i] << ", ";
  }
  output << *arrow_definitions.rbegin() << "]);;\n";

  output << "imported_KQ := PathAlgebra(GF(2), imported_Q);;\n";

  std::string short_code = Types::ladder_to_short_code(type);

  if ( type == QuiverType::Linear || type == QuiverType::Zigzag ) {
    // No commutative relations need to be imposed.
    output << "imported_A := imported_KQ;;\n";
  } else if ( type == QuiverType::Ladder_F ) {
    // WARNING : GAP vertices start counting from 1.
    output << "imported_A := imported_KQ/["
        "imported_KQ.1_2 * imported_KQ.2_4 - imported_KQ.1_3 * imported_KQ.3_4"
        "];;\n";
  } else if (short_code.size() == 2) {
    output << create_ladder_3_relations({short_code[0], short_code[1]});
  } else if (short_code.size() == 3) {
    output << create_ladder_4_relations({short_code[0], short_code[1], short_code[2]});
  }
  else {
    // figure out commutative relations -- GENERAL CASE!
    throw QuiverTypeMissingImplementationError("Output of quiver with the given type to GAP format currently not supported!");
    
    if (num_vertices > 8) {
      throw "Output of quiver to GAP format currently not supported for general quiver with more than 8 vertices!";
    }
  }

  return;
}





}
}
