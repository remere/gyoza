#include "gyoza/quivercomplex.h"

#include "gyoza/setchain.h"
#include "gyoza/common_definitions.h"

#include <map>
#include <cassert>
#include "nlohmann_json/json.hpp"
using json = nlohmann::json;

#include <functional>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/trim.hpp>

#include "gyoza/slicedhomology.h"
// #include "gyoza/repnclfb.h"

namespace gyoza {
namespace quivers {


Quiver parse_quiver(std::string name, int num_vertices) {
  Quiver quiv;
  if (name.compare(0, 6, "ladder") == 0){
    // num vertices ignored
    QuiverType ladder = Types::code_to_ladder(name);
    quiv = create_ladder_quiver(ladder);
  } else if (name == "linear"){
    if (num_vertices == -1){throw "Number of vertices could not be read from input!";}
    quiv = gyoza::quivers::create_an_quiver(num_vertices);
  } else {
    throw "Unsupported quiver detected, or quiver not declared in input";
  }
  return quiv;
}



int json_get_num_vertices(const json & inputjson) {
  // count vertices
  int n = -1;
  json::const_iterator n_it = inputjson.find("vertices");
  if (n_it != inputjson.end() && n_it->is_number()){
    n  = *n_it;
  }
  return n;
}

Quiver json_get_quiver(const json & inputjson) {
  json::const_iterator q_it = inputjson.find("quiver");
  if (q_it == inputjson.end()) {
    throw "Input json file does not declare its quiver.";
  }
  Quiver quiv;
  int num_vertices = json_get_num_vertices(inputjson);
  if (q_it->is_string()) {
    quiv = parse_quiver(*q_it, num_vertices);
  } else {
    // try to read defined quiver..
  }
  return quiv;
}

std::vector<std::string> txt_process_line(std::string cur_line) {
  std::vector<std::string> vstrings;
  // cur_line.erase(cur_line.find_last_not_of(" \t\n\r\f\v")+1);
  boost::trim(cur_line);
  if (cur_line != "" && cur_line[0] != '#'){
    boost::split(vstrings, cur_line, boost::is_any_of(", 	"), boost::token_compress_on);
  }
  return vstrings;
}


template<typename IterableType>
BirthType create_birth_type(IterableType data, std::function<bool(typename IterableType::value_type)> converter) {
  BirthType bt(data.size());
  for (unsigned int i = 0; i < data.size(); ++i) {
    bt[i] = converter(data[i]);
  }
  return bt;
}



std::vector<BirthHandler::BirthCode> json_read_birthtypes(const json & inputjson, BirthHandler & outputbh) {
  json::const_iterator types_it = inputjson.find("types");
  std::vector<BirthHandler::BirthCode> json_to_handler_code;
  if (types_it != inputjson.end() && types_it->is_array()) {
    json_to_handler_code = std::vector<BirthHandler::BirthCode>(types_it->size());
    std::function<bool(json)> cnv = [](json x){ return bool(int(x)); };
    int types_processed = 0;
    for (auto const & a_type : *types_it) {
      if ( a_type.is_array() == false ){
        throw("Invalid birth type");
      }
      BirthType bt = create_birth_type( a_type, cnv);
      json_to_handler_code[types_processed] = outputbh.add_type(bt);
      ++types_processed;
    }
  }
  return json_to_handler_code;
}

std::vector<BirthHandler::BirthCode> txt_read_birthtypes(std::istream& input, int num_input_types, BirthHandler & outputbh) {
  std::vector<std::string> vstrings;
  std::string cur_line;
  int types_processed = 0;
  std::vector<BirthHandler::BirthCode> input_to_handler_code(num_input_types);
  std::function<bool(std::string)> cnv = [](std::string x){return bool(std::stoi(x));};
  while(types_processed < num_input_types){
    if (not getline(input, cur_line)) {
      throw;
    }
    vstrings = txt_process_line(cur_line);
    if (vstrings.size() != 0){
      BirthType bt = create_birth_type( vstrings , cnv );
      input_to_handler_code[types_processed] = outputbh.add_type(bt);
      ++types_processed;
    }
  }
  return input_to_handler_code;
}







template<typename Chain_T>
void QuiverComplex<Chain_T>::read_json(std::istream& input){
  json inputjson;
  input >> inputjson;
  this->clear();
  quiv = json_get_quiver(inputjson);  
  std::vector<BirthHandler::BirthCode> json_to_handler_code = json_read_birthtypes(inputjson, births);

  json::const_iterator cells_it = inputjson.find("cells");
  if (cells_it != inputjson.end() && cells_it->is_array()) {
    int cells_processed = 0;
    for (auto const & json_cell : *cells_it) {
      if ( json_cell.is_object() == false ){
        throw("invalid cell data");
      }
      Chain_T cellbdd;
      for (auto bddx : json_cell["boundary"]) {
        // Z2 coefficients
        cellbdd.set_entry(bddx, 1);
      }
      int json_cell_type = json_cell.at("type");
      BirthHandler::BirthCode cellcode = json_to_handler_code.at(json_cell_type);

      json::const_iterator dim_it = json_cell.find("dim");
      int celldim;
      if (dim_it != json_cell.end()){
        // WARNING conversion of *dim_t to int may be a demotion!
        celldim = int(*dim_it);
      } else {
        celldim = ((cellbdd).size() == 0) ? 0 : (cellbdd).size() - 1;
      }

      CellIndex indx = this->create_cell(celldim, cellbdd, cellcode);
      assert((int)indx == cells_processed);      
      ++cells_processed;
    }
  }
  return;
}

int txt_get_next_number(std::istream& input){
  std::string cur_line;
  int number = -1;
  while(getline(input, cur_line)){
    boost::trim(cur_line);
    if (cur_line != "" && cur_line[0] != '#'){
      number = std::stoi(cur_line);
      break;
    }
  }
  return number;
}



template<typename Chain_T>
void QuiverComplex<Chain_T>::read_txt(std::istream& input){
  if (not input.good()) {
    ;
  }
  this->clear();
  std::string cur_line;
  std::vector<std::string> vstrings;
  // QUIVER PHASE
  while(getline(input, cur_line)){
    vstrings = txt_process_line(cur_line);
    if (vstrings.size() != 0){
      quiv = parse_quiver(vstrings[0], (vstrings.size() > 1) ? std::stoi(vstrings[1]) : -1);
      break;
    }
  }

  // num birthtypes.
  int num_input_types = txt_get_next_number(input);
  // Birthtypes Phase
  std::vector<BirthHandler::BirthCode> input_to_handler_code = txt_read_birthtypes(input, num_input_types, births);

  // num cells.
  int num_input_cells = txt_get_next_number(input);

  int cells_processed = 0;
  while(getline(input, cur_line)){
    vstrings = txt_process_line(cur_line);
    if (vstrings.size() != 0){
      int dim = std::stoi(vstrings[0]);
      int bdd_chain_size = ( (dim == 0) ? 0 : dim+1 );
      if (dim < 0 || vstrings.size() != bdd_chain_size + 2) {
        throw std::runtime_error("read boundary line has insufficient data!");
      }

      Chain_T cellbdd;
      for (unsigned int i = 1; i < vstrings.size() - 1; ++i) {
        // Z2 coefficients
        cellbdd.set_entry(std::stoi(vstrings[i]), 1);
      }

      int txt_cell_type = std::stoi( *vstrings.rbegin() );
      BirthHandler::BirthCode cellcode = input_to_handler_code.at(txt_cell_type);

      CellIndex indx = this->create_cell(dim, cellbdd, cellcode);
      assert((int)indx == cells_processed);
      ++cells_processed;
    }
  }


}


template<typename Chain_T>
bool QuiverComplex<Chain_T>::check_integrity() const {
  // check that the data represents an
  // actual (fits-the-definition) quiver complex.
  if (not quiv.allows_births(births)) {
    std::cerr << "Declared births not allowed by underlying quiver" << std::endl;
    return false;
  }

  int num = 0;
  for (const Cell_T & cell : cells) {
    Chain_T bdd_of_bdd;
    for (auto index : cell.bdd.get_nonzeros()){
      if (not births.less_than_eq(cell.code, cells.at(index).code)){
        std::cerr << "Births of face-coface pair not consistent" << std::endl;
        return false;
      }
      // Z2 coefficients
      bdd_of_bdd += cells.at(index).bdd;
    }
    if (bdd_of_bdd.get_nonzeros().size() != 0){
      std::cerr << "invalid bdd of bdd! cell " << num << std::endl;
      return false;
    }
    ++num;
  }
  
  return true;
}


template<typename Chain_T>
void QuiverComplex<Chain_T>::print_cells(std::ostream& os)const {
  for (const Cell_T & cell : cells) {
    os << "CELL " << cell.index << ", dim: " << cell.dimension
       << births.get_type(cell.code) << ", bdd: " << cell.bdd << "\n";
  }
}


template<typename Chain_T>
QuiverRepn QuiverComplex<Chain_T>::naive_compute_persistence(int target_dimension){
  if (not check_integrity()) {
    throw std::runtime_error("Attempted to compute persistence on quivercomplex with invalid data");
  }
  
  // ASSUMES Z2 Coefficients
  int n = quiv.get_num_vertices();
  SlicedHomology<Chain_T> pm(n);

  // quite bad; data is copied a lot.
  pm.load_cells(cells, births);

  for (int slice = 0; slice < n; ++slice) {
    pm.compute_homology_at_slice(slice, target_dimension);
  }

  std::vector<int> dimv;
  dimv.reserve(quiv.get_num_vertices()); 
  for ( auto slice : pm.sliced_homology_basis ) {
    dimv.push_back(slice.size());
  } 
  
  QuiverRepn pers_mod(quiv);
  pers_mod.set_dimension_vector(dimv);
  for (int source = 0; source < n; ++source) {
    std::set<int> successors = quiv.get_successors(source);
    for (auto target : successors) {
      pers_mod.matrix_at(source,target) = pm.compute_induced_map(source,target);
      // std::cerr << source << "-->" << target
      // << ":\n" << pers_mod.matrix_at(source,target) << std::endl;
    }
  }
  return pers_mod;
}




template<typename Chain_T>
void QuiverComplex<Chain_T>::clear(){
  quiv = Quiver{};
  cells.clear();
  births.clear();
}

template<typename Chain_T>
void QuiverComplex<Chain_T>::set_quiver(const Quiver& quiver){
  quiv = quiver;
}

template<typename Chain_T>
void QuiverComplex<Chain_T>::set_births(const BirthHandler& bh) {
  births = bh;
}

template<typename Chain_T>
typename QuiverComplex<Chain_T>::CellIndex QuiverComplex<Chain_T>::create_cell(int dim, const Chain_T& bdd,
                                                                               const BirthHandler::BirthCode& code){
  CellIndex indx = cells.size();

  Cell_T a_cell;
  a_cell.index = (typename Cell_T::Index)(indx);
  a_cell.dimension = dim;
  if (not births.is_valid_code(code)) {
    throw std::runtime_error("Attempted to create cell with invalid birthcode");
  }
  a_cell.code = code;
  a_cell.bdd = bdd;

  cells.push_back(a_cell);  
  return indx;
}

template<typename Chain_T>
typename QuiverComplex<Chain_T>::CellIndex QuiverComplex<Chain_T>::create_cell(int dim, const Chain_T& bdd, const BirthType& bt){
  CellIndex indx = cells.size();

  Cell_T a_cell;
  // TODO fix (remove) the necessity of this conversion
  // CellIndex probably resolves to unsigned int(?) for size_type.
  // for small enough input this should not be a problem.
  a_cell.index = (typename Cell_T::Index)(indx);
  a_cell.dimension = dim;
  a_cell.code = births.add_type(bt);
  a_cell.bdd = bdd;

  cells.push_back(a_cell);  
  return indx;
}


template struct Cell<gyoza::Algebra::SetChain>;  //

template class QuiverComplex<gyoza::Algebra::SetChain>;
// template std::istream& operator>>(std::istream& input, QuiverComplex<gyoza::Algebra::SetChain> & qc);


// template ladderpersistence::RepnCLfb QuiverComplex<gyoza::Algebra::SetChain>::naive_compute_persistence_as_repn<ladderpersistence::RepnCLfb>(int);

}
}
