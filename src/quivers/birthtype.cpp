#include "gyoza/birthtype.h"

namespace gyoza {
namespace quivers {

bool operator<(const BirthType& lhs, const BirthType& rhs) {
  return (lhs.data < rhs.data);
}

bool operator>(const BirthType& lhs, const BirthType& rhs) {
  return (lhs.data > rhs.data);
}

bool operator==(const BirthType& lhs, const BirthType& rhs) {
  return (lhs.data == rhs.data);
}

bool operator!=(const BirthType& lhs, const BirthType& rhs) {
  return (lhs.data != rhs.data);
}

}
}

std::ostream& operator<<(std::ostream& os, const gyoza::quivers::BirthType& bc){
  os << "("; 
  for (int i = 0; i < bc.size(); ++i){
    os << bc[i] << " ";
  }
  os << ")";
  return os;
}
