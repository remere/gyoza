#include "gyoza/quiver.h"
#include <array>

namespace gyoza {
namespace quivers {


Quiver create_ladder_f_quiver();

Quiver create_ladder_4_quiver(std::array<char, 3> type);
Quiver create_ladder_3_quiver(std::array<char, 2> type);


Quiver create_ladder_quiver(QuiverType type) {
  Quiver ans;
  std::string short_code = Types::ladder_to_short_code(type);
  if (short_code.size() == 2) {
    ans = create_ladder_3_quiver({short_code[0], short_code[1]});
  } else if (short_code.size() == 3) {
    ans = create_ladder_4_quiver({short_code[0], short_code[1], short_code[2]});
  } else if (type == QuiverType::Ladder_F) {
    ans = create_ladder_f_quiver();
  } else {
    throw QuiverTypeMissingImplementationError("Attempted to create ladder on a type not yet supported, or which is not a ladder!");
  }
  ans.type = type;
  return ans;
}

Quiver create_ladder_f_quiver(){
  /*
    2 -> 3
    ^    ^
    0 -> 1
   */
  /* GAP
    3 -> 4
    ^    ^
    1 -> 2
   */
  Quiver ans(4);
  ans.add_edge(0, 1);
  ans.add_edge(0, 2);
  ans.add_edge(1, 3);
  ans.add_edge(2, 3);
  return ans;
}


Quiver create_ladder_3_quiver(std::array<char, 2> type){
  Quiver ans(6);
  ans.add_edge(0, 3);
  ans.add_edge(1, 4);
  ans.add_edge(2, 5);

  for (int i = 0; i < 2; ++i) {
    if (type[i] == 'f') {
      ans.add_edge(i, i+1);
      ans.add_edge(i+3, i+4);
    } else if (type[i] == 'b') {
      ans.add_edge(i+1, i);
      ans.add_edge(i+4, i+3);
    } else {
      throw;
    }
  }
  return ans;
}

Quiver create_ladder_4_quiver(std::array<char, 3> type){
  Quiver ans(8);
  ans.add_edge(0, 4);
  ans.add_edge(1, 5);
  ans.add_edge(2, 6);
  ans.add_edge(3, 7);

  for (int i = 0; i < 3; ++i) {
    if (type[i] == 'f') {
      ans.add_edge(i, i+1);
      ans.add_edge(i+4, i+5);
    } else if (type[i] == 'b') {
      ans.add_edge(i+1, i);
      ans.add_edge(i+5, i+4);
    } else {
      throw;
    }
  }
  return ans;
}


Quiver create_an_quiver(int n){
  // Linear A_n(f...f) quiver.
  // n vertices: 0,1,...,n-1
  // n-1 arrows;
  if (n <= 0){
    throw("attempted to create quiver with invalid number of vertices!");
  }
  Quiver ans(n);
  for (int i = 0; i < n-1; ++i){
    ans.add_edge(i,i+1);
  }
  ans.type = QuiverType::Linear;

  return ans;
}

Quiver create_zigzag_fb_quiver(int n){
  // Zigzag A_n(fb...fb) quiver.
  // n vertices: 0,1,...,n-1
  // n-1 arrows;
  if (n <= 0){
    throw("attempted to create quiver with invalid number of vertices!");
  }
  Quiver ans(n);
  bool is_f = true;
  for (int i = 0; i < n-1; ++i){
    if (is_f) {
      ans.add_edge(i,i+1);
    } else {
      ans.add_edge(i+1,i);
    }
    is_f = not is_f;
  }
  ans.type = QuiverType::Zigzag;

  return ans;
}


Quiver create_zigzag_bf_quiver(int n){
  // Zigzag A_n(bf...bf) quiver.
  // n vertices: 0,1,...,n-1
  // n-1 arrows;
  if (n <= 0){
    throw("attempted to create quiver with invalid number of vertices!");
  }
  Quiver ans(n);
  bool is_f = false;
  for (int i = 0; i < n-1; ++i){
    if (is_f) {
      ans.add_edge(i,i+1);
    } else {
      ans.add_edge(i+1,i);
    }
    is_f = not is_f;
  }
  ans.type = QuiverType::Zigzag;

  return ans;
}


}
}
