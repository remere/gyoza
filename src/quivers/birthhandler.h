#ifndef BIRTHHANDLER_H
#define BIRTHHANDLER_H

#include <boost/optional.hpp>
#include <boost/optional/optional_io.hpp>

#include "gyoza/birthtype.h"

#include <vector>
#include <set>
#include <map>
#include <algorithm>

namespace gyoza {
namespace quivers {


class BirthHandler {
 public:
  typedef unsigned int BirthCode;

  // no non-const iterators should be given!
  typedef std::vector<BirthType>::const_iterator const_iterator;
  typedef std::vector<BirthType>::const_reverse_iterator const_reverse_iterator;

  const_iterator cbegin() const { return data.cbegin(); }
  const_iterator cend() const { return data.cend(); }
  const_reverse_iterator crbegin() const { return data.crbegin(); }
  const_reverse_iterator crend() const { return data.crend(); }

  BirthHandler() = default;

  void clear();
  int size()const;
  BirthCode add_type(const BirthType & bt);
  BirthType get_type(const BirthCode & bc) const;
  boost::optional<BirthCode> get_code(const BirthType & bt) const;
  bool is_valid_code(const BirthCode & code)const;
  std::vector<BirthType> get_birth_types() const;

  // comparing birth types
  bool less_than_eq(const BirthCode& lhs, const BirthCode& rhs)const;


 private:
  std::vector<BirthType> data;
  std::map<BirthType, BirthCode> to_code;
};

}
}



#endif
