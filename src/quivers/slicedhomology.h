#ifndef SLICEDHOMOLOGY_H
#define SLICEDHOMOLOGY_H

#include "gyoza/setchain.h"
#include "gyoza/common_definitions.h"

#include "gyoza/quivercell.h"

#include <tuple>

namespace gyoza{
namespace quivers{

template<typename Chain_T>
struct SlicedHomology {
 private:
  int num_slices;
  
 public:
  using LocalIndex = int;

  typedef Cell<Chain_T> Cell_T;
  typedef typename Cell_T::Index Index;
    
  //**initial data**
  std::vector<std::vector<Chain_T>> sliced_boundary_matrix;
  std::vector<std::vector<Chain_T>> sliced_basis;
  std::vector<std::map<Index,LocalIndex>> sliced_global_to_local;
  std::vector<int> dimensions;
  bool cells_loaded;
  //****************

  //**homology data**
  std::vector<std::set<LocalIndex>> sliced_homology_basis;
  std::vector<std::set<LocalIndex>> sliced_boundary_image_basis;
  std::vector<std::vector<LocalIndex>> sliced_lookup_table;
  std::vector<bool> sliced_homology_computed;
  //*****************

  // "convenience" functions that return references to data at a slice.
  std::vector<Chain_T>& bdd_slice(int n){return sliced_boundary_matrix.at(n);}
  std::vector<Chain_T>& basis_slice(int n){return sliced_basis.at(n);}
  std::map<Index,LocalIndex>& glocal_slice(int n){return sliced_global_to_local.at(n);}
  std::vector<LocalIndex>& lookup_slice(int n){return sliced_lookup_table.at(n);}

  SlicedHomology()=delete;
  SlicedHomology(int n) : num_slices(n), sliced_boundary_matrix(n), sliced_basis(n), sliced_global_to_local(n), sliced_homology_basis(n), sliced_boundary_image_basis(n), sliced_lookup_table(n), sliced_homology_computed(n, false)  {};

  void load_cells(const std::vector<Cell_T> & cells, const BirthHandler & births);
  void compute_homology_at_slice(int slice, int target_dimension);
  Z2Matrix compute_induced_map(int source, int target);


 private:  
  std::tuple<typename SlicedHomology<Chain_T>::Index,
             typename SlicedHomology<Chain_T>::LocalIndex>
  get_pivot_l_value(LocalIndex j, int slice);
};


}
}
#endif
