#include "gyoza/matrix_problem.h"
#include "gyoza/snf_algorithms.hpp"

#include <numeric>
#include <algorithm>

// used only for creating MatrixProblem<Z2Matrix>
#include "gyoza/common_definitions.h"

namespace gyoza {
namespace matrixproblems {

// ********************************************************************************
// Consistency checking
// ********************************************************************************

template<typename Matrix_T>
bool MatrixProblem<Matrix_T>::check_dimensions()const {
  return query_dimensions(nullptr,nullptr);
}

template<typename Matrix_T>
bool MatrixProblem<Matrix_T>::query_dimensions(std::map<int,int>* row_dims, std::map<int,int>* col_dims)const {
  std::map<int, int> row_dimensions;
  std::map<int, int> col_dimensions;

  bool everything_ok = true;
  for (auto const & blk_submatrix_pair : submatrices){
    BlockAddress blk = blk_submatrix_pair.first;
    int row_dim = blk_submatrix_pair.second.rows();
    int col_dim = blk_submatrix_pair.second.cols();

    if ( (row_dimensions.count(blk.row_index) && row_dimensions.at(blk.row_index) != row_dim) ||
         (col_dimensions.count(blk.col_index) && col_dimensions.at(blk.col_index) != col_dim) ) {
      everything_ok = false;
      break;
    }
    row_dimensions[blk.row_index] = row_dim;
    col_dimensions[blk.col_index] = col_dim;
  }
  if (row_dims != nullptr) {
    *row_dims = row_dimensions;
  }
  if (col_dims != nullptr) {
    *col_dims = col_dimensions;
  }
  return everything_ok;
}

template<typename Matrix_T>
int MatrixProblem<Matrix_T>::get_dimension_of_row(int r)const {
  const auto it = get_next_submatrix_at_row(submatrices.begin(), r);
  if (it == submatrices.end()) {
    return 0;
  } else {
    return it->second.rows();
  }
}

template<typename Matrix_T>
int MatrixProblem<Matrix_T>::get_dimension_of_col(int c)const {
  const auto it = get_next_submatrix_at_col(submatrices.begin(), c);
  if (it == submatrices.end()) {
    return 0;
  } else {
    return it->second.cols();
  }
}


template<typename Matrix_T>
bool MatrixProblem<Matrix_T>::check_indices_are_named()const {
  if (submatrices.size() != 0 && ( row_names.size() == 0 || col_names.size() == 0 )) {
    return false;
  }
  std::set<int> row_indices;
  std::set<int> col_indices;
  for (auto const & a_pair : submatrices) {
    row_indices.insert(a_pair.first.row_index);
    col_indices.insert(a_pair.first.col_index);
  }
  bool everything_ok = true;
  for (auto const & x : row_indices) {
    if (!row_names.is_number(x)) {
      everything_ok = false;
      break;
    }
  }
  for (auto const & x : col_indices) {
    if (!col_names.is_number(x)) {
      everything_ok = false;
      break;
    }
  }
  return everything_ok;
}



// ********************************************************************************
// Next in {row,col} iterators
// ********************************************************************************

template<typename MP>
struct RowFinder{
  RowFinder(int _row): row(_row){;}
  int row;
  bool operator()(const typename MP::Submatrices_T::value_type& entry)const{
    return entry.first.row_index == row;
  }
};

template<typename MP>
struct ColFinder{
  ColFinder(int _col): col(_col){;}
  int col;
  bool operator()(const typename MP::Submatrices_T::value_type& entry)const{
    return entry.first.col_index == col;
  }
};

template<typename Matrix_T>
typename MatrixProblem<Matrix_T>::Submatrices_T::iterator
MatrixProblem<Matrix_T>::get_next_submatrix_at_row(typename Submatrices_T::iterator first, int row){
  return std::find_if(first, submatrices.end(), RowFinder<MatrixProblem<Matrix_T>>(row));
}

template<typename Matrix_T>
typename MatrixProblem<Matrix_T>::Submatrices_T::iterator
MatrixProblem<Matrix_T>::get_next_submatrix_at_col(typename Submatrices_T::iterator first, int col){
  return std::find_if(first, submatrices.end(), ColFinder<MatrixProblem<Matrix_T>>(col));
}

template<typename Matrix_T>
typename MatrixProblem<Matrix_T>::Submatrices_T::const_iterator
MatrixProblem<Matrix_T>::get_next_submatrix_at_row(typename Submatrices_T::const_iterator first, int row) const {
  return std::find_if(first, submatrices.cend(), RowFinder<MatrixProblem<Matrix_T>>(row));
}

template<typename Matrix_T>
typename MatrixProblem<Matrix_T>::Submatrices_T::const_iterator
MatrixProblem<Matrix_T>::get_next_submatrix_at_col(typename Submatrices_T::const_iterator first, int col) const {
  return std::find_if(first, submatrices.cend(), ColFinder<MatrixProblem<Matrix_T>>(col));
}
// ********************************************************************************

// ********************************************************************************
// Names
// ********************************************************************************
template<typename Matrix_T>
void MatrixProblem<Matrix_T>::set_row_names(const NameHandler& row_nh) {
  row_names = row_nh;
  return;
}

template<typename Matrix_T>
void MatrixProblem<Matrix_T>::set_col_names(const NameHandler& col_nh) {
  col_names = col_nh;
  return;
}

template<typename Matrix_T>
void MatrixProblem<Matrix_T>::generate_default_names() {
  std::map<int, std::string> row_name_data;
  std::map<int, std::string> col_name_data;
  for (auto const & a_pair : submatrices) {
    int row_number = a_pair.first.row_index;
    row_name_data[row_number] = std::to_string(row_number);

    int col_number = a_pair.first.col_index;
    col_name_data[col_number] = std::to_string(col_number);
  }
  set_row_names(NameHandler(row_name_data));
  set_col_names(NameHandler(col_name_data));
}


// ********************************************************************************
// Permissions
// ********************************************************************************
template<typename Matrix_T>
void MatrixProblem<Matrix_T>::set_permissible_row_operations(const PermissibleOperations & perm) {
  permissible_row_operations = perm;
  return;
}

template<typename Matrix_T>
void MatrixProblem<Matrix_T>::set_permissible_col_operations(const PermissibleOperations & perm) {
  permissible_col_operations = perm;
  return;
}

template<typename Matrix_T>
PermissibleOperations MatrixProblem<Matrix_T>::get_permissible_row_operations() const {
  return permissible_row_operations;
}

template<typename Matrix_T>
PermissibleOperations MatrixProblem<Matrix_T>::get_permissible_col_operations() const {
  return permissible_col_operations;
}

template<typename Matrix_T>
PermissibleOperations& MatrixProblem<Matrix_T>::get_permissible_row_operations(){
  return permissible_row_operations;
}

template<typename Matrix_T>
PermissibleOperations& MatrixProblem<Matrix_T>::get_permissible_col_operations(){
  return permissible_col_operations;
}

template<typename Matrix_T>
bool MatrixProblem<Matrix_T>::check_row_operation(int source_row, int target_row)const {
  return (
      source_row == target_row ? true :
      permissible_row_operations.check_permissibility(source_row, target_row));
}

template<typename Matrix_T>
bool MatrixProblem<Matrix_T>::check_col_operation(int source_col, int target_col)const {
  return (
      source_col == target_col ? true :
      permissible_col_operations.check_permissibility(source_col, target_col));
}
// ********************************************************************************



// ********************************************************************************
// sub{row,col} operations
// ********************************************************************************
template<typename Matrix_T>
void MatrixProblem<Matrix_T>::subrow_swap(SubRow source, SubRow target){
  int source_row = source.row_index;
  int target_row = target.row_index;

  if (source_row != target_row) {
    // cant swap
    throw std::runtime_error("Cannot swap subrows in different rows!"); // fix later
  }

  auto it = get_next_submatrix_at_row(submatrices.begin(), source_row);
  for ( ; it != submatrices.end(); it = get_next_submatrix_at_row(it, source_row)){
    (it->second.row(target.subrow_index)).swap(it->second.row(source.subrow_index));
    ++it;
  }

  return;
}

template<typename Matrix_T>
void MatrixProblem<Matrix_T>::subcol_swap(SubCol source, SubCol target){
  int source_col = source.col_index;
  int target_col = target.col_index;

  if (source_col != target_col) {
    throw std::runtime_error("Cannot swap subcols in different cols!"); // fix later
    // cant swap
  }

  auto it = get_next_submatrix_at_col(submatrices.begin(), source_col);
  for ( ; it != submatrices.end(); it = get_next_submatrix_at_col(it, source_col)){
    (it->second.col(target.subcol_index)).swap(it->second.col(source.subcol_index));
    ++it;
  }

  return;
}


template<typename Matrix_T>
void MatrixProblem<Matrix_T>::subrow_add_multiple(Scalar k, SubRow source, SubRow target){
  if (source == target){
    throw std::runtime_error("Cannot add subrow to itself!");
  }
  int source_row = source.row_index;
  int target_row = target.row_index;

  if (!check_row_operation(source_row, target_row)){
    throw std::runtime_error("Matrix operation impermissible!");
  }

  auto it = get_next_submatrix_at_row(submatrices.begin(), source_row);
  if (source_row == target_row) {
    for ( ; it != submatrices.end(); it = get_next_submatrix_at_row(it, source_row)){
      (it->second.row(target.subrow_index)) += (k * (it->second.row(source.subrow_index)));
      ++it;
    }
  } else {
    for ( ; it != submatrices.end(); it = get_next_submatrix_at_row(it, source_row)){
      BlockAddress target_block(target_row, it->first.col_index);
      if (submatrices.count(target_block)) {
        submatrices[target_block].row(target.subrow_index) +=
            (k * (it->second.row(source.subrow_index)));
      }
      ++it;
    }
  }
  return;
}

template<typename Matrix_T>
void MatrixProblem<Matrix_T>::subcol_add_multiple(Scalar k, SubCol source, SubCol target){
  if (source == target){
    throw std::runtime_error("Cannot add subrow to itself!");
  }
  int source_col = source.col_index;
  int target_col = target.col_index;

  if (!check_col_operation(source_col, target_col)){
    throw std::runtime_error("Matrix operation impermissible!");
  }

  auto it = get_next_submatrix_at_col(submatrices.begin(), source_col);
  if (source_col == target_col) {
    for ( ; it != submatrices.end(); it = get_next_submatrix_at_col(it, source_col)){
      (it->second.col(target.subcol_index)) +=
          (k * (it->second.col(source.subcol_index)));
      ++it;
    }
  } else {
    for ( ; it != submatrices.end(); it = get_next_submatrix_at_col(it, source_col)){
      BlockAddress target_block(it->first.row_index, target_col);
      if (submatrices.count(target_block)) {
        submatrices[target_block].col(target.subcol_index) +=
            (k * (it->second.col(source.subcol_index)));
      }
      ++it;
    }
  }
  return;
}
// end sub{row,col} operations
// ********************************************************************************



// ********************************************************************************
// {row,col} operations
// ********************************************************************************

template<typename Matrix_T>
int MatrixProblem<Matrix_T>::split_row(int row_index, SubIndex rank){
  auto it = get_next_submatrix_at_row(submatrices.begin(), row_index);
  if (it == submatrices.end()) {
    throw std::runtime_error("row is empty, cannot split!");
    return row_index;
  }
  int new_row_number = row_names.get_next_free_number();
  row_names.inherit_name(new_row_number, row_index);  
  permissible_row_operations.inherit_permissibility(new_row_number, row_index);
  permissible_row_operations.make_permissible(new_row_number, row_index);
  permissible_row_operations.make_permissible(row_index, new_row_number);

  Submatrices_T new_submatrices;
  for ( ; it != submatrices.end(); it = get_next_submatrix_at_row(it, row_index)){
    BlockAddress new_block(new_row_number, it->first.col_index);
    // TODO: check for aliasing
    Matrix_T entry = it->second;
    new_submatrices[new_block] = entry.bottomRows(entry.rows() - rank);
    submatrices[it->first] = entry.topRows(rank);
    post_process_block(new_block, it->first);
    ++it;
  }
  submatrices.insert(new_submatrices.begin(), new_submatrices.end());
  return new_row_number;
}

template<typename Matrix_T>
int MatrixProblem<Matrix_T>::split_col(int col_index, SubIndex rank){
  auto it = get_next_submatrix_at_col(submatrices.begin(), col_index);
  if (it == submatrices.end()) {
    throw std::runtime_error("col is empty, cannot split!");    
    return col_index;
  }
  int new_col_number = col_names.get_next_free_number();
  col_names.inherit_name(new_col_number, col_index);
  permissible_col_operations.inherit_permissibility(new_col_number, col_index);
  permissible_col_operations.make_permissible(new_col_number, col_index);
  permissible_col_operations.make_permissible(col_index, new_col_number);

  Submatrices_T new_submatrices;
  for ( ; it != submatrices.end(); it = get_next_submatrix_at_col(it, col_index)){
    BlockAddress new_block(it->first.row_index, new_col_number);
    // TODO: check for aliasing
    Matrix_T entry = it->second;
    new_submatrices[new_block] = entry.rightCols(entry.cols() - rank);
    submatrices[it->first] = entry.leftCols(rank);
    post_process_block(new_block, it->first);
    ++it;
  }
  submatrices.insert(new_submatrices.begin(), new_submatrices.end());
  return new_col_number;
}

template<typename Matrix_T>
void MatrixProblem<Matrix_T>::post_process_block(const BlockAddress& new_ba,
                                                 const BlockAddress& old_ba) {
  (void)new_ba;
  (void)old_ba;
  return; 
}






template<typename Matrix_T>
void MatrixProblem<Matrix_T>::left_multiply_row(int row_index, const Matrix_T & multiplier){
  if (multiplier.rows() != multiplier.cols()) {
    throw("attempted to multiply by nonsquare change of basis!");
  }
  auto it = get_next_submatrix_at_row(submatrices.begin(), row_index);
  for ( ; it != submatrices.end(); it = get_next_submatrix_at_row(it, row_index)){
    (it->second) = multiplier * (it->second);
    ++it;
  }
}

template<typename Matrix_T>
void MatrixProblem<Matrix_T>::right_multiply_col(int col_index, const Matrix_T & multiplier){
  if (multiplier.rows() != multiplier.cols()) {
    throw("attempted to multiply by nonsquare change of basis!");
  }
  auto it = get_next_submatrix_at_col(submatrices.begin(), col_index);
  for ( ; it != submatrices.end(); it = get_next_submatrix_at_col(it, col_index)){
    (it->second) = (it->second) * multiplier;
    ++it;
  }
}


template<typename Matrix_T>
void MatrixProblem<Matrix_T>::add_left_multiple_row(const Matrix_T & multiplier, int source, int target) {
  if (source == target){
    throw("Attempted to add row to itself");
  }
  if (!check_row_operation(source, target)){
    throw std::runtime_error("Matrix operation impermissible!");
  }
  auto it = get_next_submatrix_at_row(submatrices.begin(), source);
  for ( ; it != submatrices.end(); it = get_next_submatrix_at_row(it, source)){
    BlockAddress blk(target, it->first.col_index);
    auto jt = submatrices.find(blk);
    if (jt != submatrices.end()) {
      jt->second += multiplier * (it->second);
    }
    // YES, we do NOT do the following.
    // non-defined blocks are considered "strongly zero" and aren't supposed to be changed by row/col operations
    // else {
    //   submatrices[blk] = multiplier * (it->second);
    // }
    ++it;
  }
}


template<typename Matrix_T>
void MatrixProblem<Matrix_T>::add_right_multiple_col(const Matrix_T & multiplier, int source, int target) {
  if (source == target){
    throw("Attempted to add row to itself");
  }
  if (!check_col_operation(source, target)){
    throw std::runtime_error("Matrix operation impermissible!");
  }
  auto it = get_next_submatrix_at_col(submatrices.begin(), source);
  for ( ; it != submatrices.end(); it = get_next_submatrix_at_col(it, source)){
    BlockAddress blk(it->first.row_index, target);
    auto jt = submatrices.find(blk);
    if (jt != submatrices.end()) {
      jt->second += (it->second) * multiplier;
    }
    // YES, we do NOT do the following.
    // non-defined blocks are considered "strongly zero" and aren't supposed to be changed by row/col operations
    // else {
    //   submatrices[blk] = (it->second) * multiplier;
    // }
    ++it;
  }
}


template<typename Matrix_T>
typename MatrixProblem<Matrix_T>::SubIndex
MatrixProblem<Matrix_T>::reduce_block(const BlockAddress& ba,
                                      algorithms::FullPivotSNF<Matrix_T>& snf) {
  OuterIndex row_index = ba.row_index;
  OuterIndex col_index = ba.col_index;

  auto it = submatrices.find(ba);
  if (it == submatrices.end()) {
    throw("Can't reduce nonexisting block!");
  }

  snf.compute(it->second);
  left_multiply_row( row_index, snf.get_P() );
  right_multiply_col( col_index, snf.get_Q() );
  SubIndex rank = snf.rank();

  return rank;
}


// ********************************************************************************
// informational
// ********************************************************************************

template<typename Matrix_T>
std::vector<BlockAddress> MatrixProblem<Matrix_T>::non_strongly_zero_entries() const {
  std::vector<BlockAddress> ans;
  ans.reserve(submatrices.size());
  for (const auto & entry : submatrices) {
    ans.push_back(entry.first);
  }
  return ans;
}


template<typename Matrix_T>
bool MatrixProblem<Matrix_T>::operator==(const MatrixProblem<Matrix_T>& other)const{
  return ((submatrices == other.submatrices) && (permissible_row_operations == other.permissible_row_operations) && (permissible_col_operations == other.permissible_col_operations) && (row_names == other.row_names) && (col_names == other.col_names));
}



template<typename Matrix_T>
void MatrixProblem<Matrix_T>::pretty_print(std::ostream& os)const{

  std::map<int,int> row_dims;
  std::map<int,int> col_dims;

  if ( !query_dimensions(&row_dims, &col_dims) ) {
    throw("Submatrices are improperly sized!");
  }

  int row_total = std::accumulate(row_dims.begin(), row_dims.end(), 0,
                                  [](const int cumulant, const std::pair<int,int>& a_pair){
                                    return cumulant + a_pair.second;});

  int col_total = std::accumulate(col_dims.begin(), col_dims.end(), 0,
                                  [](const int cumulant, const std::pair<int,int>& a_pair){
                                    return cumulant + a_pair.second;});


  Matrix_T res(row_total, col_total);

  int cumulant_subrow = 0;
  int cumulant_subcol = 0;
  for ( auto const & row_pair : row_dims ) {
    for ( auto const & col_pair : col_dims ) {
      BlockAddress blk(row_pair.first, col_pair.first);
      if ( submatrices.count(blk) == 1) {
        res.block(cumulant_subrow, cumulant_subcol,
                  row_pair.second, col_pair.second)
                  << submatrices.at(blk);
      }else {
        res.block(cumulant_subrow, cumulant_subcol,
                  row_pair.second, col_pair.second)
            << Matrix_T::Zero(row_pair.second, col_pair.second);
      }
      cumulant_subcol += col_pair.second;
    }
    cumulant_subcol = 0;
    cumulant_subrow += row_pair.second;
  }
  os << res << "\n";;
  return;
}



// ********************************************************************************



template class MatrixProblem<Z2Matrix>;
template struct SubRow_Address<Z2Matrix>;
template struct SubCol_Address<Z2Matrix>;

}
}
