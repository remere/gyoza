#include "catch.hpp"


#include "gyoza/matrix_problem.h"
#include "gyoza/common_definitions.h"

namespace mp = gyoza::matrixproblems;
using Z2Matrix = gyoza::Z2Matrix;
using BA = gyoza::matrixproblems::BlockAddress;
using MP =  gyoza::matrixproblems::MatrixProblem<Z2Matrix>;



TEST_CASE( "Trivial checks", "[matrixproblem]") {
  // On the empty object..
  mp::MatrixProblem<Z2Matrix> mp;

  CHECK( mp.check_indices_are_named() );
  CHECK( mp.check_dimensions() );

  // operations internal to a single row or column block
  // is always permissible.
  CHECK( mp.check_row_operation(1,1) );
  CHECK( mp.check_col_operation(1,1) );

  CHECK( mp.check_row_operation(1000,1000) );
  CHECK( mp.check_col_operation(1000,1000) );

  // the above permissions don't have to be explicitly stored.
  // empty object gives back empy objects
  CHECK( mp.get_permissible_row_operations() == mp::PermissibleOperations{} );
  CHECK( mp.get_permissible_col_operations() == mp::PermissibleOperations{} );

  CHECK( mp.get_row_names() == mp::NameHandler{} );
  CHECK( mp.get_col_names() == mp::NameHandler{} );

}


TEST_CASE( "Access", "[matrixproblem]") {
  mp::MatrixProblem<Z2Matrix> mp;

  mp[ BA(1,1) ] = Z2Matrix::Identity(2,3);
  CHECK(mp.at(BA(1,1)) == Z2Matrix::Identity(2,3));
  CHECK(mp.non_strongly_zero_entries() == std::vector<BA>(1,BA(1,1)));

  CHECK(1 == mp.erase(BA(1,1)));
  CHECK_THROWS(mp.at(BA(1,1)));
  CHECK(0 == mp.count(BA(1,1)));
  CHECK(mp.non_strongly_zero_entries() == std::vector<BA>{});
}

TEST_CASE( "Dimension checking", "[matrixproblem]") {
  mp::MatrixProblem<Z2Matrix> mp;

  mp[ BA(1,1) ] = Z2Matrix::Zero(2,3);
  CHECK( mp.check_dimensions() );

  mp[ BA(1,4) ] = Z2Matrix::Zero(1,1);
  CHECK_FALSE( mp.check_dimensions() );

  mp[ BA(1,4) ] = Z2Matrix::Zero(2,1);
  CHECK( mp.check_dimensions() );

  mp[ BA(2,2) ] = Z2Matrix::Zero(4,5);
  CHECK( mp.check_dimensions() );

  mp[ BA(3,2) ] = Z2Matrix::Zero(3,5);
  CHECK( mp.check_dimensions() );

  mp[ BA(3,1) ] = Z2Matrix::Zero(3,3);
  CHECK( mp.check_dimensions() );

  /* Matrix is:
      3   5   0   1
    2 *           *
    4     *
    3 *   *
  */

  CHECK(mp.non_strongly_zero_entries().size() == 5);
  
}





TEST_CASE( "A2f Matrix Problem",  "[matrixproblem]") {
  mp::MatrixProblem<Z2Matrix> mp;

  mp::PermissibleOperations row_prm;
  row_prm.make_permissible(1, {1,2});
  row_prm.make_permissible(2, {2,3});
  row_prm.make_permissible(3, 3);

  mp::PermissibleOperations col_prm = row_prm.get_dual();

  mp.set_permissible_row_operations(row_prm);
  mp.set_permissible_col_operations(col_prm);

  SECTION( "check permissions" ){
    CHECK( mp.get_permissible_col_operations() == col_prm );
    CHECK( mp.get_permissible_row_operations() == row_prm );
  }

  SECTION( "check names" ){
    // no matrices, automatically succeeds
    // vacuously true.
    CHECK( mp.check_indices_are_named() );

    mp[BA(1,1)] = Z2Matrix::Zero(2,3);
    CHECK_FALSE( mp.check_indices_are_named() );

    mp.generate_default_names();
    CHECK( mp.check_indices_are_named() );

    mp::NameHandler expected({{1,"1"}});
    CHECK( mp.get_row_names() == expected );
    CHECK( mp.get_col_names() == expected );

    mp::NameHandler actual_names({{1,"[2,2]"}, {2,"[1,2]"}, {3,"[1,1]"}});
    mp.set_row_names(actual_names);
    mp.set_col_names(actual_names);

    CHECK( mp.check_indices_are_named() );
    CHECK( mp.get_row_names() == actual_names );
    CHECK( mp.get_col_names() == actual_names );
  }
}
