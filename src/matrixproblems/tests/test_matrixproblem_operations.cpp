#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "gyoza/snf_algorithms.hpp"
#include "gyoza/matrix_problem.h"
#include "gyoza/common_definitions.h"


namespace mp = gyoza::matrixproblems;
using Z2Matrix = gyoza::Z2Matrix;
using BA = gyoza::matrixproblems::BlockAddress;
using MP =  gyoza::matrixproblems::MatrixProblem<Z2Matrix>;

// ******************** sub{row,col} operations ********************
TEST_CASE("sub{row,col} swap", "[matrixproblem]"){
  MP mp;

  Z2Matrix m1 = Z2Matrix::Identity(5,3);
  Z2Matrix m2 = Z2Matrix::Identity(5,4);
  mp[ BA(1,1) ] = m1;
  mp[ BA(1,2) ] = m2;
  Z2Matrix m3 = Z2Matrix::Identity(3,3);
  Z2Matrix m4 = Z2Matrix::Identity(3,4);
  mp[ BA(2,1) ] = m3;
  mp[ BA(2,2) ] = m4;

  SECTION("dimensions ok") {
    REQUIRE(mp.check_dimensions());
  }

  SECTION("identity operations") {
    const MP expected = mp;
    MP::SubRow rr = {1, 1};
    MP::SubCol cc = {2, 0};

    CHECK_NOTHROW(mp.subrow_swap(rr,rr));
    CHECK(mp == expected);
    CHECK_NOTHROW(mp.subcol_swap(cc,cc));
    CHECK(mp == expected);

    MP::SubRow outofrange_row = {10, 100};
    MP::SubCol outofrange_col = {10, 100};
    CHECK_NOTHROW(mp.subrow_swap(outofrange_row,outofrange_row));
    CHECK(mp == expected);
    CHECK_NOTHROW(mp.subcol_swap(outofrange_col,outofrange_col));
    CHECK(mp == expected);
  }

  SECTION("invalid row swaps") {
    MP::SubRow rr1 = {1, 1};
    MP::SubRow rr2 = {2, 1};
    CHECK_THROWS(mp.subrow_swap(rr1,rr2));
  }

  SECTION("invalid col swaps") {
    MP::SubCol rr1 = {1, 1};
    MP::SubCol rr2 = {2, 1};
    CHECK_THROWS(mp.subcol_swap(rr1,rr2));
  }

  SECTION("row swap") {
    MP::SubRow src = {1, 1};
    MP::SubRow trgt = {1, 2};
    mp.subrow_swap(src, trgt);

    // swap independently, get expecteds.
    m1.row(1).swap(m1.row(2));
    m2.row(1).swap(m2.row(2));

    CHECK( mp.at(BA(1,1)) == m1 );
    CHECK( mp.at(BA(1,2)) == m2 );

    // row two should not be affected!
    CHECK( mp.at(BA(2,1)) == m3 );
    CHECK( mp.at(BA(2,2)) == m4 );
  }

  SECTION("col swap") {
    MP::SubCol src = {2, 0};
    MP::SubCol trgt = {2, 2};
    mp.subcol_swap(src, trgt);

    // swap independently, get expecteds.
    m2.col(0).swap(m2.col(2));
    m4.col(0).swap(m4.col(2));

    CHECK( mp.at(BA(1,2)) == m2 );
    CHECK( mp.at(BA(2,2)) == m4 );

    // explicit definition of results!
    Z2Matrix m2expected(5,4);
    m2expected <<
        0,0,1,0,
        0,1,0,0,
        1,0,0,0,
        0,0,0,1,
        0,0,0,0;
    CHECK( mp.at(BA(1,2)) == m2expected );
    Z2Matrix m4expected(3,4);
    m4expected <<
        0,0,1,0,
        0,1,0,0,
        1,0,0,0;
    CHECK( mp.at(BA(2,2)) == m4expected );

    // col one should not be affected!
    CHECK( mp.at(BA(1,1)) == m1 );
    CHECK( mp.at(BA(2,1)) == m3 );
  }
}

TEST_CASE("sub{row,col} add multiple - within block", "[matrixproblem]"){
  MP mp;
  Z2Matrix m1 = Z2Matrix::Constant(5,3,1);
  Z2Matrix m2 = Z2Matrix::Constant(5,4,1);
  mp[ BA(1,1) ] = m1;
  mp[ BA(1,2) ] = m2;
  Z2Matrix m3 = Z2Matrix::Constant(3,3,1);
  Z2Matrix m4 = Z2Matrix::Constant(3,4,1);
  mp[ BA(2,1) ] = m3;
  mp[ BA(2,2) ] = m4;

  SECTION("dimensions ok"){
    REQUIRE(mp.check_dimensions());
  }

  SECTION("row add multiple") {
    MP::SubRow src = {1, 1};
    MP::SubRow trgt = {1, 2};
    mp.subrow_add_multiple(1, src, trgt);

    // add independently, get expecteds.
    m1.row(2) += m1.row(1);
    m2.row(2) += m2.row(1);
    CHECK(mp.at(BA(1,1)) == m1);
    CHECK(mp.at(BA(1,2)) == m2);

    // explicitly,
    Z2Matrix m1expected(5,3);
    m1expected <<
        1,1,1,
        1,1,1,
        0,0,0,
        1,1,1,
        1,1,1;
    CHECK( mp.at(BA(1,1)) == m1expected );
    Z2Matrix m2expected(5,4);
    m2expected <<
        1,1,1,1,
        1,1,1,1,
        0,0,0,0,
        1,1,1,1,
        1,1,1,1;
    CHECK( mp.at(BA(1,2)) == m2expected );

    // row two should not be affected!
    CHECK(mp.at(BA(2,1)) == m3);
    CHECK(mp.at(BA(2,2)) == m4);
  }

  SECTION("col add multiple") {
    MP::SubCol src = {2, 0};
    MP::SubCol trgt = {2, 2};
    mp.subcol_add_multiple(1,src, trgt);

    // add independently, get expecteds.
    m2.col(2) += m2.col(0);
    m4.col(2) += m4.col(0);

    CHECK( mp.at(BA(1,2)) == m2 );
    CHECK( mp.at(BA(2,2)) == m4 );

    // col one should not be affected!
    CHECK( mp.at(BA(1,1)) == m1 );
    CHECK( mp.at(BA(2,1)) == m3 );
  }
}


TEST_CASE("sub{row,col} add multiple - across blocks", "[matrixproblem]"){
  MP mp;
  Z2Matrix m1 = Z2Matrix::Constant(5,3,1);
  Z2Matrix m2 = Z2Matrix::Constant(5,4,1);
  mp[ BA(1,1) ] = m1;
  mp[ BA(1,2) ] = m2;
  Z2Matrix m3 = Z2Matrix::Constant(3,3,1);
  Z2Matrix m4 = Z2Matrix::Constant(3,4,1);
  mp[ BA(2,1) ] = m3;
  mp[ BA(2,2) ] = m4;

  SECTION("impermissible row add multiple") {
    MP::SubRow src = {2, 1};
    MP::SubRow trgt = {1, 2};
    CHECK_THROWS(mp.subrow_add_multiple(1, src, trgt));
  }

  SECTION("row add multiple") {
    MP::SubRow src = {2, 1};
    MP::SubRow trgt = {1, 2};
    // add from row 2 to row 1
    mp.get_permissible_row_operations().make_permissible(2,1);
    mp.subrow_add_multiple(1, src, trgt);

    // explicitly,
    Z2Matrix m1expected(5,3);
    m1expected <<
        1,1,1,
        1,1,1,
        0,0,0,
        1,1,1,
        1,1,1;
    CHECK( mp.at(BA(1,1)) == m1expected );
    Z2Matrix m2expected(5,4);
    m2expected <<
        1,1,1,1,
        1,1,1,1,
        0,0,0,0,
        1,1,1,1,
        1,1,1,1;
    CHECK( mp.at(BA(1,2)) == m2expected );

    // row two should not be affected!
    CHECK(mp.at(BA(2,1)) == m3);
    CHECK(mp.at(BA(2,2)) == m4);
  }


  SECTION("impermissible col add multiple") {
    MP::SubCol src = {1, 0};
    MP::SubCol trgt = {2, 2};
    CHECK_THROWS(mp.subcol_add_multiple(1,src, trgt));
  }

  SECTION("col add multiple") {
    MP::SubCol src = {1, 0};
    MP::SubCol trgt = {2, 2};
    mp.get_permissible_col_operations().make_permissible(1,2);
    mp.subcol_add_multiple(1,src, trgt);

    // add independently, get expecteds.
    m2.col(2) += m1.col(0);
    m4.col(2) += m3.col(0);

    CHECK( mp.at(BA(1,2)) == m2 );
    CHECK( mp.at(BA(2,2)) == m4 );

    // col one should not be affected!
    CHECK( mp.at(BA(1,1)) == m1 );
    CHECK( mp.at(BA(2,1)) == m3 );
  }

  SECTION("invalid self-adds") {
    MP::SubRow rr = {2, 1};
    MP::SubCol cc = {2, 1};

    CHECK_THROWS(mp.subrow_add_multiple(1,rr,rr));
    CHECK_THROWS(mp.subcol_add_multiple(1,cc,cc));
  }

  SECTION("identity: out of range adds") {
    SECTION("out of range row adds") {
      mp.get_permissible_row_operations().make_permissible(20,10);
      const MP expected = mp;
      MP::SubRow r20 = {20, 1000};
      MP::SubRow r10 = {10, 666};
      mp.subrow_add_multiple(1, r20, r10);
      CHECK(expected == mp);
    }
    SECTION("out of range col adds") {
      mp.get_permissible_col_operations().make_permissible(20,10);
      const MP expected = mp;
      MP::SubCol r20 = {20, 1000};
      MP::SubCol r10 = {10, 666};
      mp.subcol_add_multiple(1, r20, r10);
      CHECK(expected == mp);
    }
  }

  SECTION("identity: add zero multiples") {
    SECTION("row add zero multiple") {
      mp.get_permissible_row_operations().make_permissible(2,1);
      const MP expected = mp;
      MP::SubRow src = {2, 1};
      MP::SubRow trgt = {1, 2};
      mp.subrow_add_multiple(0, src, trgt);
      CHECK(expected == mp);
    }
    SECTION("col add zero multiple") {
      mp.get_permissible_col_operations().make_permissible(2,1);
      const MP expected = mp;
      MP::SubCol src = {2, 1};
      MP::SubCol trgt = {1, 2};
      mp.subcol_add_multiple(0, src, trgt);
      CHECK(expected == mp);
    }
  }
}


// ******************** {row,col} operations ********************
TEST_CASE( "split {row,col}", "[matrixproblem]" ) {
  MP mp;
  Z2Matrix m1 = Z2Matrix::Constant(5,3,1);
  Z2Matrix m2 = Z2Matrix::Constant(5,4,1);
  Z2Matrix m3 = Z2Matrix::Constant(3,3,1);
  mp[ BA(1,1) ] = m1;
  mp[ BA(1,2) ] = m2;
  mp[ BA(2,1) ] = m3;

  mp.generate_default_names();
  const mp::NameHandler col_names = mp.get_col_names();
  const mp::NameHandler row_names = mp.get_row_names();

  SECTION ("row splits"){
    int new_row = mp.split_row(1, 3);

    Z2Matrix m1expected = Z2Matrix::Constant(3,3,1);
    Z2Matrix m2expected = Z2Matrix::Constant(3,4,1);
    CHECK(mp.at(BA(1,1)) == m1expected);
    CHECK(mp.at(BA(1,2)) == m2expected);
    CHECK(mp.at(BA(2,1)) == m3);
    CHECK(mp.at(BA(new_row,1)) == Z2Matrix::Constant(2,3,1));
    CHECK(mp.at(BA(new_row,2)) == Z2Matrix::Constant(2,4,1));
    CHECK(mp.size() == 5);

    // effect on names:
    mp::NameHandler expected_names({{1,"1"}, {2,"2"}, {new_row,"1"}});
    CHECK(mp.get_row_names() == expected_names);
    CHECK(mp.get_col_names() == col_names);
  }

  SECTION ("splits - empty row"){
    CHECK_THROWS(mp.split_row(10,100));
    CHECK_THROWS(mp.split_col(10,100));
  }

  SECTION ("row splits - too large rank"){
    // (mp.split_row(1, 10));
    // cases:
    // 1. Eigen assertions enabled <=> default if EIGEN_NO_DEBUG undefined
    //   result: fails eigen assertion
    // 2. Eigen assertions disable <=> EIGEN_NO_DEBUG defined
    //                             <=  NDEBUG define
    //   result: mysterious exception: unexpected exception with message: std::bad_alloc

    // TODO: figure out how to do this intelligently
    // * rely on eigen debug checking, or code guards for bad input to split.
  }


  SECTION ("col splits") {
    int new_col = mp.split_col(1, 2);

    Z2Matrix m1expected = Z2Matrix::Constant(5,2,1);
    Z2Matrix m3expected = Z2Matrix::Constant(3,2,1);
    CHECK(mp.at(BA(1,1)) == m1expected);
    CHECK(mp.at(BA(1,2)) == m2);
    CHECK(mp.at(BA(2,1)) == m3expected);
    CHECK(mp.at(BA(1,new_col)) == Z2Matrix::Constant(5,1,1));
    CHECK(mp.at(BA(2,new_col)) == Z2Matrix::Constant(3,1,1));
    CHECK( mp.size() == 5 );


    // effect on names:
    mp::NameHandler expected_names({{1,"1"}, {2,"2"}, {new_col, "1"}});
    CHECK(mp.get_col_names() == expected_names);
    CHECK(mp.get_row_names() == row_names);
  }
}

TEST_CASE( "multiply {row,col} operations", "[matrixproblem]" ) {
  MP mp;
  const int N = 5;
  for (int i = 0; i < N; ++i) {
    mp[ BA(0,i+1) ] = Z2Matrix::Random(6,2*i+1);
    mp[ BA(i+1,0) ] = Z2Matrix::Random(2*i+1,7);
  }
  const MP original = mp;
  const std::vector<BA> expected_ba = mp.non_strongly_zero_entries();

  SECTION("dimensions ok") {
    REQUIRE(mp.check_dimensions());
  }

  SECTION( "row operations, identity" ){
    mp.left_multiply_row(0, Z2Matrix::Identity(6,6));
    CHECK(original == mp);
  }

  SECTION( "col operations, identity" ){
    mp.right_multiply_col(0, Z2Matrix::Identity(7,7));
    CHECK(original == mp);
  }


  SECTION( "row operations, random" ){
    Z2Matrix Pmultiplier = Z2Matrix::Random(6,6);
    mp.left_multiply_row(0, Pmultiplier);
    for (int i = 0; i < N; ++i) {
      CHECK(mp.at(BA(0,i+1)) == Pmultiplier * original.at(BA(0,i+1)));
      CHECK(mp.at(BA(i+1,0)) == original.at(BA(i+1,0)));
    }
    CHECK(mp.non_strongly_zero_entries() == expected_ba);
  }

  SECTION( "col operations, random" ){
    Z2Matrix Qmultiplier = Z2Matrix::Random(7,7);
    mp.right_multiply_col(0, Qmultiplier);
    for (int i = 0; i < N; ++i) {
      CHECK(mp.at(BA(i+1,0)) == original.at(BA(i+1,0)) * Qmultiplier);
      CHECK(mp.at(BA(0,i+1)) == original.at(BA(0,i+1)));
    }
    CHECK(mp.non_strongly_zero_entries() == expected_ba);
  }
}

TEST_CASE( "add multiple {row,col} operations", "[matrixproblem]" ) {
  MP mp;
  SECTION( "row add operations, identity" ){
    // identity
    std::vector<Z2Matrix> r1;
    std::vector<Z2Matrix> r2;
    for (int i = 0; i < 5; ++i) {
      r1.push_back(Z2Matrix::Random(6, 2*i+1));
      r2.push_back(Z2Matrix::Random(6, 2*i+1));
      mp[ BA(1,i) ] = r1.at(i);
      mp[ BA(2,i) ] = r2.at(i);
    }
    mp.get_permissible_row_operations().make_permissible(1, 2);
    mp.add_left_multiple_row(Z2Matrix::Identity(6,6), 1,2);
    for (int i = 0; i < 5; ++i) {
      CHECK( mp.at(BA(2,i)) == r1.at(i) + r2.at(i) );
      CHECK( mp.at(BA(1,i)) == r1.at(i) );
    }
  }

  SECTION( "row add operations, with multiplier" ){
    // identity
    std::vector<Z2Matrix> r1;
    std::vector<Z2Matrix> r2;
    Z2Matrix Pmultiplier = Z2Matrix::Random(7,6);
    for (int i = 0; i < 5; ++i) {
      r1.push_back(Z2Matrix::Random(6, 2*i+1));
      r2.push_back(Z2Matrix::Random(7, 2*i+1));
      mp[ BA(1,i) ] = r1.at(i);
      mp[ BA(2,i) ] = r2.at(i);
    }
    mp.get_permissible_row_operations().make_permissible(1, 2);
    mp.add_left_multiple_row(Pmultiplier, 1,2);
    for (int i = 0; i < 5; ++i) {
      CHECK( mp.at(BA(2,i)) == (Pmultiplier * r1.at(i)) + r2.at(i) );
      CHECK( mp.at(BA(1,i)) == r1.at(i) );
    }
  }

   SECTION( "row col operations, with multiplier" ){
    // identity
    std::vector<Z2Matrix> c1;
    std::vector<Z2Matrix> c2;
    Z2Matrix Pmultiplier = Z2Matrix::Random(6,7);
    for (int i = 0; i < 5; ++i) {
      c1.push_back(Z2Matrix::Random(2*i+1, 6));
      c2.push_back(Z2Matrix::Random(2*i+1, 7));
      mp[ BA(i,1) ] = c1.at(i);
      mp[ BA(i,2) ] = c2.at(i);
    }
    mp.get_permissible_col_operations().make_permissible(1, 2);
    mp.add_right_multiple_col(Pmultiplier, 1,2);
    for (int i = 0; i < 5; ++i) {
      CHECK( mp.at(BA(i,2)) == (c1.at(i) * Pmultiplier) + c2.at(i) );
      CHECK( mp.at(BA(i,1)) == c1.at(i) );
    }
  }

}


// ******************** operations and strongly zero  ********************
TEST_CASE( "strongly zero invariance", "[matrixproblem]" ) {
  MP mp;
  Z2Matrix m1 = Z2Matrix::Constant(5,3,1);
  mp[ BA(1,1) ] = m1;
  Z2Matrix m3 = Z2Matrix::Constant(3,3,1);
  mp[ BA(2,1) ] = m3;
  Z2Matrix m4 = Z2Matrix::Constant(3,4,1);
  mp[ BA(2,2) ] = m4;

  mp.get_permissible_row_operations().make_permissible(2,1);
  mp.get_permissible_col_operations().make_permissible(1,2);

  const std::vector<BA> expected_ba = mp.non_strongly_zero_entries();
  const MP original = mp;

  /*
    [ * Z ]
    [ * * ]
    where Z at BA(1,2) is strongly zero
   */

  SECTION("sub swap operations") {
    SECTION("row swap") {
      MP::SubRow src = {1, 1};
      MP::SubRow trgt = {1, 2};
      mp.subrow_swap(src, trgt);
      CHECK(mp.non_strongly_zero_entries() == expected_ba);
    }
    SECTION("col swap") {
      MP::SubCol src = {2, 0};
      MP::SubCol trgt = {2, 2};
      mp.subcol_swap(src, trgt);
      CHECK(mp.non_strongly_zero_entries() == expected_ba);
    }
  }


  SECTION("subrow add multiple operations") {
    MP::SubRow src = {2, 1};
    MP::SubRow trgt = {1, 2};

    mp.subrow_add_multiple(1, src, trgt);

    CHECK(!(original == mp));
    CHECK(mp.non_strongly_zero_entries() == expected_ba);

    Z2Matrix m1expected(5,3);
    m1expected <<
        1,1,1,
        1,1,1,
        0,0,0,
        1,1,1,
        1,1,1;
    CHECK(mp.at(BA(1,1)) == m1expected);
    CHECK(mp.at(BA(2,1)) == m3);
    CHECK(mp.at(BA(2,2)) == m4);
  }

  SECTION("subcol add multiple operations") {
    MP::SubCol src = {1, 0};
    MP::SubCol trgt = {2, 2};

    mp.subcol_add_multiple(1, src, trgt);

    CHECK(!(original == mp));
    CHECK(mp.non_strongly_zero_entries() == expected_ba);

    Z2Matrix m4expected(3,4);
    m4expected <<
        1,1,0,1,
        1,1,0,1,
        1,1,0,1;

    CHECK(mp.at(BA(1,1)) == m1);
    CHECK(mp.at(BA(2,1)) == m3);
    CHECK(mp.at(BA(2,2)) == m4expected);
  }


  SECTION("row add multiple operations") {
    mp.add_left_multiple_row(Z2Matrix::Identity(5,3), 2, 1);

    CHECK(!(original == mp));
    CHECK(mp.non_strongly_zero_entries() == expected_ba);

    Z2Matrix m1expected(5,3);
    m1expected <<
        0,0,0,
        0,0,0,
        0,0,0,
        1,1,1,
        1,1,1;
    CHECK(mp.at(BA(1,1)) == m1expected);
    CHECK(mp.at(BA(2,1)) == m3);
    CHECK(mp.at(BA(2,2)) == m4);
  }

  SECTION("col add multiple operations") {
    mp.add_right_multiple_col(Z2Matrix::Identity(3,4), 1, 2);

    CHECK(!(original == mp));
    CHECK(mp.non_strongly_zero_entries() == expected_ba);

    Z2Matrix m4expected(3,4);
    m4expected <<
        0,0,0,1,
        0,0,0,1,
        0,0,0,1;

    CHECK(mp.at(BA(1,1)) == m1);
    CHECK(mp.at(BA(2,1)) == m3);
    CHECK(mp.at(BA(2,2)) == m4expected);
  }


}



// ******************** Reduction operation ********************
TEST_CASE( "reduce block", "[matrixproblem]" ) {
  MP mp;

  Z2Matrix m1 = Z2Matrix::Constant(5,3,1);
  m1.row(0).head(2).setZero();
  // m1 is rank 2.

  gyoza::algorithms::FullPivotSNF<Z2Matrix> sf(m1);

  Z2Matrix right = Z2Matrix::Identity(5,5);
  Z2Matrix down = Z2Matrix::Identity(3,3);

  mp[BA(1,1)] = m1;
  mp[BA(1,2)] = right;
  mp[BA(2,1)] = down;

  CHECK( mp.reduce_block( BA(1,1) ) == 2 );
  CHECK( mp.at(BA(1,2)) * m1 * mp.at(BA(2,1)) == mp.at(BA(1,1)) );

  CHECK(mp[BA(1,1)] == sf.get_snf());
  CHECK(mp[BA(1,2)] == sf.get_P());
  CHECK(mp[BA(2,1)] == sf.get_Q());
}
