#include "catch.hpp"


#include "gyoza/matrix_problem.h"
#include "gyoza/common_definitions.h"

using Z2Matrix = gyoza::Z2Matrix;

typedef gyoza::matrixproblems::BlockAddress BA;
typedef gyoza::matrixproblems::SubRow_Address<Z2Matrix> SR;
typedef gyoza::matrixproblems::SubCol_Address<Z2Matrix> SC;



TEST_CASE("construction of addresses", "[matrixproblem]") {
  SR src = {1, 2};
  CHECK(src.row_index == 1);
  CHECK(src.subrow_index == 2);

  SC col = {2,3};
  CHECK(col.col_index == 2);
  CHECK(col.subcol_index == 3);


  BA ba{2,3};
  CHECK(ba.row_index == 2);
  CHECK(ba.col_index == 3); 
}

TEST_CASE("operators on addresses", "[matrixproblem]") {
  BA ba1{2,3};
  BA ba2 = ba1;
  BA ba3{3,2};

  CHECK(ba1 == ba1);
  CHECK(ba1 == ba2);
  CHECK_FALSE(ba1 != ba2);
  CHECK(ba1 != ba3); 
}
