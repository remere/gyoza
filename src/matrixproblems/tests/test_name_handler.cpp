#include "catch.hpp"
#include "gyoza/name_handler.h"


namespace mp=gyoza::matrixproblems;
TEST_CASE("Basic NameHandler tests", "[namehandler]"){
  std::map<int, std::string> data = { {0, "foo"},
                                      {1, "0"},
                                      {2, "s"},
                                      {3, "foo"},
                                      {4, "bar"}};
  gyoza::matrixproblems::NameHandler nh( data );
  CHECK(nh.get_data_map() == data);
  CHECK(nh.is_name("foo"));
  CHECK(nh.get_name(1) == "0");
  CHECK(nh.get_next_free_number() == 5);
  CHECK(nh.size() == 5);

  CHECK(nh.get_numbers("foo") == std::vector<int>{0,3});
  CHECK(nh.get_numbers("s") == std::vector<int>{2});

  // 5 gets name of 2.
  nh.inherit_name(5, 2);
  CHECK(nh.get_name(5) == nh.get_name(2));
  CHECK(nh.get_numbers("s") == std::vector<int>{2,5});

  // 3 gets name of 2.
  nh.inherit_name(3, 2);
  CHECK(nh.get_name(3) == "s");

  std::map<int, std::string> expected = { {0, "foo"},
                                          {1, "0"},
                                          {2, "s"},
                                          {3, "s"},
                                          {4, "bar"},
                                          {5, "s"}};
  CHECK( nh.get_data_map() == expected );
  
  // 7 tries to get 6's name, which is unamed.
  CHECK_THROWS( nh.inherit_name(7,6) ); 
}

TEST_CASE("namehandler reverse", "[namehandler]"){
  std::map<int, std::string> data = { {0, "foo"},
                                      {1, "0"},
                                      {2, "s"},
                                      {3, "foo"},
                                      {4, "bar"}};
  mp::NameHandler nh(data);
  mp::NameHandler nh_rev = nh.get_reverse(4);
  CHECK(nh_rev.get_name(4) == "foo");
  CHECK(nh_rev.get_name(3) == "0");
  CHECK(nh_rev.get_name(2) == "s");
  CHECK(nh_rev.get_name(1) == "foo");
  CHECK(nh_rev.get_name(0) == "bar");

}
