#include "catch.hpp"
#include "gyoza/permissible_operations.h"

namespace mp = gyoza::matrixproblems;

TEST_CASE("Creation of permissible operation", "[permissibles]") {
  mp::PermissibleOperations prm;
  prm.make_permissible(1, 3); 

  SECTION("set of targets") {
    prm.make_permissible(1, 3);
    REQUIRE(prm.get_permissible_targets(1).size() == 1);
  }

  SECTION("add and removing permissible operations 1") {
    mp::PermissibleOperations initial_prm = prm;

    int x = 1;
    int y = 2;

    REQUIRE(!prm.check_permissibility(x, y));
    prm.make_permissible(x, y);
    REQUIRE(prm.check_permissibility(x, y));
    prm.make_unpermissible(x,y);
    REQUIRE(!prm.check_permissibility(x, y));

    REQUIRE( prm == initial_prm );
  }

  SECTION("add and removing permissible operations 2") {
    prm.make_permissible(1, {-1,0,1,2});
    REQUIRE(prm.get_permissible_targets(1).size() == 5);
    std::set<int> expected = {-1,0,1,2,3};
    REQUIRE(prm.get_permissible_targets(1) == expected);

    prm.make_unpermissible(1);
    REQUIRE(prm.get_permissible_targets(1).empty());
    REQUIRE(prm == mp::PermissibleOperations{});
  }



  SECTION("nonsymmetry of permissible operations") {
    REQUIRE(!prm.check_permissibility(3, 1));
  }

  SECTION("adding multiple permissions"){
    mp::PermissibleOperations expected;
    expected.make_permissible(1, {1,2,3,4,5,6});

    prm.make_permissible(1, {1,2,3,4});
    prm.make_permissible(1, {3,4,5,6});

    REQUIRE( prm == expected );
  }


}


TEST_CASE("Inheriting permissibles ", "[permissibles]") {
  mp::PermissibleOperations sample;
  sample.make_permissible(0, 1);
  sample.make_permissible(1, 0);  

  SECTION("inheriting from nonexistent number") {
    mp::PermissibleOperations expected = sample;
    sample.inherit_permissibility(10, 100);
    CHECK(sample == expected);

    sample.inherit_permissibility(0, 100);
    CHECK(sample == expected);
  }

  SECTION("copies a loop") {
    mp::PermissibleOperations expected;
    expected.make_permissible(0, {1,10});
    expected.make_permissible(1, 0);
    expected.make_permissible(10, 0);

    sample.inherit_permissibility(10, 1);
    CHECK(sample == expected);
  }

  SECTION("both existing"){
    sample.make_permissible(2, {3,4,5});
    sample.make_permissible(3, {2,4,5});

    mp::PermissibleOperations expected = sample;
    expected.make_permissible(2,0);
    expected.make_permissible(0,2);

    sample.inherit_permissibility(2, 1);
    CHECK(sample == expected); 
  } 
}

TEST_CASE("Transforming permissibles", "[permissibles]") {
  mp::PermissibleOperations sample;
  sample.make_permissible(0, {1,2});
  sample.make_permissible(1, {1,3});

  mp::PermissibleOperations expected_dual;
  expected_dual.make_permissible(1, {0,1});
  expected_dual.make_permissible(2, 0);
  expected_dual.make_permissible(3, 1);

  CHECK(sample.get_dual() == expected_dual); 

  // reverse over n = 2
  mp::PermissibleOperations expected_reverse;
  expected_reverse.make_permissible(2, {1,0});
  expected_reverse.make_permissible(1, {1,-1});

  CHECK(sample.get_reverse(2) == expected_reverse); 
}
