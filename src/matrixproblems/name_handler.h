#ifndef NAME_HANDLER_H
#define NAME_HANDLER_H

#include <string>
#include <vector>
#include <map>
#include <iostream>

namespace gyoza {
namespace matrixproblems {

class NameHandler {
  // provides storage of a map
  // number ---> name (std::string)
  // where names are expected to be repeated very often,
  // so that storage is managed indirectly.

  // The number of distinct names is expected to be small compared to
  // the number of numbers!

  // Intended usage is to define all the names at the start,
  // and then let new numbers inherit names by copying from
  // a preexisting number.

  // After-the-fact insertion of a new name
  // will degrade performance for name lookups.
 public:
  typedef std::vector<std::string>::size_type IndexInVec;

  NameHandler()=default;
  NameHandler(const std::map<int, std::string>& data);
  
  void reset_data(const std::map<int, std::string>& data);

  std::string at(int number)const;
  std::string get_name(int number)const;
  std::vector<int> get_numbers(const std::string& name)const;

  std::vector<std::string> get_names()const{return stored_names;}
  
  bool is_number(int number)const;
  std::map<int,IndexInVec>::size_type size()const{return number_to_storage.size();}  

  bool is_name(const std::string & name)const; 
  // ?? int insert_name(std::string name);

  void inherit_name(int inheritee_number, int progenitor_number);
  int get_next_free_number()const;
  

  std::map<int, std::string> get_data_map()const;

  NameHandler get_reverse(int n)const;

  bool operator==(const NameHandler& other)const;
  friend std::ostream& operator<<(std::ostream& os, const NameHandler& that);

 private:
  bool guaranteed_sorted;
  std::vector<std::string> stored_names;
  std::map<int, IndexInVec> number_to_storage; 
};

}
}

#endif
