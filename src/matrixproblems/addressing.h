#ifndef ADDRESSING_H
#define ADDRESSING_H

#include <ostream>

namespace gyoza {
namespace matrixproblems {

enum {RowMin=0,RowMax=1,ColMin=2,ColMax=3};

// two-step addressing. row, subrow indices, col, subcol indices.
template<typename Matrix_T>
struct SubRow_Address {
  typedef typename Matrix_T::Index Index;
  int row_index;
  Index subrow_index;
  bool operator==(const SubRow_Address& other)const{
    return (row_index == other.row_index) && (subrow_index == other.subrow_index);
  }
};

template<typename Matrix_T>
struct SubCol_Address {
  typedef typename Matrix_T::Index Index;
  int col_index;
  Index subcol_index;
  bool operator==(const SubCol_Address& other)const{
    return (col_index == other.col_index) && (subcol_index == other.subcol_index);
  }
};

struct BlockAddress {
  int row_index;
  int col_index;
  BlockAddress()=default;
  BlockAddress(int r, int c): row_index(r), col_index(c){;}
  bool operator<(const BlockAddress& rhs) const {    
    return (row_index < rhs.row_index) ||
        ((row_index == rhs.row_index) && (col_index < rhs.col_index));
  } 
  bool operator==(const BlockAddress& rhs) const {
    return (col_index == rhs.col_index) && (row_index == rhs.row_index);
  }
  bool operator!=(const BlockAddress& rhs) const {
    return (not (*this == rhs));
  }
  friend std::ostream& operator<<(std::ostream& os,
                                  const BlockAddress& that){
    os << that.row_index << "," << that.col_index;
    return os;
  } 
};

}
}

#endif
