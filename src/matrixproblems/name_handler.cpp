#include "gyoza/name_handler.h"
#include <algorithm>
#include <tuple>

namespace gyoza {
namespace matrixproblems {
NameHandler::NameHandler(const std::map<int, std::string>& data) :
    guaranteed_sorted(false), stored_names(), number_to_storage() {
  reset_data(data);
}

void NameHandler::reset_data(const std::map<int, std::string>& data) {
  stored_names.clear();
  number_to_storage.clear();
  
  stored_names.reserve(data.size());
  std::vector< std::string > values;
  values.reserve(data.size());
 
  for (auto const & a_pair : data) {
    values.emplace_back( a_pair.second); 
  }
  
  std::sort(values.begin(), values.end());
  std::unique_copy(values.begin(), values.end(),
                   std::back_inserter(stored_names));

  std::map<std::string, IndexInVec> name_to_index;
  for (IndexInVec i = 0; i < stored_names.size(); ++i) {
    name_to_index[stored_names.at(i)] = i;
  }

  for (auto const & a_pair : data) {
    number_to_storage[a_pair.first] = name_to_index.at(a_pair.second);
  }

  guaranteed_sorted = true; 
}

std::string NameHandler::get_name(int number)const{
  return stored_names.at( number_to_storage.at(number) );
}

std::vector<int> NameHandler::get_numbers(const std::string& name)const{
  std::vector<int> ans;
  auto it = std::find(stored_names.cbegin(), stored_names.cend(), name);
  if (it == stored_names.cend()){
    return ans;
  }
  IndexInVec idx = std::distance(stored_names.cbegin(), it);
  for (auto data : number_to_storage){
    if (data.second == idx) {
      ans.push_back(data.first);
    }
  }
  return ans;
}

std::string NameHandler::at(int number)const{
  return get_name(number);
}

bool NameHandler::is_number(int number)const{
  return bool(number_to_storage.count(number));
}

bool NameHandler::is_name(const std::string & name)const{
  if (guaranteed_sorted) {
    return std::binary_search(stored_names.begin(), stored_names.end(), name);
  } else {
    return ( std::find(stored_names.begin(),
                       stored_names.end(),
                       name) != stored_names.end() );
  }
}

void NameHandler::inherit_name(int inheritee_number, int progenitor_number){
  number_to_storage[inheritee_number] = number_to_storage.at(progenitor_number);
  return;
}

int NameHandler::get_next_free_number()const{
  int largest_number = (number_to_storage.rbegin()->first);
  return ++largest_number;
}

std::map<int, std::string> NameHandler::get_data_map()const {
  std::map<int, std::string> ans;
  for (auto const & a_pair : number_to_storage) {
    ans[a_pair.first] = stored_names.at(a_pair.second);
  }
  return ans;
}

NameHandler NameHandler::get_reverse(int n)const {
  std::map<int, std::string> new_data_map;
  for (auto const & a_pair : number_to_storage) {
    new_data_map[n - a_pair.first] = stored_names.at(a_pair.second);
  }
  NameHandler ans(new_data_map);
  return ans; 
}

bool NameHandler::operator==(const NameHandler& other)const{
  return get_data_map() == other.get_data_map();
}

std::ostream& operator<<(std::ostream& os, const NameHandler& that){
  for (auto const & a_pair : that.number_to_storage) {
    os << a_pair.first << ": " <<  that.stored_names.at(a_pair.second) << "\n";
  }
  return os;
}




}
}
