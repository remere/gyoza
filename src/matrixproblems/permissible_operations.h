#ifndef PERMISSIBLE_OPERATIONS_H
#define PERMISSIBLE_OPERATIONS_H

#include <map>
#include <set>
#include <iostream>


namespace gyoza {
namespace matrixproblems {

class PermissibleOperations {
 public:
  PermissibleOperations()=default;

  void make_permissible(int source, int target);
  void make_permissible(int source, const std::set<int> & targets);

  void make_unpermissible(int source);
  void make_unpermissible(int source, int target);

  void inherit_permissibility(int inheritee_number, int progenitor_number);

  PermissibleOperations get_dual() const;
  PermissibleOperations get_reverse(int n) const;

  bool is_source(int query)const;
  bool is_target(int query)const;

  bool check_permissibility(int source, int target)const;
  std::set<int> get_permissible_targets(int source)const; 

  void clear();

  bool operator==(const PermissibleOperations& other)const;
  friend std::ostream& operator<<(std::ostream& os, const PermissibleOperations& that);

 private:
  std::map<int, std::set<int>> permissibles;

};





}
}



#endif
