#ifndef MATRIX_PROBLEM_H
#define MATRIX_PROBLEM_H

#include <vector>
#include <map>
#include <set>

#include <ostream>
#include <exception>

#include "gyoza/name_handler.h"
#include "gyoza/permissible_operations.h"
#include "gyoza/addressing.h"
#include "gyoza/snf_algorithms.hpp"

namespace gyoza {
namespace matrixproblems {

// Matrix_T should be an Eigen::Matrix Type.
template<typename Matrix_T>
class MatrixProblem {
 public:
  typedef typename Matrix_T::Scalar Scalar;
  typedef typename Matrix_T::Index SubIndex;
  typedef int OuterIndex;
  
  using SubRow=SubRow_Address<Matrix_T>;
  using SubCol=SubCol_Address<Matrix_T>;
  using Submatrices_T=std::map<BlockAddress, Matrix_T>;
  

  // access to blocksubmatrices
  Matrix_T& operator[](const BlockAddress& blk){return submatrices[blk];}
  Matrix_T at(const BlockAddress& blk)const{return submatrices.at(blk);}

  typename Submatrices_T::size_type
  erase(const BlockAddress& blk){return submatrices.erase(blk);}

  typename Submatrices_T::size_type
  count(const BlockAddress& blk)const{return submatrices.count(blk);}
  
  typename Submatrices_T::size_type
  size()const {return submatrices.size();}

  //
  bool check_dimensions()const;
  bool check_indices_are_named()const;

  //
  int get_dimension_of_row(int r)const; 
  int get_dimension_of_col(int c)const; 
  
  // sub{row,col} operations
  void subrow_swap(SubRow source, SubRow target);
  void subcol_swap(SubCol source, SubCol target);
  
  void subrow_add_multiple(Scalar k, SubRow source, SubRow target);
  void subcol_add_multiple(Scalar k, SubCol source, SubCol target);


  // {row,col} operations
  int split_row(int row_index, SubIndex rank);
  int split_col(int col_index, SubIndex rank);  

  void left_multiply_row(int row_index, const Matrix_T & multiplier);
  void right_multiply_col(int col_index, const Matrix_T & multiplier);

  void add_left_multiple_row(const Matrix_T & multiplier, int source, int target);
  void add_right_multiple_col(const Matrix_T & multiplier, int source, int target);
  

  SubIndex reduce_block(const BlockAddress& ba) {
    algorithms::FullPivotSNF<Matrix_T> snf;
    return reduce_block(ba,snf);
  }
  
  SubIndex reduce_block(const BlockAddress& ba,
                        algorithms::FullPivotSNF<Matrix_T>& snf);

  // names
  void set_row_names(const NameHandler& row_nh);
  void set_col_names(const NameHandler& col_nh);

  const NameHandler& get_row_names()const{return row_names;}
  const NameHandler& get_col_names()const{return col_names;}

  void generate_default_names();

  // permissions
  void set_permissible_row_operations(const PermissibleOperations& perm);
  void set_permissible_col_operations(const PermissibleOperations& perm);

  PermissibleOperations get_permissible_row_operations() const;
  PermissibleOperations get_permissible_col_operations() const;

  PermissibleOperations& get_permissible_row_operations();
  PermissibleOperations& get_permissible_col_operations();
  
  bool check_row_operation(int row_source, int row_target)const;
  bool check_col_operation(int col_source, int col_target)const;

  // informational
  std::vector<BlockAddress> non_strongly_zero_entries() const;
  
  friend std::ostream& operator<<(std::ostream& os,
                                  const MatrixProblem& that){
    for (auto const & blk_matrix_pair : that.submatrices) {
      os << blk_matrix_pair.first << ":\n" << blk_matrix_pair.second << "\n";
    }
    return os;
  }

  
  //! Experimental function to print out the entire matrix!;
  void pretty_print(std::ostream& os)const;

  // Think about this more:
  bool operator==(const MatrixProblem& other)const;

  bool query_dimensions(std::map<int,int>* row_dims, std::map<int,int>* col_dims)const ;

 protected: 
  // This should contain all submatrices "in play".
  // ie, any BlockAddress not here is assumed
  // to be empty aka nonexisting aka always-zero (not just zero).
  Submatrices_T submatrices; 

  NameHandler row_names;
  NameHandler col_names;
  
  PermissibleOperations permissible_row_operations;
  PermissibleOperations permissible_col_operations;


  // utilities for split
  virtual void post_process_block(const BlockAddress& new_ba,
                                  const BlockAddress& old_ba);

  // utilities
  typename Submatrices_T::iterator
  get_next_submatrix_at_row(typename Submatrices_T::iterator first, int row);

  typename Submatrices_T::iterator
  get_next_submatrix_at_col(typename Submatrices_T::iterator first, int col);

  typename Submatrices_T::const_iterator
  get_next_submatrix_at_row(typename Submatrices_T::const_iterator first, int row)const;

  typename Submatrices_T::const_iterator
  get_next_submatrix_at_col(typename Submatrices_T::const_iterator first, int col)const;

  
};



// class UnpermissibleError : public std::runtime_error {
//  public:
//   UnpermissibleError(int src, int trgt) : runtime_error("permissible operation error") {}
//  private:
  

// };

}
}

#endif
