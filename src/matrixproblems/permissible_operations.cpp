#include "gyoza/permissible_operations.h"
#include <algorithm>
#include <iterator>

namespace gyoza {
namespace matrixproblems {

void PermissibleOperations::make_permissible(int source, int target){
  if (permissibles.count(source) == 0){
    permissibles[source] = {target};
  } else {
    permissibles[source].insert(target);
  }
  return;
}

void PermissibleOperations::make_permissible(int source, const std::set<int> & targets) {
  if (targets.size() == 0) {
    return;
  }
  auto psource = permissibles.find(source);
  if (psource == permissibles.end()){
    permissibles[source] = targets;
  } else {
    std::set<int>& existing_targets = psource->second;
    std::set_union(targets.begin(), targets.end(),
                   existing_targets.begin(), existing_targets.end(),
                   std::inserter(existing_targets, existing_targets.end()));
  }
}

void PermissibleOperations::make_unpermissible(int source){
  auto psource = permissibles.find(source);
  if (psource != permissibles.end()){
    permissibles.erase(psource);
  }
}

void PermissibleOperations::make_unpermissible(int source, int target){
  auto psource = permissibles.find(source);
  if (psource != permissibles.end()){
    std::set<int>& existing_targets = psource->second;
    existing_targets.erase(target);
    if (existing_targets.size() == 0) {
      permissibles.erase(psource);
    }
  }
  return;
}


void PermissibleOperations::inherit_permissibility(int inheritee_number, int progenitor_number) {
  auto it = permissibles.find(inheritee_number);
  auto pt = permissibles.find(progenitor_number);

  // inherit permissibles progenitor ---> x
  if (pt != permissibles.end()) {
    if (it == permissibles.end()) {
      permissibles[inheritee_number] = pt->second;
    } else {
      (it->second).insert(pt->second.begin(), pt->second.end());
    }
    // NO
    // make_permissible(inheritee_number, progenitor_number);
    // make_permissible(progenitor_number, inheritee_number);
  }

  // inherit permissibles x ----> progenitor
  for (auto & perm : permissibles) {
    if (perm.second.count(progenitor_number) == 1) {
      perm.second.insert(inheritee_number);
    }
  }
  
  

  return;
}

bool PermissibleOperations::is_source(int query)const {
  return (get_permissible_targets(query).size() > 0);
}

bool PermissibleOperations::is_target(int query)const {
  PermissibleOperations dual = get_dual();
  return dual.is_source(query);
}

bool PermissibleOperations::check_permissibility(int source, int target)const {
  return (permissibles.count(source) == 1 and
          permissibles.at(source).count(target) == 1);
}

std::set<int> PermissibleOperations::get_permissible_targets(int source) const {
  if (permissibles.count(source)) {
    return permissibles.at(source);
  }
  return std::set<int>();
}

void PermissibleOperations::clear() {
  permissibles.clear();
}


bool PermissibleOperations::operator==(const PermissibleOperations& other)const {
  return (permissibles == other.permissibles);
}

std::ostream& operator<<(std::ostream& os, const PermissibleOperations& that) {
  for (const auto & source_comma_targets : that.permissibles) {
    os << source_comma_targets.first << " --> ";
    for (const auto & trgt : source_comma_targets.second) {
      os << trgt << ",";
    }
    os << "   ";
  }
  return os;
}


PermissibleOperations PermissibleOperations::get_dual() const {
  PermissibleOperations ans;
  for (const auto & source_comma_targets : permissibles) {
    for (const auto & t : source_comma_targets.second) {
      ans.make_permissible(t, source_comma_targets.first);
    }
  }
  return ans;
}

PermissibleOperations PermissibleOperations::get_reverse(int n) const {
  PermissibleOperations ans;
  for (const auto & source_comma_targets : permissibles) {
    for (const auto & t : source_comma_targets.second) {
      ans.make_permissible(n - source_comma_targets.first, n - t);
    }
  }
  return ans;
}

}
}
