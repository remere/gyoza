#ifndef SQUID_H
#define SQUID_H

/*
  This file contains very specialized code for plotting the AR-quiver of CL(fb), and persistence diagrams over CL(fb).
  Should probably be generalized eventually...
*/

#include <map>
#include <string>
#include <set>
#include <vector>

#define X_DISPLAY_MISSING
#include <plotter.h>


namespace gyoza {
namespace arquivers {

static const std::map<std::string, std::string> clfb_name_map = {
  {"02", "000010"} ,
  {"11", "000011"}, {"12", "010010"}, {"13", "000110"},
  {"22", "010121"},
  {"31", "010110"}, {"32", "000111"}, {"33", "010011"},
  {"40", "110110"}, {"42", "010111"}, {"44", "011011"},
  {"51", "110111"}, {"52", "010000"}, {"53", "011111"},
  {"60", "000001"}, {"62", "121111"}, {"64", "000100"},
  {"71", "011001"}, {"72", "111111"}, {"73", "110100"},
  {"80", "011000"}, {"82", "111101"}, {"84", "110000"},
  {"91", "111100"}, {"93", "111001"},
  {"a0", "100100"}, {"a2", "111000"}, {"a4", "001001"},
  {"b1", "100000"}, {"b3", "001000"}    
};

class squid_node{
 public:
  squid_node(std::string code): code(code){
    x = std::stoi(code.substr(0,1), NULL, 16);
    y = std::stoi(code.substr(1,1));        
    name = clfb_name_map.at(code);
  }
    
  void add_leaf(squid_node* other){
    leaves.insert(other);
  }
    
  int get_x() const {return x;}
  int get_y() const {return y;}
  std::string get_name() const {return name;}
  std::string get_code() const {return code;}    
  std::set<squid_node*> get_leaves() const {return leaves;}    
 private:
  std::string code;
  int x;
  int y;    
  std::set<squid_node*> leaves;
  std::string name;
};


class squid{
 public:
  squid();
  squid(squid& other) = delete;
  squid& operator=(squid& other) = delete;
    
  ~squid();

  squid_node* add_node(const std::string& code);  
    
  template<typename PlotterType>
  bool draw_pd(std::string, const std::map<std::string, unsigned int>& squid, bool show_dimv = false);

  template<typename PlotterType>
  bool draw(std::string);
  
 private:       
  std::set<squid_node*> nodes;

  // magic numbers
  static const std::vector<std::string> successors_data;
  constexpr static const int spine_location_data = 2;
  constexpr static const double y_scaling = 0.705;
  constexpr static const double line_stretch = 0.85;

  // paramaters
  PlotterParams params;

  // helpers:
  std::pair<double,double> get_coords(squid_node const *);
  std::vector<double> get_line_coords(squid_node const * p_source,
                                      squid_node const * p_target,
                                      bool show_dimv);
};

}
}



#endif
