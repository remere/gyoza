#include "squid.h"

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/trim.hpp>

#include <iostream>
#include <fstream>
#include <sstream>

namespace gyoza {
namespace arquivers {

squid::squid(){
  params.setplparam("PAGESIZE", (char*)"a4");
  // params.setplparam("BITMAPSIZE", (char*)"2048x1024");
  // params.setplparam("ROTATION", (char*)"90");

  for (const auto & line: squid::successors_data) {
    std::vector<std::string> vertex_codes;
    boost::split(vertex_codes, line, boost::is_space(), boost::token_compress_on);

    if (vertex_codes.size() == 0){
      continue;
    }

    auto it = vertex_codes.cbegin();
    squid_node* p_root = add_node(*it);
    ++it;
    for ( ; it < vertex_codes.cend(); ++it){
      squid_node* p_leaf = add_node(*it);
      p_root->add_leaf(p_leaf);
    }
  }
}

squid::~squid() {
  for (auto& a_node: nodes){
    delete a_node;
  }
}

squid_node* squid::add_node(const std::string& code) {
  auto it = std::find_if(nodes.begin(), nodes.end(), [&code](squid_node * const & p_node ){return p_node->get_code()==code;});
  squid_node* p_root = (squid_node*)0;
  if (it == nodes.end()){
    p_root = new squid_node(code);
    nodes.insert(p_root);
  }else{
    p_root = *it;
  }
  return p_root;
}

std::pair<double,double> squid::get_coords(squid_node const * p_node) {
  double x = (double)(p_node->get_x());
  double y = (double)(p_node->get_y() - squid::spine_location_data) * squid::y_scaling;

  return std::make_pair(x, y);
}

std::vector<double> squid::get_line_coords(squid_node const * p_source,
                                           squid_node const * p_target,
                                           bool show_dimv) {
  double from_x, from_y;
  std::tie(from_x, from_y) = get_coords(p_source);

  double to_x, to_y;
  std::tie(to_x, to_y) = get_coords(p_target);

  double additional_shrink = (show_dimv ? 0.9 : 1);
  if (to_y == from_y) {
    additional_shrink *= 0.9;
  }

  //restretch
  double rto_x = from_x + additional_shrink * line_stretch*(to_x-from_x);
  double rto_y = from_y + additional_shrink * line_stretch*(to_y-from_y);

  double rfrom_x = to_x + additional_shrink * line_stretch*(from_x-to_x);
  double rfrom_y = to_y + additional_shrink * line_stretch*(from_y-to_y);

  return {rfrom_x, rfrom_y, rto_x, rto_y};
}


template<typename PlotterType>
void init_plotter(PlotterType* plotter) {
  plotter->fontname("AvantGarde-Book");
  plotter->fspace(-1, -3.4, 12, 3.4);
  plotter->flinewidth(0.005);
  plotter->pencolorname("black");
  // plotter->linemod("disconnected");
  // plotter->capmod("round");
  plotter->erase();
}



template<typename PlotterType>
bool squid::draw_pd(std::string pic_name, const std::map<std::string, uint>& pd, bool show_dimv){

  std::ofstream ofile(pic_name);
  if (not ofile.is_open()) {
    return false;
  }

  PlotterType* plotter = new PlotterType(std::cin, ofile, std::cerr, params);

  if (plotter->openpl()< 0){
    std::cerr << "Cannot open Plotter\n";
    return false;
  }
  init_plotter(plotter);

  for (const auto& p_node: nodes ){
    double from_x, from_y;
    std::tie(from_x, from_y) = get_coords(p_node);
    plotter->fmove(from_x, from_y);

    std::string dimv = p_node->get_name();
    std::stringstream label;
    if (show_dimv) {
      label << "\\mk\\sp" << dimv.substr(3,3) << "\\ep\\rt\\sb" << dimv.substr(0,3) << "\\eb:";
    }
    auto it = pd.find(p_node->get_name());
    label << ( it != pd.end() ? it->second : 0);

    plotter->alabel('c','c', label.str().c_str());

    for (const auto& p_leaf: p_node->get_leaves()){
      std::vector<double> vdat = get_line_coords(p_node, p_leaf, show_dimv);
      plotter->fline(vdat[0], vdat[1], vdat[2], vdat[3]);
      plotter->endpath();
    }
  }

  if (plotter->closepl() < 0){
    std::cerr << "Cannot close Plotter\n";
  }

  delete plotter;
  ofile.close();
  return true;
}




template<typename PlotterType>
bool squid::draw(std::string pic_name){
  std::ofstream ofile(pic_name);
  if (not ofile.is_open()) {
    return false;
  }

  PlotterType* plotter = new PlotterType(std::cin, ofile, std::cerr, params);

  if (plotter->openpl()< 0){
    std::cerr << "Cannot open Plotter\n";
    return false;
  }
  init_plotter(plotter);

  for (const auto& p_node: nodes ){
    double from_x, from_y;
    std::tie(from_x, from_y) = get_coords(p_node);
    plotter->fmove(from_x , from_y );

    std::string dimv = p_node->get_name();
    std::stringstream label;
    label << "\\mk\\sp" << dimv.substr(3,3) << "\\ep\\rt\\sb" << dimv.substr(0,3) << "\\eb";
    plotter->alabel('c','c', label.str().c_str());

    for (const auto& p_leaf: p_node->get_leaves()){
      std::vector<double> vdat = get_line_coords(p_node, p_leaf, true);
      plotter->fline(vdat[0], vdat[1], vdat[2], vdat[3]);
      plotter->endpath();
    }
  }

  if (plotter->closepl() < 0){
    std::cerr << "Cannot close Plotter\n";
  }

  delete plotter;
  ofile.close();
  return true;
}

const std::vector<std::string> squid::successors_data = {"02 11 12 13",
                                                         "11 22",
                                                         "12 22",
                                                         "13 22",
                                                         "22 33 31",
                                                         "33 44 42",
                                                         "31 42 40",
                                                         "44 53",
                                                         "42 53 51",
                                                         "40 51",
                                                         "53 64 62",
                                                         "51 62 60",
                                                         "64 73",
                                                         "62 73 71",
                                                         "60 71",
                                                         "73 84 82",
                                                         "71 82 80",
                                                         "84 93",
                                                         "82 93 91",
                                                         "80 91",
                                                         "93 a4 a2",
                                                         "91 a2 a0",
                                                         "a4 b3",
                                                         "a2 b3 b1",
                                                         "a0 b1",
                                                         "b3",
                                                         "b1",
                                                         "22 32",
                                                         "32 42",
                                                         "42 52",
                                                         "52 62",
                                                         "62 72",
                                                         "72 82"};



template bool squid::draw_pd<GIFPlotter>(std::string pic_name, const std::map<std::string, unsigned int>&, bool);
template bool squid::draw_pd<SVGPlotter>(std::string pic_name, const std::map<std::string, unsigned int>&, bool);
template bool squid::draw_pd<PSPlotter>(std::string pic_name, const std::map<std::string, unsigned int>&, bool);

template bool squid::draw<GIFPlotter>(std::string pic_name);
template bool squid::draw<SVGPlotter>(std::string pic_name);
template bool squid::draw<PSPlotter>(std::string pic_name);

}
}
