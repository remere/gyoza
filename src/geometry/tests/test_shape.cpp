#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "gyoza/shape.h"
#include <sstream>

TEST_CASE("Shape", "[alphashape]") {
  gyoza::geometry::Shape x;

  std::stringstream data;
  data << "2 2 2 0\n"
       << "2 2 -2 0\n"
       << "2 -2 2 0\n"
       << "2 -2 -2 0\n"
       << "-2 2 2 0\n"
       << "-2 2 -2 0\n"
       << "-2 -2 2 0\n"
       << "-2 -2 -2 0\n";
  x.read_file(data,false);
  std::cerr << "HI\n";
  x.output_slice(4, std::cerr);
  std::cerr << std::endl; 
  
  gyoza::quivers::QuiverComplex<gyoza::Algebra::SetChain> qc = x.create_quiver_complex({0,4,8,12});
  qc.print_cells(std::cerr);
  CHECK(qc.check_integrity());

}
