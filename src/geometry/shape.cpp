#include "gyoza/shape.h"

// cpp includes
#include <CGAL/Object.h>
#include <CGAL/iterator.h>

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <algorithm>


namespace gyoza {
namespace geometry {

// helpers for bdd computation
struct Cell_info_3 {
  typedef Shape::FiltrationIndex Index;

  Cell_info_3() {
    for(std::size_t i=0; i<6; i++) {
      edge_index_[i] = boost::none;
    }
    for(std::size_t i=0; i<4; i++) {
      facet_index_[i] = boost::none;
    }
  }

  int edge_conv(int i, int j) {
    if(i>j) std::swap(i,j);
    if(i==0 && j==1) return 0;
    if(i==0 && j==2) return 1;
    if(i==0 && j==3) return 2;
    if(i==1 && j==2) return 3;
    if(i==1 && j==3) return 4;
    if(i==2 && j==3) return 5;
    //should'nt reach here!
    throw;
    return -1;
  }

  bool has_edge_index(int i, int j) {
    return bool(edge_index_[edge_conv(i,j)]);
  }

  Index edge_index(int i, int j) {
    CGAL_assertion(has_edge_index(i,j));
    int k = edge_conv(i,j);
    return edge_index_[k].get();
  }

  bool has_facet_index(int i) {
    CGAL_assertion(i>=0 && i<4);
    return bool(facet_index_[i]);
  }

  Index facet_index(int i) {
    CGAL_assertion(has_facet_index(i));
    return facet_index_[i].get();
  }

  void set_edge_index(int i, int j, Index I) {
    edge_index_[edge_conv(i,j)]=I;
  }

  void set_facet_index(int i, Index I) {
    facet_index_[i]=I;
  }
 private:
  boost::optional<Index> edge_index_[6];
	boost::optional<Index> facet_index_[4];
};

void set_index_of_edge(const Triangulation_3& T, const Edge& e,
                       std::map<Cell_handle, Cell_info_3>& c_info,
                       Shape::FiltrationIndex I) {
	Vertex_handle v1 = e.first->vertex(e.second);
  Vertex_handle v2 = e.first->vertex(e.third);

  Cell_circulator ch=T.incident_cells(e);
  Cell_circulator ch_start=ch;
  int count=0;
  do {
    c_info[(Cell_handle)ch].set_edge_index(ch->index(v1), ch->index(v2), I);
    ch++;
    count++;
  } while(ch!=ch_start);
}

void set_index_of_facet(const Triangulation_3& T, const Facet& f,
                        std::map<Cell_handle, Cell_info_3>& c_info,
                        Shape::FiltrationIndex I) {
  c_info[f.first].set_facet_index(f.second, I);
  Facet mf = T.mirror_facet(f);
  c_info[mf.first].set_facet_index(mf.second, I);
}
// end helpers for bdd computation


void Shape::output_slice(FT alpha, std::ostream& output) const {
  Vertex_handle v;
  Edge e;
  Facet f;
  Cell_handle c;

  auto ub = std::upper_bound(alphas.begin(), alphas.end(), alpha);
  auto ait = alphas.cbegin();

  // *ub is first item strictly greater than alpha.
  // everything up to and equal alpha.
  for (auto cit = cells.cbegin(); ait < ub; ++ait, ++cit) {
    const CGAL::Object& obj = *cit;
    if(CGAL::assign(v,obj)) {
      output << "0 " << handle_to_original_index.at(v) << "\n";
    } else if(CGAL::assign(e,obj)) {
      Vertex_handle v1 = e.first->vertex(e.second);
      Vertex_handle v2 = e.first->vertex(e.third);

      OriginalIndex i1 = handle_to_original_index.at(v1);
      OriginalIndex i2 = handle_to_original_index.at(v2);

      output << "1 " << i1 << " " << i2 << "\n";
    } else if(CGAL::assign(f,obj)) {
      Vertex_handle v1 = (f.first)->vertex( (f.second+1)%4 );
      Vertex_handle v2 = (f.first)->vertex( (f.second+2)%4 );
      Vertex_handle v3 = (f.first)->vertex( (f.second+3)%4 );

      OriginalIndex i1 = handle_to_original_index.at(v1);
      OriginalIndex i2 = handle_to_original_index.at(v2);
      OriginalIndex i3 = handle_to_original_index.at(v3);

      output << "2 " << i1 << " " << i2 << " " << i3 << "\n";
    } else if(CGAL::assign(c,obj)) {
      output << "3 ";
      for (int j=0; j < 4-1; ++j) {
        output <<  handle_to_original_index.at(c->vertex(j)) << " ";
      }
      output << handle_to_original_index.at(c->vertex(3)) << "\n";
    } else {
      throw;
    }
  }
}


std::vector<std::pair<Shape::Simplex,Shape::FiltrationIndex>> Shape::get_simplices_between(boost::optional<FT> alpha0, boost::optional<FT> alpha1, int dim) const {
  std::vector<std::pair<Shape::Simplex,Shape::FiltrationIndex>> ans;
  if (dim < 0 or dim >= 4) {
    return ans;
  }
  
  std::vector<FT>::const_iterator ait;
  if (alpha0 == boost::none) {
    ait = alphas.begin();
  } else {
    // *bb is first item greater than alpha0.
    ait = std::upper_bound(alphas.begin(), alphas.end(), alpha0.get());    
  }
      
  std::vector<FT>::const_iterator dd;
  if (alpha1 == boost::none) {
    dd = alphas.end();
  } else {
  // *dd is first item greater than alpha1.
    dd = std::upper_bound(alphas.begin(), alphas.end(), alpha1.get());
  }  

  Vertex_handle v;
  Edge e;
  Facet f;
  Cell_handle c;

  // all things alpha0 < *ait <= alpha1
  FiltrationIndex idx = std::distance(alphas.begin(), ait);  
  while( ait < dd ) {
    const CGAL::Object& obj = cells.at(idx);
    Simplex smplx;
    if(dim == 0 && CGAL::assign(v,obj)) {
      smplx = {handle_to_original_index.at(v)};
    } else if(dim == 1 && CGAL::assign(e,obj)) {
      OriginalIndex i1 = handle_to_original_index.at(e.first->vertex(e.second));
      OriginalIndex i2 = handle_to_original_index.at(e.first->vertex(e.third));
      smplx = {i1,i2};
    } else if(dim == 2 && CGAL::assign(f,obj)) {
      Vertex_handle v1 = (f.first)->vertex( (f.second+1)%4 );
      Vertex_handle v2 = (f.first)->vertex( (f.second+2)%4 );
      Vertex_handle v3 = (f.first)->vertex( (f.second+3)%4 );

      OriginalIndex i1 = handle_to_original_index.at(v1);
      OriginalIndex i2 = handle_to_original_index.at(v2);
      OriginalIndex i3 = handle_to_original_index.at(v3);

      smplx = {i1,i2,i3};
    } else if(dim == 3 && CGAL::assign(c,obj)) {
      for (int j=0; j < 4; ++j) {
        smplx.insert(handle_to_original_index.at(c->vertex(j)));
      }
    }
    if (smplx.size() != 0){      
      assert( alphas.at(idx) == *ait);
      //ans.push_back(std::make_pair(smplx,idx));
      ans.emplace_back(smplx,idx);
    }
    ++ait;
    ++idx;
  }
  return ans;
}

Shape::Simplex Shape::get_simplex_at(FiltrationIndex idx) const {
  Vertex_handle v;
  Edge e;
  Facet f;
  Cell_handle c;
  const CGAL::Object & obj = cells.at(idx);

  Simplex smplx;
  if(CGAL::assign(v,obj)) {
    smplx = {handle_to_original_index.at(v)};
  } else if(CGAL::assign(e,obj)) {
    OriginalIndex i1 = handle_to_original_index.at(e.first->vertex(e.second));
    OriginalIndex i2 = handle_to_original_index.at(e.first->vertex(e.third));
    smplx = {i1,i2};
  } else if(CGAL::assign(f,obj)) {
    Vertex_handle v1 = (f.first)->vertex( (f.second+1)%4 );
    Vertex_handle v2 = (f.first)->vertex( (f.second+2)%4 );
    Vertex_handle v3 = (f.first)->vertex( (f.second+3)%4 );

    OriginalIndex i1 = handle_to_original_index.at(v1);
    OriginalIndex i2 = handle_to_original_index.at(v2);
    OriginalIndex i3 = handle_to_original_index.at(v3);

    smplx = {i1,i2,i3};
  } else if(CGAL::assign(c,obj)) {
    for (int j=0; j < 4; ++j) {
      smplx.insert(handle_to_original_index.at(c->vertex(j)));
    }
  } else {
    throw;
  }
  return smplx;
}




quivers::QuiverComplex<gyoza::Algebra::SetChain> Shape::create_quiver_complex(std::vector<FT> cuts)const {
  quivers::QuiverComplex<gyoza::Algebra::SetChain> qc;
  if (cuts.size() == 0) {
    return qc;
  }
  std::sort(cuts.begin(), cuts.end());

  int num_quiver_vertices = cuts.size();
  quivers::Quiver An = quivers::create_an_quiver(num_quiver_vertices);
  qc.set_quiver(An);

  quivers::BirthType bt(num_quiver_vertices);
  bt.flip();
  quivers::BirthHandler bh;
  std::vector<quivers::BirthHandler::BirthCode> codes;
  for (int b = 0; b < num_quiver_vertices; ++b) {
    codes.push_back(bh.add_type(bt));
    bt[b] = false;
  }
  qc.set_births(bh);

  auto cuts_it = cuts.cbegin();
  int current_birth = 0;
  for (unsigned int current_cell = 0; current_cell < cells.size(); ++current_cell) {
    FT current_alpha = alphas[current_cell];
    while (current_alpha > *cuts_it) {
      ++current_birth;
      ++cuts_it;
      if (cuts_it == cuts.cend()){
        break;
      }
    }
    if (current_birth >= num_quiver_vertices) {
      break;
    }

    const std::set<FiltrationIndex>& faces = boundaries.at(current_cell);
    gyoza::Algebra::SetChain bdd;
    for (const auto & x : faces) {
      bdd.set_entry(x,1);
    }
    int dim = ((faces.size() == 0) ? 0 : faces.size()-1);
    qc.create_cell( dim, bdd, codes.at(current_birth));
  }
  return qc;
}


void Shape::read_file(std::istream& is, bool do_square_radius) {
  //read input
  std::string next_line;
  while( getline( is, next_line ) ) {
    if( next_line != "" && next_line[ 0 ] != '#' ) {
      std::stringstream sstr(next_line);
      std::vector<std::string> vstrings;
      boost::split(vstrings, next_line,
                   boost::is_any_of(", 	"),
                   boost::token_compress_on);

      std::vector<double> vdoubles(vstrings.size());
      std::transform(vstrings.begin(), vstrings.end(),
                     vdoubles.begin(),
                     [](const std::string& val)
                     {
                       return std::stod(val);
                     });

      if (vdoubles.size() == 4) {
        if (do_square_radius) {
          lp.emplace_back( Bare_point(vdoubles.at(0),vdoubles.at(1),vdoubles.at(2)), vdoubles.at(3)*vdoubles.at(3) );
        } else {
          lp.emplace_back( Bare_point(vdoubles.at(0),vdoubles.at(1),vdoubles.at(2)), vdoubles.at(3));
        }
      } else {
        return ;
      }
    }
  }
  shape3.clear();
  shape3.make_alpha_shape(lp.begin(), lp.end());

  update_vertex_handle_to_original_index();
  create_filtration();
  compute_boundaries();
}

// Helpers:
Shape::OriginalIndex Shape::find_original_index(const Weighted_point& p)const {
  const auto it = std::find(lp.cbegin(),lp.cend(), p);
  if (it == lp.cend()) {
    throw;
  }
  return std::distance(lp.cbegin(), it);
}

void Shape::update_vertex_handle_to_original_index() {
  handle_to_original_index.clear();
  for (Alpha_shape_3::Finite_vertices_iterator v_itr = shape3.finite_vertices_begin();
       v_itr != shape3.finite_vertices_end();
       ++v_itr){
    handle_to_original_index[Vertex_handle(v_itr)] = find_original_index(v_itr->point());
  }
  return;
}


void Shape::create_filtration() {
  auto itc = std::back_inserter(cells);
  auto ita = std::back_inserter(alphas);
  typedef CGAL::cpp11::tuple<std::back_insert_iterator<std::vector<CGAL::Object>>,
                             std::back_insert_iterator<std::vector<FT>>> ITERS;
  typedef CGAL::cpp11::tuple<CGAL::Object, FT>  VALS;
  CGAL::Dispatch_output_iterator<VALS,ITERS> dispatch_it(itc,ita);

  shape3.filtration_with_alpha_values(dispatch_it);
}

void Shape::compute_boundaries() {
  Vertex_handle v;
  Edge e;
  Facet f;
  Cell_handle c;

  boundaries.clear();
  boundaries = std::vector<std::set<FiltrationIndex>>(cells.size());

  std::map<Vertex_handle, FiltrationIndex> vh_to_index;
  std::map<Cell_handle, Cell_info_3> c_info;

  FiltrationIndex curr_index = 0;
  for (auto cit = cells.cbegin(); cit != cells.cend(); ++cit) {
    const CGAL::Object& obj = *cit;
    if(CGAL::assign(v,obj)) {
      vh_to_index[v] = curr_index;
    } else if(CGAL::assign(e,obj)) {
      Vertex_handle v1 = e.first->vertex(e.second);
      Vertex_handle v2 = e.first->vertex(e.third);
      FiltrationIndex i1 = vh_to_index.at(v1);
      FiltrationIndex i2 = vh_to_index.at(v2);

      boundaries[curr_index] = {i1,i2};
      set_index_of_edge(shape3, e, c_info, curr_index);
    } else if(CGAL::assign(f,obj)) {
      FiltrationIndex i1= c_info.at(f.first).edge_index((f.second+1)%4, (f.second+2)%4 );
      FiltrationIndex i2= c_info.at(f.first).edge_index((f.second+1)%4, (f.second+3)%4 );
      FiltrationIndex i3= c_info.at(f.first).edge_index((f.second+2)%4, (f.second+3)%4 );

      boundaries[curr_index] = {i1,i2, i3};
      set_index_of_facet(shape3, f, c_info, curr_index);
    } else if(CGAL::assign(c,obj)) {
      boundaries[curr_index].clear();
      for (int j=0; j < 4; ++j) {
        FiltrationIndex i = c_info.at(c).facet_index(j);
        boundaries[curr_index].insert(i);
      }
    } else {
      throw;
    }
    ++curr_index;
  }
}







}
}
