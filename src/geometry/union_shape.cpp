#include "gyoza/union_shape.h"
#include <string>

namespace gyoza {
namespace geometry {
bool UnionShape::read_files(std::istream& left, std::istream& right){
  shape_left.read_file(left, true);
  shape_right.read_file(right, true);
  return true;
}


void UnionShape::clear() {
  simplices.clear();
  simplex_to_ui.clear();
  birthcodes.clear();
  boundaries.clear();
  bh.clear();
}


std::set<UnionShape::UnionIndex> convert_bdd(Shape::FiltrationIndex idx,
                                             const std::map<Shape::Simplex, UnionShape::UnionIndex>& s_to_ui,
                                             const Shape& shape){
  std::set<UnionShape::UnionIndex> bdd;
  for (Shape::FiltrationIndex bdd_i : shape.get_boundary_at(idx)) {
    Shape::Simplex simplex = shape.get_simplex_at(bdd_i);
    UnionShape::UnionIndex bdd_u = s_to_ui.at(simplex);
    bdd.insert(bdd_u);
  }
  return bdd;
}

bool UnionShape::add_simplex(Shape::Simplex simplex, const quivers::BirthType & bt){
  auto it = simplex_to_ui.find(simplex);
  UnionShape::UnionIndex idx;
  bool added = false;
  if (it != simplex_to_ui.end()) {
    // is common to both, was already added..
    idx = it->second;
    quivers::BirthType new_type = bh.get_type(birthcodes[idx]).get_union_with(bt);
    birthcodes[idx] = bh.add_type(new_type);    
  } else {
    idx = simplex_to_ui.size();
    simplex_to_ui[simplex] = idx;
    simplices.push_back(simplex);
    birthcodes.push_back(bh.add_type(bt));
    added = true;
  }
  return added;
}


quivers::QuiverComplex<gyoza::Algebra::SetChain> UnionShape::compute_ladder(FT r, FT s) {
  current_r = r;
  current_s = s;
  clear();

  using BirthType=gyoza::quivers::BirthType;
  // things born before r
  const BirthType leftonly_UD = {1,1,0,1,1,0};
  const BirthType rightonly_UD = {0,1,1,0,1,1};
  
  const BirthType leftonly_U = {0,0,0,1,1,0};
  const BirthType rightonly_U = {0,0,0,0,1,1};
  

  for (int dim = 0; dim < 4; ++dim) {
    for (const auto & lx_i : shape_left.get_simplices_between(boost::none,r,dim)) {
      bool is_new = add_simplex( lx_i.first, leftonly_UD);
      if (is_new) {
        auto bdd = convert_bdd(lx_i.second, simplex_to_ui, shape_left);
        boundaries.push_back(bdd);
      }
    }

    for (const auto & rx_i : shape_right.get_simplices_between(boost::none,r,dim)) {
      bool is_new = add_simplex( rx_i.first, rightonly_UD);
      if (is_new) {
        auto bdd = convert_bdd(rx_i.second, simplex_to_ui, shape_right);
        boundaries.push_back(bdd);
      }
    }

    // Simplices born on or after s;
    for (const auto & lx_i : shape_left.get_simplices_between(r,s,dim)) {
      bool is_new = add_simplex( lx_i.first, leftonly_U);
      if (is_new) {
        auto bdd = convert_bdd(lx_i.second, simplex_to_ui, shape_left);
        boundaries.push_back(bdd);
      }
    }

    for (const auto & rx_i : shape_right.get_simplices_between(r, s,dim)) {
      bool is_new = add_simplex( rx_i.first, rightonly_U);
      if (is_new) {
        auto bdd = convert_bdd(rx_i.second, simplex_to_ui, shape_right);
        boundaries.push_back(bdd);
      }
    }
  }

  return get_current_quivercomplex();
}

quivers::QuiverComplex<gyoza::Algebra::SetChain> UnionShape::get_current_quivercomplex() const {
  quivers::QuiverComplex<gyoza::Algebra::SetChain> qc;

  quivers::Quiver ladderfb = quivers::create_ladder_quiver(quivers::QuiverType::Ladder_FB);
  qc.set_quiver(ladderfb);
  qc.set_births(bh);

  for (UnionIndex current_simplex = 0; current_simplex < simplices.size(); ++current_simplex) {
    gyoza::Algebra::SetChain bdd;
    for (const auto & x : boundaries.at(current_simplex)) {
      bdd.set_entry(x,1);
    }
    int dim = simplices.at(current_simplex).size()-1;
    qc.create_cell( dim, bdd, birthcodes.at(current_simplex));
  }
  return qc;
}

}
}
