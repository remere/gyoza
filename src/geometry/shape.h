#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>

#include "gyoza/cgal_header_defs.h"
#include "gyoza/quivercomplex.h"
#include "gyoza/setchain.h"

#include <boost/optional.hpp>

namespace gyoza {
namespace geometry {

class Shape {

 public:
  typedef std::vector<Weighted_point> PointContainer;
  typedef PointContainer::size_type OriginalIndex;

  typedef std::vector<CGAL::Object> CellContainer;
  typedef CellContainer::size_type FiltrationIndex;

  typedef std::set<OriginalIndex> Simplex;

  Shape():shape3(0, Alpha_shape_3::GENERAL){}
  Shape(const Shape&) = delete;

  PointContainer get_points()const{return lp;}
  void read_file(std::istream& is, bool do_square_radius=true);

  void output_slice(FT alpha, std::ostream& output) const ;
  std::vector<std::pair<Simplex,FiltrationIndex>> get_simplices_between(boost::optional<FT> alpha0, boost::optional<FT> alpha1, int dim) const;
  std::vector<std::set<FiltrationIndex>> get_boundaries()const {return boundaries;}
  std::set<FiltrationIndex> get_boundary_at(FiltrationIndex idx)const {
    return boundaries.at(idx);
  }
  Simplex get_simplex_at(FiltrationIndex idx) const;
  

  quivers::QuiverComplex<gyoza::Algebra::SetChain> create_quiver_complex(std::vector<FT> cuts) const;

  

 private:
  PointContainer lp;
  Alpha_shape_3 shape3;

  std::map<Vertex_handle, OriginalIndex> handle_to_original_index;

  std::vector<CGAL::Object> cells;
  std::vector<FT> alphas;
  std::vector<std::set<FiltrationIndex>> boundaries;

  OriginalIndex find_original_index(const Weighted_point&)const;
  void update_vertex_handle_to_original_index();
  void create_filtration();
  void compute_boundaries();

};

}
}

#endif
