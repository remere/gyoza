#ifndef UNION_SHAPE_H
#define UNION_SHAPE_H

#include "gyoza/cgal_header_defs.h"
#include "gyoza/shape.h"

#include "gyoza/quivercomplex.h"
#include "gyoza/setchain.h"

#include <iostream>
#include <map>

namespace gyoza {
namespace geometry {
class UnionShape{
 public: 
  typedef Shape::OriginalIndex OriginalIndex;
  typedef Shape::Simplex Simplex;
  typedef unsigned int UnionIndex;
  
  UnionShape() = default;
  void clear();
    
  bool read_files(std::istream& left, std::istream& right);

  // r < s
  quivers::QuiverComplex<gyoza::Algebra::SetChain> compute_ladder(FT r, FT s);
  quivers::QuiverComplex<gyoza::Algebra::SetChain> get_current_quivercomplex() const;

  bool add_simplex(Simplex simplex, const quivers::BirthType & bt);
    
      
 private:
  Shape shape_left;
  Shape shape_right;

  FT current_r;
  FT current_s;

  quivers::BirthHandler bh;  
  std::vector<Simplex> simplices;
  std::map<Simplex, UnionIndex> simplex_to_ui;
  std::vector<gyoza::quivers::BirthHandler::BirthCode> birthcodes;
  std::vector<std::set<UnionIndex>> boundaries;

  

  
};

}
}

#endif
