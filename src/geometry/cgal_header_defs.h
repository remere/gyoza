#ifndef CGAL_HEADER_DEFS_H
#define CGAL_HEADER_DEFS_H

// CGAL includes
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
// #include <CGAL/Regular_triangulation_euclidean_traits_3.h>
#include <CGAL/Regular_triangulation_3.h>
#include <CGAL/Alpha_shape_3.h>
#include <CGAL/Alpha_shape_vertex_base_3.h>

// CGAL typedefs, exact alpha comparisons:
typedef CGAL::Exact_predicates_inexact_constructions_kernel Ker;

typedef CGAL::Regular_triangulation_vertex_base_3<Ker> Rvb;
typedef CGAL::Regular_triangulation_cell_base_3<Ker> Rcb;


// typedef CGAL::Regular_triangulation_euclidean_traits_3<Ker> Gt_3;

typedef CGAL::Alpha_shape_vertex_base_3<Ker, Rvb, CGAL::Tag_true, CGAL::Tag_true> Vb_3;
typedef CGAL::Alpha_shape_cell_base_3<Ker, Rcb, CGAL::Tag_true, CGAL::Tag_true> Cb_3;

typedef CGAL::Triangulation_data_structure_3<Vb_3, Cb_3>  Ts_3;

typedef CGAL::Regular_triangulation_3<Ker, Ts_3>         Triangulation_3;
typedef CGAL::Alpha_shape_3<Triangulation_3, CGAL::Tag_true>              Alpha_shape_3;

typedef Alpha_shape_3::FT          FT;

typedef Alpha_shape_3::Cell_handle          Cell_handle;
typedef Alpha_shape_3::Vertex_handle        Vertex_handle;
typedef Alpha_shape_3::Facet                Facet;
typedef Alpha_shape_3::Edge                 Edge;
typedef Triangulation_3::Weighted_point                Weighted_point;
typedef Triangulation_3::Bare_point                    Bare_point;

typedef Triangulation_3::Finite_cells_iterator    Finite_cells_iterator;
typedef Triangulation_3::Finite_facets_iterator   Finite_facets_iterator;
typedef Triangulation_3::Finite_edges_iterator    Finite_edges_iterator;
typedef Triangulation_3::Finite_vertices_iterator Finite_vertices_iterator;

typedef Triangulation_3::Cell_circulator Cell_circulator;

// typedef CGAL::Alpha_status<Alpha_shape_3::FT>                Alpha_status;
// typedef CGAL::Compact_container<Alpha_status>       Alpha_status_container;
// typedef Alpha_status_container::iterator            Alpha_status_iterator;
// typedef std::pair<Vertex_handle, Vertex_handle>     Vertex_handle_pair;

#endif
