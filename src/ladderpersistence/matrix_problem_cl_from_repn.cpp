#include "gyoza/matrix_problem_cl.h"
#include "gyoza/snf_algorithms.hpp"
#include "gyoza/algo.h"

namespace gyoza {
namespace ladderpersistence {


matrixproblems::LadderMatrixProblem convert_clf(quivers::QuiverRepn repn){
  matrixproblems::LadderMatrixProblem ans;
  std::vector<matrixproblems::BlockAddress> nonzeros;
  set_cl_labels<quivers::QuiverType::Ladder_F>(ans, nonzeros);

  algorithms::FullPivotSNF<Z2Matrix> snf_upper( repn.matrix_at(2,3), true);
  algorithms::FullPivotSNF<Z2Matrix> snf_lower( repn.matrix_at(0,1), true );

  int up_12 = snf_upper.rank();
  int up_11 = snf_upper.cols() - up_12;
  int up_22 = snf_upper.rows() - up_12;

  int lw_12 = snf_lower.rank();
  int lw_11 = snf_lower.cols() - lw_12;
  int lw_22 = snf_lower.rows() - lw_12;


  // transformed arrow:
  Z2Matrix left = snf_upper.get_Q_inv() * repn.matrix_at(0,2) * snf_lower.get_Q();
  Z2Matrix right = snf_upper.get_P() * repn.matrix_at(1,3) * snf_lower.get_P_inv();

  // blocks
  typedef gyoza::matrixproblems::BlockAddress BA;
  // 1:2 to 1:2
  ans[ BA(1,1) ] = left.block(0,0, up_12, lw_12);

  // 2:2 to 2:2
  ans[ BA(2,0) ] = right.block(up_12,lw_12, up_22, lw_22);

  // 1:1 to 1:1
  ans[ BA(0,2) ] = left.block(up_12,lw_12, up_11, lw_11);

  // 2:2 to 1:2
  ans[ BA(1,0) ] = right.block(0,lw_12, up_12, lw_22);

  // 1:2 to 1:1
  ans[ BA(0,1) ] = left.block(up_12, 0, up_11, lw_12);

  return ans;

}

matrixproblems::LadderMatrixProblem convert_clfb(quivers::QuiverRepn repn){
  matrixproblems::LadderMatrixProblem ans;
  std::vector<matrixproblems::BlockAddress> nonzeros;
  set_cl_labels<quivers::QuiverType::Ladder_FB>(ans, nonzeros);

  std::map<std::pair<int,int>, int> pd_up;
  std::tuple<Z2Matrix,Z2Matrix,Z2Matrix> up_P = algorithms::solve_zigzag_fb(repn.matrix_at(3,4), repn.matrix_at(5,4), &pd_up);

  std::map<std::pair<int,int>, int> pd_lw;
  std::tuple<Z2Matrix,Z2Matrix,Z2Matrix> lw_P = algorithms::solve_zigzag_fb(repn.matrix_at(0,1), repn.matrix_at(2,1), &pd_lw);

  // vertical arrows, before transformation induced by solve_zigzag
  Z2Matrix phi_0 =  repn.matrix_at(0,3);
  Z2Matrix phi_1 =  repn.matrix_at(1,4);
  Z2Matrix phi_2 =  repn.matrix_at(2,5);

  // do the necessary transforms;
  algorithms::FullPivotSNF<Z2Matrix> inverter;

  inverter.compute( std::get<0>(up_P) );
  phi_0 = inverter.get_inverse().get() * phi_0 * std::get<0>(lw_P);

  inverter.compute( std::get<1>(lw_P) );
  phi_1 = std::get<1>(up_P) * phi_1 * inverter.get_inverse().get();

  inverter.compute( std::get<2>(up_P) );
  phi_2 = inverter.get_inverse().get() * phi_2 * std::get<2>(lw_P);


  using BA = matrixproblems::BlockAddress;
  /* Recall:
    col names:
      {{0,"2:2"},
      {1,"2:3"},
      {2,"1:2"},
      {3,"1:3"},
      {4,"1:1"},
      {5,"3:3"}}
    row names is reversed.
  */
  // row 0, corresponds to of 3:3 of up.
  ans[BA{0,1}] = phi_2.bottomRows(pd_up.at({3,3})).leftCols(pd_lw.at({2,3}));
  ans[BA{0,3}] = phi_2.bottomRows(pd_up.at({3,3})).middleCols(pd_lw.at({2,3}),
                                                              pd_lw.at({1,3}));
  ans[BA{0,5}] = phi_2.bottomRows(pd_up.at({3,3})).rightCols(pd_lw.at({3,3}));

  // row 1, corresponds to 1:1 of up
  ans[BA{1,3}] = phi_0.bottomRows(pd_up.at({1,1})).leftCols(pd_lw.at({1,3}));
  ans[BA{1,2}] = phi_0.bottomRows(pd_up.at({1,1})).middleCols(pd_lw.at({1,3}),
                                                              pd_lw.at({1,2}));
  ans[BA{1,4}] = phi_0.bottomRows(pd_up.at({1,1})).rightCols(pd_lw.at({1,1}));

  // row 2, corresponds to 1:3 of up
  ans[BA{2,3}] = phi_0.topRows(pd_up.at({1,3})).leftCols(pd_lw.at({1,3}));
  ans[BA{2,2}] = phi_0.topRows(pd_up.at({1,3})).middleCols(pd_lw.at({1,3}),
                                                           pd_lw.at({1,2}));

  // row 3, 1:4 of up
  ans[BA{3,2}] = phi_0.middleRows(pd_up.at({1,3}),
                                  pd_up.at({1,2})).middleCols(pd_lw.at({1,3}),
                                                              pd_lw.at({1,2}));

  // col 0, corresponds to 2:2 of lw
  ans[BA{2,0}] = phi_1.rightCols(pd_lw.at({2,2})).topRows(pd_up.at({1,3}));
  ans[BA{3,0}] = phi_1.rightCols(pd_lw.at({2,2})).middleRows(pd_up.at({1,3}),
                                                            pd_up.at({1,2}));
  ans[BA{4,0}] = phi_1.rightCols(pd_lw.at({2,2})).middleRows(pd_up.at({1,3})+
                                                            pd_up.at({1,2}),
                                                            pd_up.at({2,3}));
  ans[BA{5,0}] = phi_1.rightCols(pd_lw.at({2,2})).bottomRows(pd_up.at({2,2}));

  // col 1, corresponds to 2:3 of lw
  ans[BA{2,1}] = phi_1.middleCols(pd_lw.at({1,3}) + pd_lw.at({1,2}),
                                   pd_lw.at({2,3})).topRows(pd_up.at({1,3}));
  ans[BA{4,1}] = phi_1.middleCols(pd_lw.at({1,3}) + pd_lw.at({1,2}),
                                   pd_lw.at({2,3})).middleRows(pd_up.at({1,3})+
                                                            pd_up.at({1,2}),
                                                            pd_up.at({2,3}));
  return ans;
}


}
}
