#include "gyoza/ladder_indec.h"

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/optional.hpp>

namespace gyoza{
namespace matrixproblems {

template<unsigned int N>
IntervalIndec<N>::IntervalIndec(unsigned int b, unsigned int d): birth(0), death(N+1){
  set_birth(b);
  set_death(d);
}

template<unsigned int N>
unsigned int IntervalIndec<N>::get_birth()const{
  return birth;
}

template<unsigned int N>
unsigned int IntervalIndec<N>::get_death()const{
  return death;
}

template<unsigned int N>
std::vector<int> IntervalIndec<N>::get_dimension_vector()const {
  std::vector<int> ans(N,0);
  if (not is_valid()) {
    return ans;
  }
  for (unsigned int i = birth; i <= death; ++i) {
    ans[i-1] = 1;
  }
  return ans;
}

template<unsigned int N>
void IntervalIndec<N>::set_birth(unsigned int b) {
  if (0 < b && b <= N && b <= death) {
    birth = b;
  }
}

template<unsigned int N>
void IntervalIndec<N>::set_death(unsigned int d) {
  if (birth <= d && 0 < d && d <= N) {
    death = d;
  }
}

template<unsigned int N>
bool IntervalIndec<N>::is_valid()const{
  return (0 < birth && birth <= N &&
          0 < death && death <= N &&
          birth <= death);
}

template<unsigned int N>
bool IntervalIndec<N>::operator==(const IntervalIndec<N>& other) const{
  return (birth == other.birth) && (death == other.death);
}

template<unsigned int N>
bool IntervalIndec<N>::operator<(const IntervalIndec<N>& other) const{
  return std::make_pair(birth, death) < std::make_pair(other.birth, other.death);
}

template<unsigned int N>
boost::optional<IntervalIndec<N>> string_to_interval(const std::string& code) {
  std::vector<std::string> vstrings;
  boost::split(vstrings, code, boost::is_any_of(":,"), boost::token_compress_on); 
  if (vstrings.size() != 2) {
    return boost::none;
  }
  IntervalIndec<N> ans(std::stoi(vstrings[0]), std::stoi(vstrings[1])); 
  if (not ans.is_valid()){
    return boost::none;
  }
  return ans;
}


// ********** Ladder Indecomposables ********************

template<unsigned int N>
void LadderIndec<N>::insert_to_row_space(IntervalIndec<N> ind){
  if (ind.is_valid()) {
    row_space.insert(ind);
  }
}

template<unsigned int N>
void LadderIndec<N>::insert_to_col_space(IntervalIndec<N> ind){
  if (ind.is_valid()) {
    col_space.insert(ind);
  }
}

template<unsigned int N>
std::string LadderIndec<N>::to_string()const{
  std::stringstream ss;
  ss << *this;
  return ss.str();
}

template<unsigned int N>
std::vector<int> LadderIndec<N>::get_dimension_vector()const{
  std::vector<int> ans(2*N, 0);
  for (const auto & col : col_space) {
    std::vector<int> summand_dimvec = col.get_dimension_vector();
    for (unsigned int i = 0; i < N; ++i) {
      ans[i] += summand_dimvec.at(i);
    }
  }

  for (const auto & row : row_space) {
    std::vector<int> summand_dimvec = row.get_dimension_vector();
    for (unsigned int i = 0; i < N; ++i) {
      ans[N+i] += summand_dimvec.at(i);
    }
  }
  return ans;
}

template<unsigned int N>
bool LadderIndec<N>::operator==(const LadderIndec& other)const {
  return (row_space == other.row_space) && (col_space == other.col_space);
}

template<unsigned int N>
bool LadderIndec<N>::operator<(const LadderIndec& other)const {
  // TODO: fix
  return std::make_pair(row_space,col_space) <
    std::make_pair(other.row_space, other.col_space);
}


template<unsigned int N>
boost::optional<LadderIndec<N>> abstract_to_ladder_indec(const AbstractIndec & ind) {
  LadderIndec<N> lad;
  for (const auto & row_name : ind.involved_rows) {
    boost::optional<IntervalIndec<N>> as_int = string_to_interval<N>(row_name);
    if (as_int == boost::none) {
      return boost::none;
    }
    lad.insert_to_row_space(as_int.get()); 
  }
  for (const auto & col_name : ind.involved_cols) {
    boost::optional<IntervalIndec<N>> as_int = string_to_interval<N>(col_name);
    if (as_int == boost::none) {
      return boost::none;
    }
    lad.insert_to_col_space(as_int.get()); 
  }
  return lad; 
}


template class IntervalIndec<2u>;
template class IntervalIndec<3u>;
template class IntervalIndec<4u>;

template class LadderIndec<2u>;
template class LadderIndec<3u>;
template class LadderIndec<4u>;

template
boost::optional<LadderIndec<2u>> abstract_to_ladder_indec<2u>(const AbstractIndec &);

template
boost::optional<LadderIndec<3u>> abstract_to_ladder_indec<3u>(const AbstractIndec &);

template
boost::optional<LadderIndec<4u>> abstract_to_ladder_indec<4u>(const AbstractIndec & ind);

template
boost::optional<IntervalIndec<2u>> string_to_interval<2u>(const std::string&);

template
boost::optional<IntervalIndec<3u>> string_to_interval<3u>(const std::string&);

template
boost::optional<IntervalIndec<4u>> string_to_interval<4u>(const std::string&);

}
}
