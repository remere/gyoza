#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "gyoza/gyoza_ladderpersistence.h"
// #include "gyoza/repnclfb.h"
#include <boost/optional/optional_io.hpp>

namespace mp = gyoza::matrixproblems;
namespace lp = gyoza::ladderpersistence;
namespace qv = gyoza::quivers;

using BA=mp::BlockAddress;
using Z2Matrix=gyoza::Z2Matrix;

TEST_CASE("Ladder Matrix Problem - empty", "[laddermatrixproblem]") {
  mp::LadderMatrixProblem x;

  SECTION("Empty lmp") {
    SECTION("Unitialized") {
      CHECK(x.verify_status());

      boost::optional<int> n = x.number_of_noniso_summands();
      CHECK(n != boost::none);
      CHECK(n.get() == 0);
    }

    SECTION("Empty Initialized") {
      x.initialize();

      CHECK(x.verify_status());
      boost::optional<int> n = x.number_of_noniso_summands();
      CHECK(n != boost::none);
      CHECK(n.get() == 0);
    }

    SECTION("Empty uninitialized, reduced") {
      x.matrix_reduction(100);
      CHECK(x.verify_status());
      boost::optional<int> n = x.number_of_noniso_summands();
      CHECK(n != boost::none);
      CHECK(n.get() == 0);
    } 
  }
}

TEST_CASE("Ladder Matrix Problem - status", "[laddermatrixproblem]") {
  mp::LadderMatrixProblem x;
  
  SECTION("status at init") {
    x[BA(1,1)] = Z2Matrix::Identity(2,2);
    x[BA(2,2)] = Z2Matrix::Zero(3,3);

    SECTION("Unitialized status") {
      CHECK_FALSE(x.verify_status());
      CHECK(x.get_status_inconsistent_blocks().size() == 2); 
    }

    SECTION("Status does not reflect actual numeric value at init") {
      x.initialize_status();
      const mp::StatusHandler xstat = x.get_status();
      CHECK(xstat.get_status(BA(1,1)) == mp::BlockStatus::Unprocessed);
      CHECK(xstat.get_status(BA(2,2)) == mp::BlockStatus::Unprocessed);
      CHECK(xstat.has_no_unprocessed() == false);
      // initial status should always be valid.
      CHECK(x.verify_status());
    }
  } 
}

TEST_CASE("Ladder Matrix Problem - reduction", "[laddermatrixproblem]") {
  mp::LadderMatrixProblem phi = lp::MatrixProblemCL<qv::QuiverType::Ladder_FB>::create_random_problem(10);
  phi.initialize();

  SECTION("reduction - commutative monoid property"){

    SECTION("identity") {
      const mp::LadderMatrixProblem expected = phi;
      phi.matrix_reduction(0);
      CHECK(phi == expected);
      CHECK(phi.verify_status());
    }

    SECTION("1+1=2") {
      mp::LadderMatrixProblem phi_copy = phi;
      phi_copy.matrix_reduction(2);
      
      phi.matrix_reduction(1);
      phi.matrix_reduction(1);
      
      CHECK(phi == phi_copy);
      CHECK(phi.verify_status());
    }

    SECTION("1+2=2+1") {
      mp::LadderMatrixProblem phi_copy = phi;
      phi_copy.matrix_reduction(2);
      phi_copy.matrix_reduction(1);
      
      phi.matrix_reduction(1);
      phi.matrix_reduction(2);
      
      CHECK(phi == phi_copy);
      CHECK(phi.verify_status());
    } 

  }

}


// ************************************************************
// to be separated, then hidden, after *planned* refactor 

TEST_CASE("Ladder Matrix Problem - internals", "[laddermatrixproblem]") {
  SECTION("utility") {
    const Z2Matrix x = Z2Matrix::Identity(2,2);
    const Z2Matrix y = Z2Matrix::Identity(3,3);
    mp::LadderMatrixProblem::TransformPair t(x,y);
    
    CHECK(t.trans == x);
    CHECK(t.invtrans == y); 
  }

  mp::LadderMatrixProblem phi;
  
  SECTION("edge reduction - row side effect first") {
    phi[BA(0,0)] = Z2Matrix::Random(5, 7);
    phi[BA(0,1)] = Z2Matrix::Identity(5, 5);
    phi[BA(1,1)] = Z2Matrix::Identity(5, 5);
    phi[BA(1,2)] = Z2Matrix::Identity(5, 5);
    phi.initialize();
    /*
      [ *E. ]
      [ .EE ]
    */
    phi.set_status(BA(0,1),mp::BlockStatus::Identity);
    phi.set_status(BA(1,1),mp::BlockStatus::Identity);
    phi.set_status(BA(1,2),mp::BlockStatus::Identity);

    CHECK(phi.verify_status());

    phi.edge_reduction(BA(0,0));    

    CHECK(phi.verify_status());
    CHECK(phi.at(BA(0,1)) == Z2Matrix::Identity(5, 5));
    CHECK(phi.at(BA(1,1)) == Z2Matrix::Identity(5, 5));
    CHECK(phi.at(BA(1,2)) == Z2Matrix::Identity(5, 5)); 
  }

  SECTION("edge reduction - loops!") {
    SECTION("loop rooted at edge reduction target") {
      phi[BA(0,0)] = Z2Matrix::Random(5, 5);
      phi[BA(0,1)] = Z2Matrix::Identity(5, 5);
      phi[BA(1,0)] = Z2Matrix::Identity(5, 5);
      phi[BA(1,1)] = Z2Matrix::Identity(5, 5);
      phi.initialize();
      /*
        [ *E ]
        [ EE ]
      */
      phi.set_status(BA(0,1),mp::BlockStatus::Identity);
      phi.set_status(BA(1,0),mp::BlockStatus::Identity);
      phi.set_status(BA(1,1),mp::BlockStatus::Identity);
      CHECK(phi.verify_status()); 
      
      CHECK_THROWS(phi.edge_reduction(BA(0,0)));
    }

    SECTION("loop not containing edge reduction target, row first") {
      phi[BA(0,0)] = Z2Matrix::Random(5, 7);
      phi[BA(0,1)] = Z2Matrix::Identity(5, 5);
      phi[BA(0,2)] = Z2Matrix::Identity(5, 5);
      phi[BA(1,1)] = Z2Matrix::Identity(5, 5);
      phi[BA(1,2)] = Z2Matrix::Identity(5, 5);      
      phi.initialize();
      /*
        [ *EE ]
        [ .EE ]
      */
      phi.set_status(BA(0,1),mp::BlockStatus::Identity);
      phi.set_status(BA(0,2),mp::BlockStatus::Identity);
      phi.set_status(BA(1,1),mp::BlockStatus::Identity);
      phi.set_status(BA(1,2),mp::BlockStatus::Identity);
      CHECK(phi.verify_status()); 
      
      CHECK_THROWS(phi.edge_reduction(BA(0,0)));
    }

    SECTION("loop not containing edge reduction target, col first") {
      phi[BA(0,0)] = Z2Matrix::Random(7, 5);
      phi[BA(1,0)] = Z2Matrix::Identity(5, 5);
      phi[BA(2,0)] = Z2Matrix::Identity(5, 5);
      phi[BA(1,1)] = Z2Matrix::Identity(5, 5);
      phi[BA(2,1)] = Z2Matrix::Identity(5, 5);      
      phi.initialize();
      /*
        [ *. ]
        [ EE ]
        [ EE ]
      */
      phi.set_status(BA(1,0),mp::BlockStatus::Identity);
      phi.set_status(BA(2,0),mp::BlockStatus::Identity);
      phi.set_status(BA(1,1),mp::BlockStatus::Identity);
      phi.set_status(BA(2,1),mp::BlockStatus::Identity);
      CHECK(phi.verify_status()); 
      
      CHECK_THROWS(phi.edge_reduction(BA(0,0)));
    } 
  }

  SECTION("regularization - flagged matrix shouldn't be used") {
    phi[BA(0,0)] = Z2Matrix::Zero(5,5);    
    phi[BA(0,1)] = Z2Matrix::Random(5,5);
    
    phi[BA(1,0)] = Z2Matrix::Identity(5, 5);
    phi[BA(1,1)] = Z2Matrix::Identity(5, 5);    
    phi.initialize();
    phi.get_permissible_row_operations().make_permissible(1, 0);    
    /*
          0 1 
      0 [ 0 * ]
      1 [ E E ]
      row ops: 1 to 0
    */
    phi.set_status(BA(0,0),mp::BlockStatus::Zero); 
    phi.set_status(BA(1,0),mp::BlockStatus::Identity);
    phi.set_status(BA(1,1),mp::BlockStatus::Identity);
    CHECK(phi.verify_status()); 

    std::set<BA> visited;
    mp::ProcessTree pt =  phi.row_erasable(BA(0,1), nullptr, visited);
    CHECK(pt.get_root().get() == nullptr);
  }
      





  
  SECTION("regularization - loops?") {
    phi[BA(0,0)] = Z2Matrix::Zero(5,5);
    phi[BA(0,1)] = Z2Matrix::Identity(5,5);
    phi[BA(0,2)] = Z2Matrix::Random(5,5);
    
    phi[BA(1,0)] = Z2Matrix::Zero(5, 5);
    phi[BA(1,1)] = Z2Matrix::Random(5, 5);
    phi[BA(1,2)] = Z2Matrix::Identity(5, 5);
    
    phi[BA(2,0)] = Z2Matrix::Random(5, 5);
    phi[BA(2,1)] = Z2Matrix::Identity(5, 5);      
    phi.initialize();
    phi.get_permissible_row_operations().make_permissible(2, 1);
    phi.get_permissible_col_operations().make_permissible(1, 0);
    phi.get_permissible_col_operations().make_permissible(2, 0);
    /*
          0 1 2
      0 [ 0 E * ]
      1 [ 0 * E ]
      2 [ * E . ]
      row ops: 2 to 1,
      col ops: 1 to 0, 2 to 0
    */
    phi.set_status(BA(0,0),mp::BlockStatus::Zero);
    phi.set_status(BA(1,0),mp::BlockStatus::Zero);
    
    phi.set_status(BA(0,1),mp::BlockStatus::Identity);
    phi.set_status(BA(1,2),mp::BlockStatus::Identity); 
    phi.set_status(BA(2,1),mp::BlockStatus::Identity);
    phi.pretty_print_status(std::cerr);
    CHECK(phi.verify_status()); 

    std::set<BA> visited;
    CHECK_THROWS(phi.row_erasable(BA(1,1), nullptr, visited ));
  } 
}



TEST_CASE("Ladder Matrix Problem - status - internals", "[laddermatrixproblem]") {
  mp::LadderMatrixProblem x; 
  
  x[BA(1,1)] = Z2Matrix::Identity(2,2);
  x[BA(2,2)] = Z2Matrix::Zero(3,3); 
    
  SECTION("incomplete specification") {
    x.set_status(BA(1,1), mp::BlockStatus::Unprocessed);
    CHECK_FALSE(x.verify_status());
  }

  SECTION("over-specification") {
    x.initialize_status();
    CHECK(x.verify_status());
      
    x.set_status(BA(3,3), mp::BlockStatus::Unprocessed);
    std::vector<BA> as = x.get_status_inconsistent_blocks(); 
    CHECK_FALSE(x.verify_status());

    // null status is ok
    x.set_status(BA(3,3), mp::BlockStatus::Null);
    CHECK(x.verify_status()); 
  }

  SECTION("incorrect specification") {
    x.initialize_status();

    SECTION("status zero") {
      x.set_status(BA(1,1), mp::BlockStatus::Zero);
      CHECK_FALSE(x.verify_status());
    }

    SECTION("status identity") {
      x.set_status(BA(2,2), mp::BlockStatus::Identity);
      CHECK_FALSE(x.verify_status());
    }

    SECTION("status null") {
      x.set_status(BA(2,2), mp::BlockStatus::Null);
      CHECK_FALSE(x.verify_status());
    } 
  } 
}
