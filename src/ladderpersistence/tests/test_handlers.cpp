#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <map>
#include "gyoza/orderhandler.h"
#include "gyoza/statushandler.h"


namespace mp = gyoza::matrixproblems;

TEST_CASE("Orderhandler - trivialities", "[orderhandler]") {
  SECTION("empty") {
    mp::OrderHandler x;
    CHECK(x.size() == 0);
    CHECK_FALSE(x.has_value(0));
  }

  SECTION("max < min") {
    mp::OrderHandler x(4,-2);
    CHECK(x.size() == 0);

    mp::OrderHandler y(4,3);
    CHECK(y.size() == 0);
    CHECK_FALSE(y.has_value(4));

    mp::OrderHandler z(4,4);
    CHECK(z.size() == 1);
    CHECK(z.has_value(4));
  }
}

TEST_CASE("Orderhandler - actual", "[orderhandler]") {
  mp::OrderHandler x(-2,4);

  SECTION("consective numbers") {
    CHECK(x.size() == 7);
    CHECK(x.has_value(4));
    CHECK(x.has_value(-2));

    int expected_value = -2;
    mp::OrderHandler::size_type sz = 0;
    for (auto it = x.begin(); it != x.end(); ++it) {
      CHECK(*it == expected_value);
      ++expected_value;
      ++sz;
    }
    CHECK(sz == x.size());
  }

  SECTION("push back") {
    CHECK(x.push_back(-3));
    CHECK(x.size() == 8);

    // value is already in x, rejected!
    CHECK_FALSE(x.push_back(2));
    CHECK(x.size() == 8);

    const std::vector<int> expected_values = {-2,-1,0,1,2,3,4,-3};
    auto jt = expected_values.cbegin();
    for (auto it = x.begin(); it != x.end(); ++it) {
      CHECK(*it == *jt);
      ++jt;
    }
  }

  SECTION("push front") {
    CHECK(x.push_front(100));
    CHECK(x.size() == 8);

    // value is already in x, rejected!
    CHECK_FALSE(x.push_front(-2));
    CHECK(x.size() == 8);

    const std::vector<int> expected_values = {100,-2,-1,0,1,2,3,4};
    auto jt = expected_values.cbegin();
    for (auto it = x.begin(); it != x.end(); ++it) {
      CHECK(*it == *jt);
      ++jt;
    }
  }

  SECTION("insert after") {
    SECTION("middle") {
      CHECK(x.insert_after(2,100));
      CHECK(x.size() == 8);

      const std::vector<int> expected_values = {-2,-1,0,1,2,100,3,4};
      auto jt = expected_values.cbegin();
      for (auto it = x.begin(); it != x.end(); ++it) {
        CHECK(*it == *jt);
        ++jt;
      }
    }
    SECTION("duplicate value") {
      CHECK_FALSE(x.insert_after(2,3));
      CHECK(x.size() == 7);
    }
    SECTION("prior value not found") {
      CHECK_FALSE(x.insert_after(42,100));
      CHECK(x.size() == 7);
    }
  }
}

std::set<gyoza::matrixproblems::BlockAddress> v_to_s(std::vector<gyoza::matrixproblems::BlockAddress> inp) {
  return std::set<gyoza::matrixproblems::BlockAddress>(inp.begin(), inp.end());
}

TEST_CASE("Statushandler empty", "[statushandler]") {
  using BA = gyoza::matrixproblems::BlockAddress;
  mp::StatusHandler x;
  CHECK(x.has_no_unprocessed() == true);
  CHECK(x.size() == 0);
  CHECK(x.get_status(BA(1,1)) == mp::BlockStatus::Null);
  CHECK(x.get_identity_blocks().size() == 0);
  CHECK(x.get_partition_of_identities().size() == 0);
}

TEST_CASE("Statushandler", "[statushandler]") {
  using BA = gyoza::matrixproblems::BlockAddress;
  mp::StatusHandler x;

  for (int i = 0; i < 5; ++i) {
    x.set_status(BA(i,i), mp::BlockStatus::Identity);
  }


  SECTION("data preserved") {
    CHECK(x.has_no_unprocessed() == true);
    for (int i = 0; i < 5; ++i) {
      CHECK(x.get_status(BA(i,i)) ==  mp::BlockStatus::Identity);
    }
    std::set<BA> identities_set = v_to_s(x.get_identity_blocks());
    const std::set<BA> expected = {{0,0},{1,1},{2,2},{3,3},{4,4}};
    CHECK(identities_set == expected);
    CHECK(x.get_partition_of_identities().size() == 5);
  }

  SECTION("check bounds"){
    x.set_status(BA(-1,5), mp::BlockStatus::Identity);

    auto bdds = x.get_bounds();
    CHECK(std::get<mp::RowMin>(bdds) == -1);
    CHECK(std::get<mp::RowMax>(bdds) == 4);
    CHECK(std::get<mp::ColMin>(bdds) == 0);
    CHECK(std::get<mp::ColMax>(bdds) == 5);

    x.clear();
    CHECK_THROWS(x.get_bounds());
  }
}




TEST_CASE("Statushandler - identity queries", "[statushandler]") {
  using BA = gyoza::matrixproblems::BlockAddress;
  mp::StatusHandler x;

  x.set_status(BA(-1,-1), mp::BlockStatus::Identity);
  x.set_status(BA(0,0), mp::BlockStatus::Identity);
  x.set_status(BA(0,1), mp::BlockStatus::Identity);
  x.set_status(BA(-1,1), mp::BlockStatus::Identity);

  x.set_status(BA(1,2), mp::BlockStatus::Identity);


  const std::set<BA> expected_identities = {{-1,-1},{0,0},{0,1},{-1,1},{1,2}};

  const std::set<BA> expected_cluster1 = {{-1,-1},{0,0},{0,1},{-1,1}};
  const std::set<BA> expected_cluster2 = {{1,2}};

  SECTION("identity-only") {
    std::set<BA> identities_set = v_to_s(x.get_identity_blocks());
    CHECK(expected_identities == identities_set);

    std::vector<std::vector<BA>> partitions = x.get_partition_of_identities();
    REQUIRE(partitions.size() == 2);

    std::set<BA> first_detected_cluster = v_to_s(partitions[0]);
    std::set<BA> second_detected_cluster = v_to_s(partitions[1]);
    if (first_detected_cluster.size() == 4) {
      CHECK(first_detected_cluster == expected_cluster1);
      CHECK(second_detected_cluster == expected_cluster2);
    } else if (first_detected_cluster.size() == 1) {
      CHECK(first_detected_cluster == expected_cluster2);
      CHECK(second_detected_cluster == expected_cluster1);
    } else {
      INFO("clusters do not match!");
      CHECK(false);
    }
  }

  SECTION("with unprocessed and zero blocks") {
    x.set_status(BA(-1,0), mp::BlockStatus::Unprocessed);
    x.set_status(BA(0,-1), mp::BlockStatus::Zero);
    x.set_status(BA(1,1), mp::BlockStatus::Unprocessed);

    SECTION("identity partition"){
      std::set<BA> identities_set = v_to_s(x.get_identity_blocks());
      CHECK(expected_identities == identities_set);

      std::vector<std::vector<BA>> partitions = x.get_partition_of_identities();
      REQUIRE(partitions.size() == 2);
      std::set<BA> first_detected_cluster = v_to_s(partitions[0]);
      std::set<BA> second_detected_cluster = v_to_s(partitions[1]);
      if (first_detected_cluster.size() == 4) {
        CHECK(first_detected_cluster == expected_cluster1);
        CHECK(second_detected_cluster == expected_cluster2);
      } else if (first_detected_cluster.size() == 1) {
        CHECK(first_detected_cluster == expected_cluster2);
        CHECK(second_detected_cluster == expected_cluster1);
      } else {
        INFO("clusters do not match!");
        CHECK(false);
      }
    }

    SECTION("query without identities"){
      std::vector<int> row_indices;
      std::vector<int> col_indices;
      x.query_rows_cols_without_identities(&row_indices, &col_indices);

      CHECK(row_indices.size() == 0);
      CHECK(col_indices.size() == 0);

      x.set_status(BA(2,3), mp::BlockStatus::Unprocessed);
      x.query_rows_cols_without_identities(&row_indices, &col_indices);
      CHECK(row_indices == std::vector<int>(1,2));
      CHECK(col_indices == std::vector<int>(1,3));
    }
  }
}



TEST_CASE("Statushandler - split query", "[statushandler]") {
  using BA = gyoza::matrixproblems::BlockAddress;
  mp::StatusHandler x;
  std::set<BA> identities_split;

  SECTION("simple cases") {
    const std::set<BA> expected_identities_split;
    
    SECTION("empty") {
      x.query_splitting(BA(1,1), identities_split); 
      CHECK(identities_split == expected_identities_split);
    } 
    
    SECTION("no identity neighbors") {
      x.set_status(BA(0,0), mp::BlockStatus::Identity);
      x.set_status(BA(1,1), mp::BlockStatus::Unprocessed); 
      x.query_splitting(BA(1,1), identities_split); 
      CHECK(identities_split == expected_identities_split);
    }
    
    SECTION("null block no identity neighbors") {
      x.set_status(BA(0,0), mp::BlockStatus::Identity);
      x.query_splitting(BA(1,1), identities_split); 
      CHECK(identities_split == expected_identities_split);
    }
  } 
  

  SECTION("block to split is already identity") {
    x.set_status(BA(1,1), mp::BlockStatus::Identity);

    x.query_splitting(BA(1,1), identities_split);

    const std::set<BA> expected_identities_split = {{1,1}};
    CHECK(identities_split == expected_identities_split);
  }

  SECTION("multi-split row") {
    x.set_status(BA(0,1), mp::BlockStatus::Identity);
    x.set_status(BA(1,1), mp::BlockStatus::Unprocessed);
    x.set_status(BA(2,1), mp::BlockStatus::Identity);
    x.set_status(BA(3,1), mp::BlockStatus::Unprocessed);

    x.query_splitting(BA(1,1), identities_split);

    const std::set<BA> expected_identities_split = {{0,1},{2,1}};
    CHECK(identities_split == expected_identities_split);
  }

  SECTION("multi-split col") {
    x.set_status(BA(1,0), mp::BlockStatus::Identity);
    x.set_status(BA(1,1), mp::BlockStatus::Unprocessed);
    x.set_status(BA(1,2), mp::BlockStatus::Identity);
    x.set_status(BA(1,3), mp::BlockStatus::Unprocessed);

    x.query_splitting(BA(1,1), identities_split);

    const std::set<BA> expected_identities_split = {{1,0},{1,2}};
    CHECK(identities_split == expected_identities_split);
  }

  SECTION("box-type 1") {
    x.set_status(BA(1,1), mp::BlockStatus::Unprocessed);
    x.set_status(BA(0,1), mp::BlockStatus::Identity);
    x.set_status(BA(0,0), mp::BlockStatus::Identity);
    /*
      EE
      .*
     */

    x.query_splitting(BA(1,1), identities_split);

    const std::set<BA> expected_identities_split = {{0,1},{0,0}};
    CHECK(identities_split == expected_identities_split);
  }

  SECTION("box-type 2") {
    x.set_status(BA(1,1), mp::BlockStatus::Unprocessed);
    x.set_status(BA(1,0), mp::BlockStatus::Identity);
    x.set_status(BA(0,0), mp::BlockStatus::Identity);
    /*
      E.
      E*
     */

    x.query_splitting(BA(1,1), identities_split);

    const std::set<BA> expected_identities_split = {{1,0},{0,0}};
    CHECK(identities_split == expected_identities_split);
  }

  SECTION("box-type 3") {
    x.set_status(BA(1,1), mp::BlockStatus::Unprocessed);
    x.set_status(BA(0,1), mp::BlockStatus::Identity);
    x.set_status(BA(1,0), mp::BlockStatus::Identity);
    x.set_status(BA(0,0), mp::BlockStatus::Identity);
    /*
      EE
      E*
     */

    x.query_splitting(BA(1,1), identities_split);

    const std::set<BA> expected_identities_split = {{0,0},{0,1},{1,0}};
    CHECK(identities_split == expected_identities_split);
  }

  SECTION("box-type 4 - block to split is already identity") {
    x.set_status(BA(1,1), mp::BlockStatus::Identity);
    x.set_status(BA(0,1), mp::BlockStatus::Identity);
    x.set_status(BA(1,0), mp::BlockStatus::Identity);
    x.set_status(BA(0,0), mp::BlockStatus::Identity);
    /*
      EE
      EE
     */

    x.query_splitting(BA(1,1), identities_split);

    const std::set<BA> expected_identities_split = {{0,0},{0,1},{1,0},{1,1}};
    CHECK(identities_split == expected_identities_split);
  }

}
