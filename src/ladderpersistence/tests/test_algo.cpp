#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <map>
#include "gyoza/algo.h"
#include "gyoza/gyoza.h"
#include "gyoza/gyoza_geometry.h" 


using Z2Matrix = gyoza::Z2Matrix;

TEST_CASE("inconsistent dimensions", "[afb]") {
  Z2Matrix f(1,2);
  Z2Matrix b(2,2);

  CHECK_THROWS(gyoza::algorithms::solve_zigzag_fb(f,b)); 
}

TEST_CASE("trivialities", "[afb][algorithms]") {
  std::map<std::pair<int,int>, int> pd;

  SECTION("empty middle") {
    Z2Matrix f(0,10);
    Z2Matrix b(0,20);
    gyoza::algorithms::solve_zigzag_fb(f,b, &pd);
    CHECK(pd.at({1,3}) == 0);
    CHECK(pd.at({1,2}) == 0);
    CHECK(pd.at({2,3}) == 0);
    CHECK(pd.at({2,2}) == 0);
    CHECK(pd.at({1,1}) == 10);
    CHECK(pd.at({3,3}) == 20);
  }
  SECTION("empty sides") {
    Z2Matrix f(5,0);
    gyoza::algorithms::solve_zigzag_fb(f,f, &pd);
    CHECK(pd.at({1,3}) == 0);
    CHECK(pd.at({1,2}) == 0);
    CHECK(pd.at({2,3}) == 0);
    CHECK(pd.at({2,2}) == 5);
    CHECK(pd.at({1,1}) == 0);
    CHECK(pd.at({3,3}) == 0);
  }
  SECTION("left identity") {
    Z2Matrix f = Z2Matrix::Identity(5,5);
    Z2Matrix b(5,0);
    gyoza::algorithms::solve_zigzag_fb(f,b, &pd);
    CHECK(pd.at({1,3}) == 0);
    CHECK(pd.at({1,2}) == 5);
    CHECK(pd.at({2,3}) == 0);
    CHECK(pd.at({2,2}) == 0);
    CHECK(pd.at({1,1}) == 0);
    CHECK(pd.at({3,3}) == 0);
  }
  SECTION("right identity") {
    Z2Matrix b = Z2Matrix::Identity(6,6);
    Z2Matrix f(6,0);
    gyoza::algorithms::solve_zigzag_fb(f,b, &pd);
    CHECK(pd.at({1,3}) == 0);
    CHECK(pd.at({1,2}) == 0);
    CHECK(pd.at({2,3}) == 6);
    CHECK(pd.at({2,2}) == 0);
    CHECK(pd.at({1,1}) == 0);
    CHECK(pd.at({3,3}) == 0);
  }
  SECTION("both identity") {
    Z2Matrix f = Z2Matrix::Identity(4,4);    
    gyoza::algorithms::solve_zigzag_fb(f,f, &pd);
    CHECK(pd.at({1,3}) == 4);
    CHECK(pd.at({1,2}) == 0);
    CHECK(pd.at({2,3}) == 0);
    CHECK(pd.at({2,2}) == 0);
    CHECK(pd.at({1,1}) == 0);
    CHECK(pd.at({3,3}) == 0);
  }
  SECTION("zeros") {
    Z2Matrix f = Z2Matrix::Zero(4,5);
    Z2Matrix b = Z2Matrix::Zero(4,6);
    gyoza::algorithms::solve_zigzag_fb(f,b, &pd);
    CHECK(pd.at({1,3}) == 0);
    CHECK(pd.at({1,2}) == 0);
    CHECK(pd.at({2,3}) == 0);
    CHECK(pd.at({2,2}) == 4);
    CHECK(pd.at({1,1}) == 5);
    CHECK(pd.at({3,3}) == 6);
  } 
}


void check_transformations(const std::tuple<Z2Matrix,Z2Matrix,Z2Matrix>& trans,
                           const Z2Matrix& f,
                           const Z2Matrix& b,
                           const std::map<std::pair<int,int>, int>& pd) {
  // check that solve_zigzag_fb actually puts the matrices in
  // expected simplified form.

  // verify form of fprime
  {
    Z2Matrix fprime = std::get<1>(trans) * f * std::get<0>(trans);
    
    int size = pd.at({1,3});
    CHECK(fprime.leftCols(size).topRows(size).isIdentity());
  
    size  = pd.at({1,3}) + pd.at({1,2});
    CHECK(fprime.leftCols(size).topRows(size).isIdentity());

    Z2Matrix mask = Z2Matrix::Zero(fprime.rows(), fprime.cols());
    for (int i = 0; i < size; ++i) {
      mask(i,i) = 1;
    }
    CHECK( (fprime - mask).isZero() );
  }

  // verify form of bprime
  {
    Z2Matrix bprime = std::get<1>(trans) * b * std::get<2>(trans);
    Z2Matrix mask = Z2Matrix::Zero(bprime.rows(), bprime.cols()); 
    {
      const int size_2_to_3 = pd.at({2,3});
      const int start_row_2_to_3 = pd.at({1,3}) + pd.at({1,2});
      CHECK(bprime.leftCols(size_2_to_3).middleRows(start_row_2_to_3,
                                                    size_2_to_3).isIdentity());
      mask.leftCols(size_2_to_3).middleRows(start_row_2_to_3, size_2_to_3) =
          bprime.leftCols(size_2_to_3).middleRows(start_row_2_to_3, size_2_to_3); 
    }

    {
      const int size_1_to_3 = pd.at({1,3});
      const int start_col_1_to_3 = pd.at({2,3});
      CHECK(bprime.middleCols(start_col_1_to_3,
                              size_1_to_3).topRows(size_1_to_3).isIdentity());
      mask.middleCols(start_col_1_to_3, size_1_to_3).topRows(size_1_to_3) = 
          bprime.middleCols(start_col_1_to_3, size_1_to_3).topRows(size_1_to_3);
    }
    CHECK( (bprime - mask).isZero() );
  }

  // verify sizes
  CHECK(f.cols() == pd.at({1,3}) + pd.at({1,2}) + pd.at({1,1}));
  CHECK(b.cols() == pd.at({1,3}) + pd.at({2,3}) + pd.at({3,3}));
  CHECK(f.rows() == b.rows());
  CHECK(f.rows() == pd.at({1,3}) + pd.at({1,2}) + pd.at({2,3}) + pd.at({2,2}));
  return;
}


TEST_CASE("Generate A(fb) zigzag module by union shape, with known answer", "[afb][algorithms]") {
  gyoza::geometry::UnionShape x;
  std::ifstream left_file("sample_pc2.txt");
  std::ifstream right_file("sample_pc2e.txt");
  x.read_files(left_file, right_file);

  gyoza::quivers::QuiverComplex<gyoza::Algebra::SetChain> qc = x.compute_ladder(1,2);
  gyoza::quivers::QuiverRepn h = qc.naive_compute_persistence(1);

  std::map<std::pair<int,int>, int> pd;
  auto ans = gyoza::algorithms::solve_zigzag_fb(h.matrix_at(3,4), h.matrix_at(5,4), &pd);

  CHECK(pd.at({1,3}) == 16);
  CHECK(pd.at({1,2}) == 2);
  CHECK(pd.at({2,3}) == 1);
  CHECK(pd.at({2,2}) == 0);
  CHECK(pd.at({1,1}) == 2);
  CHECK(pd.at({3,3}) == 2);

  check_transformations(ans, h.matrix_at(3,4), h.matrix_at(5,4), pd);
}


TEST_CASE("Generate random A(fb), test zigzag transforms", "[afb][algorithms]") {
  Z2Matrix f = gyoza::algorithms::random_matrix_rank_uniform(10,11);
  Z2Matrix b = gyoza::algorithms::random_matrix_rank_uniform(10,9);

  std::map<std::pair<int,int>, int> pd;
  auto ans = gyoza::algorithms::solve_zigzag_fb(f,b, &pd);

  check_transformations(ans, f, b, pd);

}
