#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <boost/optional/optional_io.hpp>
#include "gyoza/abstract_indec.h"
#include "gyoza/ladder_indec.h"


namespace mp = gyoza::matrixproblems;

TEST_CASE("Abstract Indecomposables", "[abstractindec]") {
  mp::AbstractIndec empty_indec;
  SECTION("empty") {
    CHECK(empty_indec.involved_rows.size() == 0);
    CHECK(empty_indec.involved_cols.size() == 0);

    CHECK_FALSE(empty_indec.is_identity_arrow());
    CHECK(empty_indec == mp::AbstractIndec{});
  }

  SECTION("singletons") {
    mp::AbstractIndec h1 = mp::singletion_row_indec("hello");
    mp::AbstractIndec h2 = mp::singletion_col_indec("world");

    CHECK(h1.involved_rows.size() == 1);
    CHECK(h1.involved_cols.size() == 0);

    CHECK(h2.involved_rows.size() == 0);
    CHECK(h2.involved_cols.size() == 1);

    CHECK_FALSE(h1.is_identity_arrow());
    CHECK_FALSE(h2.is_identity_arrow());

    CHECK(empty_indec != h1);
    CHECK(empty_indec != h2);
    CHECK(h2 != h1);
  }

  SECTION("empty strings") {
    mp::AbstractIndec h1 = mp::singletion_row_indec("");
    mp::AbstractIndec h2 = mp::singletion_col_indec("");

    CHECK(empty_indec != h1);
    CHECK(empty_indec != h2);
    CHECK(h2 != h1);
  }

  SECTION("identity arrow"){
    mp::AbstractIndec id{ {"a"},{"a"}};
    CHECK(id.is_identity_arrow());
  } 
}


TEST_CASE("Interval Indecomposables", "[intervalindec]") {
  SECTION("construction") {
    mp::IntervalIndec<4> i(2,3);
    CHECK(i.get_birth() == 2);
    CHECK(i.get_death() == 3);

    SECTION("modification") {
      i.set_birth(3);
      CHECK(i.get_birth() == 3);

      // invalid modification simply ignored.
      i.set_birth(4);
      CHECK(i.get_birth() == 3);

      i.set_death(4);
      CHECK(i.get_death() == 4);

      // invalid modification simply ignored.
      i.set_death(2);
      CHECK(i.get_death() == 4);

      const mp::IntervalIndec<4> expected(3,4);
      CHECK(i == expected);
    }

    SECTION("string conversion N = 3"){
      const std::vector<std::string> strings = {"1:3", "1:2", "1:1",
                                                "2:3", "2:2",
                                                "3:3"};
      const std::vector<mp::IntervalIndec<3>> expecteds =
          {mp::IntervalIndec<3>{1,3},  mp::IntervalIndec<3>{1,2},  mp::IntervalIndec<3>{1,1}, mp::IntervalIndec<3>{2,3},  mp::IntervalIndec<3>{2,2}, mp::IntervalIndec<3>{3,3}};

      for (unsigned int i = 0; i < strings.size(); ++i) {
        std::cerr << strings.at(i) << std::endl;
        boost::optional<mp::IntervalIndec<3>> res = mp::string_to_interval<3>(strings.at(i));
        CHECK(res != boost::none);
        CHECK(res.get() == expecteds.at(i)); 
      } 
    }

    SECTION("string conversion: N = 4"){
      const std::vector<std::string> strings = {"1:4", "1:3", "1:2", "1:1",
                                                "2:4", "2:3", "2:2",
                                                "3:4", "3:3",
                                                "4:4"};

      const std::vector<std::string> strings_comma={"1,4", "1,3", "1,2", "1,1",
                                                    "2,4", "2,3", "2,2",
                                                    "3,4", "3,3",
                                                    "4,4"};
      const std::vector<mp::IntervalIndec<4>> expecteds =
          {mp::IntervalIndec<4>{1,4},  mp::IntervalIndec<4>{1,3},  mp::IntervalIndec<4>{1,2},  mp::IntervalIndec<4>{1,1},  mp::IntervalIndec<4>{2,4},  mp::IntervalIndec<4>{2,3},  mp::IntervalIndec<4>{2,2},  mp::IntervalIndec<4>{3,4},  mp::IntervalIndec<4>{3,3},  mp::IntervalIndec<4>{4,4}};

      for (unsigned int i = 0; i < strings.size(); ++i) {
        boost::optional<mp::IntervalIndec<4>> res = mp::string_to_interval<4>(strings.at(i));
        CHECK(res != boost::none);
        CHECK(res.get() == expecteds.at(i)); 
      }

      for (unsigned int i = 0; i < strings_comma.size(); ++i) {        
        boost::optional<mp::IntervalIndec<4>> res = mp::string_to_interval<4>(strings_comma.at(i));
        CHECK(res != boost::none);
        CHECK(res.get() == expecteds.at(i)); 
      } 
    }
    
  }
  
  SECTION("invalids") {
    mp::IntervalIndec<4> i1(4,3);
    CHECK_FALSE(i1.is_valid());

    mp::IntervalIndec<4> i2(2,6);
    CHECK_FALSE(i2.is_valid());

    mp::IntervalIndec<4> i3(7,8);
    CHECK_FALSE(i3.is_valid()); 
  }
  

}



TEST_CASE("Ladder Indecomposables", "[ladderindec]") {
  // TODO: add more checks

  // test LadderIndec<N>::to_string

  SECTION("object and dimension vector") {
     mp::LadderIndec<3> ind1;
     ind1.insert_to_col_space(mp::IntervalIndec<3>{1,3});
     ind1.insert_to_col_space(mp::IntervalIndec<3>{2,2});

     mp::LadderIndec<3> ind2;
     ind2.insert_to_col_space(mp::IntervalIndec<3>{1,2});
     ind2.insert_to_col_space(mp::IntervalIndec<3>{2,3});

     CHECK(ind1.get_dimension_vector() == ind2.get_dimension_vector());
     CHECK_FALSE(ind1 == ind2);
  }

  SECTION("multiples") {
    mp::LadderIndec<3> ind;
    mp::IntervalIndec<3> intv(1,2);

    ind.insert_to_col_space(mp::IntervalIndec<3>{1,3});
    ind.insert_to_row_space(mp::IntervalIndec<3>{1,3});

    SECTION("col multiples") {
      ind.insert_to_col_space(intv);
      ind.insert_to_col_space(intv);
      CHECK(ind.get_dimension_vector() == (std::vector<int>{3,3,1,1,1,1}));
    }
    SECTION("row multiples") {
      ind.insert_to_row_space(intv);
      ind.insert_to_row_space(intv);
      CHECK(ind.get_dimension_vector() == (std::vector<int>{1,1,1,3,3,1}));
    }
  }



  SECTION("singleton abstract to ladder indec") {
    const std::vector<std::string> codes = {"1:1", "1:3", "2:2"};

    SECTION("col singleton") {
      const std::vector< std::vector<int> > expected_dimvs =
          { {1,0,0,0,0,0},
            {1,1,1,0,0,0},
            {0,1,0,0,0,0}
          };
      for (unsigned int i = 0; i < 3; ++i) {
        mp::AbstractIndec h1 = mp::singletion_col_indec(codes[i]);
        auto ans = mp::abstract_to_ladder_indec<3>(h1);
        CHECK(ans != boost::none);
        CHECK(ans.get().get_dimension_vector() == expected_dimvs[i]);
      }
    }
    SECTION("row singleton") {
      const std::vector< std::vector<int> > expected_dimvs =
          { {0,0,0,1,0,0},
            {0,0,0,1,1,1},
            {0,0,0,0,1,0}
          };
      for (unsigned int i = 0; i < 3; ++i) {
        mp::AbstractIndec h1 = mp::singletion_row_indec(codes[i]);
        auto ans = mp::abstract_to_ladder_indec<3>(h1);
        CHECK(ans != boost::none);
        CHECK(ans.get().get_dimension_vector() == expected_dimvs[i]);
      }
    }
  }

}
