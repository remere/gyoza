#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <queue>
#include "gyoza/processtree.h"
#include <boost/optional/optional_io.hpp>

namespace mp = gyoza::matrixproblems;
using BA=mp::BlockAddress;

TEST_CASE("Process Tree - empty", "[processtree]") {
  mp::ProcessTree pt;
  CHECK(pt.get_root() == nullptr);
}

TEST_CASE("Process Tree", "[processtree]") {
  BA b1(1,1);
  BA b2(2,2);

  SECTION("root checks") {
    mp::ProcessTree pt(b1,b2);
    CHECK(pt.get_root() != nullptr);
    CHECK(pt.get_root()->get_target() == b1);
    CHECK(pt.get_root()->get_eraser() == b2);
  }

  SECTION("vertices are pointers"){
    mp::ProcessTree pt(b1,b2);
    mp::ProcessTree pt_another(b1,b2);
    mp::ProcessVertex pv(b1,b2);

    CHECK(*(pt.get_root()) == pv);
    CHECK(*(pt.get_root()) == *(pt_another.get_root()));

    CHECK(pt.get_root() != pt_another.get_root());
  }

  SECTION("tree attaching"){
    mp::ProcessTree pt(b1,b2);

    SECTION("infinite tree"){
      // self-referent infinite tree created:
      pt.attach_tree_to_root(pt);
      auto node = pt.get_root();
      CHECK(node == *(node->get_successors().begin()));
    }
  }

  SECTION("normal(?) usage"){
    mp::ProcessTree pt(BA(-1,-1), BA(0,0));
    for (int i = 0; i < 5; ++i) {
      mp::ProcessTree temp(BA(i,i), BA(0,0));
      temp.attach_tree_to_root(pt);
      temp.attach_tree_to_root(mp::ProcessTree(BA(i,i), BA(1,1)));
      pt = temp;
    }

    std::set<mp::ProcessVertex> visited;
    std::queue<std::shared_ptr<mp::ProcessVertex>> Q;
    Q.push(pt.get_root());
    while (not Q.empty()) {
      auto p_vertex = Q.front();
      Q.pop();

      REQUIRE(visited.count(*p_vertex) == 0);
      visited.insert(*p_vertex);

      for (const auto & p_suc : p_vertex->get_successors()) {
        Q.push(p_suc);
      }
    }
    CHECK(visited.size() == 11);
  }

}


TEST_CASE("Process Vertex", "[processtree]") {
  BA b1(1,1);
  BA b2(2,2);

  mp::ProcessVertex pv1(b1,b2);
  mp::ProcessVertex pv2(b2,b1);

  SECTION("member checks") {
    CHECK(pv1.get_target() == b1);
    CHECK(pv1.get_eraser() == b2);
  }

  SECTION("comparisons") {
    mp::ProcessVertex pv3(b1,b2);
    CHECK(pv1 != pv2);
    CHECK(pv1 == pv3);

    // successors don't affect equality
    pv1.add_as_successor(std::make_shared<mp::ProcessVertex>(b1,b1));
    CHECK(pv1.get_successors().size() == 1);
    CHECK(pv1 == pv3);
  }

  SECTION("successors"){
    pv1.add_as_successor(nullptr);
    CHECK(pv1.get_successors().size() == 0);
  }

}
