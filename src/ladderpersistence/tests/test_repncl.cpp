#define CATCH_CONFIG_MAIN
#include "catch.hpp"

// #include "gyoza/repnclf.h"
// #include "gyoza/repnclfb.h"
// #include "gyoza/repncl3.h"
// #include "gyoza/repncl4.h"

#include "gyoza/matrix_problem_cl.h"
#include <sstream>
#include <boost/optional/optional_io.hpp>
#include "gyoza/log_control.h"


namespace lp = gyoza::ladderpersistence;
namespace mp = gyoza::matrixproblems;
using qt = gyoza::quivers::QuiverType;

bool check_ones_problem_decomposition(const std::map<mp::AbstractIndec, int>& decompo) {
  bool status = true;
  for (const auto & indec_mult_pair : decompo){
    if ( indec_mult_pair.first.is_identity_arrow() ) {
      if ( indec_mult_pair.second != 1) {
        std::cerr << "Expected 1, but instead: "  << indec_mult_pair.first << "-> " << indec_mult_pair.second << "\n";
        status = false;
      }
    } else {
      if ( indec_mult_pair.second != 0) {
        std::cerr << "Expected 0, but instead: "  << indec_mult_pair.first << "-> " << indec_mult_pair.second << "\n";
        status = false;
      }
    } 
  }
  return status; 
}


template<typename MPCL_T>
void cl_num_indecs_tester(int correct_answer) {
  mp::LadderMatrixProblem phi = MPCL_T::get_ones_problem();
  phi.initialize();
  phi.matrix_reduction(100);

  boost::optional<int> num = phi.number_of_noniso_summands();
  CHECK(num != boost::none);
  CHECK(num.get() == correct_answer);

  boost::optional<std::map<mp::AbstractIndec, int>>
      decompo =  phi.get_abstract_decomposition();
  CHECK(check_ones_problem_decomposition(decompo.get()));
}




TEST_CASE("ones problem", "[laddermatrixproblem]"){
  SECTION("f"){
    cl_num_indecs_tester<lp::MatrixProblemCL<qt::Ladder_F>>(11);
  }

  SECTION("fb"){
    cl_num_indecs_tester<lp::MatrixProblemCL<qt::Ladder_FB>>(30);
  }

  SECTION("bf"){
    cl_num_indecs_tester<lp::MatrixProblemCL<qt::Ladder_BF>>(30);
  }

  SECTION("ff"){
    cl_num_indecs_tester<lp::MatrixProblemCL<qt::Ladder_FF>>(29);
  }

  SECTION("bb"){
    cl_num_indecs_tester<lp::MatrixProblemCL<qt::Ladder_BB>>(29);
  }


  SECTION("fff"){
    cl_num_indecs_tester<lp::MatrixProblemCL<qt::Ladder_FFF>>(76);
  }

  SECTION("bbb"){
    cl_num_indecs_tester<lp::MatrixProblemCL<qt::Ladder_BBB>>(76);
  }

  SECTION("ffb"){
    cl_num_indecs_tester<lp::MatrixProblemCL<qt::Ladder_FFB>>(83);
  }

  SECTION("fbb"){
    cl_num_indecs_tester<lp::MatrixProblemCL<qt::Ladder_FBB>>(83);
  }

  SECTION("bfb"){
    cl_num_indecs_tester<lp::MatrixProblemCL<qt::Ladder_BFB>>(88);
  }

  SECTION("fbf"){
    cl_num_indecs_tester<lp::MatrixProblemCL<qt::Ladder_FBF>>(88);
  }
  
  SECTION("bbf"){
    cl_num_indecs_tester<lp::MatrixProblemCL<qt::Ladder_BBF>>(83);
  }

  SECTION("bff"){
    cl_num_indecs_tester<lp::MatrixProblemCL<qt::Ladder_BFF>>(83);
  }
}


TEST_CASE("lmp ascii read: ones ladderF", "[laddermatrixproblem]"){
  std::string data = "#1:1 1:1\n1 1\n1\n#2:2 2:2\n1 1\n1\n#1:2 1:2\n1 1\n1\n";

  SECTION("usual procedure"){
    std::stringstream ss(data);
    auto ans = lp::MatrixProblemCL<qt::Ladder_F>::lmp_ascii_read(ss);
    REQUIRE(ans != boost::none);

    ans->initialize();
    ans->matrix_reduction(3);
    boost::optional<std::map<mp::AbstractIndec, int>> decompo =  ans->get_abstract_decomposition();
    CHECK(check_ones_problem_decomposition(decompo.get()));
  }

  SECTION("usual procedure - more entries"){
    data.append("#1:1 1:2\n1 1\n1\n#1:2 2:2\n1 1\n1");
    std::stringstream ss(data);
    auto ans = lp::MatrixProblemCL<qt::Ladder_F>::lmp_ascii_read(ss);
    REQUIRE(ans != boost::none);

    ans->initialize();
    ans->matrix_reduction(11);
    boost::optional<std::map<mp::AbstractIndec, int>> decompo =  ans->get_abstract_decomposition();
    CHECK(check_ones_problem_decomposition(decompo.get()));
  }

  SECTION("nonsense block address"){
    data[1] = '2';
    std::stringstream ss(data);
    std::cerr << std::endl << data << std::endl;
    auto ans = lp::MatrixProblemCL<qt::Ladder_F>::lmp_ascii_read(ss);
    REQUIRE(ans == boost::none);
  }

  SECTION("dimension mismatch"){
    data.append("#1:1 1:2\n1 2\n01");
    std::stringstream ss(data);
    std::cerr << std::endl << data << std::endl;
    auto ans = lp::MatrixProblemCL<qt::Ladder_F>::lmp_ascii_read(ss);
    REQUIRE(ans == boost::none);
  }

}
