#ifndef ABSTRACT_INDEC_H
#define ABSTRACT_INDEC_H

#include <string>
#include <set>
#include <iostream>

namespace gyoza {
namespace matrixproblems {

class AbstractIndec {
 public:
  AbstractIndec():involved_rows{}, involved_cols{}{}
  AbstractIndec(std::set<std::string> rows,
                std::set<std::string> cols): involved_rows(rows), involved_cols(cols) {}
  
  std::set<std::string> involved_rows;
  std::set<std::string> involved_cols;

  bool is_identity_arrow()const{
    return ((involved_rows.size() == 1) && (involved_rows == involved_cols));
  }

	bool operator==(const AbstractIndec & other)const {
		return ((involved_rows == other.involved_rows) && (involved_cols == other.involved_cols));
	}

  bool operator!=(const AbstractIndec & other)const {
		return (not (this->operator==(other)));
	}
	
  bool operator<(const AbstractIndec & other)const{
    return ((involved_rows < other.involved_rows) || (!(other.involved_rows < involved_rows) && involved_cols < other.involved_cols)); 
    // return std::make_pair(involved_rows, involved_cols) <
    //   std::make_pair(other.involved_rows, other.involved_cols);
  }
  
  friend std::ostream& operator<<(std::ostream& os, const AbstractIndec& obj){
    os << "rows: ";
    for (const auto & x: obj.involved_rows) {
      os << x << " ";
    }
    os << "cols: ";
    for (const auto & x: obj.involved_cols) {
      os << x << " ";
    }
    os << "\n";

    return os;
  }
  
};

AbstractIndec singletion_row_indec(const std::string& name); 
AbstractIndec singletion_col_indec(const std::string& name); 

}
}

#endif
