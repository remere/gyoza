#ifndef ORDERHANDLER_H
#define ORDERHANDLER_H
#include <list>
#include <set>
#include <ostream>

namespace gyoza {
namespace matrixproblems {
// typedef std::list<int> OrderHandler;

class OrderHandler {
 public:
  typedef std::list<int> data_type ;
  typedef data_type::size_type size_type;
  typedef data_type::const_iterator const_iterator;
  typedef data_type::const_reverse_iterator const_reverse_iterator;
  typedef data_type::value_type value_type;
  
  OrderHandler() = default;
  OrderHandler(int min, int max):data{}{
    generate_default_order(min, max);
  }
  
  const_iterator begin()const{return data.begin();}
  const_iterator end()const{return data.end();}

  const_iterator cbegin()const{return data.cbegin();}
  const_iterator cend()const{return data.cend();}

  const_reverse_iterator crbegin()const{return data.crbegin();}
  const_reverse_iterator crend()const{return data.crend();}

  size_type size()const{return data.size();} 
  bool has_value(const value_type & value);
  
  void generate_default_order(int min, int max);  
  bool insert_after(const value_type & prior, const value_type & post);
  bool push_back(const value_type & value);  
  bool push_front(const value_type & value);

  // TODO: features for removing values?!

  friend std::ostream& operator<<(std::ostream& os,
                                  const OrderHandler& that){
    for (auto it = that.cbegin(); it != that.cend(); ++it) {
      os << *it << " ";
    }
    
    return os;
  } 

 protected:
  std::set<int> values;
  std::list<int> data;

};


}
}





#endif
