\documentclass[11pt]{article}

\usepackage{amsmath,amssymb,amsthm}
\usepackage{mathptmx}

\usepackage{amsmath,amssymb}
\usepackage{braket}
\usepackage{tikz}
\usetikzlibrary{tikzmark, matrix, decorations.markings, calc, arrows}
\usepackage{tikz-cd}

% for blackboard bold math
\usepackage{bbm}

% for algorithms
\usepackage[noend]{algpseudocode}
\algrenewcommand\algorithmicindent{2.0em}
\usepackage{algorithm}
\usepackage{multicol}
\usepackage{listings}


\usepackage{enumitem}
\usepackage{fancybox}

\newtheorem{notation}{\textbf{\underline{Notation}}}

% Hiroshi's macro

\newcommand{\z}{\mathbb{Z}}

\renewcommand{\a}{\mathbb{A}}
\newcommand{\aaa}{\a_2\otimes\a_3}
\newcommand{\aaan}{\a_2\otimes\a_n} 

\newcommand{\AR}{Auslander-Reiten }

\newcommand{\antn}{\a_n(\tau_n)}

\newcommand{\rank}{\mathop\textrm{rank}}

\newcommand{\SNF}{ \begin{smallmatrix} E & 0 \\ 0 & 0 \end{smallmatrix} }

\newtheorem{theorem}{Theorem}

% Emer's macro
\DeclareMathOperator{\rep}{rep}
\DeclareMathOperator{\arr}{arr}
\newcommand{\Id}{\mathrm{id}}

% \newcommand{\rep}{\mathrm{rep}}
% \newcommand{\Hom}{\mathrm{Hom}}
\newcommand{\Mod}{\mathrm{mod}}
\newcommand{\im}{\mathrm{im}~}
\newcommand{\matc}[1]{\mathcal{#1}}
\newcommand{\KQ}{K\!Q}
\newcommand{\CL}{C\!L}
\newcommand{\intv}{\mathbb{I}}
\newcommand{\reltoeq}{\trianglerighteq}
\newcommand{\relto}{\vartriangleright}

\newcommand{\dimv}[2]{\raisebox{1ex}{$\overset{#1}{\underset{#2}{}}$}}

\algnewcommand{\LineComment}[1]{\State \(//\) #1}

\makeatletter
\def\Int #1{\expandafter\Int@i#1\@nil}
\def\Int@i #1,#2\@nil{{#1{:}#2}}

\def\IntC #1{\expandafter\IntC@i#1\@nil}
\def\IntC@i #1,#2\@nil{{#1{,}#2}}
\makeatother


% ************************************************************
% For the construction of matrices and impermissible arrows
% ************************************************************

% from http://tex.stackexchange.com/questions/22801/correct-delimiter-height-in-tikz
\makeatletter
\def\tikz@delimiter#1#2#3#4#5#6#7#8{%
  \bgroup
    \pgfextra{\let\tikz@save@last@fig@name=\tikz@last@fig@name}%
    node[outer sep=0pt,inner sep=0pt,draw=none,fill=none,anchor=#1,at=(\tikz@last@fig@name.#2),#3]
    {%
      {\nullfont\pgf@process{\pgfpointdiff{\pgfpointanchor{\tikz@last@fig@name}{#4}}{\pgfpointanchor{\tikz@last@fig@name}{#5}}}}%
      \delimitershortfall\z@% as suggested by morbusg (maximum space not covered by a delimiter = 0)
      \resizebox*{!}{#8}{$\left#6\vcenter{\hrule height .5#8 depth .5#8 width0pt}\right#7$}%
    }
    \pgfextra{\global\let\tikz@last@fig@name=\tikz@save@last@fig@name}%
  \egroup%
}
\makeatother

\newcommand{\mattikz}[1]{
   \begin{tikzpicture}[
      baseline = (p.center), 
      ampersand replacement=\&,
      decoration={
        markings,%  switch on markings
        mark=%  actually add a mark
        at position 0.5
        with{
          \draw[-] (-2pt,-2pt) -- (2pt,2pt);
          \draw[-] (2pt,-2pt) -- (-2pt,2pt);
        }
      }]
      {#1}
    \end{tikzpicture}
}

\newcommand{\matheader}{
  \matrix[matrix of math nodes, nodes in empty cells, column sep=0mm, row sep=0mm,
  inner sep = 0mm, left delimiter = {[},right delimiter = {]},
  every node/.append style={
    anchor=center,text depth = 0.5em,text
    height=1em,minimum width=1.5em}](p)
}

\newcommand{\rowempty}{
  \matrix[matrix of math nodes, column sep=0mm, row sep=0mm,
  inner sep = 0mm, left delimiter = {[},right delimiter = {]},
  nodes in empty cells,
  every node/.append style={
    anchor=center,text depth = 0em,text
    height=3mm, minimum width=1.8em}](p)
  {
    \vphantom{.}\\
  };
}

% ********** For impermissibles **********
\newcommand{\rowarrow}[2]{
  \draw[->, red, postaction={decorate}] #1  to[out=30,in=330] #2;
}

\newcommand{\colarrow}[2]{
  \draw[->, red, postaction={decorate}] #1 to[out=240,in=300] #2;
}
% ****************************************


\begin{document}

\title{Documentation of algorithms and choices made}
\author{Emerson G. Escolar}

\maketitle

Throughout, let $K = \mathbb{F}_2$ the finite field of two elements. Like all finite fields, this is known to be a perfect field.

The action of a matrix on a vector shall be denoted by multiplication from the left, in the usual convention for matrix-columnvector multiplication.

\section{Modules over $\a_3(fb)$, via a matrix problem}

In algo.h, we have the function \lstinline|solve_zigzag_fb|:
\begin{center}
  {\scriptsize
    \lstinputlisting[language=c++, numbers=left,firstnumber=13,firstline=13,lastline=16]{../algo.h}
  }
\end{center}

The input is a pair of matrices $f$, $b$ representing the two maps in some representation
\begin{equation}
  \begin{tikzcd}
    K^{d_0} \rar{f} & K^{d_1} & K^{d_2} \lar[swap]{b}.
  \end{tikzcd}
  \label{eq:input_zigzag}
\end{equation}


Where the output is a triple of matrices $\{P_0, P_1, P_2\}$ (corresponding to basis changes) such that
\[
  \begin{array}{rcl}
    f' &=& P_1 f P_0, \\
    b' &=& P_1 b P_2,
  \end{array}
\]
and $f'$, $b'$ are in ``nice'' forms where the decomposition of the representation Eq.~\eqref{eq:input_zigzag} can be read of these forms in an ``obvious'' way. This will be explained below.

\paragraph{Augemented matrix problem}
First, form the augmented matrix problem:
\[
  \mattikz{
    \matheader{
      \mathbbm{1} \& \&  \\
      \& \mathbbm{1} \& \\
      f \& b \& \mathbbm{1} \\
    };
    \foreach[count = \i] \v in {0,1,2}{
      \node[left=3.5ex] at (p-\i-1) {\scriptsize\v};
		}		%
    \foreach[count = \i] \v in {0,1,2}{
      \node[above=1.5ex] at (p-1-\i) {\scriptsize\v};
		}		% 
  }  
\]
where the $\mathbbm{1}$ are identity matrices of appropriate sizes. Initially, all cross-block operations are impermissible.

The labels $\scriptsize{i}$ (annoyingly) do not correspond to the vertices $0$, $1$, $2$ of the quiver $\a_n(fb)$ directly. Instead, label $\scriptsize{2}$ is the vertex $1$, and labels $\scriptsize{0}$, $\scriptsize{1}$ are vertices $0$ and $2$, respectively.

At the end of the procedure we describe below, we get 
\[
  \mattikz{
    \matheader{
      \mathbbm{1} \& \&  \\
      \& \mathbbm{1} \& \\
      f \& b \& \mathbbm{1} \\
    };
    \foreach[count = \i] \v in {0,1,2}{
      \node[left=3.5ex] at (p-\i-1) {\scriptsize\v};
		}		%
    \foreach[count = \i] \v in {0,1,2}{
      \node[above=1.5ex] at (p-1-\i) {\scriptsize\v};
		}		% 
  }
  \cong
  \mattikz{
    \matheader{
      P_0 \& \&  \\
      \& P_2 \& \\
      f' \& b' \& P_1 \\
    };
    \foreach[count = \i] \v in {0,1,2}{
      \node[left=3.5ex] at (p-\i-1) {\scriptsize\v};
		}		%
    \foreach[count = \i] \v in {0,1,2}{
      \node[above=1.5ex] at (p-1-\i) {\scriptsize\v};
		}		% 
  }  
\]
where $P_i$, $f'$, and $b'$ are as before (note the indices of $P_i$!). In the sequel, we hide the rows $0$, $1$ and the column $2$:
\[
  \mattikz{
    \matheader{      
      f \& b \\
    };
    \foreach[count = \i] \v in {2}{
      \node[left=3.5ex] at (p-\i-1) {\scriptsize\v};
		}		%
    \foreach[count = \i] \v in {0,1}{
      \node[above=1.5ex] at (p-1-\i) {\scriptsize\v};
		}		% 
  }
\]
as they do not directly affect what matrix operations we take to reduce the matrix problem. Rather they are just used to keep track of the transformations.

\paragraph{Procedure}
There are many ways to obtain ``nice'' forms for $f$ and $b$ using the matrix problem formalism. Below is just one way, and is the one implemented in the code.

Moreover, new rows and columns created as a result of splitting Smith normal form blocks shall be denoted $r_i$ or $c_i$ for the $i$ the new row or column, respectively.

\begin{enumerate}
\item Reduce block $(2,0)$
  \[
    \mattikz{
      \matheader{      
        E \& 0 \& b_1 \\
        0 \& 0 \& b_2 \\
      };
      \foreach[count = \i] \v in {2, \(r_1\)}{
        \node[left=3.5ex] at (p-\i-1) {\scriptsize\v};
      }		%
      \foreach[count = \i] \v in {0, \(c_1\), 1}{
        \node[above=1.5ex] at (p-1-\i) {\scriptsize\v};
      }		% 
    }
  \]
  Let $k_1$ be the rank of block $(2,0)$ originally. Equivalently,
  $k_1$ is the size of the $E$ obtained at this step, which now occupies block $(2,0)$ wholly after the splitting.
\item Reduce block $(r_1,1)$
  \[
    \mattikz{
      \matheader{      
        E \& 0 \& b_3 \& b_4  \\
        0 \& 0 \& E \& 0 \\
        0 \& 0 \& 0 \& 0 \\
      };
      \foreach[count = \i] \v in {2, \(r_1\), \(r_2\) }{
        \node[left=3.5ex] at (p-\i-1) {\scriptsize\v};
      }		%
      \foreach[count = \i] \v in {0, \(c_1\), 1, \(c_2\) }{
        \node[above=1.5ex] at (p-1-\i) {\scriptsize\v};
      }		% 
    }
  \]
  Let $k_2$ be the size of the resulting $E$.
\item Add left $-b_3$ multiple of row  $r_1$ to row $2$. Note: do not just erase block $(2,1)$. Remember that we are trying to keep track of the transformations.
  \[
    \mattikz{
      \matheader{      
        E \& 0 \& 0 \& b_4  \\
        0 \& 0 \& E \& 0 \\
        0 \& 0 \& 0 \& 0 \\
      };
      \foreach[count = \i] \v in {2, \(r_1\), \(r_2\) }{
        \node[left=3.5ex] at (p-\i-1) {\scriptsize\v};
      }		%
      \foreach[count = \i] \v in {0, \(c_1\), 1, \(c_2\) }{
        \node[above=1.5ex] at (p-1-\i) {\scriptsize\v};
      }		% 
    }
  \]
\item Reduce block $(2,c_2)$. Two things to remember: as a side effect, the $E$ in block $(2,0)$ gets messed up. Fix it via column operations on column $0$. Second, column $0$ gets split as well.
  \[
    \mattikz{
      \matheader{      
        E \& 0 \& 0 \& 0 \& E \& 0 \\
        0 \& E \& 0 \& 0 \& 0 \& 0 \\
        0 \& 0 \& 0 \& E \& 0 \& 0 \\
        0 \& 0 \& 0 \& 0 \& 0 \& 0 \\
      };
      \foreach[count = \i] \v in {2, \(r_3\), \(r_1\), \(r_2\) }{
        \node[left=3.5ex] at (p-\i-1) {\scriptsize\v};
      }		%
      \foreach[count = \i] \v in {0, \(c_4\), \(c_1\), 1, \(c_2\), \(c_3\) }{
        \node[above=1.5ex] at (p-1-\i) {\scriptsize\v};
      }		% 
    }
  \]
  Let $k_3$ be the size of the resulting $E$ at $(2,c_2)$. The other $E$ that splits now has size $k_3$ at $(2,0)$ and size $k_1 - k_3$ at $(r_3, c_4)$.  
\end{enumerate}

These are our chosen nice forms $f'$ and $b'$:
\[
  \mattikz{
    \matheader{      
      E \& 0 \& 0 \& 0 \& E \& 0 \\
      0 \& E \& 0 \& 0 \& 0 \& 0 \\
      0 \& 0 \& 0 \& E \& 0 \& 0 \\
      0 \& 0 \& 0 \& 0 \& 0 \& 0 \\
    };
    \foreach[count = \i] \v in {2, \(r_3\), \(r_1\), \(r_2\) }{
      \node[left=3.5ex] at (p-\i-1) {\scriptsize\v};
    }		%
    \foreach[count = \i] \v in {0, \(c_4\), \(c_1\), 1, \(c_2\), \(c_3\) }{
      \node[above=1.5ex] at (p-1-\i) {\scriptsize\v};
    }		% 
  }
\]

Looking at the domains and codomains, it is *clear* what columns/rows correspond to what spaces in the interval indecomposables $a:b$ = $\mathbb{I}[a,b]$ of the quiver
\[
  \a_3(fb):
  \begin{tikzcd}
    1 \rar & 2 & 3 \lar.
\end{tikzcd}
\]
These are labeled below, together with dimensions, in red:
\[
  \mattikz{  
    \matheader[column sep = 1.5em]
    {      
      E \& 0 \& 0 \& 0 \& E \& 0 \\
      0 \& E \& 0 \& 0 \& 0 \& 0 \\
      0 \& 0 \& 0 \& E \& 0 \& 0 \\
      0 \& 0 \& 0 \& 0 \& 0 \& 0 \\
    };
    \foreach[count = \i] \v in {1:3, 1:2, 2:3, 2:2 }{
      \node[left=3.5ex] at (p-\i-1) {\scriptsize\v};
    }		%
    \foreach[count = \i] \v in {1:3, 1:2, 1:1, 2:3, 1:3, 3:3}{
      \node[above=1.5ex] at (p-1-\i) {\scriptsize\v};
    }		%


    % dimensions:
    \foreach[count = \i] \v in {\(k_3\), \(k_1-k_3\), \(k_2\), \(d_1-k_1-k_2\)}{
      \node[right=3.5ex] at (p-\i-6) {\color{red}\scriptsize\v};
    }		%

    \foreach[count = \i] \v in {\(k_3\), \(k_1-k_3\), \(d_0-k_1\), \(k_2\), \(k_3\), \(d_2-k_2-k_3 \)}{
      \node[below=1.5ex] at (p-4-\i) {\color{red}\scriptsize\v};
    }		%
  }
\]
Note that these numbers (in red) are the multiplicities in the persistence diagram of $(f,b)$.


\section{From $\CL_3(fb)$ representation to matrix problem}
Consider the quiver
\[
  \CL_3(fb):
  \begin{tikzcd}
    3 \rar & 4 & 5 \lar \\
    0 \rar\uar & 1\uar & 2 \lar\uar
  \end{tikzcd},
\]
where we notate arrows by $\alpha_{i,j}: j \rightarrow i$ and where composition is written from the left.

For a representation $V = (V(i), V(\alpha))$ of $\CL_3(fb)$, do the following.

\paragraph{Change of bases} 

First, fix some bases for the spaces of $V$, and we identify maps of $V$ with matrix forms. Moreover, name these matrices by the following
\[  
  \begin{tikzcd}
    K^{d_3} \rar{f_u} & K^{d_4} & K^{d_5} \lar[swap]{b_u} \\
    K^{d_0} \rar{f_l}\uar{\phi_0} & K^{d_1}\uar{\phi_1} & K^{d_2} \lar[swap]{b_l}\uar{\phi_2}
  \end{tikzcd}
\]
where $V(i) \cong K^{d_i}$ by the choice of basis.


View $(f_u,b_u) = (V(\alpha_{4,3}), V(\alpha_{4,5}))$ as a zigzag.
Likewise, do the same for $(f_l,b_l) = (V(\alpha_{1,0}),
V(\alpha_{1,2}))$. Then we can use the method described in the previous section to get ``nice'' forms for these maps.

Specifically, we get invertible maps $P^0_u, P^1_u, P^2_u$ such that
\[
  \begin{array}{rcl}
    f_u' &=& P^1_u f_u P^0_u, \\
    b_u' &=& P^1_u b_u P^2_u,
  \end{array} 
\]
and invertible maps $P^0_l, P^1_l, P^2_l$ such that
\[
  \begin{array}{rcl}
    f_l' &=& P^1_l f_l P^0_l, \\
    b_l' &=& P^1_l b_l P^2_l,
  \end{array} 
\]
and where the primed matrices $f'_u, b'_u, f'_l, b'_l$ are in their
``nice'' forms, of appropriate sizes.

Compute
\[
  \begin{array}{rcl}
    \phi_0' &=& (P^0_u)^{-1} \phi_0 (P^0_l), \\
    \phi_1' &=& (P^1_u) \phi_1 (P^1_l)^{-1}, \\
    \phi_2' &=& (P^2_u)^{-1} \phi_2 (P^2_l).    
  \end{array} 
\]
Then, it can be checked that 
\begin{equation}
  \label{eq:transformed_matrices}
  \begin{tikzcd}
    K^{d_3} \rar{f_u'} & K^{d_4} & K^{d_5} \lar[swap]{b_u'} \\
    K^{d_0} \rar{f_l'}\uar{\phi_0'} & K^{d_1}\uar{\phi_1'} & K^{d_2} \lar[swap]{b_l'}\uar{\phi_2'}
  \end{tikzcd}
\end{equation}
is isomorphic as representation of $\CL_3(fb)$ to $V$. 


\paragraph{Analysis of block entries}
Denote the matrix entries of Eq.~\eqref{eq:transformed_matrices} by
the following:
\[
  \newcommand\phizero{
    \left[
    \begin{smallmatrix}
      A_1 & B_1 & C_1 \\
      D_1 & E_1 & F_1 \\
      G_1 & H_1 & I_1  
    \end{smallmatrix}
    \right]
  }
  \newcommand\phione{
    \left[
    \begin{smallmatrix}
      A_2 & B_2 & C_2 & D_2\\
      E_2 & F_2 & G_2 & H_2\\
      I_2 & J_2 & K_2 & L_2\\
      M_2 & N_2 & O_2 & P_2      
    \end{smallmatrix}
    \right]
  }
  \newcommand\phitwo{
    \left[
    \begin{smallmatrix}
      A_3 & B_3 & C_3 \\
      D_3 & E_3 & F_3 \\
      G_3 & H_3 & I_3  
    \end{smallmatrix}
    \right]
  }
  \newcommand\effprime{
    \left[
    \begin{smallmatrix}
      E & 0 & 0\\
      0 & E & 0\\
      0 & 0 & 0\\
      0 & 0 & 0
    \end{smallmatrix}
    \right]
  }
  \newcommand\beeprime{
    \left[
      \begin{smallmatrix}
        0 & E & 0\\
        0 & 0 & 0\\
        E & 0 & 0\\
        0 & 0 & 0
      \end{smallmatrix}
    \right]
  }
  \begin{tikzcd}[column sep=5em, row sep=4em]
    K^{d_3} \rar{\effprime} & K^{d_4} & K^{d_5} \lar[swap]{\beeprime} \\
    K^{d_0} \rar[swap]{\effprime}\uar{\phizero} & K^{d_1}\uar{\phione} & K^{d_2} \lar{\beeprime}\uar[swap]{\phitwo}
  \end{tikzcd}
\]

By commutativity, we get that this diagram is equal to:
\[
  \newcommand\phizero{
    \left[
    \begin{smallmatrix}
      A_1 & B_1 & 0 \\
      0   & E_1 & 0 \\
      G_1 & H_1 & I_1  
    \end{smallmatrix}
    \right]
  }
  \newcommand\phione{
    \left[
    \begin{smallmatrix}
      A_1 & B_1 & C_2 & D_2\\
      0   & E_1 & 0   & H_2\\
      0   & 0   & K_2 & L_2\\
      0   & 0   & 0   & P_2      
    \end{smallmatrix}
    \right]
  }
  \newcommand\phitwo{
    \left[
    \begin{smallmatrix}
      K_2 & I_2 & 0 \\
      C_2 & A_2 & 0 \\
      G_3 & H_3 & I_3  
    \end{smallmatrix}
    \right]
  }
  \newcommand\effprime{
    \left[
    \begin{smallmatrix}
      E & 0 & 0\\
      0 & E & 0\\
      0 & 0 & 0\\
      0 & 0 & 0
    \end{smallmatrix}
    \right]
  }
  \newcommand\beeprime{
    \left[
      \begin{smallmatrix}
        0 & E & 0\\
        0 & 0 & 0\\
        E & 0 & 0\\
        0 & 0 & 0
      \end{smallmatrix}
    \right]
  }
  \begin{tikzcd}[column sep=5em, row sep=4em]
    K^{d_3} \rar{\effprime} & K^{d_4} & K^{d_5} \lar[swap]{\beeprime} \\
    K^{d_0} \rar[swap]{\effprime}\uar{\phizero} & K^{d_1}\uar{\phione} & K^{d_2} \lar{\beeprime}\uar[swap]{\phitwo}
  \end{tikzcd}
\]

Now, the top and bottom rows are in their respective ``nice'' forms. Looking at the corresponding spaces, $\phi_0'$, $\phi_1'$, and $\phi_2'$ are 
\[
  \mattikz{  
    \matheader
    {
      A_1 \& B_1 \& 0 \\
      0   \& E_1 \& 0 \\
      G_1 \& H_1 \& I_1 \\      
    };
    \foreach[count = \i] \v in {1:3, 1:2, 1:1}{
      \node[left=3.5ex] at (p-\i-1) {\scriptsize\v};
    }		%
    \foreach[count = \i] \v in {1:3, 1:2, 1:1}{    
      \node[above=1.5ex] at (p-1-\i) {\scriptsize\v};
    }		% 
  }
  ,\quad
  \mattikz{  
    \matheader
    {
      A_1 \& B_1 \& C_2 \& D_2\\
      0   \& E_1 \& 0   \& H_2\\
      0   \& 0   \& K_2 \& L_2\\
      0   \& 0   \& 0   \& P_2\\
    };
    \foreach[count = \i] \v in {1:3, 1:2, 2:3, 2:2}{
      \node[left=3.5ex] at (p-\i-1) {\scriptsize\v};
    }		%
    \foreach[count = \i] \v in {1:3, 1:2, 2:3, 2:2}{    
      \node[above=1.5ex] at (p-1-\i) {\scriptsize\v};
    }		% 
  }
  ,\quad
  \mattikz{  
    \matheader
    {
      K_2 \& I_2 \& 0 \\
      C_2 \& A_2 \& 0 \\
      G_3 \& H_3 \& I_3\\
    };
    \foreach[count = \i] \v in {2:3, 1:3, 3:3}{
      \node[left=3.5ex] at (p-\i-1) {\scriptsize\v};
    }		%
    \foreach[count = \i] \v in {2:3, 1:3, 3:3}{    
      \node[above=1.5ex] at (p-1-\i) {\scriptsize\v};
    }		% 
  }
\]
respectively.

\paragraph{Note on notation} For example, consider $B_1$ in $\phi_0'$ labeled with column 1:2 and row 1:3. It ``means'' the following. Let $L$ be the bottom row of $V$. Via the change of basis discussed above, we have written
\[
  L \cong \bigoplus_{1\leq i \leq j \leq 3} \intv[i,j]^{m_{i,j}}
\]
Likewise,
\[
  U \cong \bigoplus_{1\leq i \leq j \leq 3} \intv[i,j]^{m'_{i,j}}
\]
where $U$ is the top row of $V$.

Let $L_{1:2}$ be the \emph{vector space} at vertex $1$ of the summand isomorphic to $\intv[1,2]^{m_{1,2}}$ of $L$. Likewise, let $U_{1:3}$ be the \emph{vector space} at vertex $1$ of the summand isomorphic to $\intv[1,3]^{m'_{1,3}}$ of $U$. Then, $B_1$ is simply the map $p_{1:3} \phi_0 \iota_{1:2}$ with respect to the chosen bases, where $\iota_{1:2}$ is the inclusion from the space at vertex $1$ of the summand isomorphic to $\intv[1,2]^{m_{1,2}}$ of $L$, and $p_{1:3}$ is a similarly constructed projection to a summand of the upper row.

\paragraph{Into the matrix problem}
Thus,
\[
  \mattikz{
    \matheader{
      { } \&
      G_3 \&
      { } \&
      H_3 \&
      { } \&
      I_3 \\
      { } \&
      { } \&
      H_1 \&
      G_1 \&
      I_1 \&
      { } \\ 
      D_2 \&
      C_2 \&
      B_1 \&
      A_1 \&
      { } \&
      { } \\
      H_2 \&
      { } \&
      E_1 \&
      { } \&
      { } \&
      { } \\
      L_2 \&
      K_2 \&
      { } \&
      { } \&
      { } \&
      { } \\
      P_2 \&
      { } \&
      { } \&
      { } \&
      { } \&
      { } \\
    };
    \node[anchor = south east, left = 12pt] (p-0-0) at (p-2-1.west |- p-1-1.north west) {};

    \foreach[count = \i] \v in {\(\Int{3, 3}\), \(\Int{1, 1}\), \(\Int{1, 3}\), \(\Int{1, 2}\), \(\Int{2, 3}\), \(\Int{2, 2}\)}{
      \node (p-\i-0) at (p-0-0 |- p-\i-\i) {};
      \path (p-\i-0.north) -- (p-\i-0.south) node [midway, left] { \v }; 
    }

    \foreach[count = \i] \v in {\(\Int{2, 2}\), \(\Int{2, 3}\), \(\Int{1, 2}\), \(\Int{1, 3}\), \(\Int{1, 1}\), \(\Int{3, 3}\)}{
      \node (p-0-\i) at (p-\i-\i |- p-0-0) {};
      \path (p-0-\i.west) -- (p-0-\i.east) node [midway, above] { \v };
    }
  }
\]
is the corresponding matrix problem of the input $V$.


\paragraph{Theoretical justification}
Recall the following theorem.
\begin{theorem}
  \label{isothm}
  Let $\tau$ be an orientation of length $n$. There is an isomorphism of
  $K$-categories
	\[
		F: \rep{\CL_n(\tau)} \cong \arr{\rep{\a_n(\tau)}}.
	\]
\end{theorem}

Let $M$ be a representation of $\CL_n(\tau)$ with $n\leq 4$. We identify $M$ with its corresponding
$F(M) = (\phi \colon V \rightarrow W)$
in $\arr{\rep{\a_n(\tau)}}$. Note that
$V = (V_i, f_\alpha)$ is in $\rep{\a_n(\tau)}$ and thus can be decomposed as
\begin{equation*}
	\eta_V \colon V \cong 
  \underset{1 \leq a \leq b \leq n}{\bigoplus} \intv[a, b]^{m_{a, b}}, \;\; 
  (m_{a, b} \in \mathbb{Z}_{\geq0}\text{: multiplicity})
\end{equation*}
A similar isomorphism $\eta_W$ can be
obtained for $W$. Through these isomorphisms, define
\begin{equation*}
	\Phi = \eta_W \phi \eta_V^{-1} \colon 
  \underset{1 \leq a \leq b \leq n}{\bigoplus} \intv[a, b]^{m_{a, b}} 
  \rightarrow 
  \underset{1 \leq c \leq d \leq n}{\bigoplus} \intv[c, d]^{m'_{c, d}}.
\end{equation*}
In fact, $(\eta_V,\eta_W) : \phi \rightarrow \Phi$ is an isomorphism
in $\arr{\rep{\a_n(\tau)}}$ so that $\phi$ and $\Phi$ are isomorphic.

The procedure to write the top and bottom rows of $M$ in ``nice'' forms gives us the representation $\Phi$.

Moreover, $\Phi$ can be written in a block matrix form
\begin{equation}
  \label{eq:blockmatrix}
	\Phi = \left[{\Phi_{(a, b)}^{(c, d)}}\right]
\end{equation}
where each block matrix entry
$\Phi_{(a, b)}^{(c, d)}  \colon \intv[a, b]^{m_{a, b}} \rightarrow \intv[c, d]^{m'_{c, d}}$
is obtained from $\Phi$ by the appropriate inclusion and projection.
That is, $\Phi_{(a, b)}^{(c, d)}$ is the composition of
\[
  \begin{tikzcd}
    \intv[a, b]^{m_{a, b}} \rar{\iota} & 
    \underset{1 \leq a \leq b \leq n}{\bigoplus} \intv[a, b]^{m_{a, b}}  \rar{\Phi} & 
    \underset{1 \leq c \leq d \leq n}{\bigoplus} \intv[c, d]^{m'_{c, d}} \rar{\pi} & 
    \intv[c, d]^{m'_{c, d}}.
  \end{tikzcd}  
\]
The analysis of sources and targets of the blocks of $\phi_i'$ we get these compositions with the inclusions and projections.



\end{document} 

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
