#ifndef MATRIX_PROBLEM_CL_H
#define MATRIX_PROBLEM_CL_H

#include "gyoza/quiverrepn.h"
#include "gyoza/ladder_matrix_problem.h"
#include <istream>

namespace gyoza {
namespace ladderpersistence {

template<quivers::QuiverType T>
class MatrixProblemCL {
 public:
  MatrixProblemCL()=default;

  static matrixproblems::LadderMatrixProblem create_random_problem(int d);
  static matrixproblems::LadderMatrixProblem get_ones_problem();

  static boost::optional<matrixproblems::LadderMatrixProblem> get_matrix_problem(quivers::QuiverRepn);
  static boost::optional<matrixproblems::LadderMatrixProblem> lmp_ascii_read(std::istream& input);
};

template<quivers::QuiverType T>
void set_cl_labels(matrixproblems::LadderMatrixProblem& ans,
                   std::vector<matrixproblems::BlockAddress>& nonzeros);

}
}







#endif
