#include "gyoza/abstract_indec.h"

namespace gyoza {
namespace matrixproblems {
AbstractIndec singletion_row_indec(const std::string& name) {
  return AbstractIndec({name},std::set<std::string>{}); 
}

AbstractIndec singletion_col_indec(const std::string& name) {
  return AbstractIndec(std::set<std::string>{},{name}); 
}

}
}
