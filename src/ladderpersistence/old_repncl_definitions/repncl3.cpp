#include "gyoza/repncl3.h"

namespace gyoza {
namespace ladderpersistence {

template<quivers::QuiverType T>
void get_cl3_data(matrixproblems::NameHandler& col_names,
                  std::vector<matrixproblems::BlockAddress>& nonzeros);

template<quivers::QuiverType T>
void set_cl3_labels(matrixproblems::LadderMatrixProblem& ans,
                    std::vector<matrixproblems::BlockAddress>& nonzeros) {
  matrixproblems::NameHandler col_names;
  get_cl3_data<T>(col_names, nonzeros);

  namespace mp=matrixproblems;
  const int max_index = 5;

  mp::NameHandler row_names = col_names.get_reverse(max_index);
  mp::PermissibleOperations col_prm;
  for (auto ba : nonzeros) {
    col_prm.make_permissible(max_index - ba.row_index, ba.col_index);
  }
  mp::PermissibleOperations row_prm = col_prm.get_dual().get_reverse(max_index);

  ans.set_permissible_col_operations(col_prm);
  ans.set_permissible_row_operations(row_prm);

  ans.set_row_names(row_names);
  ans.set_col_names(col_names);
  return;
}

template<quivers::QuiverType T>
matrixproblems::LadderMatrixProblem RepnCL3<T>::create_random_problem(int d){
  matrixproblems::LadderMatrixProblem ans;
  std::vector<matrixproblems::BlockAddress> nonzeros;

  set_cl3_labels<T>(ans, nonzeros);
  for (auto ba : nonzeros) {
    ans[ba] = Z2Matrix::Random(d, d);
  }

  return ans;
}

template<quivers::QuiverType T>
matrixproblems::LadderMatrixProblem RepnCL3<T>::get_ones_problem(){
  matrixproblems::LadderMatrixProblem ans;
  std::vector<matrixproblems::BlockAddress> nonzeros;
  set_cl3_labels<T>(ans, nonzeros);

  for (auto ba : nonzeros) {
    ans[ba] = Z2Matrix::Identity(1,1);
  }

  return ans;
}


// hardcoding:
template class RepnCL3<quivers::QuiverType::Ladder_FB>;
template class RepnCL3<quivers::QuiverType::Ladder_BF>;
template class RepnCL3<quivers::QuiverType::Ladder_FF>;
template class RepnCL3<quivers::QuiverType::Ladder_BB>;


template<>
void get_cl3_data<quivers::QuiverType::Ladder_FF>
(gyoza::matrixproblems::NameHandler& col_names, std::vector<matrixproblems::BlockAddress>& nonzeros){
  col_names = gyoza::matrixproblems::NameHandler({
      {0,"3:3"},
      {1,"2:3"},
      {2,"2:2"},
      {3,"1:3"},
      {4,"1:2"},
      {5,"1:1"}
    });

  nonzeros = {
    {0,3}, {0,4}, {0,5},
    {1,1}, {1,2}, {1,3}, {1,4},
    {2,0}, {2,1}, {2,3},
    {3,1}, {3,2},
    {4,0}, {4,1},
    {5,0}
  };
}


template<>
void get_cl3_data<quivers::QuiverType::Ladder_FB>
(gyoza::matrixproblems::NameHandler& col_names, std::vector<matrixproblems::BlockAddress>& nonzeros){
  col_names = gyoza::matrixproblems::NameHandler({
      {0,"2:2"},
      {1,"2:3"},
      {2,"1:2"},
      {3,"1:3"},
      {4,"3:3"},
      {5,"1:1"}
    });

  nonzeros = {
    {0,2}, {0,3}, {0,5},
    {1,1}, {1,3}, {1,4},
    {2,0}, {2,1}, {2,2}, {2,3},
    {3,0}, {3,2},
    {4,0}, {4,1},
    {5,0}
  };
}


template<>
void get_cl3_data<quivers::QuiverType::Ladder_BF>
(gyoza::matrixproblems::NameHandler& col_names, std::vector<matrixproblems::BlockAddress>& nonzeros){
  col_names = gyoza::matrixproblems::NameHandler({
      {0,"3:3"},
      {1,"1:1"},
      {2,"1:3"},
      {3,"2:3"},
      {4,"1:2"},
      {5,"2:2"}
    });

  nonzeros = {
    {0,2}, {0,3}, {0,4}, {0,5},
    {1,1}, {1,2}, {1,4},
    {2,0}, {2,2}, {2,3},
    {3,0}, {3,1}, {3,2},
    {4,1},
    {5,0}
  };
}


template<>
void get_cl3_data<quivers::QuiverType::Ladder_BB>
(gyoza::matrixproblems::NameHandler& col_names, std::vector<matrixproblems::BlockAddress>& nonzeros){
  col_names = gyoza::matrixproblems::NameHandler({
      {0,"1:1"},
      {1,"1:2"},
      {2,"2:2"},
      {3,"1:3"},
      {4,"2:3"},
      {5,"3:3"}
    });

  nonzeros = {
    {0,3}, {0,4}, {0,5},
    {1,1}, {1,2}, {1,3}, {1,4},
    {2,0}, {2,1}, {2,3},
    {3,1}, {3,2},
    {4,0}, {4,1},
    {5,0}
  };
}


}
}
