#include "gyoza/repnclbfb.h"
#include "gyoza/snf_algorithms.hpp"


namespace gyoza {
namespace ladderpersistence {

void set_a2bfb_labels(matrixproblems::LadderMatrixProblem& ans,
                      std::vector<matrixproblems::BlockAddress>& nonzeros){
  namespace mp=gyoza::matrixproblems;
  const int max_index = 9;
  gyoza::matrixproblems::NameHandler col_names({
      {0,"1:1"},
      {1,"3:3"},
      {2,"1:3"},
      {3,"3:4"},
      {4,"2:3"},
      {5,"1:4"},
      {6,"2:4"},
      {7,"1:2"},
      {8,"4:4"},
      {9,"2:2"}
    });
    
  mp::NameHandler row_names = col_names.get_reverse(max_index);
  nonzeros = {
    {0,2}, {0,4}, {0,5}, {0,6}, {0,7}, {0,9},
    {1,3}, {1,5}, {1,6}, {1,8},
    {2,0}, {2,2}, {2,5}, {2,7},
    {3,1}, {3,2}, {3,3}, {3,4}, {3,5}, {3,6},
    {4,0}, {4,1}, {4,2}, {4,3}, {4,5}, {5,1},
    {5,2}, {5,4},
    {6,1}, {6,3},
    {7,0}, {7,1}, {7,2},
    {8,1},
    {9,0}
  };

  gyoza::matrixproblems::PermissibleOperations col_prm;
  for (auto ba : nonzeros) {
    col_prm.make_permissible(max_index - ba.row_index, ba.col_index);     
  } 
  mp::PermissibleOperations row_prm = col_prm.get_dual().get_reverse(max_index);

  ans.set_permissible_col_operations(col_prm); 
  ans.set_permissible_row_operations(row_prm);
  
  ans.set_row_names(row_names);
  ans.set_col_names(col_names);
  return;
}

matrixproblems::LadderMatrixProblem RepnCLbfb::create_random_problem(int d){
  matrixproblems::LadderMatrixProblem ans;
  std::vector<matrixproblems::BlockAddress> nonzeros;
  
  set_a2bfb_labels(ans, nonzeros); 
  for (auto ba : nonzeros) {
    ans[ba] = Z2Matrix::Random(d, d); 
  } 
  
  return ans;
}

matrixproblems::LadderMatrixProblem RepnCLbfb::get_ones_problem(){
  matrixproblems::LadderMatrixProblem ans;  
  std::vector<matrixproblems::BlockAddress> nonzeros;
  set_a2bfb_labels(ans, nonzeros); 

  for (auto ba : nonzeros) {
    ans[ba] = Z2Matrix::Identity(1,1); 
  } 
  
  return ans;
}



}
}
