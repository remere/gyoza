#include "gyoza/repncl4.h"

namespace gyoza {
namespace ladderpersistence {

template<quivers::QuiverType T>
void get_cl4_data(matrixproblems::NameHandler& col_names,
                  std::vector<matrixproblems::BlockAddress>& nonzeros);

template<quivers::QuiverType T>
void set_cl4_labels(matrixproblems::LadderMatrixProblem& ans,
                    std::vector<matrixproblems::BlockAddress>& nonzeros) {
  matrixproblems::NameHandler col_names;
  get_cl4_data<T>(col_names, nonzeros);

  namespace mp=matrixproblems;
  const int max_index = 9;

  mp::NameHandler row_names = col_names.get_reverse(max_index);
  mp::PermissibleOperations col_prm;
  for (auto ba : nonzeros) {
    col_prm.make_permissible(max_index - ba.row_index, ba.col_index);
  }
  mp::PermissibleOperations row_prm = col_prm.get_dual().get_reverse(max_index);

  ans.set_permissible_col_operations(col_prm);
  ans.set_permissible_row_operations(row_prm);

  ans.set_row_names(row_names);
  ans.set_col_names(col_names);
  return;
}

template<quivers::QuiverType T>
matrixproblems::LadderMatrixProblem RepnCL4<T>::create_random_problem(int d){
  matrixproblems::LadderMatrixProblem ans;
  std::vector<matrixproblems::BlockAddress> nonzeros;

  set_cl4_labels<T>(ans, nonzeros);
  for (auto ba : nonzeros) {
    ans[ba] = Z2Matrix::Random(d, d);
  }

  return ans;
}

template<quivers::QuiverType T>
matrixproblems::LadderMatrixProblem RepnCL4<T>::get_ones_problem(){
  matrixproblems::LadderMatrixProblem ans;
  std::vector<matrixproblems::BlockAddress> nonzeros;
  set_cl4_labels<T>(ans, nonzeros);

  for (auto ba : nonzeros) {
    ans[ba] = Z2Matrix::Identity(1,1);
  }

  return ans;
}

// hardcoding:

template class RepnCL4<quivers::QuiverType::Ladder_FFF>;
template class RepnCL4<quivers::QuiverType::Ladder_FFB>;
template class RepnCL4<quivers::QuiverType::Ladder_BFB>;
template class RepnCL4<quivers::QuiverType::Ladder_BBF>;

template class RepnCL4<quivers::QuiverType::Ladder_BBB>;
template class RepnCL4<quivers::QuiverType::Ladder_FBB>;
template class RepnCL4<quivers::QuiverType::Ladder_FBF>;
template class RepnCL4<quivers::QuiverType::Ladder_BFF>;

template<>
void get_cl4_data<quivers::QuiverType::Ladder_FFF>
(gyoza::matrixproblems::NameHandler& col_names, std::vector<matrixproblems::BlockAddress>& nonzeros){
  col_names = gyoza::matrixproblems::NameHandler({
      {0,"4:4"},
      {1,"3:4"},
      {2,"3:3"},
      {3,"2:4"},
      {4,"2:3"},
      {5,"1:4"},
      {6,"2:2"},
      {7,"1:3"},
      {8,"1:2"},
      {9,"1:1"}
    });

  nonzeros = {
    {0,5}, {0,7}, {0,8}, {0,9},
    {1,3}, {1,4}, {1,5}, {1,6}, {1,7}, {1,8},
    {2,1}, {2,2}, {2,3}, {2,4}, {2,5}, {2,7},
    {3,3}, {3,4}, {3,6},
    {4,0}, {4,1}, {4,3}, {4,5},
    {5,1}, {5,2}, {5,3}, {5,4},
    {6,0}, {6,1}, {6,3},
    {7,1}, {7,2},
    {8,0}, {8,1},
    {9,0}
  };
}

template<>
void get_cl4_data<quivers::QuiverType::Ladder_FFB>
(gyoza::matrixproblems::NameHandler& col_names, std::vector<matrixproblems::BlockAddress>& nonzeros){
  col_names = gyoza::matrixproblems::NameHandler({
      {0,"3:3"},
      {1,"3:4"},
      {2,"2:3"},
      {3,"2:4"},
      {4,"1:3"},
      {5,"2:2"},
      {6,"1:4"},
      {7,"4:4"},
      {8,"1:2"},
      {9,"1:1"}
    });
  nonzeros = {
    {0,4}, {0,6}, {0,8}, {0,9},
    {1,2}, {1,3}, {1,4}, {1,5}, {1,6}, {1,8},
    {2,1}, {2,3}, {2,6}, {2,7},
    {3,0}, {3,1}, {3,2}, {3,3}, {3,4}, {3,6},
    {4,2}, {4,3}, {4,5},
    {5,0}, {5,2}, {5,4},
    {6,0}, {6,1}, {6,2}, {6,3},
    {7,0}, {7,2},
    {8,0}, {8,1},
    {9,0}
  } ;
}


template<>
void get_cl4_data<quivers::QuiverType::Ladder_BFB>
(gyoza::matrixproblems::NameHandler& col_names, std::vector<matrixproblems::BlockAddress>& nonzeros){
  col_names = gyoza::matrixproblems::NameHandler({
      {0,"3:3"},
      {1,"1:1"},
      {2,"3:4"},
      {3,"1:3"},
      {4,"2:3"},
      {5,"1:4"},
      {6,"2:4"},
      {7,"1:2"},
      {8,"4:4"},
      {9,"2:2"}
    });

  nonzeros = {
    {0,3}, {0,4}, {0,5}, {0,6}, {0,7}, {0,9},
    {1,2}, {1,5}, {1,6}, {1,8},
    {2,1}, {2,3}, {2,5}, {2,7},
    {3,0}, {3,2}, {3,3}, {3,4}, {3,5}, {3,6},
    {4,0}, {4,1}, {4,2}, {4,3}, {4,5},
    {5,0}, {5,3}, {5,4},
    {6,0}, {6,1}, {6,3},
    {7,0}, {7,2},
    {8,1},
    {9,0}
  };
}


template<>
void get_cl4_data<quivers::QuiverType::Ladder_BBF>
(gyoza::matrixproblems::NameHandler& col_names, std::vector<matrixproblems::BlockAddress>& nonzeros){
  col_names = gyoza::matrixproblems::NameHandler({
      {0,"1:1"},
      {1,"4:4"},
      {2,"1:2"},
      {3,"2:2"},
      {4,"1:4"},
      {5,"2:4"},
      {6,"1:3"},
      {7,"3:4"},
      {8,"2:3"},
      {9,"3:3"}
    });

  nonzeros = {
    {0,4}, {0,5}, {0,6}, {0,7}, {0,8}, {0,9},
    {1,2}, {1,3}, {1,4}, {1,5}, {1,6}, {1,8},
    {2,1}, {2,4}, {2,5}, {2,7},
    {3,0}, {3,2}, {3,4}, {3,6},
    {4,1}, {4,2}, {4,3}, {4,4}, {4,5},
    {5,0}, {5,1}, {5,2}, {5,4},
    {6,2}, {6,3},
    {7,0}, {7,2},
    {8,1},
    {9,0}
  };
}


template<>
void get_cl4_data<quivers::QuiverType::Ladder_BBB>
(gyoza::matrixproblems::NameHandler& col_names, std::vector<matrixproblems::BlockAddress>& nonzeros){
  col_names = gyoza::matrixproblems::NameHandler({
      {0,"1:1"},
      {1,"1:2"},
      {2,"2:2"},
      {3,"1:3"},
      {4,"2:3"},
      {5,"1:4"},
      {6,"3:3"},
      {7,"2:4"},
      {8,"3:4"},
      {9,"4:4"}
    });

  nonzeros = {
    {0,5}, {0,7}, {0,8}, {0,9},
    {1,3}, {1,4}, {1,5}, {1,6}, {1,7}, {1,8},
    {2,1}, {2,2}, {2,3}, {2,4}, {2,5}, {2,7},
    {3,3}, {3,4}, {3,6},
    {4,0}, {4,1}, {4,3}, {4,5},
    {5,1}, {5,2}, {5,3}, {5,4},
    {6,0}, {6,1}, {6,3},
    {7,1}, {7,2},
    {8,0}, {8,1},
    {9,0}
  };
}


template<>
void get_cl4_data<quivers::QuiverType::Ladder_FBB>
(gyoza::matrixproblems::NameHandler& col_names, std::vector<matrixproblems::BlockAddress>& nonzeros){
  col_names = gyoza::matrixproblems::NameHandler({
      {0,"2:2"},
      {1,"2:3"},
      {2,"1:2"},
      {3,"2:4"},
      {4,"1:3"},
      {5,"3:3"},
      {6,"1:4"},
      {7,"3:4"},
      {8,"1:1"},
      {9,"4:4"}
    });

  nonzeros = {
    {0,3}, {0,6}, {0,7}, {0,9},
    {1,2}, {1,4}, {1,6}, {1,8},
    {2,1}, {2,3}, {2,4}, {2,5}, {2,6}, {2,7},
    {3,0}, {3,1}, {3,2}, {3,3}, {3,4}, {3,6},
    {4,1}, {4,4}, {4,5},
    {5,0}, {5,1}, {5,2}, {5,4},
    {6,0}, {6,1}, {6,3},
    {7,0}, {7,2},
    {8,0}, {8,1},
    {9,0}
  };
}


template<>
void get_cl4_data<quivers::QuiverType::Ladder_FBF>
(gyoza::matrixproblems::NameHandler& col_names, std::vector<matrixproblems::BlockAddress>& nonzeros){
  col_names = gyoza::matrixproblems::NameHandler({
      {0,"4:4"},
      {1,"2:2"},
      {2,"2:4"},
      {3,"1:2"},
      {4,"2:3"},
      {5,"1:4"},
      {6,"3:4"},
      {7,"1:3"},
      {8,"3:3"},
      {9,"1:1"}
    });

  nonzeros = {
    {0,3}, {0,5}, {0,7}, {0,9},
    {1,2}, {1,4}, {1,5}, {1,6}, {1,7}, {1,8},
    {2,1}, {2,2}, {2,3}, {2,4}, {2,5}, {2,7},
    {3,0}, {3,2}, {3,5}, {3,6},
    {4,0}, {4,1}, {4,2}, {4,3}, {4,5},
    {5,1}, {5,2}, {5,4},
    {6,1}, {6,3},
    {7,0}, {7,1}, {7,2},
    {8,1},
    {9,0}
  };
}


template<>
void get_cl4_data<quivers::QuiverType::Ladder_BFF>
(gyoza::matrixproblems::NameHandler& col_names, std::vector<matrixproblems::BlockAddress>& nonzeros){
  col_names = gyoza::matrixproblems::NameHandler({
      {0,"4:4"},
      {1,"3:4"},
      {2,"1:1"},
      {3,"3:3"},
      {4,"1:4"},
      {5,"2:4"},
      {6,"1:3"},
      {7,"2:3"},
      {8,"1:2"},
      {9,"2:2"}
    });

  nonzeros = {
    {0,4}, {0,5}, {0,6}, {0,7}, {0,8}, {0,9},
    {1,2}, {1,4}, {1,6}, {1,8},
    {2,1}, {2,3}, {2,4}, {2,5}, {2,6}, {2,7},
    {3,1}, {3,2}, {3,3}, {3,4}, {3,6},
    {4,0}, {4,1}, {4,4}, {4,5},
    {5,0}, {5,1}, {5,2}, {5,4},
    {6,1}, {6,3},
    {7,2},
    {8,0}, {8,1},
    {9,0}
  };
}


}
}
