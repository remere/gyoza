#ifndef REPNCL3_H
#define REPNCL3_H

#include "gyoza/quiverrepn.h"
#include "gyoza/ladder_matrix_problem.h"

namespace gyoza {
namespace ladderpersistence {

template<quivers::QuiverType T>
class RepnCL3: public quivers::QuiverRepn {
 public:
  RepnCL3() : QuiverRepn(quivers::create_ladder_quiver(T)) { };

  static matrixproblems::LadderMatrixProblem create_random_problem(int d);
  static matrixproblems::LadderMatrixProblem get_ones_problem();
};

}
}



#endif
