#include "gyoza/repnclfff.h"
#include "gyoza/snf_algorithms.hpp"


namespace gyoza {
namespace ladderpersistence {

void set_a2fff_labels(matrixproblems::LadderMatrixProblem& ans,
                      std::vector<matrixproblems::BlockAddress>& nonzeros){
  namespace mp=gyoza::matrixproblems;
  const int max_index = 9;
  gyoza::matrixproblems::NameHandler col_names( {
      {0,"4:4"},
      {1,"3:4"},
      {2,"3:3"},
      {3,"2:4"},
      {4,"2:3"},
      {5,"1:4"},
      {6,"2:2"},
      {7,"1:3"},
      {8,"1:2"},
      {9,"1:1"}});
  mp::NameHandler row_names = col_names.get_reverse(max_index);
  nonzeros = {
    {0,5}, {0,7}, {0,8}, {0,9},
    {1,3}, {1,4}, {1,5}, {1,6}, {1,7}, {1,8},
    {2,1}, {2,2}, {2,3}, {2,4}, {2,5}, {2,7},
    {3,3}, {3,4}, {3,6},
    {4,0}, {4,1}, {4,3}, {4,5},
    {5,1}, {5,2}, {5,3}, {5,4},
    {6,0}, {6,1}, {6,3},
    {7,1}, {7,2},
    {8,0}, {8,1},
    {9,0} 
  };

  gyoza::matrixproblems::PermissibleOperations col_prm;
  for (auto ba : nonzeros) {
    col_prm.make_permissible(max_index - ba.row_index, ba.col_index);     
  } 
  mp::PermissibleOperations row_prm = col_prm.get_dual().get_reverse(max_index);

  ans.set_permissible_col_operations(col_prm); 
  ans.set_permissible_row_operations(row_prm);
  
  ans.set_row_names(row_names);
  ans.set_col_names(col_names);
  return;
}

matrixproblems::LadderMatrixProblem RepnCLfff::create_random_problem(int d){
  matrixproblems::LadderMatrixProblem ans;
  std::vector<matrixproblems::BlockAddress> nonzeros;
  
  set_a2fff_labels(ans, nonzeros); 
  for (auto ba : nonzeros) {
    ans[ba] = Z2Matrix::Random(d, d); 
  } 
  
  return ans;
}

matrixproblems::LadderMatrixProblem RepnCLfff::get_ones_problem(){
  matrixproblems::LadderMatrixProblem ans;  
  std::vector<matrixproblems::BlockAddress> nonzeros;
  set_a2fff_labels(ans, nonzeros); 

  for (auto ba : nonzeros) {
    ans[ba] = Z2Matrix::Identity(1,1); 
  } 
  
  return ans;
}



}
}
