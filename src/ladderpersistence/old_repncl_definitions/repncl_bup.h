#ifndef REPNCL_H
#define REPNCL_H

#include "gyoza/quiver.h"

#include "gyoza/repnclf.h"
#include "gyoza/repnclfb.h"

#include "gyoza/repncl4.h"

namespace gyoza {
namespace ladderpersistence {

namespace internals{

template<quivers::QuiverType T>
struct clbase{};

template<quivers::QuiverType T>
struct cltype{
  ~cltype()=0;
  typedef clbase<T> type;
};

template <>
struct cltype<quivers::QuiverType::Ladder_F> {
  typedef RepnCLf type;
};
template <>
struct cltype<quivers::QuiverType::Ladder_FB> {
  typedef RepnCLfb type;
};
template <>
struct cltype<quivers::QuiverType::Ladder_FFF> {
  typedef RepnCL4<quivers::QuiverType::Ladder_FFF> type;
};
template <>
struct cltype<quivers::QuiverType::Ladder_FFB> {
  typedef RepnCL4<quivers::QuiverType::Ladder_FFB> type;
};
template <>
struct cltype<quivers::QuiverType::Ladder_BFB> {
  typedef RepnCL4<quivers::QuiverType::Ladder_BFB> type;
};

}

template<quivers::QuiverType T>
using RepnCL = typename gyoza::ladderpersistence::internals::cltype<T>::type; 

}
}

#endif
