#ifndef REPNCLF_H
#define REPNCLF_H

#include "gyoza/quiverrepn.h"
#include "gyoza/ladder_matrix_problem.h"

namespace gyoza {
namespace ladderpersistence {

class RepnCLf : public quivers::QuiverRepn {
 public:
  RepnCLf() : QuiverRepn(quivers::create_ladder_quiver(quivers::QuiverType::Ladder_F)) { };
                         
  boost::optional<matrixproblems::LadderMatrixProblem> get_matrix_problem() const override;

  static matrixproblems::LadderMatrixProblem create_random_problem(int d);
  static matrixproblems::LadderMatrixProblem get_ones_problem();

}; 


}
}

#endif
