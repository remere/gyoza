#include "gyoza/repnclfb.h"
#include "gyoza/snf_algorithms.hpp"
#include "gyoza/algo.h"
#include "gyoza/random_z2matrix.h"

namespace gyoza {
namespace ladderpersistence {

void set_a2fb_labels(matrixproblems::LadderMatrixProblem& ans,
                     std::vector<matrixproblems::BlockAddress>& nonzeros){
  namespace mp=gyoza::matrixproblems;
  
  const int max_index = 5;
  mp::NameHandler col_names( {
      {0,"2:2"},
      {1,"2:3"},
      {2,"1:2"},
      {3,"1:3"},
      {4,"3:3"},
      {5,"1:1"}} );
  mp::NameHandler row_names = col_names.get_reverse(max_index);
  nonzeros = {
    {0,2}, {0,3}, {0,5},
    {1,1}, {1,3}, {1,4},
    {2,0}, {2,1}, {2,2}, {2,3},
    {3,0}, {3,2},
    {4,0}, {4,1},
    {5,0}
  }; 
  
  mp::PermissibleOperations col_prm;
  for (auto ba : nonzeros) {
    col_prm.make_permissible(max_index - ba.row_index, ba.col_index);     
  }
  mp::PermissibleOperations row_prm = col_prm.get_dual().get_reverse(max_index); 
  
  ans.set_permissible_col_operations(col_prm); 
  ans.set_permissible_row_operations(row_prm);
  
  ans.set_row_names(row_names);
  ans.set_col_names(col_names);
  return;
} 


boost::optional<matrixproblems::LadderMatrixProblem> RepnCLfb::get_matrix_problem()const {
  matrixproblems::LadderMatrixProblem ans;
  std::vector<matrixproblems::BlockAddress> nonzeros;
  set_a2fb_labels(ans, nonzeros);

  std::map<std::pair<int,int>, int> pd_up;
  std::tuple<Z2Matrix,Z2Matrix,Z2Matrix> up_P = algorithms::solve_zigzag_fb(matrix_at(3,4), matrix_at(5,4), &pd_up);

  std::map<std::pair<int,int>, int> pd_lw;
  std::tuple<Z2Matrix,Z2Matrix,Z2Matrix> lw_P = algorithms::solve_zigzag_fb(matrix_at(0,1), matrix_at(2,1), &pd_lw);

  // vertical arrows, before transformation induced by solve_zigzag
  Z2Matrix phi_0 =  matrix_at(0,3);
  Z2Matrix phi_1 =  matrix_at(1,4);
  Z2Matrix phi_2 =  matrix_at(2,5);

  // do the necessary transforms;
  algorithms::FullPivotSNF<Z2Matrix> inverter;
  
  inverter.compute( std::get<0>(up_P) ); 
  phi_0 = inverter.get_inverse().get() * phi_0 * std::get<0>(lw_P);

  inverter.compute( std::get<1>(lw_P) );
  phi_1 = std::get<1>(up_P) * phi_1 * inverter.get_inverse().get();

  inverter.compute( std::get<2>(up_P) ); 
  phi_2 = inverter.get_inverse().get() * phi_2 * std::get<2>(lw_P); 


  using BA = matrixproblems::BlockAddress;
  /* Recall:
    col names:
      {{0,"2:2"},
      {1,"2:3"},
      {2,"1:2"},
      {3,"1:3"},
      {4,"1:1"},
      {5,"3:3"}} 
    row names is reversed.
  */
  // row 0, corresponds to of 3:3 of up.
  ans[BA{0,1}] = phi_2.bottomRows(pd_up.at({3,3})).leftCols(pd_lw.at({2,3}));
  ans[BA{0,3}] = phi_2.bottomRows(pd_up.at({3,3})).middleCols(pd_lw.at({2,3}),
                                                              pd_lw.at({1,3}));
  ans[BA{0,5}] = phi_2.bottomRows(pd_up.at({3,3})).rightCols(pd_lw.at({3,3}));

  // row 1, corresponds to 1:1 of up
  ans[BA{1,3}] = phi_0.bottomRows(pd_up.at({1,1})).leftCols(pd_lw.at({1,3}));
  ans[BA{1,2}] = phi_0.bottomRows(pd_up.at({1,1})).middleCols(pd_lw.at({1,3}),
                                                              pd_lw.at({1,2}));
  ans[BA{1,4}] = phi_0.bottomRows(pd_up.at({1,1})).rightCols(pd_lw.at({1,1}));

  // row 2, corresponds to 1:3 of up
  ans[BA{2,3}] = phi_0.topRows(pd_up.at({1,3})).leftCols(pd_lw.at({1,3}));
  ans[BA{2,2}] = phi_0.topRows(pd_up.at({1,3})).middleCols(pd_lw.at({1,3}),
                                                           pd_lw.at({1,2}));

  // row 3, 1:4 of up
  ans[BA{3,2}] = phi_0.middleRows(pd_up.at({1,3}),
                                  pd_up.at({1,2})).middleCols(pd_lw.at({1,3}),
                                                              pd_lw.at({1,2}));

  // col 0, corresponds to 2:2 of lw
  ans[BA{2,0}] = phi_1.rightCols(pd_lw.at({2,2})).topRows(pd_up.at({1,3}));
  ans[BA{3,0}] = phi_1.rightCols(pd_lw.at({2,2})).middleRows(pd_up.at({1,3}),
                                                            pd_up.at({1,2}));
  ans[BA{4,0}] = phi_1.rightCols(pd_lw.at({2,2})).middleRows(pd_up.at({1,3})+
                                                            pd_up.at({1,2}),
                                                            pd_up.at({2,3}));
  ans[BA{5,0}] = phi_1.rightCols(pd_lw.at({2,2})).bottomRows(pd_up.at({2,2}));

  // col 1, corresponds to 2:3 of lw
  ans[BA{2,1}] = phi_1.middleCols(pd_lw.at({1,3}) + pd_lw.at({1,2}), 
                                   pd_lw.at({2,3})).topRows(pd_up.at({1,3}));
  ans[BA{4,1}] = phi_1.middleCols(pd_lw.at({1,3}) + pd_lw.at({1,2}), 
                                   pd_lw.at({2,3})).middleRows(pd_up.at({1,3})+
                                                            pd_up.at({1,2}),
                                                            pd_up.at({2,3})); 
  return ans; 
}


matrixproblems::LadderMatrixProblem RepnCLfb::create_random_problem(int d){
  matrixproblems::LadderMatrixProblem ans;
  std::vector<matrixproblems::BlockAddress> nonzeros;
  
  set_a2fb_labels(ans, nonzeros); 
  for (auto ba : nonzeros) {
    // ans[ba] = Z2Matrix::Random(d, d); 
    ans[ba] = algorithms::random_matrix_rank_uniform(d,d);
    // ans[ba] = algorithms::random_matrix_with_rank(d,d, int(d/2));
  } 
  
  return ans;
}

matrixproblems::LadderMatrixProblem RepnCLfb::get_ones_problem(){
  matrixproblems::LadderMatrixProblem ans;
  std::vector<matrixproblems::BlockAddress> nonzeros;
  
  set_a2fb_labels(ans, nonzeros); 
  for (auto ba : nonzeros) {
    ans[ba] = Z2Matrix::Identity(1,1); 
  } 
  
  return ans;
}



}
}
