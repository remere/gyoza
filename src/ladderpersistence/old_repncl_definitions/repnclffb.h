#ifndef REPNCLFFB_H
#define REPNCLFFB_H

#include "gyoza/quiverrepn.h"
#include "gyoza/ladder_matrix_problem.h"

namespace gyoza {
namespace ladderpersistence {

class RepnCLffb : public quivers::QuiverRepn {
 public:
  RepnCLffb() : QuiverRepn(quivers::create_ladder_quiver(quivers::QuiverType::Ladder_FFB)) { };

  static matrixproblems::LadderMatrixProblem create_random_problem(int d);
  static matrixproblems::LadderMatrixProblem get_ones_problem(); 
};


}
}

#endif
