#include "gyoza/repnclf.h"
#include "gyoza/snf_algorithms.hpp"
#include "gyoza/random_z2matrix.h"

namespace gyoza {
namespace ladderpersistence {

void set_a2f_labels(matrixproblems::LadderMatrixProblem& ans){
  gyoza::matrixproblems::NameHandler col_names( {{0,"2:2"},{1,"1:2"},{2,"1:1"}} );
  gyoza::matrixproblems::NameHandler row_names( {{0,"1:1"},{1,"1:2"},{2,"2:2"}} );
  gyoza::matrixproblems::PermissibleOperations prm;
  prm.make_permissible(2, 1);
  prm.make_permissible(1, 0);
  ans.set_permissible_row_operations(prm);
  ans.set_permissible_col_operations(prm);

  ans.set_row_names(row_names);
  ans.set_col_names(col_names);
  return;
}

boost::optional<matrixproblems::LadderMatrixProblem> RepnCLf::get_matrix_problem()const {
  matrixproblems::LadderMatrixProblem ans;
  set_a2f_labels(ans);

  algorithms::FullPivotSNF<Z2Matrix> snf_upper( matrix_at(2,3), true);
  algorithms::FullPivotSNF<Z2Matrix> snf_lower( matrix_at(0,1), true );

  int up_12 = snf_upper.rank();
  int up_11 = snf_upper.cols() - up_12;
  int up_22 = snf_upper.rows() - up_12;

  int lw_12 = snf_lower.rank();
  int lw_11 = snf_lower.cols() - lw_12;
  int lw_22 = snf_lower.rows() - lw_12;


  // transformed arrow:
  Z2Matrix left = snf_upper.get_Q_inv() * matrix_at(0,2) * snf_lower.get_Q();
  Z2Matrix right = snf_upper.get_P() * matrix_at(1,3) * snf_lower.get_P_inv();

  // blocks
  typedef gyoza::matrixproblems::BlockAddress BA;
  // 1:2 to 1:2
  ans[ BA(1,1) ] = left.block(0,0, up_12, lw_12);

  // 2:2 to 2:2
  ans[ BA(2,0) ] = right.block(up_12,lw_12, up_22, lw_22);

  // 1:1 to 1:1
  ans[ BA(0,2) ] = left.block(up_12,lw_12, up_11, lw_11);

  // 2:2 to 1:2
  ans[ BA(1,0) ] = right.block(0,lw_12, up_12, lw_22);

  // 1:2 to 1:1
  ans[ BA(0,1) ] = left.block(up_12, 0, up_11, lw_12);

  return ans;
}

matrixproblems::LadderMatrixProblem RepnCLf::create_random_problem(int d){
  matrixproblems::LadderMatrixProblem ans;
  std::vector<matrixproblems::BlockAddress> nonzeros = {{1,1},{2,0},{0,2},{1,0},{0,1}};

  set_a2f_labels(ans);
  for (auto ba : nonzeros) {
    // ans[ba] = Z2Matrix::Random(d, d);
    ans[ba] = algorithms::random_matrix_rank_uniform(d,d);
    // ans[ba] = algorithms::random_matrix_with_rank(d,d, int(d/2));
  }
  return ans;
}

matrixproblems::LadderMatrixProblem RepnCLf::get_ones_problem(){
  matrixproblems::LadderMatrixProblem ans;
  std::vector<matrixproblems::BlockAddress> nonzeros = {{1,1},{2,0},{0,2},{1,0},{0,1}};

  set_a2f_labels(ans);
  for (auto ba : nonzeros) {
    ans[ba] = Z2Matrix::Identity(1,1);
  }

  return ans;
}

}
}
