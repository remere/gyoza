#ifndef REPNCLFFF_H
#define REPNCLFFF_H

#include "gyoza/quiverrepn.h"
#include "gyoza/ladder_matrix_problem.h"

namespace gyoza {
namespace ladderpersistence {

class RepnCLfff : public quivers::QuiverRepn {
 public:
  RepnCLfff() : QuiverRepn(quivers::create_ladder_quiver(quivers::QuiverType::Ladder_FFF)) { };

  static matrixproblems::LadderMatrixProblem create_random_problem(int d);
  static matrixproblems::LadderMatrixProblem get_ones_problem(); 
};


}
}

#endif
