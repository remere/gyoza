#ifndef REPNCLBFB_H
#define REPNCLBFB_H

#include "gyoza/quiverrepn.h"
#include "gyoza/ladder_matrix_problem.h"

namespace gyoza {
namespace ladderpersistence {

class RepnCLbfb : public quivers::QuiverRepn {
 public:
  RepnCLbfb() : QuiverRepn(quivers::create_ladder_quiver(quivers::QuiverType::Ladder_BFB)) { };

  static matrixproblems::LadderMatrixProblem create_random_problem(int d);
  static matrixproblems::LadderMatrixProblem get_ones_problem(); 
};


}
}

#endif
