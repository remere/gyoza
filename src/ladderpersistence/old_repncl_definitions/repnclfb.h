#ifndef REPNCLFB_H
#define REPNCLFB_H

#include "gyoza/quiverrepn.h"
#include "gyoza/ladder_matrix_problem.h"

namespace gyoza {
namespace ladderpersistence {

class RepnCLfb : public quivers::QuiverRepn {
 public:
  RepnCLfb() : QuiverRepn(quivers::create_ladder_quiver(quivers::QuiverType::Ladder_FB)) { };

  RepnCLfb(const quivers::Quiver & quiver) : QuiverRepn(quivers::create_ladder_quiver(quivers::QuiverType::Ladder_FB)) {
    if (quiver.get_type() != quivers::QuiverType::Ladder_FB) {
      throw;
    }
  }; 
  
  boost::optional<matrixproblems::LadderMatrixProblem> get_matrix_problem() const override;

  static matrixproblems::LadderMatrixProblem create_random_problem(int d);
  static matrixproblems::LadderMatrixProblem get_ones_problem();
}; 


}
}

#endif
