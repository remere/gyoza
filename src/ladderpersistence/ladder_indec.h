#ifndef LADDER_INDEC_H
#define LADDER_INDEC_H

#include <string>
#include <set>
#include <vector>
#include <sstream>
#include "gyoza/abstract_indec.h"
#include <boost/optional.hpp>

namespace gyoza{
namespace matrixproblems {

template<unsigned int N>
class IntervalIndec {
  // interval over quiver with vertices
  // 1, 2, ... , N.
 public:
  IntervalIndec(unsigned int b, unsigned int d);

  unsigned int get_birth()const;
  unsigned int get_death()const;

  // dimension at vertex i : 1, 2, ... N
  // is placed into position (index) i-1 in vector
  std::vector<int> get_dimension_vector()const;

  void set_birth(unsigned int b); 
  void set_death(unsigned int d);

  bool is_valid()const;

  bool operator==(const IntervalIndec<N>& other) const;
  bool operator<(const IntervalIndec<N>& other) const;

  friend std::ostream& operator<<(std::ostream& os, const IntervalIndec<N>& obj){
    os << "(" << obj.get_birth() << ", " << obj.get_death() << ")"; 
    return os;
  }

 private:
  unsigned int birth;
  unsigned int death;  
};

template<unsigned int N>
boost::optional<IntervalIndec<N>> string_to_interval(const std::string& code);
  
template<unsigned int N>
class LadderIndec{
 public:
  void insert_to_row_space(IntervalIndec<N> ind);
  void insert_to_col_space(IntervalIndec<N> ind);
  
  std::string to_string()const;
  std::vector<int> get_dimension_vector()const;
  
  bool operator==(const LadderIndec& other)const;
  bool operator<(const LadderIndec& other)const;
  
  friend std::ostream& operator<<(std::ostream& os, const LadderIndec& ind) {
    std::vector<int> dimv = ind.get_dimension_vector();
    os << "[";
    for (unsigned int i = 0; i < dimv.size() - 1; ++i) {
      os << dimv[i] << ",";
    }
    os << *(dimv.rbegin()) << "]";
    return os;
  }
  
 private:
  std::multiset<IntervalIndec<N>> row_space;
  std::multiset<IntervalIndec<N>> col_space; 
};

template<unsigned int N>
boost::optional<LadderIndec<N>> abstract_to_ladder_indec(const AbstractIndec & ind);


}
}


#endif
