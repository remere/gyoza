#ifndef PROCESSTREE_H
#define PROCESSTREE_H

#include <memory>
#include <set>
#include "gyoza/addressing.h"

namespace gyoza {
namespace matrixproblems {

class ProcessVertex {
 public:
  ProcessVertex(const BlockAddress & target,
                const BlockAddress & identity): vT(target), vE(identity), successors{} {}

  BlockAddress get_target()const{return vT;}
  BlockAddress get_eraser()const{return vE;}
  std::set<std::shared_ptr<ProcessVertex>> get_successors()const{return successors;}

  void add_as_successor(std::shared_ptr<ProcessVertex> v){
    if (v.get() != nullptr) {
      successors.insert(v);
    }
  }

  bool operator==(const ProcessVertex& other)const{
    return ((vT == other.get_target()) && (vE == other.get_eraser()));
  }
  bool operator!=(const ProcessVertex& other)const{
    return (not operator==(other));
  }

  bool operator<(const ProcessVertex& rhs) const {
    return (vT < rhs.vT) ||
        ((vT == rhs.vT) && (vE < rhs.vE));
  }

 private:
  BlockAddress vT;
  BlockAddress vE;
  std::set<std::shared_ptr<ProcessVertex>> successors;
};

class ProcessTree {
 public:
  ProcessTree():root(nullptr){};
  ProcessTree(const BlockAddress & target,
              const BlockAddress & identity):
      root(std::make_shared<ProcessVertex>(target,identity)){;}

  std::shared_ptr<ProcessVertex> get_root()const{return root;}

  void attach_tree_to_root(ProcessTree t) {
    root->add_as_successor(t.get_root());
  }

 private:
  std::shared_ptr<ProcessVertex> root;
};

}
}


#endif
