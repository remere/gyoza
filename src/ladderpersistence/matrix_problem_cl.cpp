#include "gyoza/matrix_problem_cl.h"
#include "gyoza/snf_algorithms.hpp"
#include "gyoza/algo.h"
#include "gyoza/log_control.h"

namespace gyoza {
namespace ladderpersistence {

namespace mp=matrixproblems;

template<quivers::QuiverType T>
void get_cl_data(mp::NameHandler& col_names,
                 std::vector<mp::BlockAddress>& nonzeros);

template<quivers::QuiverType T>
void set_cl_labels(mp::LadderMatrixProblem& ans,
                   std::vector<mp::BlockAddress>& nonzeros) {
  mp::NameHandler col_names;
  get_cl_data<T>(col_names, nonzeros);

  const int max_index = col_names.get_data_map().rbegin()->first;

  mp::NameHandler row_names = col_names.get_reverse(max_index);
  mp::PermissibleOperations col_prm;
  for (auto ba : nonzeros) {
    col_prm.make_permissible(max_index - ba.row_index, ba.col_index);
  }
  mp::PermissibleOperations row_prm = col_prm.get_dual().get_reverse(max_index);

  ans.set_permissible_col_operations(col_prm);
  ans.set_permissible_row_operations(row_prm);

  ans.set_row_names(row_names);
  ans.set_col_names(col_names);
  return;
}

template<quivers::QuiverType T>
mp::LadderMatrixProblem MatrixProblemCL<T>::create_random_problem(int d){
  mp::LadderMatrixProblem ans;
  std::vector<mp::BlockAddress> nonzeros;

  set_cl_labels<T>(ans, nonzeros);
  for (auto ba : nonzeros) {
    ans[ba] = Z2Matrix::Random(d, d);
  }

  return ans;
}

template<quivers::QuiverType T>
mp::LadderMatrixProblem MatrixProblemCL<T>::get_ones_problem(){
  mp::LadderMatrixProblem ans;
  std::vector<mp::BlockAddress> nonzeros;
  set_cl_labels<T>(ans, nonzeros);

  for (auto ba : nonzeros) {
    ans[ba] = Z2Matrix::Identity(1,1);
  }

  return ans;
}



boost::optional<mp::BlockAddress> name_to_ba_helper(const mp::LadderMatrixProblem& lmp, const std::string& row_name, const std::string& col_name){
  std::vector<int> row_nums = lmp.get_row_names().get_numbers(row_name);
  std::vector<int> col_nums = lmp.get_col_names().get_numbers(col_name);

  if (row_nums.size() == 0 or col_nums.size() == 0) {
    if (do_log(LogLevel::LOG_ERROR)){
      std::cerr << "Row/column pair string unrecognized: " << row_name << " " << col_name << std::endl;
    }
    return boost::none;
  }

  if (row_nums.size() > 1 or col_nums.size() > 1) {
    if (do_log(LogLevel::LOG_WARNING)){
      std::cerr << "name_to_ba_helper called on row/col pair name string: " << row_name << " " << col_name << " with more than one occurence in matrix problem.\n";
      std::cerr << "This is function is not supposed to be called on such a matrix problem." << std::endl;
    }
    return boost::none;
  }

  return mp::BlockAddress{*(row_nums.cbegin()),*(col_nums.cbegin())};
}

template<quivers::QuiverType T>
boost::optional<mp::LadderMatrixProblem> MatrixProblemCL<T>::lmp_ascii_read(std::istream& input){
  mp::LadderMatrixProblem ans;
  std::vector<mp::BlockAddress> nonzeros;
  set_cl_labels<T>(ans, nonzeros);
  std::set<mp::BlockAddress> nonzeros_set(nonzeros.begin(), nonzeros.end());

  std::string line;
  bool no_problems = true;

  // read to next '#' character that indicates start of a matrix block.
  while (std::getline(input, line, '#')) {
    if (std::getline(input, line)) {
      std::string row_name, col_name;
      std::stringstream ss(line);
      ss >> row_name >> col_name;

      boost::optional<mp::BlockAddress> ba = name_to_ba_helper(ans, row_name, col_name);
      auto mat_read = gyoza::z2matrix_ascii_read(input);

      if (ba == boost::none) {
        if (do_log(LogLevel::LOG_ERROR)){
          std::cerr << "Block unrecognized: " << row_name << " " << col_name << std::endl;
        }
        no_problems = false;
        break;
      }
      if (mat_read == boost::none) {
        if (do_log(LogLevel::LOG_ERROR)){
          std::cerr << "Failed to read matrix at block: " << row_name << " " << col_name << std::endl;
        }
        no_problems = false;
        break;
      }
      if (nonzeros_set.count(*ba) == 0){
        if (do_log(LogLevel::LOG_ERROR)){
          std::cerr << "Block: " << row_name << " " << col_name << " cannot be nonzero in given orientation." << std::endl;
        }
        no_problems = false;
        break;
      }
      ans[*ba] = *mat_read;
    }
  }
  if (not ans.check_dimensions()) {
    log<LogLevel::LOG_ERROR>("LMP read from input has inconsistent dimensions.", std::cerr);
    no_problems = false;
  }

  if (no_problems) {
    return ans;
  } else {
    return boost::none;
  }
}





mp::LadderMatrixProblem convert_clfb(quivers::QuiverRepn repn);
mp::LadderMatrixProblem convert_clf(quivers::QuiverRepn repn);

template<quivers::QuiverType T>
boost::optional<mp::LadderMatrixProblem> MatrixProblemCL<T>::get_matrix_problem(quivers::QuiverRepn repn){
  if (repn.get_quiver().get_type() == quivers::QuiverType::Ladder_FB) {
    return convert_clfb(repn);
  } else if (repn.get_quiver().get_type() == quivers::QuiverType::Ladder_F) {
    return convert_clf(repn);
  }

  return boost::none;
}

// hardcoding:
template class MatrixProblemCL<quivers::QuiverType::Ladder_F>;

template class MatrixProblemCL<quivers::QuiverType::Ladder_FB>;
template class MatrixProblemCL<quivers::QuiverType::Ladder_BF>;
template class MatrixProblemCL<quivers::QuiverType::Ladder_FF>;
template class MatrixProblemCL<quivers::QuiverType::Ladder_BB>;

// hardcoding:

template class MatrixProblemCL<quivers::QuiverType::Ladder_FFF>;
template class MatrixProblemCL<quivers::QuiverType::Ladder_FFB>;
template class MatrixProblemCL<quivers::QuiverType::Ladder_BFB>;
template class MatrixProblemCL<quivers::QuiverType::Ladder_BBF>;

template class MatrixProblemCL<quivers::QuiverType::Ladder_BBB>;
template class MatrixProblemCL<quivers::QuiverType::Ladder_FBB>;
template class MatrixProblemCL<quivers::QuiverType::Ladder_FBF>;
template class MatrixProblemCL<quivers::QuiverType::Ladder_BFF>;


// length 2
template<>
void get_cl_data<quivers::QuiverType::Ladder_F>
(mp::NameHandler& col_names, std::vector<mp::BlockAddress>& nonzeros){
  col_names = mp::NameHandler({
      {0,"2:2"},
      {1,"1:2"},
      {2,"1:1"}
    });
  nonzeros = {{1,1},{2,0},{0,2},{1,0},{0,1}};
}


// length 3
template<>
void get_cl_data<quivers::QuiverType::Ladder_FF>
(mp::NameHandler& col_names, std::vector<mp::BlockAddress>& nonzeros){
  col_names = mp::NameHandler({
      {0,"3:3"},
      {1,"2:3"},
      {2,"2:2"},
      {3,"1:3"},
      {4,"1:2"},
      {5,"1:1"}
    });

  nonzeros = {
    {0,3}, {0,4}, {0,5},
    {1,1}, {1,2}, {1,3}, {1,4},
    {2,0}, {2,1}, {2,3},
    {3,1}, {3,2},
    {4,0}, {4,1},
    {5,0}
  };
}


template<>
void get_cl_data<quivers::QuiverType::Ladder_FB>
(mp::NameHandler& col_names, std::vector<mp::BlockAddress>& nonzeros){
  col_names = mp::NameHandler({
      {0,"2:2"},
      {1,"2:3"},
      {2,"1:2"},
      {3,"1:3"},
      {4,"3:3"},
      {5,"1:1"}
    });

  nonzeros = {
    {0,2}, {0,3}, {0,5},
    {1,1}, {1,3}, {1,4},
    {2,0}, {2,1}, {2,2}, {2,3},
    {3,0}, {3,2},
    {4,0}, {4,1},
    {5,0}
  };
}


template<>
void get_cl_data<quivers::QuiverType::Ladder_BF>
(mp::NameHandler& col_names, std::vector<mp::BlockAddress>& nonzeros){
  col_names = mp::NameHandler({
      {0,"3:3"},
      {1,"1:1"},
      {2,"1:3"},
      {3,"2:3"},
      {4,"1:2"},
      {5,"2:2"}
    });

  nonzeros = {
    {0,2}, {0,3}, {0,4}, {0,5},
    {1,1}, {1,2}, {1,4},
    {2,0}, {2,2}, {2,3},
    {3,0}, {3,1}, {3,2},
    {4,1},
    {5,0}
  };
}


template<>
void get_cl_data<quivers::QuiverType::Ladder_BB>
(mp::NameHandler& col_names, std::vector<mp::BlockAddress>& nonzeros){
  col_names = mp::NameHandler({
      {0,"1:1"},
      {1,"1:2"},
      {2,"2:2"},
      {3,"1:3"},
      {4,"2:3"},
      {5,"3:3"}
    });

  nonzeros = {
    {0,3}, {0,4}, {0,5},
    {1,1}, {1,2}, {1,3}, {1,4},
    {2,0}, {2,1}, {2,3},
    {3,1}, {3,2},
    {4,0}, {4,1},
    {5,0}
  };
}


// length 4

template<>
void get_cl_data<quivers::QuiverType::Ladder_FFF>
(mp::NameHandler& col_names, std::vector<mp::BlockAddress>& nonzeros){
  col_names = mp::NameHandler({
      {0,"4:4"},
      {1,"3:4"},
      {2,"3:3"},
      {3,"2:4"},
      {4,"2:3"},
      {5,"1:4"},
      {6,"2:2"},
      {7,"1:3"},
      {8,"1:2"},
      {9,"1:1"}
    });

  nonzeros = {
    {0,5}, {0,7}, {0,8}, {0,9},
    {1,3}, {1,4}, {1,5}, {1,6}, {1,7}, {1,8},
    {2,1}, {2,2}, {2,3}, {2,4}, {2,5}, {2,7},
    {3,3}, {3,4}, {3,6},
    {4,0}, {4,1}, {4,3}, {4,5},
    {5,1}, {5,2}, {5,3}, {5,4},
    {6,0}, {6,1}, {6,3},
    {7,1}, {7,2},
    {8,0}, {8,1},
    {9,0}
  };
}

template<>
void get_cl_data<quivers::QuiverType::Ladder_FFB>
(mp::NameHandler& col_names, std::vector<mp::BlockAddress>& nonzeros){
  col_names = mp::NameHandler({
      {0,"3:3"},
      {1,"3:4"},
      {2,"2:3"},
      {3,"2:4"},
      {4,"1:3"},
      {5,"2:2"},
      {6,"1:4"},
      {7,"4:4"},
      {8,"1:2"},
      {9,"1:1"}
    });
  nonzeros = {
    {0,4}, {0,6}, {0,8}, {0,9},
    {1,2}, {1,3}, {1,4}, {1,5}, {1,6}, {1,8},
    {2,1}, {2,3}, {2,6}, {2,7},
    {3,0}, {3,1}, {3,2}, {3,3}, {3,4}, {3,6},
    {4,2}, {4,3}, {4,5},
    {5,0}, {5,2}, {5,4},
    {6,0}, {6,1}, {6,2}, {6,3},
    {7,0}, {7,2},
    {8,0}, {8,1},
    {9,0}
  } ;
}


template<>
void get_cl_data<quivers::QuiverType::Ladder_BFB>
(mp::NameHandler& col_names, std::vector<mp::BlockAddress>& nonzeros){
  col_names = mp::NameHandler({
      {0,"3:3"},
      {1,"1:1"},
      {2,"3:4"},
      {3,"1:3"},
      {4,"2:3"},
      {5,"1:4"},
      {6,"2:4"},
      {7,"1:2"},
      {8,"4:4"},
      {9,"2:2"}
    });

  nonzeros = {
    {0,3}, {0,4}, {0,5}, {0,6}, {0,7}, {0,9},
    {1,2}, {1,5}, {1,6}, {1,8},
    {2,1}, {2,3}, {2,5}, {2,7},
    {3,0}, {3,2}, {3,3}, {3,4}, {3,5}, {3,6},
    {4,0}, {4,1}, {4,2}, {4,3}, {4,5},
    {5,0}, {5,3}, {5,4},
    {6,0}, {6,1}, {6,3},
    {7,0}, {7,2},
    {8,1},
    {9,0}
  };
}


template<>
void get_cl_data<quivers::QuiverType::Ladder_BBF>
(mp::NameHandler& col_names, std::vector<mp::BlockAddress>& nonzeros){
  col_names = mp::NameHandler({
      {0,"1:1"},
      {1,"4:4"},
      {2,"1:2"},
      {3,"2:2"},
      {4,"1:4"},
      {5,"2:4"},
      {6,"1:3"},
      {7,"3:4"},
      {8,"2:3"},
      {9,"3:3"}
    });

  nonzeros = {
    {0,4}, {0,5}, {0,6}, {0,7}, {0,8}, {0,9},
    {1,2}, {1,3}, {1,4}, {1,5}, {1,6}, {1,8},
    {2,1}, {2,4}, {2,5}, {2,7},
    {3,0}, {3,2}, {3,4}, {3,6},
    {4,1}, {4,2}, {4,3}, {4,4}, {4,5},
    {5,0}, {5,1}, {5,2}, {5,4},
    {6,2}, {6,3},
    {7,0}, {7,2},
    {8,1},
    {9,0}
  };
}


template<>
void get_cl_data<quivers::QuiverType::Ladder_BBB>
(mp::NameHandler& col_names, std::vector<mp::BlockAddress>& nonzeros){
  col_names = mp::NameHandler({
      {0,"1:1"},
      {1,"1:2"},
      {2,"2:2"},
      {3,"1:3"},
      {4,"2:3"},
      {5,"1:4"},
      {6,"3:3"},
      {7,"2:4"},
      {8,"3:4"},
      {9,"4:4"}
    });

  nonzeros = {
    {0,5}, {0,7}, {0,8}, {0,9},
    {1,3}, {1,4}, {1,5}, {1,6}, {1,7}, {1,8},
    {2,1}, {2,2}, {2,3}, {2,4}, {2,5}, {2,7},
    {3,3}, {3,4}, {3,6},
    {4,0}, {4,1}, {4,3}, {4,5},
    {5,1}, {5,2}, {5,3}, {5,4},
    {6,0}, {6,1}, {6,3},
    {7,1}, {7,2},
    {8,0}, {8,1},
    {9,0}
  };
}


template<>
void get_cl_data<quivers::QuiverType::Ladder_FBB>
(mp::NameHandler& col_names, std::vector<mp::BlockAddress>& nonzeros){
  col_names = mp::NameHandler({
      {0,"2:2"},
      {1,"2:3"},
      {2,"1:2"},
      {3,"2:4"},
      {4,"1:3"},
      {5,"3:3"},
      {6,"1:4"},
      {7,"3:4"},
      {8,"1:1"},
      {9,"4:4"}
    });

  nonzeros = {
    {0,3}, {0,6}, {0,7}, {0,9},
    {1,2}, {1,4}, {1,6}, {1,8},
    {2,1}, {2,3}, {2,4}, {2,5}, {2,6}, {2,7},
    {3,0}, {3,1}, {3,2}, {3,3}, {3,4}, {3,6},
    {4,1}, {4,4}, {4,5},
    {5,0}, {5,1}, {5,2}, {5,4},
    {6,0}, {6,1}, {6,3},
    {7,0}, {7,2},
    {8,0}, {8,1},
    {9,0}
  };
}


template<>
void get_cl_data<quivers::QuiverType::Ladder_FBF>
(mp::NameHandler& col_names, std::vector<mp::BlockAddress>& nonzeros){
  col_names = mp::NameHandler({
      {0,"4:4"},
      {1,"2:2"},
      {2,"2:4"},
      {3,"1:2"},
      {4,"2:3"},
      {5,"1:4"},
      {6,"3:4"},
      {7,"1:3"},
      {8,"3:3"},
      {9,"1:1"}
    });

  nonzeros = {
    {0,3}, {0,5}, {0,7}, {0,9},
    {1,2}, {1,4}, {1,5}, {1,6}, {1,7}, {1,8},
    {2,1}, {2,2}, {2,3}, {2,4}, {2,5}, {2,7},
    {3,0}, {3,2}, {3,5}, {3,6},
    {4,0}, {4,1}, {4,2}, {4,3}, {4,5},
    {5,1}, {5,2}, {5,4},
    {6,1}, {6,3},
    {7,0}, {7,1}, {7,2},
    {8,1},
    {9,0}
  };
}


template<>
void get_cl_data<quivers::QuiverType::Ladder_BFF>
(mp::NameHandler& col_names, std::vector<mp::BlockAddress>& nonzeros){
  col_names = mp::NameHandler({
      {0,"4:4"},
      {1,"3:4"},
      {2,"1:1"},
      {3,"3:3"},
      {4,"1:4"},
      {5,"2:4"},
      {6,"1:3"},
      {7,"2:3"},
      {8,"1:2"},
      {9,"2:2"}
    });

  nonzeros = {
    {0,4}, {0,5}, {0,6}, {0,7}, {0,8}, {0,9},
    {1,2}, {1,4}, {1,6}, {1,8},
    {2,1}, {2,3}, {2,4}, {2,5}, {2,6}, {2,7},
    {3,1}, {3,2}, {3,3}, {3,4}, {3,6},
    {4,0}, {4,1}, {4,4}, {4,5},
    {5,0}, {5,1}, {5,2}, {5,4},
    {6,1}, {6,3},
    {7,2},
    {8,0}, {8,1},
    {9,0}
  };
}


}
}
