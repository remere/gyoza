#include "gyoza/orderhandler.h"
#include <numeric>
#include <algorithm>

namespace gyoza {
namespace matrixproblems {

void OrderHandler::generate_default_order(int min, int max){
  data.clear();
  if (max >= min) {
    size_type count = max - min + 1;
    data = data_type(count, 0);
    std::iota(data.begin(), data.end(), min);
  }
  values.insert(data.begin(), data.end());
  return; 
}
  
bool OrderHandler::has_value(const value_type & value) {
  return bool(values.count(value));
}

bool OrderHandler::insert_after(const value_type & prior,
                                const value_type & post) {    
  auto in_values = values.find(post);
  auto it = std::find(data.begin(), data.end(), prior);
  if (in_values != values.end() || it == data.end()) {
    // new value "post" already exists, or
    // prior value "prior" not found
    return false;
  } 
  ++it;
  data.insert(it, post);
  values.insert(in_values, post);
  return true;
}

bool OrderHandler::push_back(const value_type & value) {
  auto in_values = values.find(value);
  if (in_values != values.end()) {
    return false;
  }
  data.push_back(value);    
  values.insert(in_values, value);
  return true;
}

  
bool OrderHandler::push_front(const value_type & value) {
  auto in_values = values.find(value);
  if (in_values != values.end()) {
    return false;
  }
  data.push_front(value);
  values.insert(in_values, value);
  return true;
}



}
}
