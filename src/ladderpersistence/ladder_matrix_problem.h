#ifndef LADDER_MATRIX_PROBLEM_H
#define LADDER_MATRIX_PROBLEM_H

#include "gyoza/matrix_problem.h"
#include "gyoza/statushandler.h"
#include "gyoza/processtree.h"
#include <set>
#include <vector>
#include <boost/optional.hpp>

// used only for creating MatrixProblem<Z2Matrix>
#include "gyoza/common_definitions.h"

#include "abstract_indec.h"

namespace gyoza{
namespace matrixproblems {

class LadderMatrixProblem : public MatrixProblem<gyoza::Z2Matrix> {
 public:
  typedef MatrixProblem<gyoza::Z2Matrix>::SubIndex SubIndex;
  
  LadderMatrixProblem()=default; 
  
  void initialize();
  void initialize_status();

  bool verify_status()const;
  std::vector<BlockAddress> get_status_inconsistent_blocks()const; 

  const OrderHandler& get_row_order()const {return row_order;}
  const OrderHandler& get_col_order()const {return col_order;}

  BlockStatus get_status(const BlockAddress & ba)const;
  const StatusHandler& get_status()const {return status;}
  void pretty_print_status(std::ostream& os)const;
  void print_reduction_debug_info(std::ostream& os)const;
  
  int matrix_reduction(int max_count, bool split_empty=true);
  boost::optional<int> number_of_noniso_summands()const;


  // on persistence diagrams
  template<unsigned int N> bool attempt_compute_pd();  
  bool output_pd(std::ostream& os)const;

  boost::optional<std::map<AbstractIndec, int>> get_abstract_decomposition()const;


  // want to somehow hide:
  
  // ******************** next block ********************
  boost::optional<BlockAddress> get_next_unprocessed_block() const;
  boost::optional<BlockAddress> get_next_untargeted_unprocessed_block() const;

  // ****************** edge reduction ******************
  SubIndex edge_reduction(const BlockAddress& v);
  
  struct TransformPair {
    TransformPair(const Z2Matrix & P,
                  const Z2Matrix & Pinv):trans(P), invtrans(Pinv){}; 

    Z2Matrix trans;
    Z2Matrix invtrans; 
  }; 

  void row_fix(const BlockAddress & v,
               const TransformPair & P,
               bool use_inverse,
               std::set<int> & visited_rows,
               std::set<int> & visited_cols);
  void col_fix(const BlockAddress & v,
               const TransformPair & P,
               bool use_inverse,
               std::set<int> & visited_rows,
               std::set<int> & visited_cols); 
  
  void split_snf(const BlockAddress & v, SubIndex rank);



  // ******************** regularization ******************** 
  void execute_process(const ProcessTree& pt); 
  ProcessTree row_erasable(const BlockAddress& target,
                           BlockAddress const * p_flagged,
                           std::set<BlockAddress> visited)const;
  ProcessTree col_erasable(const BlockAddress& target,
                           BlockAddress const * p_flagged,
                           std::set<BlockAddress> visited)const;
  ProcessTree erasable(const BlockAddress& target,
                       BlockAddress const * p_flagged = nullptr,
                       std::set<BlockAddress> visited = std::set<BlockAddress>{})const;
  unsigned int greedy_erase();
  void neighbor_erase(const BlockAddress& v);

  void set_status(const BlockAddress& key, const BlockStatus& stat){
    status.set_status(key, stat);
  }
  
 protected:
  // overwrite split functions to incorporate status update!  
  void post_process_block(const BlockAddress& new_ba,
                          const BlockAddress& old_ba) override;
  
 private:
  StatusHandler status;
  OrderHandler row_order;
  OrderHandler col_order;

  boost::optional<std::map<std::string, int>> pd = boost::none;

};

}
}


#endif
