#include "gyoza/statushandler.h"
#include <algorithm>
#include <numeric>
#include <memory>

namespace gyoza {
namespace matrixproblems {

std::ostream& operator<<(std::ostream& out, const BlockStatus value){
  static std::map<BlockStatus, char> chars;
  if (chars.size() == 0){
    chars[BlockStatus::Unprocessed] = '*';
    chars[BlockStatus::Identity] = 'E';
    chars[BlockStatus::Zero] = '0';
    chars[BlockStatus::Null] = '.';
  }
  return out << chars[value];
}

BlockStatus StatusHandler::get_status(const BlockAddress& key)const {
  auto it = data.find(key);
  if (it == data.end()) {
    return BlockStatus::Null;
  } else {
    return it->second;
  }
}

void StatusHandler::set_status(const BlockAddress& key, const BlockStatus& status) {
  data[key] = status;
}

void StatusHandler::clear() {
  data.clear();
}

bool StatusHandler::has_no_unprocessed()const{
  for (auto const & ba_stat_pair: data) {
    if (ba_stat_pair.second == BlockStatus::Unprocessed) {
      return false;
    }
  }
  return true; 
}

std::vector<BlockAddress> StatusHandler::get_identity_blocks()const{
  std::vector<BlockAddress> ans;
  for (auto const & ba_stat_pair: data) {
    if (ba_stat_pair.second == BlockStatus::Identity) {
      ans.push_back(ba_stat_pair.first);
    }
  }
  return ans; 
}

void StatusHandler::query_rows_cols_without_identities(std::vector<int> * row_indices, std::vector<int> * col_indices) const {
  if (row_indices == nullptr && col_indices == nullptr) {
    return;
  }

  auto bdds = get_bounds();
  const int rowmin = std::get<RowMin>(bdds);
  const int colmin = std::get<ColMin>(bdds);
  const int nrows = std::get<RowMax>(bdds) - std::get<RowMin>(bdds) + 1;
  const int ncols = std::get<ColMax>(bdds) - std::get<ColMin>(bdds) + 1;
  
  std::vector<bool> row_has_id(nrows, false);
  std::vector<bool> col_has_id(ncols, false); 
  std::vector<BlockAddress> identities = get_identity_blocks();
  for (auto const & ba : identities) {
    row_has_id[ ba.row_index - rowmin ] = true;
    col_has_id[ ba.col_index - colmin ] = true;
  } 
  
  if (row_indices != nullptr) {
    row_indices->clear();
    for (int r = std::get<RowMin>(bdds); r <= std::get<RowMax>(bdds); ++r){
      if (row_has_id[ r - rowmin ] == false) {
        row_indices->push_back(r);
      }
    } 
  }
  if (col_indices != nullptr) {
    col_indices->clear();
    for (int c = std::get<ColMin>(bdds); c <= std::get<ColMax>(bdds); ++c){
      if (col_has_id[ c - colmin ] == false) {
        col_indices->push_back(c);
      }
    } 
  }
  return; 
}


template<typename T>
void replace_value_by(std::map<int, T>& data,
                      const T & search_value,
                      const T & replacement_value) {
  for (auto it = data.begin(); it != data.end(); ++it){
    if (it->second == search_value) {
      it->second = replacement_value;
    }
  }
  return;
} 

std::vector<std::vector<BlockAddress>> StatusHandler::get_partition_of_identities()const{
  using Cluster = std::vector<BlockAddress>;
  using ClusterPtr = std::shared_ptr<Cluster>;
  
  std::set<ClusterPtr> cluster_pointers;  
  std::map<int, ClusterPtr> row_to_cluster;
  std::map<int, ClusterPtr> col_to_cluster;

  std::vector<BlockAddress> identities = get_identity_blocks();
  for (const auto & ba : identities) {
    auto row_it = row_to_cluster.find(ba.row_index);
    auto col_it = col_to_cluster.find(ba.col_index);

    bool row_found = (row_it != row_to_cluster.end());
    bool col_found = (col_it != col_to_cluster.end());

    if ((not row_found) && (not col_found)) {
      ClusterPtr new_cluster = std::make_shared<Cluster>(1,ba);
      cluster_pointers.insert(new_cluster);
      row_to_cluster[ba.row_index] = new_cluster;
      col_to_cluster[ba.col_index] = new_cluster;
    }
    else if (row_found && (not col_found)) {
      ClusterPtr p_cluster = row_it->second;
      p_cluster->push_back(ba);
      row_to_cluster[ba.row_index] = p_cluster;
      col_to_cluster[ba.col_index] = p_cluster;
    }
    else if ((not row_found) && col_found) {
      ClusterPtr p_cluster = col_it->second;
      p_cluster->push_back(ba);
      row_to_cluster[ba.row_index] = p_cluster;
      col_to_cluster[ba.col_index] = p_cluster;
    }
    else if (row_found && col_found) {
      ClusterPtr p_row_cluster = row_it->second;
      ClusterPtr p_col_cluster = col_it->second;
      p_row_cluster->push_back(ba);
      // concatenate
      p_row_cluster->insert(p_row_cluster->end(),
                            p_col_cluster->begin(),
                            p_col_cluster->end());
      // col_cluster now merged into row_cluster, so
      // should be removed.
      replace_value_by(row_to_cluster, p_col_cluster, p_row_cluster);
      replace_value_by(col_to_cluster, p_col_cluster, p_row_cluster);
      cluster_pointers.erase(p_col_cluster);      
    } 
  }

  std::vector<Cluster> ans(cluster_pointers.size());
  int j = 0; 
  for (auto it = cluster_pointers.begin(); it != cluster_pointers.end();
       ++it) {
    ans[j] = (*(*it)); 
    ++j;
  } 
  return ans; 
}



std::vector<BlockAddress>
StatusHandler::get_identity_row_neighbors(const BlockAddress & ba)const {
  std::vector<BlockAddress> ans;
  for (auto it = get_next_at_row(data.begin(), ba.row_index);
       it != data.end();
       it = get_next_at_row(it, ba.row_index)) {
    if ((it->first != ba) &&
        get_status(it->first) == BlockStatus::Identity) {
      ans.push_back(it->first);
    }
    ++it;
  }
  return ans; 
}

std::vector<BlockAddress>
StatusHandler::get_nonzero_row_neighbors(const BlockAddress & ba)const {
  std::vector<BlockAddress> ans;
  for (auto it = get_next_at_row(data.begin(), ba.row_index);
       it != data.end();
       it = get_next_at_row(it, ba.row_index)) {
    if ((it->first != ba) &&
        get_status(it->first) != BlockStatus::Null &&
        get_status(it->first) != BlockStatus::Zero) {
      ans.push_back(it->first);
    }
    ++it;
  }
  return ans; 
}

std::vector<BlockAddress>
StatusHandler::get_identity_col_neighbors(const BlockAddress & ba)const {
  std::vector<BlockAddress> ans;
  for (auto it = get_next_at_col(data.begin(), ba.col_index);
       it != data.end();
       it = get_next_at_col(it, ba.col_index)) {
    if ((it->first != ba) &&
        get_status(it->first) == BlockStatus::Identity) {
      ans.push_back(it->first);
    }
    ++it;
  }
  return ans; 
}

std::vector<BlockAddress>
StatusHandler::get_nonzero_col_neighbors(const BlockAddress & ba)const {
  std::vector<BlockAddress> ans;
  for (auto it = get_next_at_col(data.begin(), ba.col_index);
       it != data.end();
       it = get_next_at_col(it, ba.col_index)) {
    if ((it->first != ba) &&
        get_status(it->first) != BlockStatus::Null &&
        get_status(it->first) != BlockStatus::Zero) {
      ans.push_back(it->first);
    }
    ++it;
  }
  return ans; 
}

std::vector<BlockAddress>
StatusHandler::get_nonzero_neighbors(const BlockAddress & ba)const {
  std::vector<BlockAddress> ans = get_nonzero_row_neighbors(ba);
  std::vector<BlockAddress> ans_c = get_nonzero_col_neighbors(ba);

  ans.reserve(ans.size() + ans_c.size());
  ans.insert(ans.end(), ans_c.begin(), ans_c.end());
  
  return ans; 
} 




std::tuple<int,int,int,int> StatusHandler::get_bounds() const {
  if (data.size() == 0) {
    throw std::runtime_error("No bounds can be defined for emptyness");
  }

  auto it = data.cbegin();
  int min_row_index = it->first.row_index;
  int max_row_index = it->first.row_index;
  int min_col_index = it->first.col_index;
  int max_col_index = it->first.col_index;
  ++it;

  for ( ; it != data.cend(); ++it) {
    min_row_index = std::min(it->first.row_index, min_row_index);
    max_row_index = std::max(it->first.row_index, max_row_index);

    min_col_index = std::min(it->first.col_index, min_col_index);
    max_col_index = std::max(it->first.col_index, max_col_index);
  }
  return std::tuple<int,int,int,int>{min_row_index, max_row_index,
			min_col_index, max_col_index};
}

void StatusHandler::pretty_print(std::ostream& os,
                                 OrderHandler row_order,
                                 OrderHandler col_order)const {
  for (auto it = row_order.cbegin(); it != row_order.cend(); ++it) {
    for (auto jt = col_order.cbegin(); jt != col_order.cend(); ++jt) {
      os << get_status( BlockAddress(*it, *jt) );
    }
    os << "\n";
  }
}

void StatusHandler::pretty_print(std::ostream& os) const {
  if (data.size() == 0) {
    os << "[]\n";
    return;
  }
  std::tuple<int,int,int,int> bdds = get_bounds();
  OrderHandler row_order(std::get<RowMin>(bdds), std::get<RowMax>(bdds));
  OrderHandler col_order(std::get<ColMin>(bdds), std::get<ColMax>(bdds));

  pretty_print(os, row_order, col_order);
}


std::map<BlockAddress, BlockStatus>::iterator
StatusHandler::get_next_at_row(std::map<BlockAddress, BlockStatus>::iterator first, int row) {
  return std::find_if(first, data.end(), [&row](const std::map<BlockAddress, BlockStatus>::value_type & value){return value.first.row_index == row;});
}

std::map<BlockAddress, BlockStatus>::iterator
StatusHandler::get_next_at_col(std::map<BlockAddress, BlockStatus>::iterator first, int col) {
  return std::find_if(first, data.end(), [&col](const std::map<BlockAddress, BlockStatus>::value_type & value){return value.first.col_index == col;});
}

std::map<BlockAddress, BlockStatus>::const_iterator
StatusHandler::get_next_at_row(std::map<BlockAddress, BlockStatus>::const_iterator first, int row)const{
  return std::find_if(first, data.end(), [&row](const std::map<BlockAddress, BlockStatus>::value_type & value){return value.first.row_index == row;});
}

std::map<BlockAddress, BlockStatus>::const_iterator
StatusHandler::get_next_at_col(std::map<BlockAddress, BlockStatus>::const_iterator first, int col)const{
  return std::find_if(first, data.end(), [&col](const std::map<BlockAddress, BlockStatus>::value_type & value){return value.first.col_index == col;});
}



// ********** helpers for query splitting **********

std::map<BlockAddress, BlockStatus>::const_iterator
get_next_at_row(std::map<BlockAddress, BlockStatus>::const_iterator first,
                std::map<BlockAddress, BlockStatus>::const_iterator pastlast,
                int row);
std::map<BlockAddress, BlockStatus>::const_iterator
get_next_at_col(std::map<BlockAddress, BlockStatus>::const_iterator first,
                std::map<BlockAddress, BlockStatus>::const_iterator pastlast,
                int col);

void col_splitting_helper(const BlockAddress & v,
                          const std::map<BlockAddress, BlockStatus> & data, 
                          std::set<BlockAddress>& identities_split,
                          std::set<int> & visited_rows,
                          std::set<int> & visited_cols);

void row_splitting_helper(const BlockAddress & v,
                          const std::map<BlockAddress, BlockStatus> & data, 
                          std::set<BlockAddress>& identities_split,
                          std::set<int> & visited_rows,
                          std::set<int> & visited_cols); 

void StatusHandler::query_splitting(const BlockAddress & v,
                                    std::set<BlockAddress>& identities_split) const {
  identities_split.clear(); 

  std::set<int> visited_rows;
  std::set<int> visited_cols;
  row_splitting_helper(v, data,
                       identities_split,
                       visited_rows, visited_cols);
  col_splitting_helper(v, data,
                       identities_split,
                       visited_rows, visited_cols);
  if (get_status(v) == BlockStatus::Identity) {
    identities_split.insert(v);
  }
  
  return;
} 


// HELPERS, implementations
std::map<BlockAddress, BlockStatus>::const_iterator
get_next_at_row(std::map<BlockAddress, BlockStatus>::const_iterator first,
                std::map<BlockAddress, BlockStatus>::const_iterator pastlast,
                int row){
  return std::find_if(first,
                      pastlast,
                      [&row](const std::map<BlockAddress, BlockStatus>::value_type & value){return value.first.row_index == row;});
}

std::map<BlockAddress, BlockStatus>::const_iterator
get_next_at_col(std::map<BlockAddress, BlockStatus>::const_iterator first,
                std::map<BlockAddress, BlockStatus>::const_iterator pastlast,
                int col){
  return std::find_if(first,
                      pastlast,
                      [&col](const std::map<BlockAddress, BlockStatus>::value_type & value){return value.first.col_index == col;});}


void row_splitting_helper(const BlockAddress & v,
                          const std::map<BlockAddress, BlockStatus> & data,
                          std::set<BlockAddress>& identities_split, 
                          std::set<int> & visited_rows,
                          std::set<int> & visited_cols){
  if (visited_rows.count(v.row_index) == 1) {
    return;
  }
  visited_rows.insert(v.row_index); 
  
  for (auto it = get_next_at_row(data.begin(), data.end(), v.row_index);
       it != data.end();
       it = get_next_at_row(it, data.end(), v.row_index)) {
    int cur_col = it->first.col_index;
    if (it->second == BlockStatus::Identity
        && visited_cols.count(cur_col) == 0) {
      identities_split.insert(it->first); 
      col_splitting_helper(it->first, data,
                           identities_split, 
                           visited_rows, visited_cols);
    }
    ++it;
  }
  
}

void col_splitting_helper(const BlockAddress & v,
                          const std::map<BlockAddress, BlockStatus> & data,
                          std::set<BlockAddress>& identities_split, 
                          std::set<int> & visited_rows,
                          std::set<int> & visited_cols){
  if (visited_cols.count(v.col_index) == 1) {
    return;
  } 
  visited_cols.insert(v.col_index);
  
  for (auto it = get_next_at_col(data.begin(), data.end(), v.col_index);
       it != data.end();
       it = get_next_at_col(it, data.end(), v.col_index)) {
    int cur_row = it->first.row_index;
    if (it->second == BlockStatus::Identity
        && visited_rows.count(cur_row) == 0
        ){
      identities_split.insert(it->first); 
      row_splitting_helper(it->first, data,
                           identities_split,
                           visited_rows, visited_cols); 
    }
    ++it;
  } 
} 


}
}
