#ifndef STATUSHANDLER_H
#define STATUSHANDLER_H

#include "gyoza/addressing.h"
#include "gyoza/orderhandler.h"
#include <map>
#include <vector>
#include <iostream>

namespace gyoza {
namespace matrixproblems {

enum class BlockStatus {Unprocessed,Identity,Zero,Null};
std::ostream& operator<<(std::ostream& out, const BlockStatus value);

class StatusHandler {
 public:
  typedef std::map<BlockAddress, BlockStatus> data_type;
  typedef data_type::size_type size_type; 
  
  void clear();
  size_type size()const{return data.size();}
  bool is_empty()const{return (size() == 0);}
  
  BlockStatus get_status(const BlockAddress& key)const;
  void set_status(const BlockAddress& key, const BlockStatus& status);
  
  bool has_no_unprocessed()const; 

  // On identities as a whole
  std::vector<BlockAddress> get_identity_blocks()const;  
  void query_rows_cols_without_identities(std::vector<int> * row_indices,
                                          std::vector<int> * col_indices) const;
  /* Let I be the set of identity blocks. 
     P_1, P_2, ... P_N is said to be a partition of identity blocks if:
     1. Pi are pairwise disjoint, and have union I.
     2. If x, y in I are (row or col) neighbors, then there is an i such that 
     x and y are in P_i. 
   */
  std::vector<std::vector<BlockAddress>> get_partition_of_identities()const;

  /* Two unequal blocks are said to be {row,col} neighbors if they share the same {row, col}. A block is NOT a neighbor to itself.
   */ 
  // On neighbors of a block
  std::vector<BlockAddress> get_identity_row_neighbors(const BlockAddress & ba)const;
  std::vector<BlockAddress> get_identity_col_neighbors(const BlockAddress & ba)const;

  std::vector<BlockAddress> get_nonzero_row_neighbors(const BlockAddress & ba)const;
  std::vector<BlockAddress> get_nonzero_col_neighbors(const BlockAddress & ba)const;
  std::vector<BlockAddress> get_nonzero_neighbors(const BlockAddress & ba)const; 
  
  /* Essentially, like identity neighbors, but neighbors of neighbors
     are also considered..
  */
  void query_splitting(const BlockAddress & v,
                       std::set<BlockAddress> & identities_split)const; 

  std::tuple<int,int,int,int> get_bounds() const;

  void pretty_print(std::ostream& os) const; 
  void pretty_print(std::ostream& os, OrderHandler row_order, OrderHandler col_order)const;  

 protected:
  std::map<BlockAddress, BlockStatus> data;

  std::map<BlockAddress, BlockStatus>::iterator
  get_next_at_row(std::map<BlockAddress, BlockStatus>::iterator first, int row);

  std::map<BlockAddress, BlockStatus>::iterator
  get_next_at_col(std::map<BlockAddress, BlockStatus>::iterator first, int col);

  std::map<BlockAddress, BlockStatus>::const_iterator
  get_next_at_row(std::map<BlockAddress, BlockStatus>::const_iterator first, int row)const;

  std::map<BlockAddress, BlockStatus>::const_iterator
  get_next_at_col(std::map<BlockAddress, BlockStatus>::const_iterator first, int col)const; 
}; 

}
}

#endif
