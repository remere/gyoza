#ifndef MP_ALGO_H
#define MP_ALGO_H

#include <tuple>
#include <map>
#include "gyoza/common_definitions.h"

// other functions that are useful for computing stuff

namespace gyoza {
namespace algorithms {

std::tuple<Z2Matrix,Z2Matrix,Z2Matrix>
solve_zigzag_fb(const Z2Matrix& f,
                const Z2Matrix& b,
                std::map<std::pair<int,int>, int> * pd = nullptr);

}
}




#endif
