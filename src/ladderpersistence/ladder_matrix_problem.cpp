#include "gyoza/ladder_matrix_problem.h"
#include "gyoza/snf_algorithms.hpp"
#include <queue>
#include <cassert>

#include "gyoza/ladder_indec.h"

#include "gyoza/log_control.h"

namespace gyoza {
namespace matrixproblems {

template<typename T>
void inline add_to_pd(const T& ind,
                      const int& mult,
                      std::map<T,int>& pd) {
  if (pd.count(ind) == 0) {
    pd[ind] = mult;
  } else {
    if (do_log(LogLevel::LOG_DEBUG)) {
      std::cerr << "Abstract indecomposable detected again: " << ind << "\n";
    }
    pd[ind] += mult;
  }
}

template<unsigned int N>
bool LadderMatrixProblem::attempt_compute_pd(){
  // first check that all names are interpretable as intervals over A_N
  for (const auto & code: row_names.get_names()) {
    if (string_to_interval<N>(code) == boost::none ) {
      log<LOG_ERROR>("Invalid name detected, cannot get pd.\n",
                     std::cerr);
      return false;
    }
  }  
  for (const auto & code: col_names.get_names()) {
    if (string_to_interval<N>(code) == boost::none) {
      log<LOG_ERROR>("Invalid name detected, cannot get pd.\n",
                     std::cerr);
      return false;
    }
  }
  std::map<std::string, int> computed;

  boost::optional<std::map<AbstractIndec, int>> decompo = get_abstract_decomposition();

  if (decompo == boost::none) {
    return false;
  }

  for (const auto & indec_mult_pair : decompo.get()) {
    boost::optional<LadderIndec<N>> ladder_indec = abstract_to_ladder_indec<N>(indec_mult_pair.first);
    if (ladder_indec == boost::none) {
      log<LOG_ERROR>("Invalid name detected, cannot get pd.\n",
                     std::cerr);
      return false;
    }
    add_to_pd<std::string>(ladder_indec.get().to_string(),
                           indec_mult_pair.second,
                           computed); 
  } 
  pd = computed;
  return true; 
}


bool LadderMatrixProblem::output_pd(std::ostream& os)const{
  if (pd == boost::none) {
    return false;
  } 
  for (auto const & ind_mult_pair : pd.get()) {
    os << ind_mult_pair.first << ": " << ind_mult_pair.second << "\n"; 
  } 
  return true;
} 

boost::optional<std::map<AbstractIndec, int>> LadderMatrixProblem::get_abstract_decomposition()const{  
  if (not verify_status()){
    log<LOG_ERROR>("Matrix problem has invalid status, cannot do decomposition!\n", std::cerr);
    return boost::none;
  }
  if (status.has_no_unprocessed() == false) {
    log<LOG_ERROR>("Matrix problem has unprocessed blocks, cannot do decomposition!\n", std::cerr);
    return boost::none;
  }
  
  std::map<AbstractIndec, int> decompo;
  if (submatrices.size() == 0) {
    return decompo;
  }

  std::vector<int> row_indices;
  std::vector<int> col_indices;
  status.query_rows_cols_without_identities(&row_indices, &col_indices); 

  for (auto r: row_indices) {
    const std::string name = row_names.get_name(r); 
    add_to_pd<AbstractIndec>(singletion_row_indec(name),
                             get_dimension_of_row(r),
                             decompo); 
  }
  
  for (auto c: col_indices) {
    const std::string name = col_names.get_name(c); add_to_pd<AbstractIndec>(singletion_col_indec(name),
                             get_dimension_of_col(c),
                             decompo); 
  }

  std::vector<std::vector<BlockAddress>> parts = status.get_partition_of_identities();

  for (auto cluster : parts) {    
    std::set<int> r_in;
    std::set<int> c_in;
    for (auto ba : cluster) {      
      r_in.insert(ba.row_index);
      c_in.insert(ba.col_index);
    }
    AbstractIndec an_indec;
    for (auto c: c_in) {
      an_indec.involved_cols.insert( col_names.get_name(c) );
    }
    for (auto r: r_in) {
      an_indec.involved_rows.insert( row_names.get_name(r) );
    }

    int mult = -1;
    if (c_in.size() != 0) {
      mult = get_dimension_of_col(*(c_in.begin()));
    } else if (r_in.size() != 0) {
      mult = get_dimension_of_row(*(r_in.begin()));
    }
    add_to_pd<AbstractIndec>(an_indec,
                             mult,
                             decompo); 
  }
  return decompo; 
}


boost::optional<int> LadderMatrixProblem::number_of_noniso_summands()const{
  boost::optional<std::map<AbstractIndec, int>> decompo = get_abstract_decomposition();

  if (decompo == boost::none) {
    return boost::none;
  }
  return decompo.get().size();
}

BlockStatus LadderMatrixProblem::get_status(const BlockAddress & ba)const {
  return status.get_status(ba);
}

void LadderMatrixProblem::pretty_print_status(std::ostream& os)const{
  status.pretty_print(os, row_order, col_order);
  return;
}

bool LadderMatrixProblem::verify_status()const{
  return (get_status_inconsistent_blocks().size() == 0);
}

std::vector<BlockAddress> LadderMatrixProblem::get_status_inconsistent_blocks()const{
  std::vector<BlockAddress> inconsistencies;
  if (submatrices.size() == 0){
    return inconsistencies;
  }
  if (status.is_empty()) {
    return non_strongly_zero_entries();
  }

  std::tuple<int,int,int,int> bdds = status.get_bounds();
  for (int r = std::get<RowMin>(bdds); r <= std::get<RowMax>(bdds); ++r) {
    for (int c = std::get<ColMin>(bdds); c <= std::get<ColMax>(bdds); ++c) {
      BlockAddress ba(r,c);
      matrixproblems::BlockStatus claimed_status = status.get_status(ba);
      if (claimed_status == BlockStatus::Null && submatrices.count(ba) != 0) {
        inconsistencies.push_back(ba);
      } else if (claimed_status == BlockStatus::Zero) {
        auto it = submatrices.find(ba);
        if (it == submatrices.end() || not (it->second.isZero())) {
          inconsistencies.push_back(ba);
        }
      } else if (claimed_status == BlockStatus::Identity){
        auto it = submatrices.find(ba);
        if (it == submatrices.end() || not (it->second.isIdentity()) || not (it->second.rows() == it->second.cols())) {
          inconsistencies.push_back(ba);
        }
      } else if (claimed_status == BlockStatus::Unprocessed && submatrices.count(ba) != 1){
        inconsistencies.push_back(ba);
      }
    }
  }
  for (auto it = submatrices.cbegin(); it != submatrices.cend(); ++it) {
    if ( it->first.row_index < std::get<RowMin>(bdds) ||
         it->first.row_index > std::get<RowMax>(bdds) ||
         it->first.col_index < std::get<ColMin>(bdds) ||
         it->first.col_index > std::get<ColMax>(bdds) ) {
      inconsistencies.push_back(it->first);
    }
  } 
  
  return inconsistencies;
}

// PART1: tools
boost::optional<BlockAddress> LadderMatrixProblem::get_next_unprocessed_block()const{
  for (auto it = col_order.crbegin(); it != col_order.crend(); ++it) {
    for (auto jt = row_order.crbegin(); jt != row_order.crend(); ++jt) {
      BlockAddress ba(*jt, *it);
      if (status.get_status(ba) == BlockStatus::Unprocessed) {
        return ba;
      }
    }
  }
  return boost::none;
}

boost::optional<BlockAddress> LadderMatrixProblem::get_next_untargeted_unprocessed_block()const{
  for (auto it = col_order.crbegin(); it != col_order.crend(); ++it) {
    for (auto jt = row_order.crbegin(); jt != row_order.crend(); ++jt) {
      BlockAddress ba(*jt, *it);
      if (status.get_status(ba) != BlockStatus::Unprocessed) {
        continue;
      }

      // check targetability by nonzero neighbors on same col/ row.
      bool use_ba = true;
      for (const auto & v : status.get_nonzero_col_neighbors(ba)) {
        if (status.get_status(v) != BlockStatus::Identity && check_row_operation(v.row_index,ba.row_index)){
          use_ba = false;
          break;
        }
      }
      if (use_ba == false) {
        continue;
      }
      for (const auto & v : status.get_nonzero_row_neighbors(ba)) {
        if (status.get_status(v) != BlockStatus::Identity && check_col_operation(v.col_index,ba.col_index)){
          use_ba = false;
          break;
        }
      }
      if (use_ba == false) {
        std::cout << "Skipping use of: " << ba << std::endl;
        continue;
      }
      return ba;
    }
  }
  return boost::none;
}

void LadderMatrixProblem::row_fix(const BlockAddress & v,
                                  const TransformPair & P,
                                  bool use_inverse,
                                  std::set<int> & visited_rows,
                                  std::set<int> & visited_cols){
  std::vector<BlockAddress> side_effects = status.get_identity_row_neighbors(v);
  if (visited_rows.count(v.row_index) != 0 && side_effects.size() != 0) {
    throw std::runtime_error("loop detected in identity fixing routines");
  }
  // algorithms::FullPivotSNF<Z2Matrix> snf(at(v));
  left_multiply_row(v.row_index, (use_inverse ? P.invtrans : P.trans) );

  visited_rows.insert(v.row_index);
  for (const auto & ba : side_effects) {
    col_fix(ba, P, not use_inverse, visited_rows, visited_cols);
  }
}

void LadderMatrixProblem::col_fix(const BlockAddress & v,
                                  const TransformPair & P,
                                  bool use_inverse,
                                  std::set<int> & visited_rows,
                                  std::set<int> & visited_cols){
  std::vector<BlockAddress> side_effects = status.get_identity_col_neighbors(v);
  if (visited_cols.count(v.col_index) != 0 && side_effects.size() != 0) {
    throw std::runtime_error("loop detected in identity fixing routines");
  }
  // algorithms::FullPivotSNF<Z2Matrix> snf(at(v));
  right_multiply_col(v.col_index, (use_inverse ? P.invtrans : P.trans) );

  visited_cols.insert(v.col_index);
  for (const auto & ba : side_effects) {
    row_fix(ba, P, not use_inverse, visited_rows, visited_cols);
  }
}

// PART2: edge reduction
LadderMatrixProblem::SubIndex LadderMatrixProblem::edge_reduction(const BlockAddress& v){
  std::vector<BlockAddress> row_effects = status.get_identity_row_neighbors(v);
  std::vector<BlockAddress> col_effects = status.get_identity_col_neighbors(v);

  const bool has_side_effects = (row_effects.size() > 0 ||
                                 col_effects.size() > 0);
  const bool transform_inverses_needed = has_side_effects;

  algorithms::FullPivotSNF<Z2Matrix> snf(transform_inverses_needed);
  const SubIndex rk = reduce_block(v, snf);

  if (has_side_effects) {
    TransformPair rowtrans(snf.get_P(), snf.get_P_inv());
    TransformPair coltrans(snf.get_Q(), snf.get_Q_inv());

    std::set<int> visited_rows;
    std::set<int> visited_cols;

    for (const auto & ba : row_effects) {
      // yes, row side effects use rowtrans to fix.
      col_fix(ba, rowtrans, true, visited_rows, visited_cols);
    }
    for (const auto & ba : col_effects) {
      row_fix(ba, coltrans, true, visited_rows, visited_cols);
    }
  }
  return rk;
}

void LadderMatrixProblem::post_process_block(const BlockAddress& new_ba,
                                             const BlockAddress& old_ba) {
  status.set_status(new_ba, status.get_status(old_ba));
  return;
}

// status-setting helpers
void set_snf_status(int r, int rprime, int c, int cprime, StatusHandler& status) {
  using BS=matrixproblems::BlockStatus;
  status.set_status(BlockAddress(r, c), BS::Identity);
  status.set_status(BlockAddress(r, cprime), BS::Zero);
  status.set_status(BlockAddress(rprime, c), BS::Zero);
  status.set_status(BlockAddress(rprime, cprime), BS::Zero);
  return;
}

void set_split_identity_status(int r, int rprime, int c, int cprime, StatusHandler& status) {
  using BS=matrixproblems::BlockStatus;
  status.set_status(BlockAddress(r, c), BS::Identity);
  status.set_status(BlockAddress(rprime, c), BS::Zero);
  status.set_status(BlockAddress(r, cprime), BS::Zero);
  status.set_status(BlockAddress(rprime, cprime), BS::Identity);
  return;
}

void LadderMatrixProblem::split_snf(const BlockAddress & v, const SubIndex rank) {
  std::set<BlockAddress> identities_split;
  status.query_splitting(v, identities_split);

  std::map<int, int> row_old_to_new;
  std::map<int, int> col_old_to_new;

  std::set<int> row_indices_split = {v.row_index};
  std::set<int> col_indices_split = {v.col_index};

  for (auto const & ba: identities_split) {
    row_indices_split.insert(ba.row_index);
    col_indices_split.insert(ba.col_index);
  }

  for (auto r : row_indices_split) {
    int rprime = split_row(r, rank);
    row_old_to_new[r] = rprime;
    row_order.insert_after(r, rprime);
  }
  for (auto c : col_indices_split) {
    int cprime = split_col(c, rank);
    col_old_to_new[c] = cprime;
    col_order.insert_after(c, cprime);
  }

  // disallow identity from being added back to new row/col:
  permissible_row_operations.make_unpermissible(v.row_index, row_old_to_new.at(v.row_index));

  permissible_col_operations.make_unpermissible(v.col_index, col_old_to_new.at(v.col_index));

  // go back and fix things that were originally identity.
  for (auto const & ba : identities_split) {
    if (ba == v) {
      continue;
    }
      
    assert(status.get_status(ba) == matrixproblems::BlockStatus::Identity);
    const int r = ba.row_index;
    const int c = ba.col_index;
    set_split_identity_status(r, row_old_to_new.at(r),
                              c, col_old_to_new.at(c), status);
  }
  set_snf_status(v.row_index, row_old_to_new.at(v.row_index),
                 v.col_index, col_old_to_new.at(v.col_index), status);
  return;
}


// PART3: erasability
ProcessTree LadderMatrixProblem::erasable(const BlockAddress& target,
                                          BlockAddress const * p_flagged,
                                          std::set<BlockAddress> visited)const {
  ProcessTree pr = row_erasable(target,p_flagged,visited);
  if (pr.get_root().get() != nullptr){
    return pr;
  }
  return col_erasable(target,p_flagged,visited);
}

enum class Decision {Skip, Use, Fail};

std::pair<Decision, ProcessTree>
erasble_helper(const BlockAddress & effector,
               const BlockAddress & affected,
               const std::set<BlockAddress>& visited,
               LadderMatrixProblem const * lmp) {
  ProcessTree ans;
  if ( (lmp->get_status(effector) == BlockStatus::Null) ||
       (lmp->get_status(effector) == BlockStatus::Zero) ){
    // cannot effect anything.
    return {Decision::Skip,ans};
  }
  BlockStatus affected_status = lmp->get_status(affected);
  if ( (affected_status == BlockStatus::Unprocessed) ||
       (affected_status == BlockStatus::Null) ) {
    // will not be affected, no problem
    return {Decision::Skip,ans};
  }
  if (visited.count(affected) == 1) {
    throw std::runtime_error("loop detected in erasability check routines");
    return {Decision::Fail,ans};
  }
  if (affected_status == BlockStatus::Identity){
    return {Decision::Fail,ans};
  }
  ProcessTree subtree = lmp->erasable(affected, &effector, visited);
  // here, affected_status == BlockStatus::Zero is guaranteed
  assert(affected_status == BlockStatus::Zero);
  if (subtree.get_root().get() == nullptr) {
    return {Decision::Fail,ans};
  }
  return {Decision::Use,subtree};
}


ProcessTree LadderMatrixProblem::row_erasable(const BlockAddress& target,
                                              BlockAddress const * p_flagged,
                                              std::set<BlockAddress> visited)const {
  visited.insert(target);
  std::vector<BlockAddress> eff = status.get_identity_col_neighbors(target);
  ProcessTree ans;
  for (auto vE : eff) {
    if (not (check_row_operation(vE.row_index, target.row_index) &&
             visited.count(vE) == 0)) {
      continue;
    }
    if (p_flagged != nullptr && vE == *p_flagged) {
      continue;
    }
    bool usable = true;
    ProcessTree temp{target,vE};

    for (auto it=get_next_submatrix_at_row(submatrices.begin(), vE.row_index);
         it != submatrices.end();
         it = get_next_submatrix_at_row(it, vE.row_index)) {
      const BlockAddress & effector = it->first;
      if (vE == effector) {
        ++it;
        continue;
      }
      BlockAddress affected(target.row_index, effector.col_index);     

      Decision choice;
      ProcessTree subtree;
      std::tie(choice,subtree) = erasble_helper(effector, affected, visited, this);
      if (choice == Decision::Skip) {
        ++it;
        continue;
      } else if (choice == Decision::Fail) {
        usable = false;
        break;
      } else if (choice == Decision::Use) {
        #ifdef DEBUG
        std::cerr << "***************\n";
        std::cerr << "target: " << target << "\n";
        std::cerr << "vE: " << vE << "\n";
        std::cerr << "row effector: " << it->first << "\n";
        std::cerr << "row affected: " << affected << "\n";
        std::cerr << "***************\n";
        #endif
      
        temp.attach_tree_to_root(subtree);
        ++it;
        continue;
      } else {
        throw("shouldn't be here");
      }
    }
    if (usable) {
      ans = temp;
      break;
    }
  }
  return ans;
}

ProcessTree LadderMatrixProblem::col_erasable(const BlockAddress& target,
                                              BlockAddress const * p_flagged,
                                              std::set<BlockAddress> visited)const {
  visited.insert(target);
  
  std::vector<BlockAddress> eff = status.get_identity_row_neighbors(target);
  ProcessTree ans;
  for (auto vE : eff) {
    if (not (check_col_operation(vE.col_index, target.col_index) &&
             visited.count(vE) == 0)) {
      continue;
    }
    if (p_flagged != nullptr && vE == *p_flagged) {
      continue;
    }
    bool usable = true;
    ProcessTree temp{target,vE};

    for (auto it=get_next_submatrix_at_col(submatrices.begin(), vE.col_index);
         it != submatrices.end();
         it = get_next_submatrix_at_col(it, vE.col_index)) {
      const BlockAddress & effector = it->first;
      if (vE == effector) {
        ++it;
        continue;
      }
      BlockAddress affected(effector.row_index, target.col_index);      
      
      Decision choice;
      ProcessTree subtree;
      std::tie(choice,subtree) = erasble_helper(effector, affected, visited, this);
      if (choice == Decision::Skip) {
        ++it;
        continue;
      } else if (choice == Decision::Fail) {
        usable = false;
        break;
      } else if (choice == Decision::Use) {        
        #ifdef DEBUG
        std::cerr << "***************\n";
        std::cerr << "target: " << target << "\n";
        std::cerr << "vE: " << vE << "\n";
        std::cerr << "row effector: " << it->first << "\n";
        std::cerr << "row affected: " << affected << "\n";
        std::cerr << "***************\n";
        #endif
      
        temp.attach_tree_to_root(subtree);
        ++it;
        continue;
      } else {
        throw("shouldn't be here");
      }
    }
    if (usable) {
      ans = temp;
      break;
    }
  }
  return ans;
}
// **************************************** 

void LadderMatrixProblem::initialize() {
  if (submatrices.size() == 0) {
    return;
  }
  initialize_status();
  std::tuple<int,int,int,int> bdds = status.get_bounds();
  row_order.generate_default_order(std::get<RowMin>(bdds), std::get<RowMax>(bdds));
  col_order.generate_default_order(std::get<ColMin>(bdds), std::get<ColMax>(bdds));
  // generate_default_names();
}

void LadderMatrixProblem::initialize_status() {
  status.clear();
  for (auto it = submatrices.cbegin(); it != submatrices.cend(); ++it) {
    status.set_status(it->first, BlockStatus::Unprocessed);
  }
  return;
}

void LadderMatrixProblem::print_reduction_debug_info(std::ostream& os)const{
  os << "row order: " << row_order << "\n";
  os << "col order: " << col_order << "\n";
  os << "row operations:\n" << permissible_row_operations << "\n";
  os << "col operations:\n" << permissible_col_operations << "\n";
}

int LadderMatrixProblem::matrix_reduction(int max_count, bool split_empty) {
  boost::optional<BlockAddress> ba = get_next_unprocessed_block();
  int count = 0;
  while ( ba != boost::none) {
    if (count >= max_count ){
      break;
    }
    const BlockAddress & v = ba.get();
    // if (do_log(LOG_INFO)){
    //   std::cout << "Block: " << v << std::endl; 
    // }
    SubIndex rk = edge_reduction(v);

    if (not split_empty && rk == 0) {
      status.set_status(v, BlockStatus::Zero);
    } else {
      split_snf(v, rk);
    }

    if (do_log(LOG_INFO)){
      std::cout << "Rank: " << rk << std::endl; 
    }

    // neighbor_erase(v);
    unsigned int n = greedy_erase();
    if (do_log(LOG_INFO)) {
      std::cout << n << " blocks erased\n";
    }
    
    ba = get_next_unprocessed_block();
    ++count;

    if (do_log(LOG_INFO)) {
      std::cout << "--- After iteration " << count << " ---\n";
      pretty_print_status(std::cout);
      std::cout << "status ok? " << (verify_status() ? "ok" : "no") << "\n";
    }
    
    if (do_log(LOG_DEBUG)){
      print_reduction_debug_info(std::cout);
    }
    log<LOG_DEBUG>("---------------------------\n");
  }

  // cleanup
  // note, cleanup no longer needed if after each iteration
  // of the while loop we use greedy_erase.
  unsigned int n = greedy_erase();
  if (do_log(LOG_INFO)) {
    std::cout << n << " blocks erased\n";
  }
  
  return count;
}

unsigned int LadderMatrixProblem::greedy_erase() {
  std::set<BlockAddress> potentials;
  unsigned int n = 0;
  
  std::vector<BlockAddress> ids = status.get_identity_blocks();
  for (const auto & v : ids) {    
    std::vector<BlockAddress> neighbors = status.get_nonzero_neighbors(v);    
    std::copy(neighbors.begin(), neighbors.end(),
              std::inserter(potentials, potentials.end()));
    // also put identity block as potential erasable:
    potentials.insert(v);    
  }
  
  std::set<BlockAddress> potentials_copy = potentials;
  log<LOG_DEBUG>("Reducible neighbors: ");  
  while (potentials.size() > 0) {
    bool something_erased = false;
    for (auto ba : potentials) {
      ProcessTree pt = erasable(ba, nullptr);
      if ( pt.get_root().get() != nullptr ) {
        something_erased = true;
        if (do_log(LOG_DEBUG)){
          std::cout << ba;
          if (get_status(ba) == BlockStatus::Identity) {
            std::cout << "(identity)";
          }
          std::cout << " ";
        }
        execute_process(pt);
        ++n;
        potentials_copy.erase(ba);
      }
    }
    potentials = potentials_copy;
    if (something_erased == false) {
      break;
    }
  }
  log<LOG_DEBUG>("\n");
  return n; 
}

void LadderMatrixProblem::neighbor_erase(const BlockAddress& v) {
  std::vector<BlockAddress> neighbors = status.get_nonzero_neighbors(v);  
    
  log<LOG_DEBUG>("Reducible neighbors: "); 
  for (auto ba : neighbors) {
    ProcessTree pt = erasable(ba, nullptr);
    if ( pt.get_root().get() != nullptr ) {
      if (do_log(LOG_DEBUG)){
        std::cout << ba;
        if (get_status(ba) == BlockStatus::Identity) {
          std::cout << "(identity)";
        }
        std::cout << " ";
      }
      execute_process(pt);
    }
  }
  log<LOG_DEBUG>("\n");
  return;
}

void LadderMatrixProblem::execute_process(const ProcessTree& pt) {
  std::queue<std::shared_ptr<ProcessVertex>> Q;
  Q.push(pt.get_root());
  while (not Q.empty()) {
    std::shared_ptr<ProcessVertex> p_vertex = Q.front();
    Q.pop();
    
    #ifdef DEBUG
      std::cout << "\nCurrent pt vertex has " <<
          p_vertex->get_successors().size() << "successors\n";
    #endif
    
    for (auto p_suc : p_vertex->get_successors()) {
      Q.push(p_suc);      
    }

    BlockAddress target = p_vertex->get_target();
    BlockAddress eraser = p_vertex->get_eraser();
    if (status.get_status(eraser) != BlockStatus::Identity) {
      throw;
    }
    if (target == eraser) {
      throw;
    }

    if (target.row_index == eraser.row_index) {
      // same row, do col operations.
      this->add_right_multiple_col(-(this->operator[](target)), eraser.col_index, target.col_index);

      #ifdef DEBUG
      std::cout << "col reg: " << eraser << " to " <<  target << "\n";
      #endif
      
    } else if (target.col_index == eraser.col_index) {
      // same col, do row operations
      this->add_left_multiple_row(-(this->operator[](target)), eraser.row_index, target.row_index);
      
      #ifdef DEBUG
      std::cout << "row reg: " << eraser << " to " <<  target << "\n";
      #endif
    } else {
      throw;
    }
    
    if (submatrices.at(target).isZero()) {
      status.set_status(target, BlockStatus::Zero);
    } else {
      std::cerr << "uhoh execute process did not actually zero out target matrix..\n";
      throw;
    }
  }
  if (not verify_status()) {
    std::cerr << "uhoh execute process messed up the matrix..\n";
  }
}

template bool LadderMatrixProblem::attempt_compute_pd<2u>();
template bool LadderMatrixProblem::attempt_compute_pd<3u>();
template bool LadderMatrixProblem::attempt_compute_pd<4u>();



}
}
