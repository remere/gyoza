#include "gyoza/algo.h"
#include "gyoza/matrix_problem.h"
#include "gyoza/snf_algorithms.hpp"

namespace gyoza {
namespace algorithms {

std::tuple<Z2Matrix,Z2Matrix,Z2Matrix>
solve_zigzag_fb(const Z2Matrix& f, const Z2Matrix& b, std::map<std::pair<int,int>, int> * pd) {
  // writes the matrices of the zigzag
  // 0 --f--> 1 <--b-- 2
  // into standard form
  // 0 --f'--> 1 <--b'-- 2  
  // used transformations are returned as a tuple {P0,P1,P2}  
  //  P1 * f * P0 = f'
  //  P1 * b * P2 = b'

  if (f.rows() != b.rows()) {
    throw("invalid zigzag maps");
  }

  std::tuple<Z2Matrix,Z2Matrix,Z2Matrix> trans;

  algorithms::FullPivotSNF<Z2Matrix> snf;

  matrixproblems::MatrixProblem<Z2Matrix> fb_prob;
  matrixproblems::NameHandler names( {{0,"V0"},{1,"V2"},{2,"V1"}} );
  fb_prob.set_col_names(names);
  fb_prob.set_row_names(names); 

  typedef gyoza::matrixproblems::BlockAddress BA;
  typedef matrixproblems::MatrixProblem<Z2Matrix>::SubIndex SubIndex;
  fb_prob[ BA(2,0) ] = f;
  fb_prob[ BA(2,1) ] = b;
  int d0 = f.cols();
  int d2 = b.cols();
  int d1 = f.rows();
  // basis trackers
  fb_prob[ BA(0,0) ] = Z2Matrix::Identity(d0,d0);
  fb_prob[ BA(1,1) ] = Z2Matrix::Identity(d2,d2);
  fb_prob[ BA(2,2) ] = Z2Matrix::Identity(d1,d1);

  // rows 0 and 1, and col 2 is never gonna get split below,
  // so we can easily recover the used transformations by pieceing
  // together the parts
  Z2Matrix P0 = Z2Matrix::Identity(d0,d0);
  Z2Matrix P2 = Z2Matrix::Identity(d2,d2);
  Z2Matrix P1 = Z2Matrix::Identity(d1,d1); 
  
  // Show only row 2:
  // [ f | b ]
  const SubIndex rk1 = fb_prob.reduce_block(BA(2,0), snf);
  const int nr1 = fb_prob.split_row(2, rk1);
  const int nc1 = fb_prob.split_col(0, rk1);
  /*      0 nc1   1
    2   [ E  0  | b1 ]
    nr1 [ 0  0  | b2 ]
   */

  const SubIndex rk2 = fb_prob.reduce_block(BA(nr1,1), snf);
  const int nr2 = fb_prob.split_row(nr1, rk2);
  const int nc2 = fb_prob.split_col(1, rk2);
  /*      0 nc1   1   nc2
    2   [ E  0  | b1' b1'' ]
    nr1 [ 0  0  | E    0   ]
    nr2 [ 0  0  | 0    0   ]
   */

  fb_prob.add_left_multiple_row(-fb_prob.at(BA(2,1)) ,nr1, 2);
  /*      0 nc1   1   nc2
    2   [ E  0  | 0   b1'' ]
    nr1 [ 0  0  | E    0   ]
    nr2 [ 0  0  | 0    0   ]
   */

  algorithms::FullPivotSNF<Z2Matrix> snf_i(true);
  const SubIndex rk3 = fb_prob.reduce_block(BA(2,nc2), snf_i);
  // fix side-affected E:
  fb_prob.right_multiply_col(0, snf_i.get_P_inv());
  
  const int nr3 = fb_prob.split_row(2, rk3);
  const int nc3 = fb_prob.split_col(nc2, rk3);
  /*      0  nc1   1  nc2 nc3
    2   [ E0  0  | 0   E   0 ]
    nr3 [ 0E  0  | 0   0   0 ]
    nr1 [ 0   0  | E   0   0 ]
    nr2 [ 0   0  | 0   0   0 ]
   */
  const int nc4 = fb_prob.split_col(0, rk3);
  /*      0 nc4 nc1   1  nc2 nc3
    2   [ E  0   0  | 0   E   0 ]
    nr3 [ 0  E   0  | 0   0   0 ]
    nr1 [ 0  0   0  | E   0   0 ]
    nr2 [ 0  0   0  | 0   0   0 ]
   */

  // reconstitute transformations
  /*        V0            V2
          0 nc4 nc1   1  nc2 nc3
    2   [ E  0   0  | 0   E   0 ]       rk3
    nr3 [ 0  E   0  | 0   0   0 ]   V1  rk1-rk3
    nr1 [ 0  0   0  | E   0   0 ]       rk2
    nr2 [ 0  0   0  | 0   0   0 ]       d1-rk1-rk2
         rk3 
          rk1-rk3    rk2 rk3 
  */ 
  P0.leftCols(rk3) = fb_prob.at(BA(0,0));
  P0.middleCols(rk3,rk1-rk3) = fb_prob.at(BA(0,nc4));
  P0.rightCols(d0 - rk1) = fb_prob.at(BA(0,nc1));

  P2.leftCols(rk2) = fb_prob.at(BA(1,1));
  P2.middleCols(rk2,rk3) = fb_prob.at(BA(1,nc2));
  P2.rightCols(d2 - rk2 -rk3) = fb_prob.at(BA(1,nc3));

  P1.topRows(rk3) = fb_prob.at(BA(2,2));
  P1.middleRows(rk3, rk1-rk3) = fb_prob.at(BA(nr3,2));
  P1.middleRows(rk1, rk2) = fb_prob.at(BA(nr1,2));  
  P1.bottomRows(d1-rk1-rk2) = fb_prob.at(BA(nr2,2));

  /* decomposition theory
     m13 = rk3;
     m12 = rk1 - rk3; 
     m23 = rk2;
     m11 = d0 - rk1;
     m33 = d2 - rk2 - rk3;
     m22 = d1 - rk1 -rk2 
  */
  if (pd != nullptr) {
    pd->clear();
    std::map<std::pair<int,int>,int>& d = *pd;
    d[{1,3}] = rk3;
    d[{1,2}] = rk1 - rk3;
    d[{2,3}] = rk2;
    d[{1,1}] = d0 - rk1;
    d[{3,3}] = d2 - rk2 - rk3;
    d[{2,2}] = d1 - rk1 - rk2; 
  }

	// std::cerr << "f'\n";
  // std::cerr <<  (P1 * f * P0);

  // std::cerr << "b'\n";
  // std::cerr <<  (P1 * b * P2);

  return std::tuple<Z2Matrix,Z2Matrix,Z2Matrix>{P0,P1,P2}; 
}

}
}
