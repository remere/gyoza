#include <iostream>
#include <fstream>
#include <sstream>

#include <tclap/CmdLine.h>
#include "gyoza/gyoza.h"
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/range/algorithm_ext/erase.hpp>

std::map<std::string, unsigned int> read_pd(std::istream& is) {
  std::map<std::string, unsigned int> ans;
  while(is.good()) {
    std::string line;
    getline(is, line);
    if (line.size() == 0){
      break;
    }
    std::vector<std::string> vstrings;
    boost::split(vstrings, line, boost::is_any_of(":"), boost::token_compress_on);

    if (vstrings.size() != 2) {
      std::cerr << "Invalid input line!\n";
      std::cerr << line << std::endl;
    }

    boost::remove_erase_if(vstrings[0], boost::is_any_of("[](), "));
    boost::trim(vstrings[0]);
    
    if (vstrings[0].size() != 6) {
      std::cerr << "Invalid input line!\n";
      std::cerr << vstrings[0] << std::endl;
    }
    ans[vstrings[0]] = std::stoi(vstrings[1]);

  }
  return ans;
}



int main(int argc, char** argv) {
  try{
    TCLAP::CmdLine cmd("gyoza_plot_clfb_diagram\nThis utility draws the persistence diagram of a persistence module over a commutative ladder of orientation fb.\nThis program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; see the LICENSE file for details.", ' ', "0.1");

    TCLAP::SwitchArg dimvecArg("d", "dimvec", "Toggle showing of dimension vectors. Default: true.", true);
    cmd.add(dimvecArg);

    TCLAP::UnlabeledValueArg<std::string> inArg("input", "Filename of input persistence diagram data.", true, std::string(), "input");
    cmd.add(inArg);

    TCLAP::UnlabeledValueArg<std::string> outArg("output", "Filename (*.ps) of Postscript format file to write output to.", true, std::string(), "output");
    cmd.add(outArg);

    cmd.parse(argc, argv);

    std::map<std::string, unsigned int> pd;
    if (inArg.getValue() == "-"){
      pd = read_pd(std::cin);
    } else {
      std::ifstream ifile(inArg.getValue());
      pd = read_pd(ifile);
      ifile.close();
    }

    gyoza::arquivers::squid CLFB;
    CLFB.draw_pd<PSPlotter>(outArg.getValue(), pd, dimvecArg.getValue());
  } catch (TCLAP::ArgException &e) {
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
  }


  return 0;
}
