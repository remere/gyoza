#include <iostream>
#include <fstream>
#include <string>
#include "gyoza/gyoza.h"
#include "gyoza/gyoza_geometry.h"

#include <tclap/CmdLine.h>


int main(int argc, char** argv) {
  try{
    TCLAP::CmdLine cmd("gyoza_shape_to_an_gap\nThis utility takes as input a point cloud and chosen radius values, and outputs a GAP-format script containing a persistence module. Running gyoza_bunkai_runner on the output will compute the persistence diagram of the filtration at the chosen radius values.\nThis program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; see the LICENSE file for details.", ' ', "0.1");

    TCLAP::ValueArg<int> dimArg("d", "dim", "Homology dimension of the persistence module to construct. Defaults to 1.", false, 1, "dimension");
    cmd.add(dimArg);

    TCLAP::UnlabeledValueArg<std::string> iArg("input", "Filename of input point cloud.", true, "", "input");
    cmd.add(iArg);

    TCLAP::UnlabeledMultiArg<double> rArg("radii", "Radius values to create filtration.", true, "radii");
    cmd.add(rArg);

    cmd.parse(argc, argv);
    int dim = dimArg.getValue();

    gyoza::geometry::Shape x;
    std::ifstream in_file(iArg.getValue());
    x.read_file(in_file, true);

    std::vector<FT> cuts;
    for (auto r : rArg.getValue()) {
      cuts.emplace_back(r);
    }

    gyoza::quivers::QuiverComplex<gyoza::Algebra::SetChain> qc = x.create_quiver_complex(cuts);
    gyoza::quivers::QuiverRepn h = qc.naive_compute_persistence(dim);
    h.export_to_gap_qpa(std::cout);


  } catch (TCLAP::ArgException &e) {
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
  }

  return 0;
}
