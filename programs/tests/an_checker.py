#!/usr/bin/env python


import sys, getopt
import os
import re
import bisect

# arguments:
# an_checker.py correct_PD_file.txt cut0 cut1 cut2 ... cutN


def parse_gap_answer():
    # parse answer from GAP...
    rfile = open("gap-answer.txt", 'r')
    gap_pd = {}
    while True:
        data = rfile.readline().strip().replace('[','').replace(']','')
        if not data:
            break
        u, v = data.split(":")
        try:
            mult = int(v)
            dimvec = [int(x) for x in  u.split(",")]
        except ValueError as err:
            print(str(err))
            sys.exit(2)
        birth = next((i for i,x in enumerate(dimvec) if x!=0), None)
        death = len(dimvec) - next((i for i,x in enumerate(reversed(dimvec)) if x!=0), None) -1
        gap_pd[(birth,death)] = mult
    rfile.close()
    return gap_pd

def parse_correct_answer(fname, cuts):
    # parse correct answer, discretize to provided cuts:
    correct_pd = {}
    N = len(cuts)    
    rfile = open(fname, 'r')
    while True:
        data = rfile.readline().strip()
        if not data:
            break
        b,d = data.split(" ")
        try:
            b = float(b)
            d = float(d)
        except ValueError as err:
            print(str(err))
            sys.exit(2)
        N_b, N_d = discretize_interval(b,d,cuts)
        if N_b < N and N_d > -1 and N_b <= N_d:
            if (N_b,N_d) in correct_pd:
                correct_pd[(N_b,N_d)] += 1
            else:
                correct_pd[(N_b,N_d)] = 1
        # if N_b > N_d:
        #     print N_b , N_d
        #     print b, d
        #     print cuts
    rfile.close()
    return correct_pd


def discretize_interval(b,d, cuts):
    N_b = bisect.bisect_left(cuts,b)
    N_d = bisect.bisect_left(cuts,d)-1
    return (N_b, N_d)

if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:],"")
    except getopt.GetoptError as err:
        # print help information and exit:
        print(str(err)) # will print something like "option -a not recognized"
        sys.exit(2)

    cuts = [float(args[i]) for i in range(1,len(args))]
    cuts.sort()    
    
    gap_pd = parse_gap_answer()
    gap_pd = { x:y for x,y in gap_pd.items() if y!=0}
    correct_pd = parse_correct_answer(args[0], cuts)
    

    print(gap_pd)
    print(correct_pd)
    
    if gap_pd == correct_pd:
        sys.exit(0)
    else:
        sys.exit(1)
