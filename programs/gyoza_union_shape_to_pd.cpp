#include <iostream>
#include <fstream>
#include <string>
#include "gyoza/gyoza.h"
#include "gyoza/gyoza_geometry.h"

// #include "gyoza/repnclfb.h"

#include <tclap/CmdLine.h>

namespace lp = gyoza::ladderpersistence;
namespace mp = gyoza::matrixproblems;
namespace qv = gyoza::quivers;

int main(int argc, char** argv) {
  try{
    TCLAP::CmdLine cmd("gyoza_union_shape_to_pd\nThis utility takes as input two point clouds and two radius values, and computes the persistence diagram of the union commutative ladder persistence module, using the matrix method.\nThis program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; see the LICENSE file for details.", ' ', "0.1");

    TCLAP::ValueArg<double> rArg("r", "r_radius", "Radius value to use for constructing the lower row of the union commutative ladder persistence. Defaults to 0.", false, 0, "lower radius");
    cmd.add(rArg);

    TCLAP::ValueArg<double> sArg("s", "s_radius", "Radius value to use for constructing the upper row of the union commutative ladder persistence. Defaults to 1.", false, 1, "upper radius");
    cmd.add(sArg);

    TCLAP::ValueArg<int> dimArg("d", "dim", "Homology dimension of the union commutative ladder persistence to construct. Defaults to 1.", false, 1, "dimension");
    cmd.add(dimArg);

    TCLAP::UnlabeledValueArg<std::string> leftFile("filename1", "Filename of point cloud data to use for left side of union commutative ladder.", true, std::string(), "filename1");
    cmd.add(leftFile);

    TCLAP::UnlabeledValueArg<std::string> rightFile("filename2", "Filename of point cloud data to use for right side of union commutative ladder.", true, std::string(), "filename2");
    cmd.add(rightFile);

    cmd.parse(argc, argv);
    double r = rArg.getValue();
    double s = sArg.getValue();
    int dim = dimArg.getValue();

    gyoza::geometry::UnionShape x;
    std::ifstream left_file(leftFile.getValue());
    std::ifstream right_file(rightFile.getValue());
    x.read_files(left_file, right_file);

    gyoza::quivers::QuiverComplex<gyoza::Algebra::SetChain> qc = x.compute_ladder(r,s);
    qv::QuiverRepn h = qc.naive_compute_persistence(dim);

    mp::LadderMatrixProblem phi = lp::MatrixProblemCL<qv::QuiverType::Ladder_FB>::get_matrix_problem(h).get();
    phi.initialize();
    phi.matrix_reduction(100);
    phi.attempt_compute_pd<3>();
    phi.output_pd(std::cout);

  } catch (TCLAP::ArgException &e) {
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
  }
  

  return 0;
}
