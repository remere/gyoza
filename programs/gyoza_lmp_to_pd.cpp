#include <iostream>
#include <fstream>
#include <string>

#include "gyoza/gyoza.h"
#include <tclap/CmdLine.h>
#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string/case_conv.hpp>


namespace lp = gyoza::ladderpersistence;
namespace mp = gyoza::matrixproblems;
using qt = gyoza::quivers::QuiverType;

int main(int argc, char** argv) {
  try{
    std::string formatString = "INPUT FORMAT:\n";
    formatString += "1. First line: orientation (a sequence of symbols 'f' or 'b') of the commutative ladder.\n";
    formatString += "2. Subsequent lines, matrix blocks of the matrix problem. Each block should be so specified:\n";
    formatString += "\tFirst line: give a block address '# a:b c:d' which denotes the matrix block from interval I[a,b] to interval I[c,d]. The '#' symbol indicates the start of a block.\n";
    formatString += "\tSecond line: provide the size of the matrix 'r c', where r is the number of rows, and c, columns.\n";
    formatString += "\tSubsequent lines: matrix entries 0 or 1, with no spaces, one line for each row.\n\n";

    std::string licenseString = "This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; see the LICENSE file for details.";

    std::string descString = "gyoza_lmp_to_pd\nThis utility takes as a ladder matrix problem and computes its persistence diagram using the matrix method.\n";

    TCLAP::CmdLine cmd(descString + formatString  + licenseString, ' ', "0.1");


    TCLAP::UnlabeledValueArg<std::string> inputFile("filename", "Filename of input ladder matrix problem.\n", true, std::string(), "filename");
    cmd.add(inputFile);


    cmd.parse(argc, argv);

    std::ifstream ifile(inputFile.getValue());
    std::string chosen_type;

    std::getline(ifile, chosen_type);
    boost::algorithm::trim(chosen_type);
    boost::algorithm::to_lower(chosen_type);

    boost::optional<mp::LadderMatrixProblem> phi;
    if (chosen_type == "f") {
      phi = lp::MatrixProblemCL<qt::Ladder_F>::lmp_ascii_read(ifile);
    } else if (chosen_type == "ff") {
      phi = lp::MatrixProblemCL<qt::Ladder_FF>::lmp_ascii_read(ifile);
    } else if (chosen_type == "bb") {
      phi = lp::MatrixProblemCL<qt::Ladder_BB>::lmp_ascii_read(ifile);
    } else if (chosen_type == "bf") {
      phi = lp::MatrixProblemCL<qt::Ladder_BF>::lmp_ascii_read(ifile);
    } else if (chosen_type == "fb") {
      phi = lp::MatrixProblemCL<qt::Ladder_FB>::lmp_ascii_read(ifile);
    } else if (chosen_type == "fff") {
      phi = lp::MatrixProblemCL<qt::Ladder_FFF>::lmp_ascii_read(ifile);
    } else if (chosen_type == "ffb") {
      phi = lp::MatrixProblemCL<qt::Ladder_FFB>::lmp_ascii_read(ifile);
    } else if (chosen_type == "bfb") {
      phi = lp::MatrixProblemCL<qt::Ladder_BFB>::lmp_ascii_read(ifile);
    } else if (chosen_type == "bbf") {
      phi = lp::MatrixProblemCL<qt::Ladder_BBF>::lmp_ascii_read(ifile);
    } else if (chosen_type == "bbb") {
      phi = lp::MatrixProblemCL<qt::Ladder_BBB>::lmp_ascii_read(ifile);
    } else if (chosen_type == "fbb") {
      phi = lp::MatrixProblemCL<qt::Ladder_FBB>::lmp_ascii_read(ifile);
    } else if (chosen_type == "fbf") {
      phi = lp::MatrixProblemCL<qt::Ladder_FBF>::lmp_ascii_read(ifile);
    } else if (chosen_type == "bff") {
      phi = lp::MatrixProblemCL<qt::Ladder_BFF>::lmp_ascii_read(ifile);
    } else {
      std::cerr << "Orientation not supported: " << chosen_type << std::endl;
      return 1;
    }

    if (phi == boost::none) {
      std::cerr << "Failed to read input file." << std::endl;
      return 1;
    }
    phi->initialize();
    phi->matrix_reduction(300);

    if (chosen_type.size() == 1) {
      phi->attempt_compute_pd<2>();
      phi->output_pd(std::cout);
    } else if (chosen_type.size() == 2) {
      phi->attempt_compute_pd<3>();
      phi->output_pd(std::cout);
    } else if (chosen_type.size() == 3) {
      phi->attempt_compute_pd<4>();
      phi->output_pd(std::cout);
    }

  } catch (TCLAP::ArgException &e) {
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
  }


  return 0;
}
