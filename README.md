GYOZA
-----
This library implements the "Matrix Method" for computing indecomposable decompositions of persistence modules over commutative ladders of finite type.

The main ideas and algorithm can be found in the paper:
  H. Asashiba, E.G. Escolar, Y. Hiraoka, and H. Takeuchi.
	"Matrix Method for Persistence Modules on Commutative Ladders of Finite Type". https://arxiv.org/abs/1706.10027

"Gyouza" (餃子) is the Japanese variation of a Chinese dumpling. "Gyouretsu" (行列) is "matrix" in Japanese.


DEPENDENCIES
------------
* C++ compiler that supports c++14
* python (for some tests)
* [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page)
* [CGAL](https://www.cgal.org/) at least version 5.0.1
* [Plotutils](https://www.gnu.org/software/plotutils/)
* [TCLAP](http://tclap.sourceforge.net/)
* [GAP](https://www.gap-system.org/)

and their respective dependencies.
The above libraries may also available via apt (for Debian, Ubuntu, etc.)

For GAP, the GAP package [QPA (Quivers and Path Algebras)](https://folk.ntnu.no/oyvinso/QPA/) is also needed.

Note that you may need to put (a link of) the GAP executable into a folder in the PATH variable,
or add the GAP folder to the PATH variable.

COMPILATION AND INSTALLATION
----------------------------
After installing the above dependencies, run the following commands, making sure that the previous command succeeds before proceeding to the next one.

```
./configure
```
```
make
```
```
make check
```
```
make install
```

TROUBLESHOOTING
---------------
1. `./configure` complains that "Eigen header not found".  
    [SOLUTION] Try adding Eigen to CPPFLAGS as follows: 
    ```
    ./configure CPPFLAGS=-I/usr/include/eigen3
    ```
    or wherever you have installed Eigen.

2. `make check` fails partway with an error message:
   ```
   /usr/bin/env: ‘python’: No such file or directory
   ```  
   [SOLUTION] Check that you have python installed.
   If you have installed python3 (for example, as `/usr/bin/python3`), and `which python` fails, trying making a symlink as follows (`sudo` possibly needed):
   ```
   ln -s /usr/bin/python3 /usr/bin/python
   ```


PROGRAMS
--------
###  "Matrix Method" programs
* **gyoza_lmp_to_pd**: persistence diagram of a ladder matrix problem, as described in "Matrix Method" paper.
* **gyoza_union_shape_to_pd**: persistence diagram of the union-shape commutative ladder persistence module, using the matrix method.
* **gyoza_ladder_mp_tester**: provides a step-by-step symbolic view of what operations the "Matrix Method" main algorithm (Algorithm 1) performs.

### Plotting
* **gyoza_plot_clfb_diagram**: used for plotting persistence diagram over commutative ladder type 'fb'. Example: output of gyoza_union_shape_to_pd

### Decompositions via GAP-QPA (does not use "Matrix Method")
* **gyoza_bunkai_runner**

### Output persistence module for further processing with GAP-QPA

These programs provide output intended for use as input to gyoza_bunkai_runner.

* **gyoza_shape_to_an_gap**: computes persistence module of a filtration, restricted to a (small) number of slices.
* **gyoza_union_shape_to_gap**: computes union-shape commutative ladder persistence module.

See programs subfolder and the programs' corresponding help messages.


Note
----
This software implements the main algorithm (Algorithm 1) and needed subroutines of the "Matrix Method" paper. The algorithm computes the decomposition for an input commutative ladder matrix problem obtained of finite type.

However, the conversion of a representation of a commutative ladder into its corresponding matrix problem is not implemented. Only the special cases of types "f" "fb" are currently implemented.

Contributors
------------
* Emerson G. Escolar
* Hiroshi Takeuchi

LICENSE
-------
 Copyright (C) 2018  Emerson G. Escolar

gyoza is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
