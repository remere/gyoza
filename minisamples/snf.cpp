#include <iostream>

#include "gyoza/gyoza.h"
#include <Eigen/Core>
#include <sstream>

int main() {
  gyoza::Z2Matrix mat = gyoza::Z2Matrix::Random(10,11);
  gyoza::z2matrix_ascii_write(std::cout, mat);
  std::cout << std::endl;
  std::cout << std::endl;


  std::stringstream ss;
  gyoza::z2matrix_ascii_write(ss, mat);
  auto mat_read = gyoza::z2matrix_ascii_read(ss);
  gyoza::z2matrix_ascii_write(std::cout, *mat_read);
  std::cout << std::endl;

  std::vector<Core::Z2> foo = {0,1,1,0,0,1};
  for (auto x : foo) {
    std::cout << x;
  }
  std::cout << std::endl;



  return 0;

  char c;
  std::vector<Core::Z2> data;
  while (std::cin >> c) {
    int ic = c - '0';
    data.emplace_back(ic);
  }

  gyoza::Z2Matrix B = Eigen::Map<Eigen::Matrix<Core::Z2, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>(&data[0],data.size(),1);
  std::cout << B << std::endl;


  return 0;

  gyoza::algorithms::FullPivotSNF<gyoza::Z2Matrix> snf(mat, true);
  
  std::cout << snf.get_snf();
  std::cout << "\n********************\n";
  std::cout << snf.get_P();
  std::cout << "\n********************\n";
  std::cout << snf.get_Q();
  std::cout << "\n********************\n";
  std::cout << "\nINVERSES\n";
  std::cout << snf.get_P_inv();
  std::cout << "\n********************\n";
  std::cout << snf.get_Q_inv();
  std::cout << "\n********************\n";
  std::cout << "\nPRODUCTS\n";
  std::cout << snf.get_P_inv() * snf.get_P();
  std::cout << "\n********************\n";
  std::cout << snf.get_Q_inv() * snf.get_Q();
  std::cout << "\n********************\n";
  


  std::cout << snf.get_snf() - ( (snf.get_P() * mat ) * (snf.get_Q())) << std::endl;

  std::cout << "\n********************\n";
  std::cout << (snf.get_P_inv() * snf.get_snf() * snf.get_Q_inv()) -  mat << std::endl;


  return 0;
}
