#include <iostream>

// #include "gyoza/repnclf.h"
// #include "gyoza/repnclfb.h"

#include "gyoza/matrix_problem_cl.h"

#include "gyoza/log_control.h"
#include <unistd.h>

#include <set>
#include <vector>
#include <string>

namespace lp = gyoza::ladderpersistence;
namespace mp = gyoza::matrixproblems;
using Z2Matrix = gyoza::Z2Matrix;
using BA = gyoza::matrixproblems::BlockAddress;

void foo(mp::LadderMatrixProblem& phi) {
  phi.initialize();
  std::cout << "initial state:\n";
  phi.pretty_print_status(std::cout);

  phi.matrix_reduction(100);

  boost::optional<int> num = phi.number_of_noniso_summands();
  if (num != boost::none) {
    std::cout << "number of noniso summands: " << num.get() << std::endl;
  }
}

template<typename T>
mp::LadderMatrixProblem problem_creator(int d) {
  if (d > 1) {
    return T::create_random_problem(d);
  } else {
    return T::get_ones_problem();
  }
}

gyoza::LogLevel convert_log_level(int i) {
  switch(i) {
    case 0:
      return gyoza::LOG_NOTHING;
    case 1:
      return gyoza::LOG_CRITICAL;
    case 3:
      return gyoza::LOG_WARNING;
    case 4:
      return gyoza::LOG_INFO;
    case 5:
      return gyoza::LOG_DEBUG;
    default:
      return gyoza::LOG_ERROR;
  }
}

void display_help() {
  std::cerr << "Usage: ladder_mp_tester [-d dim] [-l loglevel] type\n";
  std::cerr << "Options:\n";
  std::cerr << "-d dimension d. If d > 1, blocks in matrix problem are size d by d randomly generated matrices.\n";
  std::cerr << "-l log level:\n";
  std::cerr << "    0 : log nothing\n";
  std::cerr << "    1 : log critical\n";
  std::cerr << "    2 : log error\n";
  std::cerr << "    3 : log warning\n";
  std::cerr << "    4 : log info\n";
  std::cerr << "    5 : log debug (default)\n";
  return;  
}


bool check_orientation(std::string orientation) {
  const std::set<char> valid_chars = {'f', 'b'};
  for (auto c : orientation) {
    if (0 == valid_chars.count(c)) {
      return false;
    }
  }
  return true;
}

int main(int argc, char* argv[]) {
  int c;
  int d = 1;
  gyoza::GLOBAL_LOG_LEVEL = gyoza::LOG_DEBUG;

  while ((c = getopt(argc,argv,"l:d:h")) != -1) {
    switch(c) {
      case 'l':
        gyoza::GLOBAL_LOG_LEVEL = convert_log_level(std::stoi(std::string(optarg)));
        break;
      case 'd':
        d = std::stoi(optarg);
        if (d < 0) {
          return 0;
        }
        break;
      case 'h':
        display_help();
        return 0;
        break;
    }
  }
  if (argc - optind < 1) {
    std::cerr << "Not enough arguments. Specify ladder type to test\n";
    display_help();
    return 1;
  }
  std::string chosen_type = std::string(argv[optind]);

  if (false == check_orientation(chosen_type) || chosen_type.size() < 1) {
    std::cerr << "Invalid orientation: " << chosen_type << std::endl;
    return 1;
  }

  if (chosen_type.size() > 3) {
    std::cerr << "Orientation too long, unsupported: " << chosen_type << std::endl;
    return 1;
  }

  using qt = gyoza::quivers::QuiverType;
  mp::LadderMatrixProblem phi;
  if (chosen_type == "f") {
    phi = problem_creator<lp::MatrixProblemCL<qt::Ladder_F>>(d);
  } else if (chosen_type == "ff") {
    phi = problem_creator<lp::MatrixProblemCL<qt::Ladder_FF>>(d);
  } else if (chosen_type == "bb") {
    phi = problem_creator<lp::MatrixProblemCL<qt::Ladder_BB>>(d);
  } else if (chosen_type == "bf") {
    phi = problem_creator<lp::MatrixProblemCL<qt::Ladder_BF>>(d);
  } else if (chosen_type == "fb") {
    phi = problem_creator<lp::MatrixProblemCL<qt::Ladder_FB>>(d);
  } else if (chosen_type == "fff") {
    phi = problem_creator<lp::MatrixProblemCL<qt::Ladder_FFF>>(d);
  } else if (chosen_type == "ffb") {
    phi = problem_creator<lp::MatrixProblemCL<qt::Ladder_FFB>>(d);
  } else if (chosen_type == "bfb") {
    phi = problem_creator<lp::MatrixProblemCL<qt::Ladder_BFB>>(d);
  } else if (chosen_type == "bbf") {
    phi = problem_creator<lp::MatrixProblemCL<qt::Ladder_BBF>>(d);
  } else if (chosen_type == "bbb") {
    phi = problem_creator<lp::MatrixProblemCL<qt::Ladder_BBB>>(d);
  } else if (chosen_type == "fbb") {
    phi = problem_creator<lp::MatrixProblemCL<qt::Ladder_FBB>>(d);
  } else if (chosen_type == "fbf") {
    phi = problem_creator<lp::MatrixProblemCL<qt::Ladder_FBF>>(d);
  } else if (chosen_type == "bff") {
    phi = problem_creator<lp::MatrixProblemCL<qt::Ladder_BFF>>(d);
  }


  foo(phi);



  if (chosen_type.size() == 2 ) {
    phi.attempt_compute_pd<3>();
  } else if (chosen_type.size() == 3) {
    phi.attempt_compute_pd<4>();
  }


  phi.output_pd(std::cout);

  std::cout << "status ok? " << (phi.verify_status() ? "ok" : "no") << "\n";

  return 0;
}
