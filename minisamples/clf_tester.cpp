#include <iostream>
#include "gyoza/gyoza.h"

namespace lp = gyoza::ladderpersistence;
namespace mp = gyoza::matrixproblems;
namespace qv = gyoza::quivers;
using Z2Matrix = gyoza::Z2Matrix;
using BA = gyoza::matrixproblems::BlockAddress;

int main () {  
  qv::QuiverRepn x(qv::create_ladder_quiver(qv::QuiverType::Ladder_F)) ;
  
  Z2Matrix m01 = Z2Matrix::Random(5, 6);
  Z2Matrix m13 = Z2Matrix::Random(4, 5);
  Z2Matrix m02 = m13 * m01;
  Z2Matrix m23 = Z2Matrix::Identity(4, 4);
  
  x.matrix_at(0,1) = m01;
  x.matrix_at(1,3) = m13;
  x.matrix_at(0,2) = m02;
  x.matrix_at(2,3) = m23;

  x.update_dimension_vector();

  x.export_to_gap_qpa(std::cout);

  mp::LadderMatrixProblem phi = lp::MatrixProblemCL<qv::QuiverType::Ladder_F>::get_matrix_problem(x).get();

  phi.initialize();
  std::cout << phi.verify_status() << std::endl;
  phi.pretty_print_status(std::cout);

  phi.matrix_reduction(12);

  boost::optional<int> num = phi.number_of_noniso_summands();
  if (num != boost::none) {
    std::cout << "number of noniso summands: " << num.get() << std::endl;
  }
  

  return 0;
    

  




}
