#include <iostream>
#include <stdio.h>
#include <stdlib.h> 
#include "gyoza/common_definitions.h"
#include "gyoza/snf_algorithms.hpp"


double inline term(int q, int i) {
  return 1 - pow(double(q),double(-i));
}

double prob_rank(int n, int rk, int q = 2) {
  double ans = 1;  
  int k = n - rk;
  for (int j = n-k+1; j <= n;  ++j) {
    ans *= term(q, j);    
  }
  for (int j = k+1; j <= n; ++j) {
    ans *= term(q,j);
  }  
  for (int j = 1; j <= k; ++j) {
    ans /= term(q,j);
  }  
  return ans/pow(q,k*k);
}



int main(int argc, char* argv[]) {
  if (argc < 3) {
    return 1;
  }
  int d = std::atoi(argv[1]);
  const int num_sample = std::atoi(argv[2]);
  
  std::vector<int> rank_counts(d+1, 0);

  for (int i = 0; i < num_sample; ++i) {
    gyoza::Z2Matrix mat = gyoza::Z2Matrix::Random(d,d);

    gyoza::algorithms::FullPivotSNF<gyoza::Z2Matrix> snf(mat);
    int rk = snf.rank();
    rank_counts[rk] += 1;
  }

  for (int i = 0; i < d+1; ++i) {
    double prob = prob_rank(d,i);
    int expected = int(round(num_sample * prob));
    printf("%3d: %4d; expected: %4d, prob: %.9f\n", i, rank_counts[i], expected, prob); 
  }

  std::cout << prob_rank(10000,10000) << std::endl;

  return 0;
}
